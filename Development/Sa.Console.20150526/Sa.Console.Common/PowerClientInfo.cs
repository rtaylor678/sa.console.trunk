﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sa.Console.Common
{
    public class PowerClientInfo
    {
        public string SiteID { get; set; }
        public string CorpName { get; set; }
        public string AffName { get; set; }
        public string PlantName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string CoordName { get; set; }
        public string CoordTitle { get; set; }
        public string CoordAddr1 { get; set; }
        public string CoordAddr2 { get; set; }
        public string CoordCity { get; set; }
        public string CoordState { get; set; }
        public string CoordZip { get; set; }
        public string CoordPhone { get; set; }
        public string CoordFax { get; set; }
        public string CoordEMail { get; set; }
        public string RptUOM { get; set; }
        public string RptHV { get; set; }
        public string RptSteamMethod { get; set; }
    }
}
