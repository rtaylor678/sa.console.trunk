﻿Imports System.Data
Imports System.Data.SqlClient
Imports SA.Internal.Console.DataObject


Public Class PowerCoordContactForm
    Dim _siteId As String
    Dim _dAL As DataObject
    Public Event UpdateCoord()
    Public Sub New(siteId As String, ByRef dAL As DataObject)
        _siteId = siteId
        _dAL = dAL
        InitializeComponent()
        ClearForm()
    End Sub

    Private Sub LoadContacts()
        'Dim params As New List(Of String)
        'params.Add("@SiteId/" & _siteId)
        'params.Add("@CorpName/" & Utilities.FixQuotes(txtCorpName.Text.Trim().Replace(",", "")))
        'params.Add("@AffName/" & Utilities.FixQuotes(txtAffName.Text.Trim().Replace(",", "")))
        'params.Add("@PlantName/" & Utilities.FixQuotes(txtPlantName.Text.Trim().Replace(",", "")))
        'params.Add("@City/" & Utilities.FixQuotes(txtPlantCity.Text.Trim().Replace(",", "")))
        'params.Add("@State/" & Utilities.FixQuotes(txtPlantState.Text.Trim().Replace(",", "")))
        'params.Add("@CoordName/" & Utilities.FixQuotes(txtName.Text.Trim().Replace(",", "")))
        'params.Add("@CoordTitle/" & Utilities.FixQuotes(txtTitle.Text.Trim().Replace(",", "")))
        'params.Add("@CoordAddr1/" & Utilities.FixQuotes(txtAddress1.Text.Trim().Replace(",", "")))
        'params.Add("@CoordAddr2/" & Utilities.FixQuotes(txtAddress2.Text.Trim().Replace(",", "")))
        'params.Add("@CoordCity/" & Utilities.FixQuotes(txtCity.Text.Trim().Replace(",", "")))
        'params.Add("@CoordState/" & Utilities.FixQuotes(txtState.Text.Trim().Replace(",", "")))
        'params.Add("@CoordZip/" & Utilities.FixQuotes(txtZip.Text.Trim().Replace(",", "")))
        'params.Add("@CoordPhone/" & Utilities.FixQuotes(txtPhone.Text.Trim().Replace(",", "")))
        'params.Add("@CoordFax/" & Utilities.FixQuotes(txtFax.Text.Trim().Replace(",", "")))
        'params.Add("@CoordEMail/" & Utilities.FixQuotes(txtEmail.Text.Trim().Replace(",", "")))
        'params.Add("@RptUOM/" & Utilities.FixQuotes(txtRptUOM.Text.Trim().Replace(",", "")))
        'params.Add("@RptHV/" & Utilities.FixQuotes(txtRptHV.Text.Trim().Replace(",", "")))
        'params.Add("@RptSteamMethod/" & Utilities.FixQuotes(txtRptSteamMethod.Text.Trim().Replace(",", "")))

        Try
            Dim sql As String = "select SiteId, CorpName,AffName,PlantName,City,State,CoordName,CoordTitle,CoordAddr1,CoordAddr2,CoordCity,CoordState,CoordZip,CoordPhone,CoordFax,CoordEMail,RptUOM,RptHV,RptSteamMethod"
            sql = sql & " FROM dbo.ClientInfo WHERE SiteID = '" & _siteId & "';"
            _dAL.SQL = sql
            Dim ds As DataSet = _dAL.Execute() ' ExecuteStoredProc("dbo." & "UpdateClientInfo", params)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    Dim row As DataRow = ds.Tables(0).Rows(0)
                    txtCorpName.Text = row("CorpName").ToString().Trim()
                    txtAffName.Text = row("AffName").ToString().Trim()
                    txtPlantName.Text = row("PlantName").ToString().Trim()
                    txtPlantCity.Text = row("City").ToString().Trim()
                    txtPlantState.Text = row("State").ToString().Trim()
                    txtName.Text = row("CoordName").ToString().Trim()
                    txtTitle.Text = row("CoordTitle").ToString().Trim()
                    txtAddress1.Text = row("CoordAddr1").ToString().Trim()
                    txtAddress2.Text = row("CoordAddr2").ToString().Trim()
                    txtCity.Text = row("CoordCity").ToString().Trim()
                    txtState.Text = row("CoordState").ToString().Trim()
                    txtZip.Text = row("CoordZip").ToString().Trim()
                    txtPhone.Text = row("CoordPhone").ToString().Trim()
                    txtFax.Text = row("CoordFax").ToString().Trim()
                    txtEmail.Text = row("CoordEMail").ToString().Trim()
                    txtRptUOM.Text = row("RptUOM").ToString().Trim()
                    txtRptHV.Text = row("RptHV").ToString().Trim()
                    txtRptSteamMethod.Text = row("RptSteamMethod").ToString().Trim()
                End If
            End If

        Catch Ex As System.Exception
            MessageBox.Show(Ex.Message)
        Finally
            _dAL.SQL = String.Empty
        End Try
    End Sub
    Private Sub ContactForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadContacts()
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        RaiseEvent UpdateCoord()
        Me.Close()
    End Sub
    Private Sub UpdateCompanyContact()

        Dim params As New List(Of String)
        params.Add("@SiteId/" & _siteId)
        params.Add("@CorpName/" & Utilities.FixQuotes(txtCorpName.Text.Trim().Replace(",", "")))
        params.Add("@AffName/" & Utilities.FixQuotes(txtAffName.Text.Trim().Replace(",", "")))
        params.Add("@PlantName/" & Utilities.FixQuotes(txtPlantName.Text.Trim().Replace(",", "")))
        params.Add("@City/" & Utilities.FixQuotes(txtPlantCity.Text.Trim().Replace(",", "")))
        params.Add("@State/" & Utilities.FixQuotes(txtPlantState.Text.Trim().Replace(",", "")))
        params.Add("@CoordName/" & Utilities.FixQuotes(txtName.Text.Trim().Replace(",", "")))
        params.Add("@CoordTitle/" & Utilities.FixQuotes(txtTitle.Text.Trim().Replace(",", "")))
        params.Add("@CoordAddr1/" & Utilities.FixQuotes(txtAddress1.Text.Trim().Replace(",", "")))
        params.Add("@CoordAddr2/" & Utilities.FixQuotes(txtAddress2.Text.Trim().Replace(",", "")))
        params.Add("@CoordCity/" & Utilities.FixQuotes(txtCity.Text.Trim().Replace(",", "")))
        params.Add("@CoordState/" & Utilities.FixQuotes(txtState.Text.Trim().Replace(",", "")))
        params.Add("@CoordZip/" & Utilities.FixQuotes(txtZip.Text.Trim().Replace(",", "")))
        params.Add("@CoordPhone/" & Utilities.FixQuotes(txtPhone.Text.Trim().Replace(",", "")))
        params.Add("@CoordFax/" & Utilities.FixQuotes(txtFax.Text.Trim().Replace(",", "")))
        params.Add("@CoordEMail/" & Utilities.FixQuotes(txtEmail.Text.Trim().Replace(",", "")))
        params.Add("@RptUOM/" & Utilities.FixQuotes(txtRptUOM.Text.Trim().Replace(",", "")))
        params.Add("@RptHV/" & Utilities.FixQuotes(txtRptHV.Text.Trim().Replace(",", "")))
        params.Add("@RptSteamMethod/" & Utilities.FixQuotes(txtRptSteamMethod.Text.Trim().Replace(",", "")))

        Try
            Dim returnValue As Integer = _dAL.ExecuteNonQuery("Console.UpdateClientInfo", params)
            'If returnValue = 0 Then
            MessageBox.Show(" Contact Updated")
            'Else
            '    MessageBox.Show(" Contact Not Updated")
            'End If
        Catch ex As System.Exception
            Dim breakPoint As String = ex.Message
            MessageBox.Show(" Contact Not Updated")
        End Try

        'Me.Close()
    End Sub

    Private Sub btnUpdate_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdate.Click
        UpdateCompanyContact()

    End Sub

    Private Sub ClearForm()
        txtCorpName.Text = String.Empty
        txtAffName.Text = String.Empty
        txtPlantName.Text = String.Empty
        txtPlantCity.Text = String.Empty
        txtPlantState.Text = String.Empty
        txtName.Text = String.Empty
        txtTitle.Text = String.Empty
        txtAddress1.Text = String.Empty
        txtAddress2.Text = String.Empty
        txtCity.Text = String.Empty
        txtState.Text = String.Empty
        txtZip.Text = String.Empty
        txtPhone.Text = String.Empty
        txtFax.Text = String.Empty
        txtEmail.Text = String.Empty
        txtRptUOM.Text = String.Empty
        txtRptHV.Text = String.Empty
        txtRptSteamMethod.Text = String.Empty
    End Sub
End Class
