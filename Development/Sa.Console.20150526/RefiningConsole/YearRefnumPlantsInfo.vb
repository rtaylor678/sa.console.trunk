﻿Public Class YearRefnumPlantsInfo
    Public StudyYears As String()
    Public RefNums As List(Of String)
    Public Plants As List(Of String)
    Public SelectedStudyYear As String
    Public SelectedRefNum As String
    Public SelectedPlant As String

    Public Sub New()
        SelectedStudyYear = String.Empty
        SelectedRefNum = String.Empty
        SelectedPlant = String.Empty
        'StudyYears = 
        RefNums = New List(Of String)
        Plants = New List(Of String)
    End Sub

End Class
