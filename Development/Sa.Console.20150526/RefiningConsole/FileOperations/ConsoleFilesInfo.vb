﻿
Public Class ConsoleFilesInfo
    Dim _usingMfiles As Boolean
    Dim _files As New List(Of ConsoleFile)
    Dim _benchmarkingParticipantName As String
    Dim _benchmarkingParticipantId As Integer
    Dim _refNum As String

    Public Sub New(MFiles As Boolean)
        _usingMfiles = MFiles
    End Sub

    Public ReadOnly Property Files As List(Of ConsoleFile)
        Get
            Return _files
        End Get
        'Set(value As List(Of ConsoleFileInfo))
        '    _files = value
        'End Set
    End Property

    Public Property BenchmarkingParticipantId As Integer
        Get
            Return _benchmarkingParticipantId
        End Get
        Set(ByVal value As Integer)
            _benchmarkingParticipantId = value
        End Set
    End Property

    Public Property BenchmarkingParticipantName As String
        Get
            Return _benchmarkingParticipantName
        End Get
        Set(ByVal value As String)
            _benchmarkingParticipantName = value
            Dim refnumber() As String = _benchmarkingParticipantName.Split(" - ")
            _refNum = refnumber(0)
        End Set
    End Property

    Public ReadOnly Property RefNum As String
        Get
            Return _refNum
        End Get
    End Property
End Class


