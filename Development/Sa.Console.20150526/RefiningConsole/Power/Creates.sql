﻿
USE [PowerWork];


GO


GO
PRINT N'Creating [Console].[ConsultantInfo]...';


GO
CREATE TABLE [Console].[ConsultantInfo] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Initials]     NVARCHAR (20)  NOT NULL,
    [FullName]     NVARCHAR (50)  NOT NULL,
    [EmailAddress] NVARCHAR (100) NULL,
    [PhoneNumber]  NVARCHAR (20)  NULL,
    CONSTRAINT [PK_Checklist] PRIMARY KEY CLUSTERED ([Id] ASC, [Initials] ASC)
);


GO
PRINT N'Creating [Console].[ContinuingIssues]...';


GO
CREATE TABLE [Console].[ContinuingIssues] (
    [IssueID]     INT            IDENTITY (1, 1) NOT NULL,
    [RefNum]      VARCHAR (12)   NOT NULL,
    [EntryDate]   DATETIME       NOT NULL,
    [Consultant]  VARCHAR (3)    NULL,
    [Note]        NVARCHAR (MAX) NOT NULL,
    [Deleted]     BIT            NULL,
    [DeletedDate] DATETIME       NULL,
    [DeletedBy]   VARCHAR (3)    NULL,
    CONSTRAINT [PK_ContinuingIssues] PRIMARY KEY NONCLUSTERED ([IssueID] ASC)
);


GO
PRINT N'Creating [Console].[Employees]...';


GO
CREATE TABLE [Console].[Employees] (
    [Initials]       NVARCHAR (10) NULL,
    [ConsultantName] NVARCHAR (80) NULL,
    [Active]         BIT           NULL,
    [Email]          NVARCHAR (80) NULL
);


GO
PRINT N'Creating [Console].[SANumbers]...';


GO
CREATE TABLE [Console].[SANumbers] (
    [ID]       INT           IDENTITY (1, 1) NOT NULL,
    [SANumber] INT           NOT NULL,
    [SiteID]   NVARCHAR (9)  NULL,
    [RefNum]   NVARCHAR (12) NULL,
    CONSTRAINT [PK_[SANumbers] PRIMARY KEY CLUSTERED ([SANumber] ASC)
);


GO
PRINT N'Creating [Console].[DF_ContinuingIssues_NoteDate]...';


GO
ALTER TABLE [Console].[ContinuingIssues]
    ADD CONSTRAINT [DF_ContinuingIssues_NoteDate] DEFAULT (getdate()) FOR [EntryDate];


GO
PRINT N'Creating [Console].[AddOrUpdateContinuingIssues]...';


GO

CREATE PROCEDURE [Console].[AddOrUpdateContinuingIssues]
	@ID int,
	@RefNum nvarchar(20),
	@Issue nvarchar(max),
	@EditedBy nvarchar(3)
AS
BEGIN
	IF EXISTS(SELECT IssueID FROM [Console].[ContinuingIssues] where IssueID=@ID)
		BEGIN
			UPDATE [Console].[ContinuingIssues] SET Deleted = 1, DeletedBy=@EditedBy where IssueID=@ID
		END
		
		INSERT INTO [Console].[ContinuingIssues] values(@RefNum,getdate(),@EditedBy,@Issue,0,null,@EditedBy)

		SELECT @@IDENTITY
END
GO
PRINT N'Creating [Console].[ChangeConsultant]...';


GO
CREATE PROCEDURE Console.ChangeConsultant
(
	@SiteId char(10),
	@Year int,
	@Consultant char(4)
)
AS
	UPDATE dbo.StudySites set Consultant = @Consultant 
	where StudyYear =@Year and SiteID=@SiteId
	AND EXISTS(SELECT * from Console.Employees E where E.Initials = RTRIM(@Consultant))
GO
PRINT N'Creating [Console].[ChangeCoord]...';


GO
CREATE PROCEDURE [Console].[ChangeCoord]
(
	 @SiteID nvarchar(15),
	 @PlantName char(50),
	 @CoordName char(40),
	 @CoordTitle char(50),
	 @CoordAddr1 char(50),
	 @CoordAddr2 char(50),
	 @CoordCity char(30),
	 @CoordState char(20),
	 @CoordZip char(15),
	 @CoordPhone char(30),
	 @CoordFax char(20),
	 @CoordEmail char(40)
)
AS

	IF( (select COUNT(SiteID) FROM [dbo].[ClientInfo] where SiteID=@SiteID)=0)
		INSERT INTO [dbo].[ClientInfo]( SiteID,PlantName,CoordName,CoordTitle,CoordAddr1,
		CoordAddr2,	CoordCity,CoordState,CoordZip,CoordPhone,CoordFax,CoordEMail) VALUES
		(@SiteID, @PlantName, @CoordName, @CoordTitle, @CoordAddr1, @CoordAddr2,
		@CoordCity, @CoordState, @CoordZip, @CoordPhone, @CoordFax, @CoordEmail)
	ELSE
		UPDATE[dbo].[ClientInfo] SET 
		CoordName = @CoordName ,
		CoordTitle = @CoordTitle, 
		CoordAddr1 = @CoordAddr1 ,
		CoordAddr2 = @CoordAddr2 ,
		CoordCity = @CoordCity ,
		CoordState = @CoordState, 
		CoordZip = @CoordZip ,
		CoordPhone = @CoordPhone ,
		CoordFax = @CoordFax ,
		CoordEMail = @CoordEMail 	
		where SiteID=@SiteID
GO
PRINT N'Creating [Console].[DeleteContinuingIssue]...';


GO

CREATE PROCEDURE [Console].[DeleteContinuingIssue]
	@IssueID int,
	@DeletedBy nvarchar(3)
AS
BEGIN
	UPDATE [Console].[ContinuingIssues] SET Deleted = 1, DeletedBy=@DeletedBy where IssueID=@IssueID
END
GO
PRINT N'Creating [Console].[GetAllConsultantsByYear]...';


GO


CREATE PROCEDURE Console.GetAllConsultantsByYear
(
	@Year int
)
AS
	SELECT DISTINCT Consultant from [dbo].[StudySites] 
	where StudyYear =@Year
	and SiteID not like '%P'
	and Consultant <> ''
	AND Consultant IS NOT NULL
GO
PRINT N'Creating [Console].[GetChecklist]...';


GO
CREATE PROCEDURE Console.GetChecklist
(
	@SiteId char(10)
)
AS
SELECT SiteId,IssueID,IssueTitle,IssueText,PostedBy,PostedTime,Completed,SetBy,SetTime FROM VAL.CHECKLIST WHERE RTRIM(SiteId) = @SiteId
GO
PRINT N'Creating [Console].[GetCompanynamesWithSiteIds]...';


GO
CREATE PROCEDURE [Console].[GetCompanynamesWithSiteIds]
(
	@Year int
)
AS
	SELECT CONCAT(RTRIM(SiteName)  , CHAR(10), CHAR(45),RTRIM(SiteID) ) As Site from [dbo].[StudySites] 
	where StudyYear = @Year and RTRIM(SiteID) NOT LIKE '%P' order by Site asc;
GO
PRINT N'Creating [Console].[GetConsultantBySiteId]...';


GO

CREATE PROCEDURE Console.GetConsultantBySiteId
(
	@SiteId nvarchar(12)
	--, @StudyYear int  --= 2016;
)
AS
select e.ConsultantName from Console.Employees e 
inner join dbo.StudySites s on s.Consultant = e.Initials
where SiteID = @SiteId
GO
PRINT N'Creating [Console].[GetConsultantEmail]...';


GO

CREATE PROCEDURE Console.GetConsultantEmail
(
	@SiteId nvarchar(12)
	--, @StudyYear int  --= 2016;
)
AS
select e.Email from Console.Employees e 
inner join dbo.StudySites s on s.Consultant = e.Initials
where SiteID = @SiteId
GO
PRINT N'Creating [Console].[GetConsultantInitials]...';


GO


CREATE PROCEDURE Console.GetConsultantInitials
(
	@SiteId char(10)
)
AS
	Select Consultant from dbo.StudySites where SiteId = @SiteId;
GO
PRINT N'Creating [Console].[GetConsultantName]...';


GO


CREATE PROCEDURE Console.GetConsultantName
(
	@ConsultantInitials nvarchar(10)
)
AS
	SELECT ConsultantName from Console.Employees  where Initials = @ConsultantInitials;
GO
PRINT N'Creating [Console].[GetConsultingOpportunities]...';


GO
CREATE PROCEDURE [Console].[GetConsultingOpportunities]
	@RefNum  nvarchar(12)
AS
BEGIN
	SELECT ConsultingOpportunities From ValidationNotes Where Refnum = @RefNum
END
GO
PRINT N'Creating [Console].[GetContinuingIssue]...';


GO
CREATE PROCEDURE Console.GetContinuingIssue
(
	@RefNum varchar(12),
	@IssueID int
)
AS
	SELECT IssueID, EntryDate,consultant,note,deleted,deleteddate,deletedby 
	From [Console].[ContinuingIssues] 
	Where RTRIM(RefNum) = @RefNum and IssueID = @IssueID and deleted = 0 order by EntryDate;
GO
PRINT N'Creating [Console].[GetContinuingIssueByConsultant]...';


GO
CREATE PROCEDURE Console.GetContinuingIssueByConsultant
(
	@RefNum varchar(12),
	@Consultant varchar(3)
)
AS
	SELECT IssueID, EntryDate,consultant,note,deleted,deleteddate,deletedby 
	From [Console].[ContinuingIssues] 
	Where RTRIM(RefNum) = @RefNum and Consultant = @Consultant and deleted = 0 order by EntryDate;
GO
PRINT N'Creating [Console].[GetCoord]...';


GO
CREATE PROCEDURE Console.GetCoord
(
	 @SiteID nvarchar(15)  --='007CC07   '; 	-- @SiteID nvarchar(15) ='PTTGC';
)
AS
	SELECT CoordName,CoordTitle,CoordAddr1,CoordAddr2,	CoordCity,
	CoordState,CoordZip,CoordPhone,CoordFax,CoordEMail
	from [dbo].[ClientInfo] where SiteID=@SiteID
GO
PRINT N'Creating [Console].[GetMFilesCompanyBenchmarkName]...';


GO

CREATE PROCEDURE  [Console].[GetMFilesCompanyBenchmarkName]
	@FacilityId nvarchar(10),
	@SiteId nvarchar(15)
AS
BEGIN
    DECLARE @Result nvarchar(200) = (SELECT CONCAT(RTRIM(c.Refnum), ' - ', RTRIM(c.CoLoc)) 
	As CompanyBenchmarkName FROM PowerWork.dbo.TSort t  LEFT JOIN MFilesSupport.dbo.CPAFacilities m ON m.FacilityID = @FacilityId  LEFT JOIN MFilesSupport.dbo.CPAParticipants p ON p.facilityid =  @FacilityId   LEFT JOIN (MFilesSupport.dbo.FacilityCompany fc INNER JOIN MFilesSupport.dbo.CPAParticipants c ON c.FacilityID = fc.CompanyID)  ON c.Study = 'Power' AND c.StudyYear = t.StudyYear AND fc.FacilityID =  @FacilityId   WHERE t.Siteid = @SiteId);
	SET @Result=RTRIM(@Result);
	SET @Result=LTRIM(@Result);
	IF LEN(@Result) = 1
		SET @Result = NULL;
	SELECT  @Result;
END
GO
PRINT N'Creating [Console].[GetRefNumsBySiteId]...';


GO
CREATE PROCEDURE [Console].[GetRefNumsBySiteId]
(
	@SiteId char(10),
	@Year int
)
AS
	SELECT Refnum from [dbo].[TSort] 
	where SiteID = @SiteId AND StudyYear = @Year AND RTRIM(Refnum) NOT LIKE '%P' order by refnum asc;
GO
PRINT N'Creating [Console].[GetSiteIds]...';


GO
CREATE PROCEDURE [Console].[GetSiteIds]
(
	@Year int
)
AS
	SELECT SiteID from [dbo].[StudySites] where StudyYear = @Year and RTRIM(SiteID) NOT LIKE '%P';
GO
PRINT N'Creating [Console].[GetTSortDataBySiteId]...';


GO

CREATE PROCEDURE Console.GetTSortDataBySiteId
(
	@SiteId nvarchar(12)
)
AS
select 
Refnum,SiteID,CompanyID,CompanyName,UnitName,UnitLabel,UnitID,CoLoc,StudyYear,
EvntYear,Continent,Country,Region,State,PricingHub,Regulated,CalcCommUnavail,
EGCRegion,EGCTechnology,EGCManualTech,CTGs,CTG_NDC,NDC,Coal_NDC,Oil_NDC,
Gas_NDC,PrecBagYN,ScrubbersYN,NumUnitsAtSite,FuelType,Metric,HHV,CurrencyID,
FormerCurrencyID,SingleShaft,NumSTs,DryCoolTower
FROM dbo.TSort 
where SiteID=@SiteId
GO
PRINT N'Creating [Console].[GetUnitnamesWithRefNums]...';


GO
CREATE PROCEDURE Console.GetUnitnamesWithRefNums
(
	@SiteId char(10),
	@Year int
)
AS
	SELECT CONCAT(RTRIM(UnitName)  , CHAR(10), CHAR(45),RTRIM(Refnum) ) As UnitnameRefnum from [dbo].[TSort] 
	where SiteID = @SiteId  AND RTRIM(SiteID) NOT LIKE '%P' AND StudyYear = @Year AND RTRIM(Refnum) NOT LIKE '%P' 
	order by UnitnameRefnum asc;
GO
PRINT N'Creating [Console].[InsertConsultingOpportunities]...';


GO

CREATE PROCEDURE [Console].[InsertConsultingOpportunities]

	@RefNum nvarchar(12),
	@ConsultingOpportunities text

AS
BEGIN
	INSERT INTO dbo.ValidationNotes (Refnum, ConsultingOpportunities) VALUES (@RefNum, @ConsultingOpportunities)
END
GO
PRINT N'Creating [Console].[UpdateCoContact]...';


GO
CREATE PROCEDURE [Console].[UpdateCoContact] 
	@SiteId nvarchar(12),
	@StudyYear int,
	@FirstName varchar(25),
	@LastName varchar(25),
	@JobTitle varchar(75),
	@Address1 varchar(75),
	@Address2 varchar(50),
	@Address3 nvarchar(50),
	@City nvarchar(30),
	@State nvarchar(30),
	@Zip nvarchar(30),
	@Country nvarchar(30),
	@Phone nvarchar(40),
	@Email nvarchar(255),
	@StAddress1 as nvarchar(75),
	@StAddress2 as nvarchar(50),
	@StAddress3 as nvarchar(50),
	@StAddress4 as nvarchar(30),
	@StAddress5 as nvarchar(30),
	@StAddress6 as nvarchar(30),
	@StAddress7 as nvarchar(30),
	@Fax as nvarchar(40),
	@ContactType as nvarchar(10),
	@Password as nvarchar(50)
AS
BEGIN
UPDATE PowerGlobal.dbo.Company_LU set Password = @Password where CompanyID = 
  (select CompanyID from dbo.TSort where SiteID = @SiteId);
IF EXISTS(
SELECT *
	FROM [dbo].[CoContactInfo] c 
	join [Console].[SANumbers] s on c.SANumber=s.SANumber
	join TSort t on s.SiteID=t.SiteID 
	WHERE t.SiteID = @SiteId
	AND t.StudyYear = @StudyYear 
	AND UPPER(c.ContactType)= @ContactType 
)
  BEGIN
   UPDATE C SET c.FirstName=@FirstName ,
		c.LastName=@LastName ,
		c.JobTitle = @JobTitle,
		c.Email =@Email,
		c.Phone = @Phone,
		c.Fax = @Fax ,
		c.MailAddr1 = @Address1 ,
		c.MailAddr2 = @Address2 ,
		c.MailAddr3 = @Address3 ,
		c.MailCity = @City  ,
		c.MailState = @State ,
		c.MailZip = @Zip ,
		c.MailCountry = @Country ,
		c.StrAddr1 = @StAddress1,
		c.StrAddr2 = @StAddress2,
		c.StrAdd3 = @StAddress3,
		c.StrCity = @StAddress4,
		c.StrState = @StAddress5,
		c.StrZip = @StAddress6,
		c.StrCountry = @StAddress7,
		c.contactType = @ContactType		
	FROM [dbo].[CoContactInfo] c 
	join [Console].[SANumbers] s on c.SANumber=s.SANumber
	join TSort t on s.SiteID=t.SiteID 
	WHERE t.SiteID= @SiteId
	AND t.StudyYear = @StudyYear 
	AND UPPER(c.ContactType)= @ContactType 	
  END
ELSE
  BEGIN
	DECLARE @SANumber int
	SET @SANumber = (SELECT SANumber from [Console].[SANumbers] s 
		join TSort t on s.SiteID=t.SiteID 
		WHERE t.SiteID = @SiteId 
		AND t.StudyYear = @StudyYear )
		INSERT INTO CoContactInfo (
			[SANumber],[ContactType] ,[FirstName] ,[LastName] ,[JobTitle] ,[Phone] ,[Fax] ,
			[Email],[StrAddr1] ,[StrAddr2] ,[StrAdd3] ,[StrCity] ,[StrState] ,[StrZip] ,
			[StrCountry] ,[MailAddr1] ,[MailAddr2] ,[MailAddr3] ,[MailCity] ,[MailState] ,
			[MailZip] ,[MailCountry] ,[CCAlt]
		) values(					
			@SANumber,
			@ContactType,
			@FirstName ,
			@LastName ,
			@JobTitle,
			@Phone,
			@Fax,
			@Email,
			@StAddress1,
			@StAddress2,
			@StAddress3,
			@StAddress4,
			@StAddress5,
			@StAddress6,
			@STAddress7,
			@Address1 ,
			@Address2 ,
			@Address3 ,
			@City  ,
			@State ,
			@Zip ,
			@Country,
			'N')
  END		
END
GO
PRINT N'Creating [Console].[UpdateConsultingOpportunities]...';


GO

CREATE PROCEDURE [Console].[UpdateConsultingOpportunities]
	@RefNum nvarchar(12),
	@ConsultingOpportunities text
AS
BEGIN
	UPDATE dbo.ValidationNotes SET ConsultingOpportunities = @ConsultingOpportunities
	WHERE Refnum=@RefNum 
END
GO
PRINT N'Creating [dbo].[UpdateClientInfo]...';


GO
CREATE PROCEDURE dbo.UpdateClientInfo
(
	@SiteID char(10)
	,
	@CorpName char(50),
	@AffName char(50),
	@PlantName char(50),
	@City char(30),
	@State char(20),
	@CoordName char(40),
	@CoordTitle char(50),
	@CoordAddr1 char(50),
	@CoordAddr2 char(50),
	@CoordCity char(30),
	@CoordState char(20),
	@CoordZip char(15),
	@CoordPhone char(30),
	@CoordFax char(20),
	@CoordEMail char(40),
	@RptUOM varchar(3),
	@RptHV varchar(3),
	@RptSteamMethod varchar(3)
	
) AS
	IF EXISTS(SELECT TOP 1 * from dbo.ClientInfo where SiteID=@SiteID) 
		BEGIN
			UPDATE dbo.ClientInfo 
			SET 
			SiteID =  RTRIM(@SiteID),
			CorpName =  RTRIM(@CorpName),
			AffName =  RTRIM(@AffName),
			PlantName =  RTRIM(@PlantName),
			City =  RTRIM(@City),
			State =  RTRIM(@State),
			CoordName =  RTRIM(@CoordName),
			CoordTitle =  RTRIM(@CoordTitle),
			CoordAddr1 =  RTRIM(@CoordAddr1),
			CoordAddr2 =  RTRIM(@CoordAddr2),
			CoordCity =  RTRIM(@CoordCity),
			CoordState =  RTRIM(@CoordState),
			CoordZip =  RTRIM(@CoordZip),
			CoordPhone =  RTRIM(@CoordPhone),
			CoordFax =  RTRIM(@CoordFax),
			CoordEMail =  RTRIM(@CoordEMail),
			RptUOM =  RTRIM(@RptUOM),
			RptHV =  RTRIM(@RptHV),
			RptSteamMethod =  RTRIM(@RptSteamMethod)
			where SiteID=@SiteID
		END
	ELSE
	BEGIN
		INSERT INTO dbo.ClientInfo (SiteID,CorpName,AffName,PlantName,City,State,CoordName,CoordTitle,CoordAddr1,CoordAddr2,CoordCity,CoordState,CoordZip,CoordPhone,CoordFax,CoordEMail,RptUOM,RptHV,RptSteamMethod)
		VALUES
		(RTRIM(@SiteID),
		RTRIM(@CorpName),
		RTRIM(@AffName),
		RTRIM(@PlantName),
		RTRIM(@City),
		RTRIM(@State),
		RTRIM(@CoordName),
		RTRIM(@CoordTitle),
		RTRIM(@CoordAddr1),
		RTRIM(@CoordAddr2),
		RTRIM(@CoordCity),
		RTRIM(@CoordState),
		RTRIM(@CoordZip),
		RTRIM(@CoordPhone),
		RTRIM(@CoordFax),
		RTRIM(@CoordEMail),
		RTRIM(@RptUOM),
		RTRIM(@RptHV),
		RTRIM(@RptSteamMethod)
		)
	END
GO
PRINT N'Creating [dbo].[UpdatePassword]...';


GO


CREATE PROCEDURE [dbo].[UpdatePassword]
(
	@RefNum nvarchar(12),
	@Password char(20) 
)
AS
BEGIN
	IF EXISTS(select * from dbo.Company_LU where CompanyID = (select CompanyID from dbo.TSort where Refnum=@RefNum))
	BEGIN
		UPDATE dbo.Company_LU 
		SET [Password] = @Password
		where CompanyID = (select CompanyID from dbo.TSort where Refnum=@RefNum)
	END
END
GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[AddOrUpdateContinuingIssues] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[AddOrUpdateContinuingIssues] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[ChangeConsultant] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[ChangeConsultant] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[ChangeCoord] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[ChangeCoord] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[DeleteContinuingIssue] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[DeleteContinuingIssue] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[GetAllConsultantsByYear] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[GetAllConsultantsByYear] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[GetChecklist] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[GetChecklist] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[GetCompanynamesWithSiteIds] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[GetCompanynamesWithSiteIds] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[GetConsultantBySiteId] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[GetConsultantBySiteId] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[GetConsultantEmail] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[GetConsultantEmail] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[GetConsultantInitials] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[GetConsultantInitials] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[GetConsultantName] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[GetConsultantName] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[GetConsultingOpportunities] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[GetConsultingOpportunities] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[GetContinuingIssue] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[GetContinuingIssue] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[GetContinuingIssueByConsultant] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[GetContinuingIssueByConsultant] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[GetCoord] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[GetCoord] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[GetMFilesCompanyBenchmarkName] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[GetMFilesCompanyBenchmarkName] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[GetRefNumsBySiteId] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[GetRefNumsBySiteId] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[GetSiteIds] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[GetSiteIds] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[GetTSortDataBySiteId] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[GetTSortDataBySiteId] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[GetUnitnamesWithRefNums] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[GetUnitnamesWithRefNums] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[InsertConsultingOpportunities] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[InsertConsultingOpportunities] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[UpdateCoContact] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[UpdateCoContact] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[UpdateConsultingOpportunities] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[Console].[UpdateConsultingOpportunities] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[ConsultantInfo] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT DELETE
    ON OBJECT::[Console].[ConsultantInfo] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT INSERT
    ON OBJECT::[Console].[ConsultantInfo] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT SELECT
    ON OBJECT::[Console].[ConsultantInfo] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT UPDATE
    ON OBJECT::[Console].[ConsultantInfo] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[ContinuingIssues] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT DELETE
    ON OBJECT::[Console].[ContinuingIssues] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT INSERT
    ON OBJECT::[Console].[ContinuingIssues] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT SELECT
    ON OBJECT::[Console].[ContinuingIssues] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT UPDATE
    ON OBJECT::[Console].[ContinuingIssues] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[Employees] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT DELETE
    ON OBJECT::[Console].[Employees] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT INSERT
    ON OBJECT::[Console].[Employees] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT SELECT
    ON OBJECT::[Console].[Employees] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT UPDATE
    ON OBJECT::[Console].[Employees] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[Console].[SANumbers] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT DELETE
    ON OBJECT::[Console].[SANumbers] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT INSERT
    ON OBJECT::[Console].[SANumbers] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT SELECT
    ON OBJECT::[Console].[SANumbers] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT UPDATE
    ON OBJECT::[Console].[SANumbers] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[dbo].[UpdateClientInfo] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[UpdateClientInfo] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT ALTER
    ON OBJECT::[dbo].[UpdatePassword] TO PUBLIC
    AS [dbo];


GO
PRINT N'Creating Permission...';


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[UpdatePassword] TO PUBLIC
    AS [dbo];


GO
PRINT N'Refreshing [Console].[AddIssue]...';


GO
EXECUTE sp_refreshsqlmodule N'[Console].[AddIssue]';


GO
PRINT N'Update complete.';


GO
