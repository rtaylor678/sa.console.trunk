﻿Imports System
Imports SA.Internal.Console.DataObject
Imports System.Data.OleDb
Imports System.Data.SqlClient

Public Class PowerChecklistManager
    'Dim _selected As TreeNode = Nothing
    Dim _dAL As DataObject = Nothing
    Dim _siteId As String = String.Empty
    Dim _userName As String = String.Empty

    Public Sub New(db As DataObject, siteId As String, userName As String)
        _dAL = db
        _siteId = siteId
        _userName = userName

    End Sub

    Public Function GetResults(chosenViewAIC As String, chosenActionAV As String, issueId As String,
            checked As Boolean?, changedText As List(Of String)) As PowerChecklistResults
        Dim results As New PowerChecklistResults()
        Try
            'populate checked 
            'results.IsChecked = checked
            If chosenActionAV.StartsWith("A") Then 'Add new
                If IsNothing(changedText) Then
                    'don't ad to db yet, just clear boxes etc.AddIssueInDB(checked, changedText)
                    results = New PowerChecklistResults()
                    results.BoxesContent = New List(Of String)
                    results.BoxesContent.Add(String.Empty)
                    results.BoxesContent.Add(String.Empty)
                    results.BoxesContent.Add(String.Empty)
                    results.Completed = False
                    results.IsChecked = False
                    'results.IsCustom = True
                    results.SelectedNodeText = String.Empty
                    results.ShadedSelectedNodeText = String.Empty
                    results.Visible = False
                Else
                    'user passing in values for new db record
                    AddIssueInDB(issueId, False, changedText)
                    results = View(issueId, results)
                    results.SelectedNodeText = changedText(0) & " - " + changedText(1)
                    results.ShadedSelectedNodeText = changedText(0) & " - " + changedText(1)
                End If
                Return results
            ElseIf chosenActionAV.StartsWith("V") Then 'view
                'populate boxes content, checked, shaded
                If Not IsNothing(checked) Then
                    'changed checkbox
                    EditIssueInDB(issueId, CBool(checked), Nothing)
                    results = View(issueId, results)

                ElseIf Not IsNothing(changedText) Then
                    'changed text
                    'NOTE: on the UI, Checked = COMPLETE
                    EditIssueInDB(issueId, Nothing, changedText)
                    results = View(issueId, results)
                    If changedText(1) <> results.BoxesContent(1) Or
                        changedText(2) <> results.BoxesContent(2) Then
                        MsgBox("No change was made in the database. This might be because you tried to change a 'non-custom' checklist item.")
                    End If


                Else 'just view
                    results = View(issueId, results)
                End If
            End If

            'now check for visibility, might need to alter shaded. 
            Select Case chosenViewAIC.ToUpper().Substring(0, 1)
                Case "A"
                    'OK
                    results.Visible = True
                Case "I"
                    If results.IsChecked Then
                        results.ShadedSelectedNodeText = String.Empty
                        results.Visible = False
                    Else
                        results.Visible = True
                    End If
                Case "C"
                    If Not results.IsChecked Then
                        results.ShadedSelectedNodeText = String.Empty
                        results.Visible = False
                    Else
                        results.Visible = True
                    End If
                Case Else
                    Throw New NotImplementedException(chosenViewAIC.ToUpper().Substring(0, 1) & " is not a valid chosenView")
            End Select

            Return results
        Catch ex As Exception
            MsgBox("Error in RefiningChecklistManager: " & ex.Message)
            Return Nothing
        End Try
    End Function

    Private Function View(issueID As String, results As PowerChecklistResults) As PowerChecklistResults
        'get boxes content and whether checked or not
        If ReadIssueFromDB(issueID, results) Then
            'choose which is shaded
            results.IsChecked = (results.Completed = "Y")
            results.SelectedNodeText = CreateNodeText(results.BoxesContent(0), results.BoxesContent(1))
            results.ShadedSelectedNodeText = results.SelectedNodeText
        End If
        Return results
    End Function

    Private Function CreateNodeText(issueID As String, issueName As String) As String
        Return issueID.Trim() & " - " & issueName.Trim()
    End Function

    Private Function ReadIssueFromDB(issueId As String, ByRef results As PowerChecklistResults) As Boolean
        Try
            Dim row As DataRow
            Dim params As New List(Of String)
            params.Add("SiteId/" + _siteId)
            params.Add("IssueID/" & issueId)

            Dim sql As String = "select SiteId,IssueID,IssueTitle,IssueText,PostedBy,PostedTime,Completed,SetBy,SetTime from Val.Checklist where RTRIM(SiteId) = '" + _siteId.Trim() + "' AND IssueID = '" + issueId + "';"
            _dAL.SQL = sql
            'Dim ds As DataSet = _dAL.ExecuteStoredProc("Console." & "GetAnIssue", params)

            Dim ds As DataSet = _dAL.Execute()
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    results.BoxesContent = New List(Of String)
                    results.BoxesContent.Add(row("IssueID").ToString.Trim())
                    results.BoxesContent.Add(row("IssueTitle").ToString.Trim())
                    results.BoxesContent.Add(row("IssueText").ToString.Trim())
                    results.Completed = row("Completed").ToString.Trim()
                    'results.IsCustom = row("IsCustom")
                End If
            End If
            Return True
        Catch ex As Exception
            MsgBox("Error in ReadIssueFromDB: " & ex.Message)
            Return False
        Finally
            _dAL.SQL = String.Empty
        End Try
    End Function

    Private Function EditIssueInDB(issueId As String, completed? As Boolean, changedText As List(Of String)) As Boolean
        'NOTE: on the UI, Checked = COMPLETE

        'Throw New NotImplementedException("Change below to use the  Console.AddIssue proc")
        'GLC added IsCustom, a non-null bit field that defaults to false (0). added an optional parameter, @IsCustom, to AddIssue that is a bit that defaults to 0. 
        Try
            Dim completedString As String = Nothing
            If Not IsNothing(completed) Then
                If CBool(completed) Then
                    completedString = "Y"
                Else
                    completedString = "N"
                End If
            End If

            Dim sql As String = "UPDATE Val.Checklist set "
            Try
                If Not IsNothing(completed) Then
                    'sql = sql & " Completed = '" & completedString & "'"
                    sql = sql & " Completed = @Completed, "
                    sql = sql & " SetBy = @SetBy, "
                    sql = sql & " SetTime = @SetTime "
                    sql = sql & " WHERE SiteId = '" & _siteId.Trim() & "' and IssueId = '" & issueId.Trim() & "'"
                    If Not IsNothing(changedText) Then
                        sql = sql & " and IsCustom=1"  'menas this is created by the user, not the datagroup")
                    End If

                    Using cx As New SqlConnection(_dAL.ConnectionString)
                        cx.Open()
                        Dim cmd As SqlCommand = New SqlCommand(sql, cx)
                        cmd.Parameters.Add(New SqlParameter("Completed", completedString))
                        cmd.Parameters.Add(New SqlParameter("SetBy", _userName))
                        cmd.Parameters.Add(New SqlParameter("SetTime", DateTime.Now))
                        cmd.ExecuteNonQuery()
                    End Using

                ElseIf Not IsNothing(changedText) Then
                    sql = sql & " IssueTitle = @IssueTitle"
                    sql = sql & ", IssueText = @IssueText"

                    sql = sql & " WHERE SiteId = '" & _siteId.Trim() & "' and IssueId = '" & issueId.Trim() & "'"
                    'If Not IsNothing(changedText) Then
                    '    sql = sql & " and IsCustom=1"  'menas this is created by the user, not the datagroup")
                    'End If

                    Using cx As New SqlConnection(_dAL.ConnectionString)
                        cx.Open()
                        Dim cmd As SqlCommand = New SqlCommand(sql, cx)
                        cmd.Parameters.Add(New SqlParameter("IssueTitle", changedText(1)))
                        cmd.Parameters.Add(New SqlParameter("IssueText", changedText(2)))
                        cmd.ExecuteNonQuery()
                    End Using
                End If

            Catch exQry As Exception
                Return False
            End Try


            'If Not IsNothing(completed) Then
            '    sql = sql & " Completed = '" & completedString & "'"
            'End If
            'If Not IsNothing(changedText) Then
            '    sql = sql & " IssueTitle = '" & changedText(1).Replace("'", "") & "'"
            '    sql = sql & ", IssueText = '" & changedText(2).Replace("'", "") & "'"
            'End If
            ''( IsCustom = 1") menas this is created by the user, not the datagroup")
            'sql = sql & " WHERE Refnum = '" & _refnum & "' and IssueId = '" & issueId.Trim() & "'"
            'If Not IsNothing(changedText) Then
            '    sql = sql & " and IsCustom=1"
            'End If
            '_dAL.SQL = sql
            '_dAL.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            MsgBox("Error in EditIssueInDB: " & ex.Message)
            Return False
        Finally
            _dAL.SQL = String.Empty
        End Try
    End Function

    Private Function AddIssueInDB(issueID As String, completed As Boolean, changedText As List(Of String)) As Boolean
        'NOTE: on the UI, Checked = COMPLETE
        'MsgBox("always populate the new Cstom field in code ( IsCustom = 1") since this is done by the user")
        'Throw New NotImplementedException("Change below to use the  Console.AddIssue proc")
        'GLC added IsCustom, a non-null bit field that defaults to false (0). added an optional parameter, @IsCustom, to AddIssue that is a bit that defaults to 0. 


        Try
            Dim completedString As String = "Y"
            If Not completed Then completedString = "N"
            Dim sql As String = "INSERT INTO Val.Checklist (SiteId,IssueID,IssueTitle,IssueText,PostedBy,PostedTime,Completed,SetBy,SetTime) values ("
            sql = sql & "'" & _siteId & "',"
            'sql = sql & "'" & issueID.Trim() & "',"
            'sql = sql & "'" & changedText(1) & "',"
            'sql = sql & "'" & changedText(2) & "',"
            sql = sql & " @IssueID,"
            sql = sql & " @IssueTitle,"
            sql = sql & " @IssueText,"
            sql = sql & "'" & _userName & "',"
            sql = sql & "GETDATE(),"
            sql = sql & "'" & completedString & "',"
            sql = sql & "'" & _userName & "',"
            sql = sql & "GETDATE())"
            'sql = sql & "1)"

            Using cx As New SqlConnection(_dAL.ConnectionString)
                cx.Open()
                Dim cmd As SqlCommand = New SqlCommand(sql, cx)
                'cmd.Parameters.Add(New SqlParameter("IssueTitle", changedText(1)))
                'cmd.Parameters.Add(New SqlParameter("Refnum", ))
                cmd.Parameters.Add(New SqlParameter("IssueID", issueID.Trim()))
                cmd.Parameters.Add(New SqlParameter("IssueTitle", changedText(1)))
                cmd.Parameters.Add(New SqlParameter("IssueText", changedText(2)))
                'cmd.Parameters.Add(New SqlParameter("PostedBy", ))
                'cmd.Parameters.Add(New SqlParameter("PostedTime", ))
                'cmd.Parameters.Add(New SqlParameter("Completed", ))
                'cmd.Parameters.Add(New SqlParameter("SetBy", ))
                'cmd.Parameters.Add(New SqlParameter("SetTime", ))
                'cmd.Parameters.Add(New SqlParameter("IsCustom", ))
                cmd.ExecuteNonQuery()
            End Using

            '_dAL.SQL = sql
            '_dAL.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            MsgBox("Error in EditIssueInDB: " & ex.Message)
            Return False
        Finally
            _dAL.SQL = String.Empty
        End Try
    End Function

End Class



Public Class PowerChecklistResults
    Private _visible As Boolean
    Public Property Visible() As Boolean
        Get
            Return _visible
        End Get
        Set(ByVal value As Boolean)
            _visible = value
        End Set
    End Property

    Private _boxesContent As List(Of String)
    Public Property BoxesContent() As List(Of String)
        Get
            Return _boxesContent
        End Get
        Set(ByVal value As List(Of String))
            _boxesContent = value
        End Set
    End Property

    Private _selectedNodeText As String
    Public Property SelectedNodeText() As String
        Get
            Return _selectedNodeText
        End Get
        Set(ByVal value As String)
            _selectedNodeText = value
        End Set
    End Property

    Private _shadedSelectedNodeText As String
    Public Property ShadedSelectedNodeText() As String
        Get
            Return _shadedSelectedNodeText
        End Get
        Set(ByVal value As String)
            _shadedSelectedNodeText = value
        End Set
    End Property

    Private _isChecked As Boolean
    Public Property IsChecked() As Boolean
        Get
            Return _isChecked
        End Get
        Set(ByVal value As Boolean)
            _isChecked = value
        End Set
    End Property

    Private _completed As String
    Public Property Completed() As String
        Get
            Return _completed
        End Get
        Set(ByVal value As String)
            _completed = value
        End Set
    End Property

    'Private _isCustom As Boolean
    'Public Property IsCustom() As String
    '    Get
    '        Return _isCustom
    '    End Get
    '    Set(ByVal value As String)
    '        _isCustom = value
    '    End Set
    'End Property


End Class
