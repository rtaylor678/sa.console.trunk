﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PowerConsole
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PowerConsole))
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnHelp = New System.Windows.Forms.Button()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.txtVersion = New System.Windows.Forms.TextBox()
        Me.VersionToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnKillProcesses = New System.Windows.Forms.Button()
        Me.btnSendPrelimPricing = New System.Windows.Forms.Button()
        Me.btnCreateMissingPNFiles = New System.Windows.Forms.Button()
        Me.btnBug = New System.Windows.Forms.Button()
        Me.btnFLValidation = New System.Windows.Forms.Button()
        Me.btnValFax = New System.Windows.Forms.Button()
        Me.btnJustLooking = New System.Windows.Forms.Button()
        Me.btnValidate = New System.Windows.Forms.Button()
        Me.btnReturnPW = New System.Windows.Forms.Button()
        Me.btnCompanyPW = New System.Windows.Forms.Button()
        Me.btnMain = New System.Windows.Forms.Button()
        Me.btnBuildVRFile = New System.Windows.Forms.Button()
        Me.btnReceiptAck = New System.Windows.Forms.Button()
        Me.btnValidatedInputFile = New System.Windows.Forms.Button()
        Me.btnDrawings = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblItemsRemaining = New System.Windows.Forms.Label()
        Me.lblBlueFlags = New System.Windows.Forms.Label()
        Me.lblRedFlags = New System.Windows.Forms.Label()
        Me.lblLastCalcDate = New System.Windows.Forms.Label()
        Me.lblLastUpload = New System.Windows.Forms.Label()
        Me.lblLastFileSave = New System.Windows.Forms.Label()
        Me.lblValidationStatus = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.btnOpenCompanyCorresp = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.btnDragTest = New System.Windows.Forms.Button()
        Me.lblCompanyCorresp = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblCoPW = New System.Windows.Forms.Label()
        Me.lblRetPW = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.listViewRefnums = New System.Windows.Forms.ListView()
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cboSiteId = New System.Windows.Forms.ComboBox()
        Me.lblCompany = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboConsultant = New System.Windows.Forms.ComboBox()
        Me.lblWarning = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.cboStudy = New System.Windows.Forms.ComboBox()
        Me.Help = New System.Windows.Forms.HelpProvider()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.tabQA = New System.Windows.Forms.TabPage()
        Me.tv = New System.Windows.Forms.TreeView()
        Me.chkUseRefnum = New System.Windows.Forms.CheckBox()
        Me.QASearch = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.lnkResultsPresentation = New System.Windows.Forms.LinkLabel()
        Me.lnkPricing = New System.Windows.Forms.LinkLabel()
        Me.lnkMisc = New System.Windows.Forms.LinkLabel()
        Me.lnkProcessData = New System.Windows.Forms.LinkLabel()
        Me.lnkOpex = New System.Windows.Forms.LinkLabel()
        Me.lnkMaintenance = New System.Windows.Forms.LinkLabel()
        Me.lnkPersonnel = New System.Windows.Forms.LinkLabel()
        Me.lnkEnergy = New System.Windows.Forms.LinkLabel()
        Me.lnkMaterialBalance = New System.Windows.Forms.LinkLabel()
        Me.lnkStudyBoundary = New System.Windows.Forms.LinkLabel()
        Me.lnkRefineryHistory = New System.Windows.Forms.LinkLabel()
        Me.tabClippy = New System.Windows.Forms.TabPage()
        Me.btnQueryQuit = New System.Windows.Forms.Button()
        Me.lblError = New System.Windows.Forms.Label()
        Me.chkSQL = New System.Windows.Forms.CheckBox()
        Me.dgClippyResults = New System.Windows.Forms.DataGridView()
        Me.btnClippySearch = New System.Windows.Forms.Button()
        Me.txtClippySearch = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.tabDD = New System.Windows.Forms.TabPage()
        Me.btnDDExit = New System.Windows.Forms.Button()
        Me.optCompanyCorr = New System.Windows.Forms.CheckBox()
        Me.txtMessageFilename = New System.Windows.Forms.TextBox()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.btnGVClear = New System.Windows.Forms.Button()
        Me.btnFilesSave = New System.Windows.Forms.Button()
        Me.dgFiles = New System.Windows.Forms.DataGridView()
        Me.OriginalFilename = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Filenames = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FileDestination = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.tabTimeGrade = New System.Windows.Forms.TabPage()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dgHistory = New System.Windows.Forms.DataGridView()
        Me.dgSummary = New System.Windows.Forms.DataGridView()
        Me.btnGradeExit = New System.Windows.Forms.Button()
        Me.btnSection = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dgGrade = New System.Windows.Forms.DataGridView()
        Me.tabSS = New System.Windows.Forms.TabPage()
        Me.chkIDR = New System.Windows.Forms.CheckBox()
        Me.btnSecureSendRefresh = New System.Windows.Forms.Button()
        Me.btnDirRefresh = New System.Windows.Forms.Button()
        Me.btnSecureSend = New System.Windows.Forms.Button()
        Me.tvwCompCorr2 = New System.Windows.Forms.TreeView()
        Me.tvwCompCorr = New System.Windows.Forms.TreeView()
        Me.tvwCorrespondence2 = New System.Windows.Forms.TreeView()
        Me.tvwDrawings2 = New System.Windows.Forms.TreeView()
        Me.tvwClientAttachments2 = New System.Windows.Forms.TreeView()
        Me.lblSecureSendDirectory2 = New System.Windows.Forms.Label()
        Me.cboDir2 = New System.Windows.Forms.ComboBox()
        Me.tvwClientAttachments = New System.Windows.Forms.TreeView()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboDir = New System.Windows.Forms.ComboBox()
        Me.tvwDrawings = New System.Windows.Forms.TreeView()
        Me.tvwCorrespondence = New System.Windows.Forms.TreeView()
        Me.tabCI = New System.Windows.Forms.TabPage()
        Me.btnIssueCancel = New System.Windows.Forms.Button()
        Me.chkNoDates = New System.Windows.Forms.CheckBox()
        Me.btnExportCI = New System.Windows.Forms.Button()
        Me.btnNewIssue = New System.Windows.Forms.Button()
        Me.btnSaveCI = New System.Windows.Forms.Button()
        Me.dgContinuingIssues = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EntryDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Issue = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Consultant = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DeletedBy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DeletedDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Deleted = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtCI = New System.Windows.Forms.TextBox()
        Me.tabNotes = New System.Windows.Forms.TabPage()
        Me.txtConsultingOpportunities = New System.Windows.Forms.TextBox()
        Me.lblConsultingOpportunities = New System.Windows.Forms.Label()
        Me.btnUnlockPN = New System.Windows.Forms.Button()
        Me.btnCreatePN = New System.Windows.Forms.Button()
        Me.btnSavePN = New System.Windows.Forms.Button()
        Me.txtNotes = New System.Windows.Forms.TextBox()
        Me.tabCheckList = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.radChecklistEditIssue = New System.Windows.Forms.RadioButton()
        Me.radChecklistAddNew = New System.Windows.Forms.RadioButton()
        Me.btnUpdateIssue = New System.Windows.Forms.Button()
        Me.cboCheckListView = New System.Windows.Forms.ComboBox()
        Me.lblCheckboxView = New System.Windows.Forms.Label()
        Me.tvIssues = New System.Windows.Forms.TreeView()
        Me.txtIssueName = New System.Windows.Forms.TextBox()
        Me.txtIssueID = New System.Windows.Forms.TextBox()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.lblItemCount = New System.Windows.Forms.Label()
        Me.btnAddIssue = New System.Windows.Forms.Button()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.ValCheckList = New System.Windows.Forms.CheckedListBox()
        Me.tabCorr = New System.Windows.Forms.TabPage()
        Me.btnManageGadsData = New System.Windows.Forms.Button()
        Me.btnCorrespRefresh = New System.Windows.Forms.Button()
        Me.btnDataFolder = New System.Windows.Forms.Button()
        Me.listViewCorrespondenceVReturn = New System.Windows.Forms.ListView()
        Me.listViewCorrespondenceVR = New System.Windows.Forms.ListView()
        Me.listViewCorrespondenceVF = New System.Windows.Forms.ListView()
        Me.btnPolishRpt = New System.Windows.Forms.Button()
        Me.btnStartReliabilityFile = New System.Windows.Forms.Button()
        Me.btnVI = New System.Windows.Forms.Button()
        Me.btnSC = New System.Windows.Forms.Button()
        Me.btnPA = New System.Windows.Forms.Button()
        Me.btnCT = New System.Windows.Forms.Button()
        Me.btnMasterEventsFile = New System.Windows.Forms.Button()
        Me.btnPT = New System.Windows.Forms.Button()
        Me.btnSpecFrac = New System.Windows.Forms.Button()
        Me.btnUnitReview = New System.Windows.Forms.Button()
        Me.lstReturnFiles = New System.Windows.Forms.ListBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.lstVRFiles = New System.Windows.Forms.ListBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.lstVFFiles = New System.Windows.Forms.ListBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.lstVFNumbers = New System.Windows.Forms.ListBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.tabContacts = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.pnlCompany = New System.Windows.Forms.Panel()
        Me.btnGeneralCorrespondence = New System.Windows.Forms.Button()
        Me.txtGCorr = New System.Windows.Forms.TextBox()
        Me.cboPCCEmail = New System.Windows.Forms.ComboBox()
        Me.cboICEmail = New System.Windows.Forms.ComboBox()
        Me.cboACEmail2 = New System.Windows.Forms.ComboBox()
        Me.lblPCCEmail = New System.Windows.Forms.TextBox()
        Me.lblPCCName = New System.Windows.Forms.TextBox()
        Me.cboCCEmail1 = New System.Windows.Forms.ComboBox()
        Me.CCEmail1 = New System.Windows.Forms.PictureBox()
        Me.lblInterimPhone = New System.Windows.Forms.TextBox()
        Me.lblAltPhone = New System.Windows.Forms.TextBox()
        Me.lblPhone = New System.Windows.Forms.TextBox()
        Me.lblInterimEmail = New System.Windows.Forms.TextBox()
        Me.lblAltEmail = New System.Windows.Forms.TextBox()
        Me.lblEmail = New System.Windows.Forms.TextBox()
        Me.lblInterimName = New System.Windows.Forms.TextBox()
        Me.lblAltName = New System.Windows.Forms.TextBox()
        Me.lblName = New System.Windows.Forms.TextBox()
        Me.btnShowIntCo = New System.Windows.Forms.Button()
        Me.btnShowAltCo = New System.Windows.Forms.Button()
        Me.btnShow = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.MainPanel = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.pnlRefinery = New System.Windows.Forms.Panel()
        Me.ConsoleTabs = New System.Windows.Forms.TabControl()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.tabQA.SuspendLayout()
        Me.tabClippy.SuspendLayout()
        CType(Me.dgClippyResults, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabDD.SuspendLayout()
        CType(Me.dgFiles, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabTimeGrade.SuspendLayout()
        CType(Me.dgHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSS.SuspendLayout()
        Me.tabCI.SuspendLayout()
        CType(Me.dgContinuingIssues, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabNotes.SuspendLayout()
        Me.tabCheckList.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.tabCorr.SuspendLayout()
        Me.tabContacts.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.pnlCompany.SuspendLayout()
        CType(Me.CCEmail1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MainPanel.SuspendLayout()
        Me.ConsoleTabs.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnHelp
        '
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(336, 38)
        Me.btnHelp.Margin = New System.Windows.Forms.Padding(4)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(36, 28)
        Me.btnHelp.TabIndex = 32
        Me.VersionToolTip.SetToolTip(Me.btnHelp, "Documentation")
        Me.btnHelp.UseVisualStyleBackColor = True
        Me.btnHelp.Visible = False
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "danger.jpg")
        Me.ImageList1.Images.SetKeyName(1, "File.bmp")
        '
        'txtVersion
        '
        Me.txtVersion.BackColor = System.Drawing.SystemColors.Control
        Me.txtVersion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVersion.Location = New System.Drawing.Point(1044, 864)
        Me.txtVersion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtVersion.Name = "txtVersion"
        Me.txtVersion.ReadOnly = True
        Me.txtVersion.Size = New System.Drawing.Size(273, 19)
        Me.txtVersion.TabIndex = 39
        Me.VersionToolTip.SetToolTip(Me.txtVersion, "This is the new version")
        '
        'btnKillProcesses
        '
        Me.btnKillProcesses.Image = CType(resources.GetObject("btnKillProcesses.Image"), System.Drawing.Image)
        Me.btnKillProcesses.Location = New System.Drawing.Point(380, 6)
        Me.btnKillProcesses.Margin = New System.Windows.Forms.Padding(4)
        Me.btnKillProcesses.Name = "btnKillProcesses"
        Me.btnKillProcesses.Size = New System.Drawing.Size(36, 28)
        Me.btnKillProcesses.TabIndex = 42
        Me.VersionToolTip.SetToolTip(Me.btnKillProcesses, "Excel and Word Process Killer - this will kill all running processes of Excel and" &
        " Word")
        Me.btnKillProcesses.UseVisualStyleBackColor = True
        '
        'btnSendPrelimPricing
        '
        Me.btnSendPrelimPricing.Location = New System.Drawing.Point(307, 48)
        Me.btnSendPrelimPricing.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSendPrelimPricing.Name = "btnSendPrelimPricing"
        Me.btnSendPrelimPricing.Size = New System.Drawing.Size(112, 28)
        Me.btnSendPrelimPricing.TabIndex = 43
        Me.btnSendPrelimPricing.Text = "Prelim Pricing"
        Me.VersionToolTip.SetToolTip(Me.btnSendPrelimPricing, "Send Preliminary Pricing")
        Me.btnSendPrelimPricing.UseVisualStyleBackColor = True
        Me.btnSendPrelimPricing.Visible = False
        '
        'btnCreateMissingPNFiles
        '
        Me.btnCreateMissingPNFiles.Location = New System.Drawing.Point(307, 82)
        Me.btnCreateMissingPNFiles.Margin = New System.Windows.Forms.Padding(1)
        Me.btnCreateMissingPNFiles.Name = "btnCreateMissingPNFiles"
        Me.btnCreateMissingPNFiles.Size = New System.Drawing.Size(112, 28)
        Me.btnCreateMissingPNFiles.TabIndex = 45
        Me.btnCreateMissingPNFiles.Text = "Missing PNs"
        Me.VersionToolTip.SetToolTip(Me.btnCreateMissingPNFiles, "Create all missing Presenter Note documents")
        Me.btnCreateMissingPNFiles.UseVisualStyleBackColor = True
        Me.btnCreateMissingPNFiles.Visible = False
        '
        'btnBug
        '
        Me.btnBug.Image = CType(resources.GetObject("btnBug.Image"), System.Drawing.Image)
        Me.btnBug.Location = New System.Drawing.Point(335, 6)
        Me.btnBug.Margin = New System.Windows.Forms.Padding(4)
        Me.btnBug.Name = "btnBug"
        Me.btnBug.Size = New System.Drawing.Size(36, 28)
        Me.btnBug.TabIndex = 46
        Me.VersionToolTip.SetToolTip(Me.btnBug, "Use this to enter a bug you have found.  Please enter all steps to reproduce the " &
        "error including Refnum chosen.")
        Me.btnBug.UseVisualStyleBackColor = True
        '
        'btnFLValidation
        '
        Me.btnFLValidation.Location = New System.Drawing.Point(157, 77)
        Me.btnFLValidation.Margin = New System.Windows.Forms.Padding(4)
        Me.btnFLValidation.Name = "btnFLValidation"
        Me.btnFLValidation.Size = New System.Drawing.Size(112, 28)
        Me.btnFLValidation.TabIndex = 54
        Me.btnFLValidation.Text = "FL Review"
        Me.VersionToolTip.SetToolTip(Me.btnFLValidation, "Fuel Lube Combo Review")
        Me.btnFLValidation.UseVisualStyleBackColor = True
        Me.btnFLValidation.Visible = False
        '
        'btnValFax
        '
        Me.btnValFax.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(112, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnValFax.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnValFax.ForeColor = System.Drawing.Color.White
        Me.btnValFax.Location = New System.Drawing.Point(157, 15)
        Me.btnValFax.Margin = New System.Windows.Forms.Padding(4)
        Me.btnValFax.Name = "btnValFax"
        Me.btnValFax.Size = New System.Drawing.Size(112, 57)
        Me.btnValFax.TabIndex = 53
        Me.btnValFax.Text = "New VF"
        Me.VersionToolTip.SetToolTip(Me.btnValFax, "Create a new Vetted Fax")
        Me.btnValFax.UseVisualStyleBackColor = False
        '
        'btnJustLooking
        '
        Me.btnJustLooking.Location = New System.Drawing.Point(307, 14)
        Me.btnJustLooking.Margin = New System.Windows.Forms.Padding(4)
        Me.btnJustLooking.Name = "btnJustLooking"
        Me.btnJustLooking.Size = New System.Drawing.Size(112, 28)
        Me.btnJustLooking.TabIndex = 49
        Me.btnJustLooking.Text = "Just Looking"
        Me.VersionToolTip.SetToolTip(Me.btnJustLooking, "IDR without Calculations")
        Me.btnJustLooking.UseVisualStyleBackColor = True
        Me.btnJustLooking.Visible = False
        '
        'btnValidate
        '
        Me.btnValidate.BackColor = System.Drawing.Color.Green
        Me.btnValidate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnValidate.ForeColor = System.Drawing.Color.White
        Me.btnValidate.Location = New System.Drawing.Point(17, 14)
        Me.btnValidate.Margin = New System.Windows.Forms.Padding(4)
        Me.btnValidate.Name = "btnValidate"
        Me.btnValidate.Size = New System.Drawing.Size(112, 58)
        Me.btnValidate.TabIndex = 50
        Me.btnValidate.Text = "IDR"
        Me.VersionToolTip.SetToolTip(Me.btnValidate, "Internal Data Review")
        Me.btnValidate.UseVisualStyleBackColor = False
        '
        'btnReturnPW
        '
        Me.btnReturnPW.Location = New System.Drawing.Point(246, 166)
        Me.btnReturnPW.Margin = New System.Windows.Forms.Padding(4)
        Me.btnReturnPW.Name = "btnReturnPW"
        Me.btnReturnPW.Size = New System.Drawing.Size(215, 28)
        Me.btnReturnPW.TabIndex = 81
        Me.VersionToolTip.SetToolTip(Me.btnReturnPW, "Copies Return File Password to the clipboard")
        Me.btnReturnPW.UseVisualStyleBackColor = True
        '
        'btnCompanyPW
        '
        Me.btnCompanyPW.Location = New System.Drawing.Point(246, 133)
        Me.btnCompanyPW.Margin = New System.Windows.Forms.Padding(4)
        Me.btnCompanyPW.Name = "btnCompanyPW"
        Me.btnCompanyPW.Size = New System.Drawing.Size(215, 28)
        Me.btnCompanyPW.TabIndex = 79
        Me.VersionToolTip.SetToolTip(Me.btnCompanyPW, "Copies Company Password to Clipboard")
        Me.btnCompanyPW.UseVisualStyleBackColor = True
        '
        'btnMain
        '
        Me.btnMain.Location = New System.Drawing.Point(343, 94)
        Me.btnMain.Margin = New System.Windows.Forms.Padding(4)
        Me.btnMain.Name = "btnMain"
        Me.btnMain.Size = New System.Drawing.Size(118, 28)
        Me.btnMain.TabIndex = 78
        Me.VersionToolTip.SetToolTip(Me.btnMain, "Consultant that has spent the most time on this refnum")
        Me.btnMain.UseVisualStyleBackColor = True
        Me.btnMain.Visible = False
        '
        'btnBuildVRFile
        '
        Me.btnBuildVRFile.Location = New System.Drawing.Point(264, 219)
        Me.btnBuildVRFile.Margin = New System.Windows.Forms.Padding(4)
        Me.btnBuildVRFile.Name = "btnBuildVRFile"
        Me.btnBuildVRFile.Size = New System.Drawing.Size(163, 28)
        Me.btnBuildVRFile.TabIndex = 46
        Me.btnBuildVRFile.Text = "Build a VR File"
        Me.VersionToolTip.SetToolTip(Me.btnBuildVRFile, "Create a new Vetted Reply Document")
        Me.btnBuildVRFile.UseVisualStyleBackColor = True
        Me.btnBuildVRFile.Visible = False
        '
        'btnReceiptAck
        '
        Me.btnReceiptAck.Location = New System.Drawing.Point(87, 219)
        Me.btnReceiptAck.Margin = New System.Windows.Forms.Padding(4)
        Me.btnReceiptAck.Name = "btnReceiptAck"
        Me.btnReceiptAck.Size = New System.Drawing.Size(169, 28)
        Me.btnReceiptAck.TabIndex = 8
        Me.btnReceiptAck.Text = "Receipt Acknowledged"
        Me.VersionToolTip.SetToolTip(Me.btnReceiptAck, "Creates a timestamped VA File")
        Me.btnReceiptAck.UseVisualStyleBackColor = True
        '
        'btnValidatedInputFile
        '
        Me.btnValidatedInputFile.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnValidatedInputFile.Enabled = False
        Me.btnValidatedInputFile.Location = New System.Drawing.Point(1128, 106)
        Me.btnValidatedInputFile.Margin = New System.Windows.Forms.Padding(1)
        Me.btnValidatedInputFile.Name = "btnValidatedInputFile"
        Me.btnValidatedInputFile.Size = New System.Drawing.Size(147, 31)
        Me.btnValidatedInputFile.TabIndex = 104
        Me.btnValidatedInputFile.Text = "Validated Input File"
        Me.btnValidatedInputFile.UseVisualStyleBackColor = True
        Me.btnValidatedInputFile.Visible = False
        '
        'btnDrawings
        '
        Me.btnDrawings.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnDrawings.Enabled = False
        Me.btnDrawings.Location = New System.Drawing.Point(1128, 311)
        Me.btnDrawings.Margin = New System.Windows.Forms.Padding(1)
        Me.btnDrawings.Name = "btnDrawings"
        Me.btnDrawings.Size = New System.Drawing.Size(147, 31)
        Me.btnDrawings.TabIndex = 105
        Me.btnDrawings.Text = " General, Drawings"
        Me.btnDrawings.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.Black
        Me.lblStatus.Location = New System.Drawing.Point(12, 864)
        Me.lblStatus.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(55, 18)
        Me.lblStatus.TabIndex = 44
        Me.lblStatus.Text = "Ready"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.Label16)
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.btnBug)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.lblItemsRemaining)
        Me.Panel3.Controls.Add(Me.btnKillProcesses)
        Me.Panel3.Controls.Add(Me.lblBlueFlags)
        Me.Panel3.Controls.Add(Me.lblRedFlags)
        Me.Panel3.Controls.Add(Me.btnHelp)
        Me.Panel3.Controls.Add(Me.lblLastCalcDate)
        Me.Panel3.Controls.Add(Me.lblLastUpload)
        Me.Panel3.Controls.Add(Me.lblLastFileSave)
        Me.Panel3.Controls.Add(Me.lblValidationStatus)
        Me.Panel3.Location = New System.Drawing.Point(891, 15)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(427, 119)
        Me.Panel3.TabIndex = 68
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(12, 89)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(96, 20)
        Me.Label16.TabIndex = 78
        Me.Label16.Text = "Last Calc:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(12, 64)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(116, 20)
        Me.Label11.TabIndex = 77
        Me.Label11.Text = "Last Upload:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(12, 39)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(103, 20)
        Me.Label7.TabIndex = 76
        Me.Label7.Text = "File Saved:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 14)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(129, 20)
        Me.Label3.TabIndex = 75
        Me.Label3.Text = "Review Status"
        '
        'lblItemsRemaining
        '
        Me.lblItemsRemaining.AutoSize = True
        Me.lblItemsRemaining.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItemsRemaining.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblItemsRemaining.Location = New System.Drawing.Point(413, 95)
        Me.lblItemsRemaining.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblItemsRemaining.Name = "lblItemsRemaining"
        Me.lblItemsRemaining.Size = New System.Drawing.Size(78, 20)
        Me.lblItemsRemaining.TabIndex = 74
        Me.lblItemsRemaining.Text = "10 Items "
        Me.lblItemsRemaining.Visible = False
        '
        'lblBlueFlags
        '
        Me.lblBlueFlags.AutoSize = True
        Me.lblBlueFlags.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBlueFlags.ForeColor = System.Drawing.Color.Navy
        Me.lblBlueFlags.Location = New System.Drawing.Point(413, 70)
        Me.lblBlueFlags.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblBlueFlags.Name = "lblBlueFlags"
        Me.lblBlueFlags.Size = New System.Drawing.Size(121, 20)
        Me.lblBlueFlags.TabIndex = 73
        Me.lblBlueFlags.Text = "100 Blue Flags"
        Me.lblBlueFlags.Visible = False
        '
        'lblRedFlags
        '
        Me.lblRedFlags.AutoSize = True
        Me.lblRedFlags.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRedFlags.ForeColor = System.Drawing.Color.Red
        Me.lblRedFlags.Location = New System.Drawing.Point(413, 46)
        Me.lblRedFlags.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblRedFlags.Name = "lblRedFlags"
        Me.lblRedFlags.Size = New System.Drawing.Size(108, 20)
        Me.lblRedFlags.TabIndex = 72
        Me.lblRedFlags.Text = "17 Red Flags"
        Me.lblRedFlags.Visible = False
        '
        'lblLastCalcDate
        '
        Me.lblLastCalcDate.AutoSize = True
        Me.lblLastCalcDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastCalcDate.Location = New System.Drawing.Point(163, 90)
        Me.lblLastCalcDate.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblLastCalcDate.Name = "lblLastCalcDate"
        Me.lblLastCalcDate.Size = New System.Drawing.Size(163, 20)
        Me.lblLastCalcDate.TabIndex = 71
        Me.lblLastCalcDate.Text = "6/8/2010 3:51:08 PM"
        '
        'lblLastUpload
        '
        Me.lblLastUpload.AutoSize = True
        Me.lblLastUpload.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastUpload.Location = New System.Drawing.Point(163, 65)
        Me.lblLastUpload.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblLastUpload.Name = "lblLastUpload"
        Me.lblLastUpload.Size = New System.Drawing.Size(163, 20)
        Me.lblLastUpload.TabIndex = 70
        Me.lblLastUpload.Text = "6/8/2010 3:51:08 PM"
        '
        'lblLastFileSave
        '
        Me.lblLastFileSave.AutoSize = True
        Me.lblLastFileSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastFileSave.Location = New System.Drawing.Point(163, 41)
        Me.lblLastFileSave.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblLastFileSave.Name = "lblLastFileSave"
        Me.lblLastFileSave.Size = New System.Drawing.Size(163, 20)
        Me.lblLastFileSave.TabIndex = 69
        Me.lblLastFileSave.Text = "6/8/2010 3:51:08 PM"
        '
        'lblValidationStatus
        '
        Me.lblValidationStatus.AutoSize = True
        Me.lblValidationStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidationStatus.Location = New System.Drawing.Point(163, 15)
        Me.lblValidationStatus.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblValidationStatus.Name = "lblValidationStatus"
        Me.lblValidationStatus.Size = New System.Drawing.Size(85, 20)
        Me.lblValidationStatus.TabIndex = 68
        Me.lblValidationStatus.Text = "Reviewing"
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.Label19)
        Me.Panel4.Controls.Add(Me.btnFLValidation)
        Me.Panel4.Controls.Add(Me.btnValFax)
        Me.Panel4.Controls.Add(Me.btnJustLooking)
        Me.Panel4.Controls.Add(Me.btnValidate)
        Me.Panel4.Controls.Add(Me.btnCreateMissingPNFiles)
        Me.Panel4.Controls.Add(Me.btnSendPrelimPricing)
        Me.Panel4.Location = New System.Drawing.Point(891, 138)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(427, 114)
        Me.Panel4.TabIndex = 69
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(132, 36)
        Me.Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(23, 17)
        Me.Label19.TabIndex = 55
        Me.Label19.Text = "->"
        Me.Label19.Visible = False
        '
        'btnOpenCompanyCorresp
        '
        Me.btnOpenCompanyCorresp.Location = New System.Drawing.Point(288, 202)
        Me.btnOpenCompanyCorresp.Margin = New System.Windows.Forms.Padding(4)
        Me.btnOpenCompanyCorresp.Name = "btnOpenCompanyCorresp"
        Me.btnOpenCompanyCorresp.Size = New System.Drawing.Size(173, 28)
        Me.btnOpenCompanyCorresp.TabIndex = 88
        Me.btnOpenCompanyCorresp.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.btnDragTest)
        Me.Panel5.Controls.Add(Me.lblCompanyCorresp)
        Me.Panel5.Controls.Add(Me.Label2)
        Me.Panel5.Controls.Add(Me.btnCompanyPW)
        Me.Panel5.Controls.Add(Me.lblCoPW)
        Me.Panel5.Controls.Add(Me.lblRetPW)
        Me.Panel5.Controls.Add(Me.btnOpenCompanyCorresp)
        Me.Panel5.Controls.Add(Me.Label17)
        Me.Panel5.Controls.Add(Me.btnReturnPW)
        Me.Panel5.Controls.Add(Me.listViewRefnums)
        Me.Panel5.Controls.Add(Me.WebBrowser1)
        Me.Panel5.Controls.Add(Me.btnMain)
        Me.Panel5.Controls.Add(Me.Label1)
        Me.Panel5.Controls.Add(Me.Label14)
        Me.Panel5.Controls.Add(Me.cboSiteId)
        Me.Panel5.Controls.Add(Me.lblCompany)
        Me.Panel5.Controls.Add(Me.Label8)
        Me.Panel5.Controls.Add(Me.cboConsultant)
        Me.Panel5.Controls.Add(Me.lblWarning)
        Me.Panel5.Controls.Add(Me.Label60)
        Me.Panel5.Controls.Add(Me.Label59)
        Me.Panel5.Controls.Add(Me.cboStudy)
        Me.Help.SetHelpKeyword(Me.Panel5, "help\hs0001.htm")
        Me.Help.SetHelpNavigator(Me.Panel5, System.Windows.Forms.HelpNavigator.Topic)
        Me.Panel5.Location = New System.Drawing.Point(25, 15)
        Me.Panel5.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel5.Name = "Panel5"
        Me.Help.SetShowHelp(Me.Panel5, True)
        Me.Panel5.Size = New System.Drawing.Size(857, 237)
        Me.Panel5.TabIndex = 70
        '
        'btnDragTest
        '
        Me.btnDragTest.Location = New System.Drawing.Point(376, 14)
        Me.btnDragTest.Margin = New System.Windows.Forms.Padding(4)
        Me.btnDragTest.Name = "btnDragTest"
        Me.btnDragTest.Size = New System.Drawing.Size(184, 28)
        Me.btnDragTest.TabIndex = 93
        Me.btnDragTest.Text = "Go To Test Site"
        Me.btnDragTest.UseVisualStyleBackColor = True
        Me.btnDragTest.Visible = False
        '
        'lblCompanyCorresp
        '
        Me.lblCompanyCorresp.AutoSize = True
        Me.lblCompanyCorresp.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyCorresp.Location = New System.Drawing.Point(4, 204)
        Me.lblCompanyCorresp.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCompanyCorresp.Name = "lblCompanyCorresp"
        Me.lblCompanyCorresp.Size = New System.Drawing.Size(229, 20)
        Me.lblCompanyCorresp.TabIndex = 92
        Me.lblCompanyCorresp.Text = "Company Correspondence"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(4, 132)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 20)
        Me.Label2.TabIndex = 77
        Me.Label2.Text = "Company"
        '
        'lblCoPW
        '
        Me.lblCoPW.AutoSize = True
        Me.lblCoPW.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCoPW.Location = New System.Drawing.Point(104, 133)
        Me.lblCoPW.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCoPW.Name = "lblCoPW"
        Me.lblCoPW.Size = New System.Drawing.Size(91, 20)
        Me.lblCoPW.TabIndex = 90
        Me.lblCoPW.Text = "Password"
        '
        'lblRetPW
        '
        Me.lblRetPW.AutoSize = True
        Me.lblRetPW.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRetPW.Location = New System.Drawing.Point(123, 166)
        Me.lblRetPW.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblRetPW.Name = "lblRetPW"
        Me.lblRetPW.Size = New System.Drawing.Size(91, 20)
        Me.lblRetPW.TabIndex = 91
        Me.lblRetPW.Text = "Password"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(4, 166)
        Me.Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(102, 20)
        Me.Label17.TabIndex = 80
        Me.Label17.Text = "Return File"
        '
        'listViewRefnums
        '
        Me.listViewRefnums.Location = New System.Drawing.Point(589, 33)
        Me.listViewRefnums.Margin = New System.Windows.Forms.Padding(4)
        Me.listViewRefnums.Name = "listViewRefnums"
        Me.listViewRefnums.Size = New System.Drawing.Size(260, 195)
        Me.listViewRefnums.TabIndex = 0
        Me.listViewRefnums.UseCompatibleStateImageBehavior = False
        Me.listViewRefnums.View = System.Windows.Forms.View.List
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Location = New System.Drawing.Point(325, 6)
        Me.WebBrowser1.Margin = New System.Windows.Forms.Padding(4)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(27, 25)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(43, 28)
        Me.WebBrowser1.TabIndex = 87
        Me.WebBrowser1.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(251, 100)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 20)
        Me.Label1.TabIndex = 76
        Me.Label1.Text = "Main"
        Me.Label1.Visible = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(3, 165)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(0, 17)
        Me.Label14.TabIndex = 75
        '
        'cboSiteId
        '
        Me.cboSiteId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboSiteId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboSiteId.FormattingEnabled = True
        Me.cboSiteId.Location = New System.Drawing.Point(96, 58)
        Me.cboSiteId.Margin = New System.Windows.Forms.Padding(4)
        Me.cboSiteId.Name = "cboSiteId"
        Me.cboSiteId.Size = New System.Drawing.Size(464, 24)
        Me.cboSiteId.Sorted = True
        Me.cboSiteId.TabIndex = 74
        '
        'lblCompany
        '
        Me.lblCompany.AutoSize = True
        Me.lblCompany.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(4, 63)
        Me.lblCompany.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(77, 20)
        Me.lblCompany.TabIndex = 73
        Me.lblCompany.Text = "Site IDs"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Gainsboro
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(7, 95)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(51, 20)
        Me.Label8.TabIndex = 71
        Me.Label8.Text = "Curr."
        '
        'cboConsultant
        '
        Me.cboConsultant.FormattingEnabled = True
        Me.cboConsultant.Location = New System.Drawing.Point(96, 98)
        Me.cboConsultant.Margin = New System.Windows.Forms.Padding(4)
        Me.cboConsultant.MaxLength = 3
        Me.cboConsultant.Name = "cboConsultant"
        Me.cboConsultant.Size = New System.Drawing.Size(85, 24)
        Me.cboConsultant.TabIndex = 70
        '
        'lblWarning
        '
        Me.lblWarning.AutoSize = True
        Me.lblWarning.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWarning.ForeColor = System.Drawing.Color.Red
        Me.lblWarning.Location = New System.Drawing.Point(100, 5)
        Me.lblWarning.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(138, 17)
        Me.lblWarning.TabIndex = 69
        Me.lblWarning.Text = "Not Current Study"
        Me.lblWarning.Visible = False
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.Location = New System.Drawing.Point(585, 12)
        Me.Label60.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(86, 20)
        Me.Label60.TabIndex = 68
        Me.Label60.Text = "RefNums"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(4, 23)
        Me.Label59.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(56, 20)
        Me.Label59.TabIndex = 66
        Me.Label59.Text = "Study"
        '
        'cboStudy
        '
        Me.cboStudy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStudy.FormattingEnabled = True
        Me.cboStudy.Location = New System.Drawing.Point(97, 22)
        Me.cboStudy.Margin = New System.Windows.Forms.Padding(4)
        Me.cboStudy.Name = "cboStudy"
        Me.cboStudy.Size = New System.Drawing.Size(183, 24)
        Me.cboStudy.TabIndex = 64
        '
        'Help
        '
        Me.Help.HelpNamespace = "help\Console2014.chm"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(20, 23)
        Me.Label46.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label46.Name = "Label46"
        Me.Help.SetShowHelp(Me.Label46, True)
        Me.Label46.Size = New System.Drawing.Size(146, 20)
        Me.Label46.TabIndex = 11
        Me.Label46.Text = "Presenter Notes"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(11, 10)
        Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label18.Name = "Label18"
        Me.Help.SetShowHelp(Me.Label18, True)
        Me.Label18.Size = New System.Drawing.Size(159, 20)
        Me.Label18.TabIndex = 31
        Me.Label18.Text = "Continuing Issues"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(11, 319)
        Me.Label29.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label29.Name = "Label29"
        Me.Help.SetShowHelp(Me.Label29, True)
        Me.Label29.Size = New System.Drawing.Size(60, 20)
        Me.Label29.TabIndex = 33
        Me.Label29.Text = "Issue:"
        Me.Label29.Visible = False
        '
        'tabQA
        '
        Me.tabQA.Controls.Add(Me.tv)
        Me.tabQA.Controls.Add(Me.chkUseRefnum)
        Me.tabQA.Controls.Add(Me.QASearch)
        Me.tabQA.Controls.Add(Me.Label22)
        Me.tabQA.Controls.Add(Me.lnkResultsPresentation)
        Me.tabQA.Controls.Add(Me.lnkPricing)
        Me.tabQA.Controls.Add(Me.lnkMisc)
        Me.tabQA.Controls.Add(Me.lnkProcessData)
        Me.tabQA.Controls.Add(Me.lnkOpex)
        Me.tabQA.Controls.Add(Me.lnkMaintenance)
        Me.tabQA.Controls.Add(Me.lnkPersonnel)
        Me.tabQA.Controls.Add(Me.lnkEnergy)
        Me.tabQA.Controls.Add(Me.lnkMaterialBalance)
        Me.tabQA.Controls.Add(Me.lnkStudyBoundary)
        Me.tabQA.Controls.Add(Me.lnkRefineryHistory)
        Me.tabQA.Location = New System.Drawing.Point(4, 25)
        Me.tabQA.Margin = New System.Windows.Forms.Padding(4)
        Me.tabQA.Name = "tabQA"
        Me.tabQA.Padding = New System.Windows.Forms.Padding(4)
        Me.tabQA.Size = New System.Drawing.Size(1299, 567)
        Me.tabQA.TabIndex = 14
        Me.tabQA.Text = "Q & A"
        Me.tabQA.UseVisualStyleBackColor = True
        '
        'tv
        '
        Me.tv.Location = New System.Drawing.Point(33, 114)
        Me.tv.Margin = New System.Windows.Forms.Padding(4)
        Me.tv.Name = "tv"
        Me.tv.Scrollable = False
        Me.tv.Size = New System.Drawing.Size(1217, 404)
        Me.tv.TabIndex = 86
        '
        'chkUseRefnum
        '
        Me.chkUseRefnum.AutoSize = True
        Me.chkUseRefnum.Location = New System.Drawing.Point(123, 59)
        Me.chkUseRefnum.Margin = New System.Windows.Forms.Padding(4)
        Me.chkUseRefnum.Name = "chkUseRefnum"
        Me.chkUseRefnum.Size = New System.Drawing.Size(108, 21)
        Me.chkUseRefnum.TabIndex = 85
        Me.chkUseRefnum.Text = "Use Refnum"
        Me.chkUseRefnum.UseVisualStyleBackColor = True
        '
        'QASearch
        '
        Me.QASearch.Location = New System.Drawing.Point(123, 26)
        Me.QASearch.Margin = New System.Windows.Forms.Padding(4)
        Me.QASearch.Name = "QASearch"
        Me.QASearch.Size = New System.Drawing.Size(299, 22)
        Me.QASearch.TabIndex = 83
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(29, 26)
        Me.Label22.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(74, 20)
        Me.Label22.TabIndex = 82
        Me.Label22.Text = "Search:"
        '
        'lnkResultsPresentation
        '
        Me.lnkResultsPresentation.AutoSize = True
        Me.lnkResultsPresentation.Location = New System.Drawing.Point(1040, 59)
        Me.lnkResultsPresentation.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lnkResultsPresentation.Name = "lnkResultsPresentation"
        Me.lnkResultsPresentation.Size = New System.Drawing.Size(139, 17)
        Me.lnkResultsPresentation.TabIndex = 10
        Me.lnkResultsPresentation.TabStop = True
        Me.lnkResultsPresentation.Tag = "Results Presentation"
        Me.lnkResultsPresentation.Text = "Results Presentation"
        '
        'lnkPricing
        '
        Me.lnkPricing.AutoSize = True
        Me.lnkPricing.Location = New System.Drawing.Point(920, 59)
        Me.lnkPricing.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lnkPricing.Name = "lnkPricing"
        Me.lnkPricing.Size = New System.Drawing.Size(51, 17)
        Me.lnkPricing.TabIndex = 9
        Me.lnkPricing.TabStop = True
        Me.lnkPricing.Tag = "Pricing"
        Me.lnkPricing.Text = "Pricing"
        '
        'lnkMisc
        '
        Me.lnkMisc.AutoSize = True
        Me.lnkMisc.Location = New System.Drawing.Point(759, 59)
        Me.lnkMisc.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lnkMisc.Name = "lnkMisc"
        Me.lnkMisc.Size = New System.Drawing.Size(97, 17)
        Me.lnkMisc.TabIndex = 8
        Me.lnkMisc.TabStop = True
        Me.lnkMisc.Tag = "Miscellaneous"
        Me.lnkMisc.Text = "Miscellaneous"
        '
        'lnkProcessData
        '
        Me.lnkProcessData.AutoSize = True
        Me.lnkProcessData.Location = New System.Drawing.Point(604, 59)
        Me.lnkProcessData.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lnkProcessData.Name = "lnkProcessData"
        Me.lnkProcessData.Size = New System.Drawing.Size(93, 17)
        Me.lnkProcessData.TabIndex = 7
        Me.lnkProcessData.TabStop = True
        Me.lnkProcessData.Tag = "Process Data"
        Me.lnkProcessData.Text = "Process Data"
        '
        'lnkOpex
        '
        Me.lnkOpex.AutoSize = True
        Me.lnkOpex.Location = New System.Drawing.Point(455, 59)
        Me.lnkOpex.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lnkOpex.Name = "lnkOpex"
        Me.lnkOpex.Size = New System.Drawing.Size(136, 17)
        Me.lnkOpex.TabIndex = 6
        Me.lnkOpex.TabStop = True
        Me.lnkOpex.Tag = "Operating Expenses"
        Me.lnkOpex.Text = "Operating Expenses"
        '
        'lnkMaintenance
        '
        Me.lnkMaintenance.AutoSize = True
        Me.lnkMaintenance.Location = New System.Drawing.Point(1160, 31)
        Me.lnkMaintenance.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lnkMaintenance.Name = "lnkMaintenance"
        Me.lnkMaintenance.Size = New System.Drawing.Size(89, 17)
        Me.lnkMaintenance.TabIndex = 5
        Me.lnkMaintenance.TabStop = True
        Me.lnkMaintenance.Tag = "Maintenance"
        Me.lnkMaintenance.Text = "Maintenance"
        '
        'lnkPersonnel
        '
        Me.lnkPersonnel.AutoSize = True
        Me.lnkPersonnel.Location = New System.Drawing.Point(1040, 28)
        Me.lnkPersonnel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lnkPersonnel.Name = "lnkPersonnel"
        Me.lnkPersonnel.Size = New System.Drawing.Size(72, 17)
        Me.lnkPersonnel.TabIndex = 4
        Me.lnkPersonnel.TabStop = True
        Me.lnkPersonnel.Tag = "Personnel"
        Me.lnkPersonnel.Text = "Personnel"
        '
        'lnkEnergy
        '
        Me.lnkEnergy.AutoSize = True
        Me.lnkEnergy.Location = New System.Drawing.Point(920, 28)
        Me.lnkEnergy.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lnkEnergy.Name = "lnkEnergy"
        Me.lnkEnergy.Size = New System.Drawing.Size(53, 17)
        Me.lnkEnergy.TabIndex = 3
        Me.lnkEnergy.TabStop = True
        Me.lnkEnergy.Tag = "Energy"
        Me.lnkEnergy.Text = "Energy"
        '
        'lnkMaterialBalance
        '
        Me.lnkMaterialBalance.AutoSize = True
        Me.lnkMaterialBalance.Location = New System.Drawing.Point(759, 28)
        Me.lnkMaterialBalance.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lnkMaterialBalance.Name = "lnkMaterialBalance"
        Me.lnkMaterialBalance.Size = New System.Drawing.Size(113, 17)
        Me.lnkMaterialBalance.TabIndex = 2
        Me.lnkMaterialBalance.TabStop = True
        Me.lnkMaterialBalance.Tag = "Material Balance"
        Me.lnkMaterialBalance.Text = "Material Balance"
        '
        'lnkStudyBoundary
        '
        Me.lnkStudyBoundary.AutoSize = True
        Me.lnkStudyBoundary.Location = New System.Drawing.Point(604, 28)
        Me.lnkStudyBoundary.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lnkStudyBoundary.Name = "lnkStudyBoundary"
        Me.lnkStudyBoundary.Size = New System.Drawing.Size(109, 17)
        Me.lnkStudyBoundary.TabIndex = 1
        Me.lnkStudyBoundary.TabStop = True
        Me.lnkStudyBoundary.Tag = "Study Boundary"
        Me.lnkStudyBoundary.Text = "Study Boundary"
        '
        'lnkRefineryHistory
        '
        Me.lnkRefineryHistory.AutoSize = True
        Me.lnkRefineryHistory.Location = New System.Drawing.Point(455, 28)
        Me.lnkRefineryHistory.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lnkRefineryHistory.Name = "lnkRefineryHistory"
        Me.lnkRefineryHistory.Size = New System.Drawing.Size(109, 17)
        Me.lnkRefineryHistory.TabIndex = 0
        Me.lnkRefineryHistory.TabStop = True
        Me.lnkRefineryHistory.Tag = "Refinery History"
        Me.lnkRefineryHistory.Text = "Refinery History"
        '
        'tabClippy
        '
        Me.tabClippy.Controls.Add(Me.btnQueryQuit)
        Me.tabClippy.Controls.Add(Me.lblError)
        Me.tabClippy.Controls.Add(Me.chkSQL)
        Me.tabClippy.Controls.Add(Me.dgClippyResults)
        Me.tabClippy.Controls.Add(Me.btnClippySearch)
        Me.tabClippy.Controls.Add(Me.txtClippySearch)
        Me.tabClippy.Controls.Add(Me.Label25)
        Me.tabClippy.Location = New System.Drawing.Point(4, 25)
        Me.tabClippy.Margin = New System.Windows.Forms.Padding(4)
        Me.tabClippy.Name = "tabClippy"
        Me.tabClippy.Size = New System.Drawing.Size(1299, 567)
        Me.tabClippy.TabIndex = 11
        Me.tabClippy.Text = "Query"
        Me.tabClippy.UseVisualStyleBackColor = True
        '
        'btnQueryQuit
        '
        Me.btnQueryQuit.Location = New System.Drawing.Point(1160, 533)
        Me.btnQueryQuit.Margin = New System.Windows.Forms.Padding(4)
        Me.btnQueryQuit.Name = "btnQueryQuit"
        Me.btnQueryQuit.Size = New System.Drawing.Size(92, 28)
        Me.btnQueryQuit.TabIndex = 46
        Me.btnQueryQuit.Text = "Exit"
        Me.btnQueryQuit.UseVisualStyleBackColor = True
        '
        'lblError
        '
        Me.lblError.AutoSize = True
        Me.lblError.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblError.ForeColor = System.Drawing.Color.DarkRed
        Me.lblError.Location = New System.Drawing.Point(0, 550)
        Me.lblError.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(0, 17)
        Me.lblError.TabIndex = 45
        '
        'chkSQL
        '
        Me.chkSQL.AutoSize = True
        Me.chkSQL.Checked = True
        Me.chkSQL.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSQL.Location = New System.Drawing.Point(148, 164)
        Me.chkSQL.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSQL.Name = "chkSQL"
        Me.chkSQL.Size = New System.Drawing.Size(58, 21)
        Me.chkSQL.TabIndex = 44
        Me.chkSQL.Text = "SQL"
        Me.chkSQL.UseVisualStyleBackColor = True
        '
        'dgClippyResults
        '
        Me.dgClippyResults.AllowUserToAddRows = False
        Me.dgClippyResults.AllowUserToDeleteRows = False
        Me.dgClippyResults.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgClippyResults.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgClippyResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgClippyResults.Location = New System.Drawing.Point(28, 194)
        Me.dgClippyResults.Margin = New System.Windows.Forms.Padding(4)
        Me.dgClippyResults.Name = "dgClippyResults"
        Me.dgClippyResults.ReadOnly = True
        Me.dgClippyResults.Size = New System.Drawing.Size(1224, 337)
        Me.dgClippyResults.TabIndex = 43
        '
        'btnClippySearch
        '
        Me.btnClippySearch.Location = New System.Drawing.Point(28, 159)
        Me.btnClippySearch.Margin = New System.Windows.Forms.Padding(4)
        Me.btnClippySearch.Name = "btnClippySearch"
        Me.btnClippySearch.Size = New System.Drawing.Size(92, 28)
        Me.btnClippySearch.TabIndex = 42
        Me.btnClippySearch.Text = "Search"
        Me.btnClippySearch.UseVisualStyleBackColor = True
        '
        'txtClippySearch
        '
        Me.txtClippySearch.Location = New System.Drawing.Point(28, 42)
        Me.txtClippySearch.Margin = New System.Windows.Forms.Padding(4)
        Me.txtClippySearch.Multiline = True
        Me.txtClippySearch.Name = "txtClippySearch"
        Me.txtClippySearch.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtClippySearch.Size = New System.Drawing.Size(1223, 109)
        Me.txtClippySearch.TabIndex = 39
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(11, 14)
        Me.Label25.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(98, 20)
        Me.Label25.TabIndex = 38
        Me.Label25.Text = "SQL Query:"
        '
        'tabDD
        '
        Me.tabDD.BackColor = System.Drawing.Color.AliceBlue
        Me.tabDD.Controls.Add(Me.btnDDExit)
        Me.tabDD.Controls.Add(Me.optCompanyCorr)
        Me.tabDD.Controls.Add(Me.txtMessageFilename)
        Me.tabDD.Controls.Add(Me.lblMessage)
        Me.tabDD.Controls.Add(Me.btnGVClear)
        Me.tabDD.Controls.Add(Me.btnFilesSave)
        Me.tabDD.Controls.Add(Me.dgFiles)
        Me.tabDD.Location = New System.Drawing.Point(4, 25)
        Me.tabDD.Margin = New System.Windows.Forms.Padding(4)
        Me.tabDD.Name = "tabDD"
        Me.tabDD.Padding = New System.Windows.Forms.Padding(4)
        Me.tabDD.Size = New System.Drawing.Size(1299, 567)
        Me.tabDD.TabIndex = 8
        Me.tabDD.Text = "Drag and Drop"
        '
        'btnDDExit
        '
        Me.btnDDExit.Location = New System.Drawing.Point(913, 527)
        Me.btnDDExit.Margin = New System.Windows.Forms.Padding(4)
        Me.btnDDExit.Name = "btnDDExit"
        Me.btnDDExit.Size = New System.Drawing.Size(107, 28)
        Me.btnDDExit.TabIndex = 51
        Me.btnDDExit.Text = "Exit"
        Me.btnDDExit.UseVisualStyleBackColor = True
        Me.btnDDExit.Visible = False
        '
        'optCompanyCorr
        '
        Me.optCompanyCorr.AutoSize = True
        Me.optCompanyCorr.Location = New System.Drawing.Point(1036, 39)
        Me.optCompanyCorr.Margin = New System.Windows.Forms.Padding(4)
        Me.optCompanyCorr.Name = "optCompanyCorr"
        Me.optCompanyCorr.Size = New System.Drawing.Size(250, 21)
        Me.optCompanyCorr.TabIndex = 38
        Me.optCompanyCorr.Text = "Email is Company Correspondence"
        Me.optCompanyCorr.UseVisualStyleBackColor = True
        '
        'txtMessageFilename
        '
        Me.txtMessageFilename.Location = New System.Drawing.Point(279, 36)
        Me.txtMessageFilename.Margin = New System.Windows.Forms.Padding(4)
        Me.txtMessageFilename.Name = "txtMessageFilename"
        Me.txtMessageFilename.Size = New System.Drawing.Size(741, 22)
        Me.txtMessageFilename.TabIndex = 37
        '
        'lblMessage
        '
        Me.lblMessage.AutoSize = True
        Me.lblMessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(27, 36)
        Me.lblMessage.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(173, 20)
        Me.lblMessage.TabIndex = 36
        Me.lblMessage.Text = "Filename for Email:"
        '
        'btnGVClear
        '
        Me.btnGVClear.Location = New System.Drawing.Point(1039, 528)
        Me.btnGVClear.Margin = New System.Windows.Forms.Padding(4)
        Me.btnGVClear.Name = "btnGVClear"
        Me.btnGVClear.Size = New System.Drawing.Size(100, 28)
        Me.btnGVClear.TabIndex = 2
        Me.btnGVClear.Text = "Clear"
        Me.btnGVClear.UseVisualStyleBackColor = True
        '
        'btnFilesSave
        '
        Me.btnFilesSave.Location = New System.Drawing.Point(1163, 528)
        Me.btnFilesSave.Margin = New System.Windows.Forms.Padding(4)
        Me.btnFilesSave.Name = "btnFilesSave"
        Me.btnFilesSave.Size = New System.Drawing.Size(100, 28)
        Me.btnFilesSave.TabIndex = 1
        Me.btnFilesSave.Text = "Save"
        Me.btnFilesSave.UseVisualStyleBackColor = True
        '
        'dgFiles
        '
        Me.dgFiles.AllowUserToAddRows = False
        Me.dgFiles.AllowUserToDeleteRows = False
        Me.dgFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgFiles.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.OriginalFilename, Me.Filenames, Me.FileDestination})
        Me.dgFiles.Location = New System.Drawing.Point(31, 78)
        Me.dgFiles.Margin = New System.Windows.Forms.Padding(4)
        Me.dgFiles.Name = "dgFiles"
        Me.dgFiles.Size = New System.Drawing.Size(1232, 442)
        Me.dgFiles.TabIndex = 0
        '
        'OriginalFilename
        '
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.OriginalFilename.DefaultCellStyle = DataGridViewCellStyle9
        Me.OriginalFilename.HeaderText = "Original Filename"
        Me.OriginalFilename.Name = "OriginalFilename"
        Me.OriginalFilename.ReadOnly = True
        Me.OriginalFilename.Width = 300
        '
        'Filenames
        '
        Me.Filenames.HeaderText = """Save As"" Filename"
        Me.Filenames.Name = "Filenames"
        Me.Filenames.Width = 300
        '
        'FileDestination
        '
        Me.FileDestination.DropDownWidth = 200
        Me.FileDestination.HeaderText = "Save Attachment Location"
        Me.FileDestination.Name = "FileDestination"
        Me.FileDestination.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.FileDestination.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.FileDestination.Width = 275
        '
        'tabTimeGrade
        '
        Me.tabTimeGrade.BackColor = System.Drawing.Color.AliceBlue
        Me.tabTimeGrade.Controls.Add(Me.Label15)
        Me.tabTimeGrade.Controls.Add(Me.Label10)
        Me.tabTimeGrade.Controls.Add(Me.dgHistory)
        Me.tabTimeGrade.Controls.Add(Me.dgSummary)
        Me.tabTimeGrade.Controls.Add(Me.btnGradeExit)
        Me.tabTimeGrade.Controls.Add(Me.btnSection)
        Me.tabTimeGrade.Controls.Add(Me.Label9)
        Me.tabTimeGrade.Controls.Add(Me.dgGrade)
        Me.tabTimeGrade.Location = New System.Drawing.Point(4, 25)
        Me.tabTimeGrade.Margin = New System.Windows.Forms.Padding(4)
        Me.tabTimeGrade.Name = "tabTimeGrade"
        Me.tabTimeGrade.Padding = New System.Windows.Forms.Padding(4)
        Me.tabTimeGrade.Size = New System.Drawing.Size(1299, 567)
        Me.tabTimeGrade.TabIndex = 5
        Me.tabTimeGrade.Text = "Time/Grade"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(656, 294)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(70, 20)
        Me.Label15.TabIndex = 54
        Me.Label15.Text = "History"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(27, 294)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(87, 20)
        Me.Label10.TabIndex = 53
        Me.Label10.Text = "Summary"
        '
        'dgHistory
        '
        Me.dgHistory.AllowUserToAddRows = False
        Me.dgHistory.AllowUserToDeleteRows = False
        Me.dgHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgHistory.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgHistory.Location = New System.Drawing.Point(652, 319)
        Me.dgHistory.Margin = New System.Windows.Forms.Padding(4)
        Me.dgHistory.Name = "dgHistory"
        Me.dgHistory.ReadOnly = True
        Me.dgHistory.Size = New System.Drawing.Size(613, 209)
        Me.dgHistory.TabIndex = 52
        '
        'dgSummary
        '
        Me.dgSummary.AllowUserToAddRows = False
        Me.dgSummary.AllowUserToDeleteRows = False
        Me.dgSummary.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgSummary.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgSummary.Location = New System.Drawing.Point(31, 319)
        Me.dgSummary.Margin = New System.Windows.Forms.Padding(4)
        Me.dgSummary.Name = "dgSummary"
        Me.dgSummary.ReadOnly = True
        Me.dgSummary.Size = New System.Drawing.Size(613, 209)
        Me.dgSummary.TabIndex = 51
        '
        'btnGradeExit
        '
        Me.btnGradeExit.Location = New System.Drawing.Point(1153, 530)
        Me.btnGradeExit.Margin = New System.Windows.Forms.Padding(4)
        Me.btnGradeExit.Name = "btnGradeExit"
        Me.btnGradeExit.Size = New System.Drawing.Size(112, 28)
        Me.btnGradeExit.TabIndex = 50
        Me.btnGradeExit.Text = "Exit"
        Me.btnGradeExit.UseVisualStyleBackColor = True
        '
        'btnSection
        '
        Me.btnSection.Location = New System.Drawing.Point(31, 7)
        Me.btnSection.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSection.Name = "btnSection"
        Me.btnSection.Size = New System.Drawing.Size(112, 28)
        Me.btnSection.TabIndex = 39
        Me.btnSection.Text = "Section"
        Me.btnSection.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(563, 11)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(75, 20)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = "Grading"
        '
        'dgGrade
        '
        Me.dgGrade.AllowUserToAddRows = False
        Me.dgGrade.AllowUserToDeleteRows = False
        Me.dgGrade.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgGrade.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgGrade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgGrade.Location = New System.Drawing.Point(31, 39)
        Me.dgGrade.Margin = New System.Windows.Forms.Padding(4)
        Me.dgGrade.Name = "dgGrade"
        Me.dgGrade.ReadOnly = True
        Me.dgGrade.Size = New System.Drawing.Size(1235, 246)
        Me.dgGrade.TabIndex = 0
        '
        'tabSS
        '
        Me.tabSS.BackColor = System.Drawing.Color.AliceBlue
        Me.tabSS.Controls.Add(Me.chkIDR)
        Me.tabSS.Controls.Add(Me.btnSecureSendRefresh)
        Me.tabSS.Controls.Add(Me.btnDirRefresh)
        Me.tabSS.Controls.Add(Me.btnSecureSend)
        Me.tabSS.Controls.Add(Me.tvwCompCorr2)
        Me.tabSS.Controls.Add(Me.tvwCompCorr)
        Me.tabSS.Controls.Add(Me.tvwCorrespondence2)
        Me.tabSS.Controls.Add(Me.tvwDrawings2)
        Me.tabSS.Controls.Add(Me.tvwClientAttachments2)
        Me.tabSS.Controls.Add(Me.lblSecureSendDirectory2)
        Me.tabSS.Controls.Add(Me.cboDir2)
        Me.tabSS.Controls.Add(Me.tvwClientAttachments)
        Me.tabSS.Controls.Add(Me.Label12)
        Me.tabSS.Controls.Add(Me.cboDir)
        Me.tabSS.Controls.Add(Me.tvwDrawings)
        Me.tabSS.Controls.Add(Me.tvwCorrespondence)
        Me.tabSS.Location = New System.Drawing.Point(4, 25)
        Me.tabSS.Margin = New System.Windows.Forms.Padding(4)
        Me.tabSS.Name = "tabSS"
        Me.tabSS.Padding = New System.Windows.Forms.Padding(4)
        Me.tabSS.Size = New System.Drawing.Size(1299, 567)
        Me.tabSS.TabIndex = 7
        Me.tabSS.Text = "Secure Send"
        '
        'chkIDR
        '
        Me.chkIDR.AutoSize = True
        Me.chkIDR.Checked = True
        Me.chkIDR.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIDR.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIDR.Location = New System.Drawing.Point(1008, 12)
        Me.chkIDR.Margin = New System.Windows.Forms.Padding(4)
        Me.chkIDR.Name = "chkIDR"
        Me.chkIDR.Size = New System.Drawing.Size(158, 21)
        Me.chkIDR.TabIndex = 53
        Me.chkIDR.Text = "Add IDR Instructions"
        Me.chkIDR.UseVisualStyleBackColor = True
        '
        'btnSecureSendRefresh
        '
        Me.btnSecureSendRefresh.Location = New System.Drawing.Point(920, 528)
        Me.btnSecureSendRefresh.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSecureSendRefresh.Name = "btnSecureSendRefresh"
        Me.btnSecureSendRefresh.Size = New System.Drawing.Size(100, 28)
        Me.btnSecureSendRefresh.TabIndex = 54
        Me.btnSecureSendRefresh.Text = "Refresh"
        Me.btnSecureSendRefresh.UseVisualStyleBackColor = True
        '
        'btnDirRefresh
        '
        Me.btnDirRefresh.Location = New System.Drawing.Point(1155, 527)
        Me.btnDirRefresh.Margin = New System.Windows.Forms.Padding(4)
        Me.btnDirRefresh.Name = "btnDirRefresh"
        Me.btnDirRefresh.Size = New System.Drawing.Size(112, 28)
        Me.btnDirRefresh.TabIndex = 50
        Me.btnDirRefresh.Text = "Exit"
        Me.btnDirRefresh.UseVisualStyleBackColor = True
        Me.btnDirRefresh.Visible = False
        '
        'btnSecureSend
        '
        Me.btnSecureSend.Location = New System.Drawing.Point(1035, 527)
        Me.btnSecureSend.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSecureSend.Name = "btnSecureSend"
        Me.btnSecureSend.Size = New System.Drawing.Size(112, 28)
        Me.btnSecureSend.TabIndex = 49
        Me.btnSecureSend.Text = "Continue"
        Me.btnSecureSend.UseVisualStyleBackColor = True
        '
        'tvwCompCorr2
        '
        Me.tvwCompCorr2.AllowDrop = True
        Me.tvwCompCorr2.CheckBoxes = True
        Me.tvwCompCorr2.ImageIndex = 0
        Me.tvwCompCorr2.ImageList = Me.ImageList1
        Me.tvwCompCorr2.Location = New System.Drawing.Point(652, 43)
        Me.tvwCompCorr2.Margin = New System.Windows.Forms.Padding(4)
        Me.tvwCompCorr2.Name = "tvwCompCorr2"
        Me.tvwCompCorr2.SelectedImageIndex = 0
        Me.tvwCompCorr2.Size = New System.Drawing.Size(613, 474)
        Me.tvwCompCorr2.TabIndex = 48
        Me.tvwCompCorr2.Visible = False
        '
        'tvwCompCorr
        '
        Me.tvwCompCorr.AllowDrop = True
        Me.tvwCompCorr.CheckBoxes = True
        Me.tvwCompCorr.ImageIndex = 0
        Me.tvwCompCorr.ImageList = Me.ImageList1
        Me.tvwCompCorr.Location = New System.Drawing.Point(23, 43)
        Me.tvwCompCorr.Margin = New System.Windows.Forms.Padding(4)
        Me.tvwCompCorr.Name = "tvwCompCorr"
        Me.tvwCompCorr.SelectedImageIndex = 0
        Me.tvwCompCorr.Size = New System.Drawing.Size(620, 474)
        Me.tvwCompCorr.TabIndex = 47
        Me.tvwCompCorr.Visible = False
        '
        'tvwCorrespondence2
        '
        Me.tvwCorrespondence2.AllowDrop = True
        Me.tvwCorrespondence2.CheckBoxes = True
        Me.tvwCorrespondence2.ImageIndex = 0
        Me.tvwCorrespondence2.ImageList = Me.ImageList1
        Me.tvwCorrespondence2.Location = New System.Drawing.Point(652, 44)
        Me.tvwCorrespondence2.Margin = New System.Windows.Forms.Padding(4)
        Me.tvwCorrespondence2.Name = "tvwCorrespondence2"
        Me.tvwCorrespondence2.SelectedImageIndex = 0
        Me.tvwCorrespondence2.Size = New System.Drawing.Size(613, 473)
        Me.tvwCorrespondence2.TabIndex = 46
        '
        'tvwDrawings2
        '
        Me.tvwDrawings2.AllowDrop = True
        Me.tvwDrawings2.CheckBoxes = True
        Me.tvwDrawings2.ImageIndex = 0
        Me.tvwDrawings2.ImageList = Me.ImageList1
        Me.tvwDrawings2.Location = New System.Drawing.Point(652, 43)
        Me.tvwDrawings2.Margin = New System.Windows.Forms.Padding(4)
        Me.tvwDrawings2.Name = "tvwDrawings2"
        Me.tvwDrawings2.SelectedImageIndex = 0
        Me.tvwDrawings2.Size = New System.Drawing.Size(613, 474)
        Me.tvwDrawings2.TabIndex = 45
        Me.tvwDrawings2.Visible = False
        '
        'tvwClientAttachments2
        '
        Me.tvwClientAttachments2.AllowDrop = True
        Me.tvwClientAttachments2.CheckBoxes = True
        Me.tvwClientAttachments2.ImageIndex = 0
        Me.tvwClientAttachments2.ImageList = Me.ImageList1
        Me.tvwClientAttachments2.Location = New System.Drawing.Point(652, 43)
        Me.tvwClientAttachments2.Margin = New System.Windows.Forms.Padding(4)
        Me.tvwClientAttachments2.Name = "tvwClientAttachments2"
        Me.tvwClientAttachments2.SelectedImageIndex = 0
        Me.tvwClientAttachments2.Size = New System.Drawing.Size(613, 474)
        Me.tvwClientAttachments2.TabIndex = 43
        Me.tvwClientAttachments2.Visible = False
        '
        'lblSecureSendDirectory2
        '
        Me.lblSecureSendDirectory2.AutoSize = True
        Me.lblSecureSendDirectory2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSecureSendDirectory2.Location = New System.Drawing.Point(655, 10)
        Me.lblSecureSendDirectory2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblSecureSendDirectory2.Name = "lblSecureSendDirectory2"
        Me.lblSecureSendDirectory2.Size = New System.Drawing.Size(93, 20)
        Me.lblSecureSendDirectory2.TabIndex = 42
        Me.lblSecureSendDirectory2.Text = "Directory:"
        '
        'cboDir2
        '
        Me.cboDir2.FormattingEnabled = True
        Me.cboDir2.Location = New System.Drawing.Point(770, 8)
        Me.cboDir2.Margin = New System.Windows.Forms.Padding(4)
        Me.cboDir2.Name = "cboDir2"
        Me.cboDir2.Size = New System.Drawing.Size(217, 24)
        Me.cboDir2.TabIndex = 41
        '
        'tvwClientAttachments
        '
        Me.tvwClientAttachments.AllowDrop = True
        Me.tvwClientAttachments.CheckBoxes = True
        Me.tvwClientAttachments.ImageIndex = 0
        Me.tvwClientAttachments.ImageList = Me.ImageList1
        Me.tvwClientAttachments.Location = New System.Drawing.Point(23, 43)
        Me.tvwClientAttachments.Margin = New System.Windows.Forms.Padding(4)
        Me.tvwClientAttachments.Name = "tvwClientAttachments"
        Me.tvwClientAttachments.SelectedImageIndex = 0
        Me.tvwClientAttachments.Size = New System.Drawing.Size(620, 473)
        Me.tvwClientAttachments.TabIndex = 40
        Me.tvwClientAttachments.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(23, 10)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(93, 20)
        Me.Label12.TabIndex = 39
        Me.Label12.Text = "Directory:"
        '
        'cboDir
        '
        Me.cboDir.FormattingEnabled = True
        Me.cboDir.Location = New System.Drawing.Point(133, 9)
        Me.cboDir.Margin = New System.Windows.Forms.Padding(4)
        Me.cboDir.Name = "cboDir"
        Me.cboDir.Size = New System.Drawing.Size(217, 24)
        Me.cboDir.TabIndex = 38
        '
        'tvwDrawings
        '
        Me.tvwDrawings.AllowDrop = True
        Me.tvwDrawings.CheckBoxes = True
        Me.tvwDrawings.ImageIndex = 0
        Me.tvwDrawings.ImageList = Me.ImageList1
        Me.tvwDrawings.Location = New System.Drawing.Point(23, 42)
        Me.tvwDrawings.Margin = New System.Windows.Forms.Padding(4)
        Me.tvwDrawings.Name = "tvwDrawings"
        Me.tvwDrawings.SelectedImageIndex = 0
        Me.tvwDrawings.Size = New System.Drawing.Size(620, 474)
        Me.tvwDrawings.TabIndex = 36
        Me.tvwDrawings.Visible = False
        '
        'tvwCorrespondence
        '
        Me.tvwCorrespondence.AllowDrop = True
        Me.tvwCorrespondence.CheckBoxes = True
        Me.tvwCorrespondence.ImageIndex = 0
        Me.tvwCorrespondence.ImageList = Me.ImageList1
        Me.tvwCorrespondence.Location = New System.Drawing.Point(23, 43)
        Me.tvwCorrespondence.Margin = New System.Windows.Forms.Padding(4)
        Me.tvwCorrespondence.Name = "tvwCorrespondence"
        Me.tvwCorrespondence.SelectedImageIndex = 0
        Me.tvwCorrespondence.Size = New System.Drawing.Size(620, 473)
        Me.tvwCorrespondence.TabIndex = 0
        '
        'tabCI
        '
        Me.tabCI.Controls.Add(Me.btnIssueCancel)
        Me.tabCI.Controls.Add(Me.Label18)
        Me.tabCI.Controls.Add(Me.chkNoDates)
        Me.tabCI.Controls.Add(Me.btnExportCI)
        Me.tabCI.Controls.Add(Me.btnNewIssue)
        Me.tabCI.Controls.Add(Me.btnSaveCI)
        Me.tabCI.Controls.Add(Me.dgContinuingIssues)
        Me.tabCI.Controls.Add(Me.txtCI)
        Me.tabCI.Controls.Add(Me.Label29)
        Me.tabCI.Location = New System.Drawing.Point(4, 25)
        Me.tabCI.Margin = New System.Windows.Forms.Padding(4)
        Me.tabCI.Name = "tabCI"
        Me.tabCI.Padding = New System.Windows.Forms.Padding(4)
        Me.tabCI.Size = New System.Drawing.Size(1299, 567)
        Me.tabCI.TabIndex = 13
        Me.tabCI.Text = "Continuing Issues"
        Me.tabCI.UseVisualStyleBackColor = True
        '
        'btnIssueCancel
        '
        Me.btnIssueCancel.Location = New System.Drawing.Point(1016, 521)
        Me.btnIssueCancel.Margin = New System.Windows.Forms.Padding(1)
        Me.btnIssueCancel.Name = "btnIssueCancel"
        Me.btnIssueCancel.Size = New System.Drawing.Size(132, 34)
        Me.btnIssueCancel.TabIndex = 32
        Me.btnIssueCancel.Text = "Cancel"
        Me.btnIssueCancel.UseVisualStyleBackColor = True
        '
        'chkNoDates
        '
        Me.chkNoDates.AutoSize = True
        Me.chkNoDates.Location = New System.Drawing.Point(159, 529)
        Me.chkNoDates.Margin = New System.Windows.Forms.Padding(4)
        Me.chkNoDates.Name = "chkNoDates"
        Me.chkNoDates.Size = New System.Drawing.Size(89, 21)
        Me.chkNoDates.TabIndex = 30
        Me.chkNoDates.Text = "No Dates"
        Me.chkNoDates.UseVisualStyleBackColor = True
        '
        'btnExportCI
        '
        Me.btnExportCI.Enabled = False
        Me.btnExportCI.Location = New System.Drawing.Point(13, 521)
        Me.btnExportCI.Margin = New System.Windows.Forms.Padding(1)
        Me.btnExportCI.Name = "btnExportCI"
        Me.btnExportCI.Size = New System.Drawing.Size(120, 34)
        Me.btnExportCI.TabIndex = 29
        Me.btnExportCI.Text = "Export to Text"
        Me.btnExportCI.UseVisualStyleBackColor = True
        '
        'btnNewIssue
        '
        Me.btnNewIssue.Location = New System.Drawing.Point(851, 521)
        Me.btnNewIssue.Margin = New System.Windows.Forms.Padding(1)
        Me.btnNewIssue.Name = "btnNewIssue"
        Me.btnNewIssue.Size = New System.Drawing.Size(127, 34)
        Me.btnNewIssue.TabIndex = 28
        Me.btnNewIssue.Text = "New Issue"
        Me.btnNewIssue.UseVisualStyleBackColor = True
        '
        'btnSaveCI
        '
        Me.btnSaveCI.Enabled = False
        Me.btnSaveCI.Location = New System.Drawing.Point(1163, 521)
        Me.btnSaveCI.Margin = New System.Windows.Forms.Padding(1)
        Me.btnSaveCI.Name = "btnSaveCI"
        Me.btnSaveCI.Size = New System.Drawing.Size(116, 34)
        Me.btnSaveCI.TabIndex = 14
        Me.btnSaveCI.Text = "Save Issue"
        Me.btnSaveCI.UseVisualStyleBackColor = True
        '
        'dgContinuingIssues
        '
        Me.dgContinuingIssues.AllowUserToAddRows = False
        Me.dgContinuingIssues.AllowUserToDeleteRows = False
        Me.dgContinuingIssues.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgContinuingIssues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgContinuingIssues.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.EntryDate, Me.Issue, Me.Consultant, Me.DeletedBy, Me.DeletedDate, Me.Deleted})
        Me.dgContinuingIssues.Location = New System.Drawing.Point(0, 34)
        Me.dgContinuingIssues.Margin = New System.Windows.Forms.Padding(4)
        Me.dgContinuingIssues.Name = "dgContinuingIssues"
        Me.dgContinuingIssues.ReadOnly = True
        Me.dgContinuingIssues.RowHeadersVisible = False
        Me.dgContinuingIssues.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgContinuingIssues.Size = New System.Drawing.Size(1267, 473)
        Me.dgContinuingIssues.TabIndex = 0
        '
        'ID
        '
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        Me.ID.Visible = False
        '
        'EntryDate
        '
        Me.EntryDate.HeaderText = "Entry Date"
        Me.EntryDate.Name = "EntryDate"
        Me.EntryDate.ReadOnly = True
        '
        'Issue
        '
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Issue.DefaultCellStyle = DataGridViewCellStyle10
        Me.Issue.HeaderText = "Issues"
        Me.Issue.Name = "Issue"
        Me.Issue.ReadOnly = True
        Me.Issue.Width = 700
        '
        'Consultant
        '
        Me.Consultant.HeaderText = "Consultant"
        Me.Consultant.Name = "Consultant"
        Me.Consultant.ReadOnly = True
        '
        'DeletedBy
        '
        Me.DeletedBy.HeaderText = "DeletedBy"
        Me.DeletedBy.Name = "DeletedBy"
        Me.DeletedBy.ReadOnly = True
        Me.DeletedBy.Visible = False
        '
        'DeletedDate
        '
        Me.DeletedDate.HeaderText = "DeletedDate"
        Me.DeletedDate.Name = "DeletedDate"
        Me.DeletedDate.ReadOnly = True
        '
        'Deleted
        '
        Me.Deleted.HeaderText = "Deleted"
        Me.Deleted.Name = "Deleted"
        Me.Deleted.ReadOnly = True
        '
        'txtCI
        '
        Me.txtCI.Location = New System.Drawing.Point(1, 337)
        Me.txtCI.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCI.Multiline = True
        Me.txtCI.Name = "txtCI"
        Me.txtCI.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCI.Size = New System.Drawing.Size(1263, 168)
        Me.txtCI.TabIndex = 24
        '
        'tabNotes
        '
        Me.tabNotes.BackColor = System.Drawing.Color.AliceBlue
        Me.tabNotes.Controls.Add(Me.txtConsultingOpportunities)
        Me.tabNotes.Controls.Add(Me.lblConsultingOpportunities)
        Me.tabNotes.Controls.Add(Me.btnUnlockPN)
        Me.tabNotes.Controls.Add(Me.btnCreatePN)
        Me.tabNotes.Controls.Add(Me.btnSavePN)
        Me.tabNotes.Controls.Add(Me.txtNotes)
        Me.tabNotes.Controls.Add(Me.Label46)
        Me.tabNotes.Location = New System.Drawing.Point(4, 25)
        Me.tabNotes.Margin = New System.Windows.Forms.Padding(4)
        Me.tabNotes.Name = "tabNotes"
        Me.tabNotes.Padding = New System.Windows.Forms.Padding(4)
        Me.tabNotes.Size = New System.Drawing.Size(1299, 567)
        Me.tabNotes.TabIndex = 2
        Me.tabNotes.Text = "Notes"
        '
        'txtConsultingOpportunities
        '
        Me.txtConsultingOpportunities.Location = New System.Drawing.Point(20, 322)
        Me.txtConsultingOpportunities.Margin = New System.Windows.Forms.Padding(4)
        Me.txtConsultingOpportunities.Multiline = True
        Me.txtConsultingOpportunities.Name = "txtConsultingOpportunities"
        Me.txtConsultingOpportunities.Size = New System.Drawing.Size(1241, 173)
        Me.txtConsultingOpportunities.TabIndex = 36
        '
        'lblConsultingOpportunities
        '
        Me.lblConsultingOpportunities.AutoSize = True
        Me.lblConsultingOpportunities.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblConsultingOpportunities.Location = New System.Drawing.Point(24, 278)
        Me.lblConsultingOpportunities.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblConsultingOpportunities.Name = "lblConsultingOpportunities"
        Me.lblConsultingOpportunities.Size = New System.Drawing.Size(217, 20)
        Me.lblConsultingOpportunities.TabIndex = 35
        Me.lblConsultingOpportunities.Text = "Consulting Opportunities"
        '
        'btnUnlockPN
        '
        Me.btnUnlockPN.Location = New System.Drawing.Point(1144, 20)
        Me.btnUnlockPN.Margin = New System.Windows.Forms.Padding(1)
        Me.btnUnlockPN.Name = "btnUnlockPN"
        Me.btnUnlockPN.Size = New System.Drawing.Size(119, 34)
        Me.btnUnlockPN.TabIndex = 34
        Me.btnUnlockPN.Text = "Unlock PN File"
        Me.btnUnlockPN.UseVisualStyleBackColor = True
        Me.btnUnlockPN.Visible = False
        '
        'btnCreatePN
        '
        Me.btnCreatePN.Location = New System.Drawing.Point(1013, 20)
        Me.btnCreatePN.Margin = New System.Windows.Forms.Padding(1)
        Me.btnCreatePN.Name = "btnCreatePN"
        Me.btnCreatePN.Size = New System.Drawing.Size(119, 34)
        Me.btnCreatePN.TabIndex = 33
        Me.btnCreatePN.Text = "Create PN File"
        Me.btnCreatePN.UseVisualStyleBackColor = True
        '
        'btnSavePN
        '
        Me.btnSavePN.Location = New System.Drawing.Point(1167, 516)
        Me.btnSavePN.Margin = New System.Windows.Forms.Padding(1)
        Me.btnSavePN.Name = "btnSavePN"
        Me.btnSavePN.Size = New System.Drawing.Size(99, 34)
        Me.btnSavePN.TabIndex = 13
        Me.btnSavePN.Text = "Save"
        Me.btnSavePN.UseVisualStyleBackColor = True
        '
        'txtNotes
        '
        Me.txtNotes.Location = New System.Drawing.Point(20, 63)
        Me.txtNotes.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNotes.Multiline = True
        Me.txtNotes.Name = "txtNotes"
        Me.txtNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtNotes.Size = New System.Drawing.Size(1241, 192)
        Me.txtNotes.TabIndex = 12
        '
        'tabCheckList
        '
        Me.tabCheckList.BackColor = System.Drawing.Color.AliceBlue
        Me.tabCheckList.Controls.Add(Me.GroupBox1)
        Me.tabCheckList.Controls.Add(Me.cboCheckListView)
        Me.tabCheckList.Controls.Add(Me.lblCheckboxView)
        Me.tabCheckList.Controls.Add(Me.tvIssues)
        Me.tabCheckList.Controls.Add(Me.txtIssueName)
        Me.tabCheckList.Controls.Add(Me.txtIssueID)
        Me.tabCheckList.Controls.Add(Me.txtDescription)
        Me.tabCheckList.Controls.Add(Me.lblItemCount)
        Me.tabCheckList.Controls.Add(Me.btnAddIssue)
        Me.tabCheckList.Controls.Add(Me.Label49)
        Me.tabCheckList.Controls.Add(Me.Label48)
        Me.tabCheckList.Controls.Add(Me.Label47)
        Me.tabCheckList.Controls.Add(Me.ValCheckList)
        Me.tabCheckList.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabCheckList.Location = New System.Drawing.Point(4, 25)
        Me.tabCheckList.Margin = New System.Windows.Forms.Padding(4)
        Me.tabCheckList.Name = "tabCheckList"
        Me.tabCheckList.Padding = New System.Windows.Forms.Padding(4)
        Me.tabCheckList.Size = New System.Drawing.Size(1299, 567)
        Me.tabCheckList.TabIndex = 3
        Me.tabCheckList.Text = "Checklist"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.radChecklistEditIssue)
        Me.GroupBox1.Controls.Add(Me.radChecklistAddNew)
        Me.GroupBox1.Controls.Add(Me.btnUpdateIssue)
        Me.GroupBox1.Location = New System.Drawing.Point(583, 4)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(405, 85)
        Me.GroupBox1.TabIndex = 36
        Me.GroupBox1.TabStop = False
        '
        'radChecklistEditIssue
        '
        Me.radChecklistEditIssue.AutoSize = True
        Me.radChecklistEditIssue.Location = New System.Drawing.Point(12, 50)
        Me.radChecklistEditIssue.Margin = New System.Windows.Forms.Padding(4)
        Me.radChecklistEditIssue.Name = "radChecklistEditIssue"
        Me.radChecklistEditIssue.Size = New System.Drawing.Size(160, 24)
        Me.radChecklistEditIssue.TabIndex = 28
        Me.radChecklistEditIssue.TabStop = True
        Me.radChecklistEditIssue.Text = "View/Edit Issue"
        Me.radChecklistEditIssue.UseVisualStyleBackColor = True
        '
        'radChecklistAddNew
        '
        Me.radChecklistAddNew.AutoSize = True
        Me.radChecklistAddNew.Location = New System.Drawing.Point(12, 17)
        Me.radChecklistAddNew.Margin = New System.Windows.Forms.Padding(4)
        Me.radChecklistAddNew.Name = "radChecklistAddNew"
        Me.radChecklistAddNew.Size = New System.Drawing.Size(155, 24)
        Me.radChecklistAddNew.TabIndex = 27
        Me.radChecklistAddNew.TabStop = True
        Me.radChecklistAddNew.Text = "Add New Issue"
        Me.radChecklistAddNew.UseVisualStyleBackColor = True
        '
        'btnUpdateIssue
        '
        Me.btnUpdateIssue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdateIssue.Location = New System.Drawing.Point(236, 32)
        Me.btnUpdateIssue.Margin = New System.Windows.Forms.Padding(4)
        Me.btnUpdateIssue.Name = "btnUpdateIssue"
        Me.btnUpdateIssue.Size = New System.Drawing.Size(120, 28)
        Me.btnUpdateIssue.TabIndex = 26
        Me.btnUpdateIssue.Text = "Update"
        Me.btnUpdateIssue.UseVisualStyleBackColor = True
        '
        'cboCheckListView
        '
        Me.cboCheckListView.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCheckListView.FormattingEnabled = True
        Me.cboCheckListView.Items.AddRange(New Object() {"All", "Complete", "Incomplete"})
        Me.cboCheckListView.Location = New System.Drawing.Point(97, 10)
        Me.cboCheckListView.Margin = New System.Windows.Forms.Padding(4)
        Me.cboCheckListView.Name = "cboCheckListView"
        Me.cboCheckListView.Size = New System.Drawing.Size(196, 28)
        Me.cboCheckListView.TabIndex = 35
        '
        'lblCheckboxView
        '
        Me.lblCheckboxView.AutoSize = True
        Me.lblCheckboxView.Location = New System.Drawing.Point(13, 16)
        Me.lblCheckboxView.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCheckboxView.Name = "lblCheckboxView"
        Me.lblCheckboxView.Size = New System.Drawing.Size(49, 20)
        Me.lblCheckboxView.TabIndex = 34
        Me.lblCheckboxView.Text = "View"
        '
        'tvIssues
        '
        Me.tvIssues.CheckBoxes = True
        Me.tvIssues.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tvIssues.Location = New System.Drawing.Point(4, 47)
        Me.tvIssues.Margin = New System.Windows.Forms.Padding(4)
        Me.tvIssues.Name = "tvIssues"
        Me.tvIssues.ShowLines = False
        Me.tvIssues.ShowPlusMinus = False
        Me.tvIssues.ShowRootLines = False
        Me.tvIssues.Size = New System.Drawing.Size(531, 504)
        Me.tvIssues.TabIndex = 33
        '
        'txtIssueName
        '
        Me.txtIssueName.Location = New System.Drawing.Point(845, 119)
        Me.txtIssueName.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIssueName.MaxLength = 50
        Me.txtIssueName.Name = "txtIssueName"
        Me.txtIssueName.Size = New System.Drawing.Size(427, 26)
        Me.txtIssueName.TabIndex = 28
        '
        'txtIssueID
        '
        Me.txtIssueID.Location = New System.Drawing.Point(583, 119)
        Me.txtIssueID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIssueID.MaxLength = 8
        Me.txtIssueID.Name = "txtIssueID"
        Me.txtIssueID.Size = New System.Drawing.Size(253, 26)
        Me.txtIssueID.TabIndex = 27
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(577, 177)
        Me.txtDescription.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(695, 373)
        Me.txtDescription.TabIndex = 31
        '
        'lblItemCount
        '
        Me.lblItemCount.AutoSize = True
        Me.lblItemCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItemCount.Location = New System.Drawing.Point(412, 17)
        Me.lblItemCount.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblItemCount.Name = "lblItemCount"
        Me.lblItemCount.Size = New System.Drawing.Size(108, 20)
        Me.lblItemCount.TabIndex = 26
        Me.lblItemCount.Text = "Issue Name"
        Me.lblItemCount.Visible = False
        '
        'btnAddIssue
        '
        Me.btnAddIssue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddIssue.Location = New System.Drawing.Point(1116, 10)
        Me.btnAddIssue.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAddIssue.Name = "btnAddIssue"
        Me.btnAddIssue.Size = New System.Drawing.Size(157, 28)
        Me.btnAddIssue.TabIndex = 25
        Me.btnAddIssue.Text = "Add New Issue"
        Me.btnAddIssue.UseVisualStyleBackColor = True
        Me.btnAddIssue.Visible = False
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.Location = New System.Drawing.Point(841, 97)
        Me.Label49.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(108, 20)
        Me.Label49.TabIndex = 16
        Me.Label49.Text = "Issue Name"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.Location = New System.Drawing.Point(579, 100)
        Me.Label48.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(79, 20)
        Me.Label48.TabIndex = 15
        Me.Label48.Text = "Issue ID"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(579, 154)
        Me.Label47.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(106, 20)
        Me.Label47.TabIndex = 14
        Me.Label47.Text = "Description"
        '
        'ValCheckList
        '
        Me.ValCheckList.FormattingEnabled = True
        Me.ValCheckList.HorizontalScrollbar = True
        Me.ValCheckList.Location = New System.Drawing.Point(4, 66)
        Me.ValCheckList.Margin = New System.Windows.Forms.Padding(4)
        Me.ValCheckList.Name = "ValCheckList"
        Me.ValCheckList.Size = New System.Drawing.Size(531, 445)
        Me.ValCheckList.TabIndex = 0
        '
        'tabCorr
        '
        Me.tabCorr.BackColor = System.Drawing.Color.AliceBlue
        Me.tabCorr.Controls.Add(Me.btnManageGadsData)
        Me.tabCorr.Controls.Add(Me.btnCorrespRefresh)
        Me.tabCorr.Controls.Add(Me.btnDataFolder)
        Me.tabCorr.Controls.Add(Me.listViewCorrespondenceVReturn)
        Me.tabCorr.Controls.Add(Me.listViewCorrespondenceVR)
        Me.tabCorr.Controls.Add(Me.listViewCorrespondenceVF)
        Me.tabCorr.Controls.Add(Me.btnPolishRpt)
        Me.tabCorr.Controls.Add(Me.btnStartReliabilityFile)
        Me.tabCorr.Controls.Add(Me.btnVI)
        Me.tabCorr.Controls.Add(Me.btnSC)
        Me.tabCorr.Controls.Add(Me.btnPA)
        Me.tabCorr.Controls.Add(Me.btnCT)
        Me.tabCorr.Controls.Add(Me.btnMasterEventsFile)
        Me.tabCorr.Controls.Add(Me.btnDrawings)
        Me.tabCorr.Controls.Add(Me.btnValidatedInputFile)
        Me.tabCorr.Controls.Add(Me.btnPT)
        Me.tabCorr.Controls.Add(Me.btnSpecFrac)
        Me.tabCorr.Controls.Add(Me.btnUnitReview)
        Me.tabCorr.Controls.Add(Me.btnBuildVRFile)
        Me.tabCorr.Controls.Add(Me.btnReceiptAck)
        Me.tabCorr.Controls.Add(Me.lstReturnFiles)
        Me.tabCorr.Controls.Add(Me.Label58)
        Me.tabCorr.Controls.Add(Me.lstVRFiles)
        Me.tabCorr.Controls.Add(Me.Label57)
        Me.tabCorr.Controls.Add(Me.lstVFFiles)
        Me.tabCorr.Controls.Add(Me.Label56)
        Me.tabCorr.Controls.Add(Me.lstVFNumbers)
        Me.tabCorr.Controls.Add(Me.Label55)
        Me.tabCorr.Location = New System.Drawing.Point(4, 25)
        Me.tabCorr.Margin = New System.Windows.Forms.Padding(4)
        Me.tabCorr.Name = "tabCorr"
        Me.tabCorr.Padding = New System.Windows.Forms.Padding(4)
        Me.tabCorr.Size = New System.Drawing.Size(1299, 567)
        Me.tabCorr.TabIndex = 4
        Me.tabCorr.Text = "IDR Correspondence"
        '
        'btnManageGadsData
        '
        Me.btnManageGadsData.Location = New System.Drawing.Point(1128, 453)
        Me.btnManageGadsData.Margin = New System.Windows.Forms.Padding(4)
        Me.btnManageGadsData.Name = "btnManageGadsData"
        Me.btnManageGadsData.Size = New System.Drawing.Size(147, 31)
        Me.btnManageGadsData.TabIndex = 112
        Me.btnManageGadsData.Text = "Manage Gads Data"
        Me.btnManageGadsData.UseVisualStyleBackColor = True
        '
        'btnCorrespRefresh
        '
        Me.btnCorrespRefresh.Location = New System.Drawing.Point(1127, 2)
        Me.btnCorrespRefresh.Margin = New System.Windows.Forms.Padding(4)
        Me.btnCorrespRefresh.Name = "btnCorrespRefresh"
        Me.btnCorrespRefresh.Size = New System.Drawing.Size(147, 31)
        Me.btnCorrespRefresh.TabIndex = 111
        Me.btnCorrespRefresh.Text = "Refresh"
        Me.btnCorrespRefresh.UseVisualStyleBackColor = True
        '
        'btnDataFolder
        '
        Me.btnDataFolder.Location = New System.Drawing.Point(1128, 526)
        Me.btnDataFolder.Margin = New System.Windows.Forms.Padding(4)
        Me.btnDataFolder.Name = "btnDataFolder"
        Me.btnDataFolder.Size = New System.Drawing.Size(147, 31)
        Me.btnDataFolder.TabIndex = 110
        Me.btnDataFolder.Text = "Data Folder"
        Me.btnDataFolder.UseVisualStyleBackColor = True
        '
        'listViewCorrespondenceVReturn
        '
        Me.listViewCorrespondenceVReturn.Location = New System.Drawing.Point(85, 438)
        Me.listViewCorrespondenceVReturn.Margin = New System.Windows.Forms.Padding(4)
        Me.listViewCorrespondenceVReturn.Name = "listViewCorrespondenceVReturn"
        Me.listViewCorrespondenceVReturn.Size = New System.Drawing.Size(995, 123)
        Me.listViewCorrespondenceVReturn.TabIndex = 109
        Me.listViewCorrespondenceVReturn.UseCompatibleStateImageBehavior = False
        '
        'listViewCorrespondenceVR
        '
        Me.listViewCorrespondenceVR.Location = New System.Drawing.Point(85, 268)
        Me.listViewCorrespondenceVR.Margin = New System.Windows.Forms.Padding(4)
        Me.listViewCorrespondenceVR.Name = "listViewCorrespondenceVR"
        Me.listViewCorrespondenceVR.Size = New System.Drawing.Size(995, 154)
        Me.listViewCorrespondenceVR.TabIndex = 108
        Me.listViewCorrespondenceVR.UseCompatibleStateImageBehavior = False
        '
        'listViewCorrespondenceVF
        '
        Me.listViewCorrespondenceVF.Location = New System.Drawing.Point(83, 9)
        Me.listViewCorrespondenceVF.Margin = New System.Windows.Forms.Padding(4)
        Me.listViewCorrespondenceVF.Name = "listViewCorrespondenceVF"
        Me.listViewCorrespondenceVF.Size = New System.Drawing.Size(997, 202)
        Me.listViewCorrespondenceVF.TabIndex = 107
        Me.listViewCorrespondenceVF.UseCompatibleStateImageBehavior = False
        Me.listViewCorrespondenceVF.Visible = False
        '
        'btnPolishRpt
        '
        Me.btnPolishRpt.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnPolishRpt.Enabled = False
        Me.btnPolishRpt.Location = New System.Drawing.Point(1128, 346)
        Me.btnPolishRpt.Margin = New System.Windows.Forms.Padding(1)
        Me.btnPolishRpt.Name = "btnPolishRpt"
        Me.btnPolishRpt.Size = New System.Drawing.Size(147, 31)
        Me.btnPolishRpt.TabIndex = 106
        Me.btnPolishRpt.Text = "Polish Report"
        Me.btnPolishRpt.UseVisualStyleBackColor = True
        '
        'btnStartReliabilityFile
        '
        Me.btnStartReliabilityFile.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnStartReliabilityFile.Enabled = False
        Me.btnStartReliabilityFile.Location = New System.Drawing.Point(1127, 73)
        Me.btnStartReliabilityFile.Margin = New System.Windows.Forms.Padding(1)
        Me.btnStartReliabilityFile.Name = "btnStartReliabilityFile"
        Me.btnStartReliabilityFile.Size = New System.Drawing.Size(147, 31)
        Me.btnStartReliabilityFile.TabIndex = 100
        Me.btnStartReliabilityFile.Text = "Start Reliability File"
        Me.btnStartReliabilityFile.UseVisualStyleBackColor = True
        Me.btnStartReliabilityFile.Visible = False
        '
        'btnVI
        '
        Me.btnVI.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnVI.Enabled = False
        Me.btnVI.Location = New System.Drawing.Point(1128, 491)
        Me.btnVI.Margin = New System.Windows.Forms.Padding(1)
        Me.btnVI.Name = "btnVI"
        Me.btnVI.Size = New System.Drawing.Size(147, 31)
        Me.btnVI.TabIndex = 101
        Me.btnVI.Text = "Vetted Input (VI)"
        Me.btnVI.UseVisualStyleBackColor = True
        '
        'btnSC
        '
        Me.btnSC.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnSC.Enabled = False
        Me.btnSC.Location = New System.Drawing.Point(1128, 201)
        Me.btnSC.Margin = New System.Windows.Forms.Padding(1)
        Me.btnSC.Name = "btnSC"
        Me.btnSC.Size = New System.Drawing.Size(147, 31)
        Me.btnSC.TabIndex = 98
        Me.btnSC.Text = "Sum Calc (SC)"
        Me.btnSC.UseVisualStyleBackColor = True
        '
        'btnPA
        '
        Me.btnPA.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnPA.Enabled = False
        Me.btnPA.Location = New System.Drawing.Point(1128, 416)
        Me.btnPA.Margin = New System.Windows.Forms.Padding(1)
        Me.btnPA.Name = "btnPA"
        Me.btnPA.Size = New System.Drawing.Size(147, 31)
        Me.btnPA.TabIndex = 99
        Me.btnPA.Text = "Gap File (PA)"
        Me.btnPA.UseVisualStyleBackColor = True
        '
        'btnCT
        '
        Me.btnCT.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnCT.Enabled = False
        Me.btnCT.Location = New System.Drawing.Point(1128, 380)
        Me.btnCT.Margin = New System.Windows.Forms.Padding(1)
        Me.btnCT.Name = "btnCT"
        Me.btnCT.Size = New System.Drawing.Size(147, 31)
        Me.btnCT.TabIndex = 97
        Me.btnCT.Text = "Client Table (CT)"
        Me.btnCT.UseVisualStyleBackColor = True
        '
        'btnMasterEventsFile
        '
        Me.btnMasterEventsFile.Location = New System.Drawing.Point(1128, 37)
        Me.btnMasterEventsFile.Margin = New System.Windows.Forms.Padding(4)
        Me.btnMasterEventsFile.Name = "btnMasterEventsFile"
        Me.btnMasterEventsFile.Size = New System.Drawing.Size(147, 31)
        Me.btnMasterEventsFile.TabIndex = 95
        Me.btnMasterEventsFile.Text = "Master Events File"
        Me.btnMasterEventsFile.UseVisualStyleBackColor = True
        '
        'btnPT
        '
        Me.btnPT.Enabled = False
        Me.btnPT.Location = New System.Drawing.Point(1128, 239)
        Me.btnPT.Margin = New System.Windows.Forms.Padding(1)
        Me.btnPT.Name = "btnPT"
        Me.btnPT.Size = New System.Drawing.Size(147, 31)
        Me.btnPT.TabIndex = 96
        Me.btnPT.Text = "Prelim Client (PT)"
        Me.btnPT.UseVisualStyleBackColor = True
        '
        'btnSpecFrac
        '
        Me.btnSpecFrac.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnSpecFrac.Enabled = False
        Me.btnSpecFrac.Location = New System.Drawing.Point(1127, 139)
        Me.btnSpecFrac.Margin = New System.Windows.Forms.Padding(1)
        Me.btnSpecFrac.Name = "btnSpecFrac"
        Me.btnSpecFrac.Size = New System.Drawing.Size(147, 31)
        Me.btnSpecFrac.TabIndex = 103
        Me.btnSpecFrac.Text = "Spec Frac"
        Me.btnSpecFrac.UseVisualStyleBackColor = True
        Me.btnSpecFrac.Visible = False
        '
        'btnUnitReview
        '
        Me.btnUnitReview.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnUnitReview.Enabled = False
        Me.btnUnitReview.Location = New System.Drawing.Point(1128, 276)
        Me.btnUnitReview.Margin = New System.Windows.Forms.Padding(1)
        Me.btnUnitReview.Name = "btnUnitReview"
        Me.btnUnitReview.Size = New System.Drawing.Size(147, 31)
        Me.btnUnitReview.TabIndex = 102
        Me.btnUnitReview.Text = "Unit Review"
        Me.btnUnitReview.UseVisualStyleBackColor = True
        '
        'lstReturnFiles
        '
        Me.lstReturnFiles.ColumnWidth = 300
        Me.lstReturnFiles.FormattingEnabled = True
        Me.lstReturnFiles.ItemHeight = 16
        Me.lstReturnFiles.Items.AddRange(New Object() {"File Name 1.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/1/2011", "File Name 2.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/2/2011", "Really really long file name that should never be used.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/3/2011"})
        Me.lstReturnFiles.Location = New System.Drawing.Point(85, 462)
        Me.lstReturnFiles.Margin = New System.Windows.Forms.Padding(4)
        Me.lstReturnFiles.Name = "lstReturnFiles"
        Me.lstReturnFiles.Size = New System.Drawing.Size(995, 100)
        Me.lstReturnFiles.TabIndex = 7
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label58.Location = New System.Drawing.Point(83, 438)
        Me.Label58.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(96, 17)
        Me.Label58.TabIndex = 6
        Me.Label58.Text = "Return Files"
        '
        'lstVRFiles
        '
        Me.lstVRFiles.ColumnWidth = 300
        Me.lstVRFiles.FormattingEnabled = True
        Me.lstVRFiles.ItemHeight = 16
        Me.lstVRFiles.Items.AddRange(New Object() {"File Name 1.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/1/2011", "File Name 2.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/2/2011", "Really really long file name that should never be used.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/3/2011"})
        Me.lstVRFiles.Location = New System.Drawing.Point(85, 290)
        Me.lstVRFiles.Margin = New System.Windows.Forms.Padding(4)
        Me.lstVRFiles.Name = "lstVRFiles"
        Me.lstVRFiles.Size = New System.Drawing.Size(995, 132)
        Me.lstVRFiles.TabIndex = 5
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label57.Location = New System.Drawing.Point(83, 266)
        Me.Label57.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(195, 17)
        Me.Label57.TabIndex = 4
        Me.Label57.Text = "Vetted Reply Files (VR*.*)"
        '
        'lstVFFiles
        '
        Me.lstVFFiles.ColumnWidth = 300
        Me.lstVFFiles.FormattingEnabled = True
        Me.lstVFFiles.ItemHeight = 16
        Me.lstVFFiles.Location = New System.Drawing.Point(83, 31)
        Me.lstVFFiles.Margin = New System.Windows.Forms.Padding(4)
        Me.lstVFFiles.Name = "lstVFFiles"
        Me.lstVFFiles.Size = New System.Drawing.Size(995, 180)
        Me.lstVFFiles.TabIndex = 3
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.Location = New System.Drawing.Point(79, 9)
        Me.Label56.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(177, 17)
        Me.Label56.TabIndex = 2
        Me.Label56.Text = "Vetted Fax Files (VF*.*)"
        '
        'lstVFNumbers
        '
        Me.lstVFNumbers.FormattingEnabled = True
        Me.lstVFNumbers.ItemHeight = 16
        Me.lstVFNumbers.Location = New System.Drawing.Point(16, 30)
        Me.lstVFNumbers.Margin = New System.Windows.Forms.Padding(4)
        Me.lstVFNumbers.Name = "lstVFNumbers"
        Me.lstVFNumbers.Size = New System.Drawing.Size(53, 532)
        Me.lstVFNumbers.Sorted = True
        Me.lstVFNumbers.TabIndex = 1
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.Location = New System.Drawing.Point(12, 9)
        Me.Label55.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(36, 17)
        Me.Label55.TabIndex = 0
        Me.Label55.Text = "VF#"
        '
        'tabContacts
        '
        Me.tabContacts.Controls.Add(Me.Panel2)
        Me.tabContacts.Controls.Add(Me.MainPanel)
        Me.tabContacts.Location = New System.Drawing.Point(4, 25)
        Me.tabContacts.Margin = New System.Windows.Forms.Padding(4)
        Me.tabContacts.Name = "tabContacts"
        Me.tabContacts.Padding = New System.Windows.Forms.Padding(4)
        Me.tabContacts.Size = New System.Drawing.Size(1299, 567)
        Me.tabContacts.TabIndex = 0
        Me.tabContacts.Text = "Contacts"
        Me.tabContacts.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.pnlCompany)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(4, 4)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1291, 193)
        Me.Panel2.TabIndex = 21
        '
        'pnlCompany
        '
        Me.pnlCompany.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.pnlCompany.AllowDrop = True
        Me.pnlCompany.BackColor = System.Drawing.Color.LightGreen
        Me.pnlCompany.Controls.Add(Me.btnGeneralCorrespondence)
        Me.pnlCompany.Controls.Add(Me.txtGCorr)
        Me.pnlCompany.Controls.Add(Me.cboPCCEmail)
        Me.pnlCompany.Controls.Add(Me.cboICEmail)
        Me.pnlCompany.Controls.Add(Me.cboACEmail2)
        Me.pnlCompany.Controls.Add(Me.lblPCCEmail)
        Me.pnlCompany.Controls.Add(Me.lblPCCName)
        Me.pnlCompany.Controls.Add(Me.cboCCEmail1)
        Me.pnlCompany.Controls.Add(Me.CCEmail1)
        Me.pnlCompany.Controls.Add(Me.lblInterimPhone)
        Me.pnlCompany.Controls.Add(Me.lblAltPhone)
        Me.pnlCompany.Controls.Add(Me.lblPhone)
        Me.pnlCompany.Controls.Add(Me.lblInterimEmail)
        Me.pnlCompany.Controls.Add(Me.lblAltEmail)
        Me.pnlCompany.Controls.Add(Me.lblEmail)
        Me.pnlCompany.Controls.Add(Me.lblInterimName)
        Me.pnlCompany.Controls.Add(Me.lblAltName)
        Me.pnlCompany.Controls.Add(Me.lblName)
        Me.pnlCompany.Controls.Add(Me.btnShowIntCo)
        Me.pnlCompany.Controls.Add(Me.btnShowAltCo)
        Me.pnlCompany.Controls.Add(Me.btnShow)
        Me.pnlCompany.Controls.Add(Me.Label6)
        Me.pnlCompany.Controls.Add(Me.Label5)
        Me.pnlCompany.Controls.Add(Me.Label4)
        Me.pnlCompany.Location = New System.Drawing.Point(5, 4)
        Me.pnlCompany.Margin = New System.Windows.Forms.Padding(4)
        Me.pnlCompany.Name = "pnlCompany"
        Me.pnlCompany.Size = New System.Drawing.Size(1260, 186)
        Me.pnlCompany.TabIndex = 0
        '
        'btnGeneralCorrespondence
        '
        Me.btnGeneralCorrespondence.Location = New System.Drawing.Point(8, 145)
        Me.btnGeneralCorrespondence.Margin = New System.Windows.Forms.Padding(4)
        Me.btnGeneralCorrespondence.Name = "btnGeneralCorrespondence"
        Me.btnGeneralCorrespondence.Size = New System.Drawing.Size(155, 28)
        Me.btnGeneralCorrespondence.TabIndex = 148
        Me.btnGeneralCorrespondence.Text = "General Corresp"
        Me.btnGeneralCorrespondence.UseVisualStyleBackColor = True
        '
        'txtGCorr
        '
        Me.txtGCorr.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtGCorr.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGCorr.Location = New System.Drawing.Point(1071, 149)
        Me.txtGCorr.Margin = New System.Windows.Forms.Padding(4)
        Me.txtGCorr.Name = "txtGCorr"
        Me.txtGCorr.ReadOnly = True
        Me.txtGCorr.Size = New System.Drawing.Size(185, 19)
        Me.txtGCorr.TabIndex = 147
        '
        'cboPCCEmail
        '
        Me.cboPCCEmail.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPCCEmail.FormattingEnabled = True
        Me.cboPCCEmail.Items.AddRange(New Object() {"To", "CC", "BCC"})
        Me.cboPCCEmail.Location = New System.Drawing.Point(968, 145)
        Me.cboPCCEmail.Margin = New System.Windows.Forms.Padding(4)
        Me.cboPCCEmail.Name = "cboPCCEmail"
        Me.cboPCCEmail.Size = New System.Drawing.Size(49, 24)
        Me.cboPCCEmail.TabIndex = 146
        '
        'cboICEmail
        '
        Me.cboICEmail.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboICEmail.FormattingEnabled = True
        Me.cboICEmail.Items.AddRange(New Object() {"To", "CC", "BCC"})
        Me.cboICEmail.Location = New System.Drawing.Point(965, 106)
        Me.cboICEmail.Margin = New System.Windows.Forms.Padding(4)
        Me.cboICEmail.Name = "cboICEmail"
        Me.cboICEmail.Size = New System.Drawing.Size(49, 24)
        Me.cboICEmail.TabIndex = 67
        '
        'cboACEmail2
        '
        Me.cboACEmail2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboACEmail2.FormattingEnabled = True
        Me.cboACEmail2.Items.AddRange(New Object() {"To", "CC", "BCC"})
        Me.cboACEmail2.Location = New System.Drawing.Point(965, 70)
        Me.cboACEmail2.Margin = New System.Windows.Forms.Padding(4)
        Me.cboACEmail2.Name = "cboACEmail2"
        Me.cboACEmail2.Size = New System.Drawing.Size(49, 24)
        Me.cboACEmail2.TabIndex = 66
        '
        'lblPCCEmail
        '
        Me.lblPCCEmail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblPCCEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPCCEmail.Location = New System.Drawing.Point(448, 149)
        Me.lblPCCEmail.Margin = New System.Windows.Forms.Padding(4)
        Me.lblPCCEmail.Name = "lblPCCEmail"
        Me.lblPCCEmail.ReadOnly = True
        Me.lblPCCEmail.Size = New System.Drawing.Size(499, 19)
        Me.lblPCCEmail.TabIndex = 135
        '
        'lblPCCName
        '
        Me.lblPCCName.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblPCCName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPCCName.Location = New System.Drawing.Point(188, 149)
        Me.lblPCCName.Margin = New System.Windows.Forms.Padding(4)
        Me.lblPCCName.Name = "lblPCCName"
        Me.lblPCCName.ReadOnly = True
        Me.lblPCCName.Size = New System.Drawing.Size(232, 19)
        Me.lblPCCName.TabIndex = 134
        '
        'cboCCEmail1
        '
        Me.cboCCEmail1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCCEmail1.FormattingEnabled = True
        Me.cboCCEmail1.Items.AddRange(New Object() {"To", "CC", "BCC"})
        Me.cboCCEmail1.Location = New System.Drawing.Point(964, 38)
        Me.cboCCEmail1.Margin = New System.Windows.Forms.Padding(4)
        Me.cboCCEmail1.Name = "cboCCEmail1"
        Me.cboCCEmail1.Size = New System.Drawing.Size(49, 24)
        Me.cboCCEmail1.TabIndex = 65
        '
        'CCEmail1
        '
        Me.CCEmail1.Image = CType(resources.GetObject("CCEmail1.Image"), System.Drawing.Image)
        Me.CCEmail1.Location = New System.Drawing.Point(979, 11)
        Me.CCEmail1.Margin = New System.Windows.Forms.Padding(4)
        Me.CCEmail1.Name = "CCEmail1"
        Me.CCEmail1.Size = New System.Drawing.Size(31, 26)
        Me.CCEmail1.TabIndex = 62
        Me.CCEmail1.TabStop = False
        '
        'lblInterimPhone
        '
        Me.lblInterimPhone.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblInterimPhone.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterimPhone.Location = New System.Drawing.Point(1069, 114)
        Me.lblInterimPhone.Margin = New System.Windows.Forms.Padding(4)
        Me.lblInterimPhone.Name = "lblInterimPhone"
        Me.lblInterimPhone.ReadOnly = True
        Me.lblInterimPhone.Size = New System.Drawing.Size(185, 19)
        Me.lblInterimPhone.TabIndex = 61
        '
        'lblAltPhone
        '
        Me.lblAltPhone.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblAltPhone.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAltPhone.Location = New System.Drawing.Point(1069, 79)
        Me.lblAltPhone.Margin = New System.Windows.Forms.Padding(4)
        Me.lblAltPhone.Name = "lblAltPhone"
        Me.lblAltPhone.ReadOnly = True
        Me.lblAltPhone.Size = New System.Drawing.Size(185, 19)
        Me.lblAltPhone.TabIndex = 60
        '
        'lblPhone
        '
        Me.lblPhone.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblPhone.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone.Location = New System.Drawing.Point(1069, 43)
        Me.lblPhone.Margin = New System.Windows.Forms.Padding(4)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.ReadOnly = True
        Me.lblPhone.Size = New System.Drawing.Size(185, 19)
        Me.lblPhone.TabIndex = 59
        '
        'lblInterimEmail
        '
        Me.lblInterimEmail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblInterimEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterimEmail.Location = New System.Drawing.Point(447, 112)
        Me.lblInterimEmail.Margin = New System.Windows.Forms.Padding(4)
        Me.lblInterimEmail.Name = "lblInterimEmail"
        Me.lblInterimEmail.ReadOnly = True
        Me.lblInterimEmail.Size = New System.Drawing.Size(500, 19)
        Me.lblInterimEmail.TabIndex = 58
        '
        'lblAltEmail
        '
        Me.lblAltEmail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblAltEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAltEmail.Location = New System.Drawing.Point(447, 76)
        Me.lblAltEmail.Margin = New System.Windows.Forms.Padding(4)
        Me.lblAltEmail.Name = "lblAltEmail"
        Me.lblAltEmail.ReadOnly = True
        Me.lblAltEmail.Size = New System.Drawing.Size(500, 19)
        Me.lblAltEmail.TabIndex = 57
        '
        'lblEmail
        '
        Me.lblEmail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(447, 41)
        Me.lblEmail.Margin = New System.Windows.Forms.Padding(4)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.ReadOnly = True
        Me.lblEmail.Size = New System.Drawing.Size(500, 19)
        Me.lblEmail.TabIndex = 56
        '
        'lblInterimName
        '
        Me.lblInterimName.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblInterimName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterimName.Location = New System.Drawing.Point(189, 112)
        Me.lblInterimName.Margin = New System.Windows.Forms.Padding(4)
        Me.lblInterimName.Name = "lblInterimName"
        Me.lblInterimName.ReadOnly = True
        Me.lblInterimName.Size = New System.Drawing.Size(232, 19)
        Me.lblInterimName.TabIndex = 55
        '
        'lblAltName
        '
        Me.lblAltName.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblAltName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAltName.Location = New System.Drawing.Point(189, 76)
        Me.lblAltName.Margin = New System.Windows.Forms.Padding(4)
        Me.lblAltName.Name = "lblAltName"
        Me.lblAltName.ReadOnly = True
        Me.lblAltName.Size = New System.Drawing.Size(232, 19)
        Me.lblAltName.TabIndex = 54
        '
        'lblName
        '
        Me.lblName.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(189, 41)
        Me.lblName.Margin = New System.Windows.Forms.Padding(4)
        Me.lblName.Name = "lblName"
        Me.lblName.ReadOnly = True
        Me.lblName.Size = New System.Drawing.Size(232, 19)
        Me.lblName.TabIndex = 53
        '
        'btnShowIntCo
        '
        Me.btnShowIntCo.Location = New System.Drawing.Point(8, 106)
        Me.btnShowIntCo.Margin = New System.Windows.Forms.Padding(4)
        Me.btnShowIntCo.Name = "btnShowIntCo"
        Me.btnShowIntCo.Size = New System.Drawing.Size(155, 28)
        Me.btnShowIntCo.TabIndex = 52
        Me.btnShowIntCo.Text = "Plant Contact"
        Me.btnShowIntCo.UseVisualStyleBackColor = True
        '
        'btnShowAltCo
        '
        Me.btnShowAltCo.Location = New System.Drawing.Point(8, 70)
        Me.btnShowAltCo.Margin = New System.Windows.Forms.Padding(4)
        Me.btnShowAltCo.Name = "btnShowAltCo"
        Me.btnShowAltCo.Size = New System.Drawing.Size(155, 28)
        Me.btnShowAltCo.TabIndex = 51
        Me.btnShowAltCo.Text = "Alternate Contact"
        Me.btnShowAltCo.UseVisualStyleBackColor = True
        '
        'btnShow
        '
        Me.btnShow.Location = New System.Drawing.Point(8, 34)
        Me.btnShow.Margin = New System.Windows.Forms.Padding(4)
        Me.btnShow.Name = "btnShow"
        Me.btnShow.Size = New System.Drawing.Size(155, 28)
        Me.btnShow.TabIndex = 50
        Me.btnShow.Text = "Company Coordinator"
        Me.btnShow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShow.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(1065, 15)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(85, 17)
        Me.Label6.TabIndex = 49
        Me.Label6.Text = "Telephone"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(443, 12)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(111, 17)
        Me.Label5.TabIndex = 48
        Me.Label5.Text = "Email Address"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(185, 12)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 17)
        Me.Label4.TabIndex = 47
        Me.Label4.Text = "Name"
        '
        'MainPanel
        '
        Me.MainPanel.BackColor = System.Drawing.Color.AliceBlue
        Me.MainPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.MainPanel.Controls.Add(Me.Panel1)
        Me.MainPanel.Controls.Add(Me.pnlRefinery)
        Me.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MainPanel.Location = New System.Drawing.Point(4, 4)
        Me.MainPanel.Margin = New System.Windows.Forms.Padding(4)
        Me.MainPanel.Name = "MainPanel"
        Me.MainPanel.Size = New System.Drawing.Size(1291, 559)
        Me.MainPanel.TabIndex = 20
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Panel1.Location = New System.Drawing.Point(9, 428)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1257, 127)
        Me.Panel1.TabIndex = 133
        Me.Panel1.Visible = False
        '
        'pnlRefinery
        '
        Me.pnlRefinery.BackColor = System.Drawing.Color.SkyBlue
        Me.pnlRefinery.Location = New System.Drawing.Point(8, 325)
        Me.pnlRefinery.Margin = New System.Windows.Forms.Padding(4)
        Me.pnlRefinery.Name = "pnlRefinery"
        Me.pnlRefinery.Size = New System.Drawing.Size(1257, 63)
        Me.pnlRefinery.TabIndex = 108
        Me.pnlRefinery.Visible = False
        '
        'ConsoleTabs
        '
        Me.ConsoleTabs.AllowDrop = True
        Me.ConsoleTabs.CausesValidation = False
        Me.ConsoleTabs.Controls.Add(Me.tabContacts)
        Me.ConsoleTabs.Controls.Add(Me.tabCorr)
        Me.ConsoleTabs.Controls.Add(Me.tabCheckList)
        Me.ConsoleTabs.Controls.Add(Me.tabNotes)
        Me.ConsoleTabs.Controls.Add(Me.tabCI)
        Me.ConsoleTabs.Controls.Add(Me.tabSS)
        Me.ConsoleTabs.Controls.Add(Me.tabTimeGrade)
        Me.ConsoleTabs.Controls.Add(Me.tabDD)
        Me.ConsoleTabs.Controls.Add(Me.tabClippy)
        Me.ConsoleTabs.Controls.Add(Me.tabQA)
        Me.ConsoleTabs.Location = New System.Drawing.Point(16, 260)
        Me.ConsoleTabs.Margin = New System.Windows.Forms.Padding(4)
        Me.ConsoleTabs.Name = "ConsoleTabs"
        Me.ConsoleTabs.SelectedIndex = 0
        Me.ConsoleTabs.Size = New System.Drawing.Size(1307, 596)
        Me.ConsoleTabs.TabIndex = 0
        '
        'PowerConsole
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.AliceBlue
        Me.ClientSize = New System.Drawing.Size(1339, 894)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.txtVersion)
        Me.Controls.Add(Me.ConsoleTabs)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.Name = "PowerConsole"
        Me.Text = "Power Console"
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.tabQA.ResumeLayout(False)
        Me.tabQA.PerformLayout()
        Me.tabClippy.ResumeLayout(False)
        Me.tabClippy.PerformLayout()
        CType(Me.dgClippyResults, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabDD.ResumeLayout(False)
        Me.tabDD.PerformLayout()
        CType(Me.dgFiles, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabTimeGrade.ResumeLayout(False)
        Me.tabTimeGrade.PerformLayout()
        CType(Me.dgHistory, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgSummary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgGrade, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSS.ResumeLayout(False)
        Me.tabSS.PerformLayout()
        Me.tabCI.ResumeLayout(False)
        Me.tabCI.PerformLayout()
        CType(Me.dgContinuingIssues, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabNotes.ResumeLayout(False)
        Me.tabNotes.PerformLayout()
        Me.tabCheckList.ResumeLayout(False)
        Me.tabCheckList.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.tabCorr.ResumeLayout(False)
        Me.tabCorr.PerformLayout()
        Me.tabContacts.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.pnlCompany.ResumeLayout(False)
        Me.pnlCompany.PerformLayout()
        CType(Me.CCEmail1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MainPanel.ResumeLayout(False)
        Me.ConsoleTabs.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnHelp As System.Windows.Forms.Button
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents txtVersion As System.Windows.Forms.TextBox
    Friend WithEvents VersionToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents btnKillProcesses As System.Windows.Forms.Button
    Friend WithEvents btnSendPrelimPricing As System.Windows.Forms.Button
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents btnCreateMissingPNFiles As System.Windows.Forms.Button
    Friend WithEvents btnBug As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblItemsRemaining As System.Windows.Forms.Label
    Friend WithEvents lblBlueFlags As System.Windows.Forms.Label
    Friend WithEvents lblRedFlags As System.Windows.Forms.Label
    Friend WithEvents lblLastCalcDate As System.Windows.Forms.Label
    Friend WithEvents lblLastUpload As System.Windows.Forms.Label
    Friend WithEvents lblLastFileSave As System.Windows.Forms.Label
    Friend WithEvents lblValidationStatus As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents btnValidate As System.Windows.Forms.Button
    Friend WithEvents btnJustLooking As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cboSiteId As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cboConsultant As System.Windows.Forms.ComboBox
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents cboStudy As System.Windows.Forms.ComboBox
    Friend WithEvents btnMain As System.Windows.Forms.Button
    Friend WithEvents btnCompanyPW As System.Windows.Forms.Button
    Friend WithEvents btnReturnPW As System.Windows.Forms.Button
    Friend WithEvents btnValFax As System.Windows.Forms.Button
    Friend WithEvents Help As System.Windows.Forms.HelpProvider
    Friend WithEvents btnFLValidation As System.Windows.Forms.Button
    Friend WithEvents tabQA As System.Windows.Forms.TabPage
    Friend WithEvents QASearch As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents lnkResultsPresentation As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkPricing As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkMisc As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkProcessData As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkOpex As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkMaintenance As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkPersonnel As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEnergy As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkMaterialBalance As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkStudyBoundary As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkRefineryHistory As System.Windows.Forms.LinkLabel
    Friend WithEvents tabClippy As System.Windows.Forms.TabPage
    Friend WithEvents btnQueryQuit As System.Windows.Forms.Button
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents chkSQL As System.Windows.Forms.CheckBox
    Friend WithEvents dgClippyResults As System.Windows.Forms.DataGridView
    Friend WithEvents btnClippySearch As System.Windows.Forms.Button
    Friend WithEvents txtClippySearch As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents tabDD As System.Windows.Forms.TabPage
    Friend WithEvents optCompanyCorr As System.Windows.Forms.CheckBox
    Friend WithEvents txtMessageFilename As System.Windows.Forms.TextBox
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents btnGVClear As System.Windows.Forms.Button
    Friend WithEvents btnFilesSave As System.Windows.Forms.Button
    Friend WithEvents dgFiles As System.Windows.Forms.DataGridView
    Friend WithEvents OriginalFilename As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Filenames As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FileDestination As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents tabTimeGrade As System.Windows.Forms.TabPage
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dgHistory As System.Windows.Forms.DataGridView
    Friend WithEvents dgSummary As System.Windows.Forms.DataGridView
    Friend WithEvents btnGradeExit As System.Windows.Forms.Button
    Friend WithEvents btnSection As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dgGrade As System.Windows.Forms.DataGridView
    Friend WithEvents tabSS As System.Windows.Forms.TabPage
    Friend WithEvents btnDirRefresh As System.Windows.Forms.Button
    Friend WithEvents btnSecureSend As System.Windows.Forms.Button
    Friend WithEvents tvwCompCorr2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwCompCorr As System.Windows.Forms.TreeView
    Friend WithEvents tvwCorrespondence2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwDrawings2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwClientAttachments2 As System.Windows.Forms.TreeView
    Friend WithEvents lblSecureSendDirectory2 As System.Windows.Forms.Label
    Friend WithEvents cboDir2 As System.Windows.Forms.ComboBox
    Friend WithEvents tvwClientAttachments As System.Windows.Forms.TreeView
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboDir As System.Windows.Forms.ComboBox
    Friend WithEvents tvwDrawings As System.Windows.Forms.TreeView
    Friend WithEvents tvwCorrespondence As System.Windows.Forms.TreeView
    Friend WithEvents tabCI As System.Windows.Forms.TabPage
    Friend WithEvents chkNoDates As System.Windows.Forms.CheckBox
    Friend WithEvents btnExportCI As System.Windows.Forms.Button
    Friend WithEvents btnNewIssue As System.Windows.Forms.Button
    Friend WithEvents txtCI As System.Windows.Forms.TextBox
    Friend WithEvents btnSaveCI As System.Windows.Forms.Button
    Friend WithEvents dgContinuingIssues As System.Windows.Forms.DataGridView
    Friend WithEvents tabNotes As System.Windows.Forms.TabPage
    Friend WithEvents btnUnlockPN As System.Windows.Forms.Button
    Friend WithEvents btnCreatePN As System.Windows.Forms.Button
    Friend WithEvents btnSavePN As System.Windows.Forms.Button
    Friend WithEvents txtNotes As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents tabCheckList As System.Windows.Forms.TabPage
    Friend WithEvents tvIssues As System.Windows.Forms.TreeView
    Friend WithEvents txtIssueName As System.Windows.Forms.TextBox
    Friend WithEvents txtIssueID As System.Windows.Forms.TextBox
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents btnUpdateIssue As System.Windows.Forms.Button
    Friend WithEvents lblItemCount As System.Windows.Forms.Label
    Friend WithEvents btnAddIssue As System.Windows.Forms.Button
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents ValCheckList As System.Windows.Forms.CheckedListBox
    Friend WithEvents tabCorr As System.Windows.Forms.TabPage
    Friend WithEvents btnPolishRpt As System.Windows.Forms.Button
    Friend WithEvents btnStartReliabilityFile As System.Windows.Forms.Button
    Friend WithEvents btnVI As System.Windows.Forms.Button
    Friend WithEvents btnSC As System.Windows.Forms.Button
    Friend WithEvents btnPA As System.Windows.Forms.Button
    Friend WithEvents btnCT As System.Windows.Forms.Button
    Friend WithEvents btnMasterEventsFile As System.Windows.Forms.Button
    Friend WithEvents btnDrawings As System.Windows.Forms.Button
    Friend WithEvents btnValidatedInputFile As System.Windows.Forms.Button
    Friend WithEvents btnPT As System.Windows.Forms.Button
    Friend WithEvents btnSpecFrac As System.Windows.Forms.Button
    Friend WithEvents btnUnitReview As System.Windows.Forms.Button
    Friend WithEvents btnBuildVRFile As System.Windows.Forms.Button
    Friend WithEvents btnReceiptAck As System.Windows.Forms.Button
    Friend WithEvents lstReturnFiles As System.Windows.Forms.ListBox
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents lstVRFiles As System.Windows.Forms.ListBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents lstVFFiles As System.Windows.Forms.ListBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents lstVFNumbers As System.Windows.Forms.ListBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents tabContacts As System.Windows.Forms.TabPage
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents pnlCompany As System.Windows.Forms.Panel
    Friend WithEvents CCEmail1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblInterimPhone As System.Windows.Forms.TextBox
    Friend WithEvents lblAltPhone As System.Windows.Forms.TextBox
    Friend WithEvents lblPhone As System.Windows.Forms.TextBox
    Friend WithEvents lblInterimEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblAltEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblInterimName As System.Windows.Forms.TextBox
    Friend WithEvents lblAltName As System.Windows.Forms.TextBox
    Friend WithEvents lblName As System.Windows.Forms.TextBox
    Friend WithEvents btnShowIntCo As System.Windows.Forms.Button
    Friend WithEvents btnShowAltCo As System.Windows.Forms.Button
    Friend WithEvents btnShow As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents MainPanel As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblPCCEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblPCCName As System.Windows.Forms.TextBox
    Friend WithEvents pnlRefinery As System.Windows.Forms.Panel
    Friend WithEvents ConsoleTabs As System.Windows.Forms.TabControl
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents chkIDR As System.Windows.Forms.CheckBox
    Friend WithEvents btnIssueCancel As System.Windows.Forms.Button
    Friend WithEvents btnDDExit As System.Windows.Forms.Button
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents chkUseRefnum As System.Windows.Forms.CheckBox
    Friend WithEvents tv As System.Windows.Forms.TreeView
    Friend WithEvents listViewCorrespondenceVF As System.Windows.Forms.ListView
    Friend WithEvents listViewCorrespondenceVR As System.Windows.Forms.ListView
    Friend WithEvents listViewCorrespondenceVReturn As System.Windows.Forms.ListView
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
    Friend WithEvents btnDataFolder As System.Windows.Forms.Button
    Friend WithEvents btnOpenCompanyCorresp As System.Windows.Forms.Button
    Friend WithEvents cboCheckListView As System.Windows.Forms.ComboBox
    Friend WithEvents lblCheckboxView As System.Windows.Forms.Label
    Friend WithEvents cboCCEmail1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboACEmail2 As System.Windows.Forms.ComboBox
    Friend WithEvents cboICEmail As System.Windows.Forms.ComboBox
    Friend WithEvents cboPCCEmail As System.Windows.Forms.ComboBox
    Friend WithEvents btnSecureSendRefresh As System.Windows.Forms.Button
    Friend WithEvents btnCorrespRefresh As System.Windows.Forms.Button
    Friend WithEvents listViewRefnums As System.Windows.Forms.ListView
    Friend WithEvents lblCoPW As System.Windows.Forms.Label
    Friend WithEvents lblRetPW As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lblCompanyCorresp As System.Windows.Forms.Label
    Friend WithEvents txtConsultingOpportunities As System.Windows.Forms.TextBox
    Friend WithEvents lblConsultingOpportunities As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents radChecklistEditIssue As System.Windows.Forms.RadioButton
    Friend WithEvents radChecklistAddNew As System.Windows.Forms.RadioButton
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EntryDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Issue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Consultant As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DeletedBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DeletedDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Deleted As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnManageGadsData As System.Windows.Forms.Button
    Friend WithEvents btnDragTest As System.Windows.Forms.Button
    Friend WithEvents btnGeneralCorrespondence As System.Windows.Forms.Button
    Friend WithEvents txtGCorr As System.Windows.Forms.TextBox


End Class
