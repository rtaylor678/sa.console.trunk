﻿USE [PowerWork]

GO
PRINT N'Creating [Console].[ConsultantInfo]...';


GO
CREATE TABLE [Console].[ConsultantInfo] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Initials]     NVARCHAR (20)  NOT NULL,
    [FullName]     NVARCHAR (50)  NOT NULL,
    [EmailAddress] NVARCHAR (100) NULL,
    [PhoneNumber]  NVARCHAR (20)  NULL,
    CONSTRAINT [PK_Checklist] PRIMARY KEY CLUSTERED ([Id] ASC, [Initials] ASC)
);


GO
PRINT N'Creating [Console].[ContinuingIssues]...';


GO
CREATE TABLE [Console].[ContinuingIssues] (
    [IssueID]     INT            IDENTITY (1, 1) NOT NULL,
    [RefNum]      VARCHAR (12)   NOT NULL,
    [EntryDate]   DATETIME       NOT NULL,
    [Consultant]  VARCHAR (3)    NULL,
    [Note]        NVARCHAR (MAX) NOT NULL,
    [Deleted]     BIT            NULL,
    [DeletedDate] DATETIME       NULL,
    [DeletedBy]   VARCHAR (3)    NULL,
    CONSTRAINT [PK_ContinuingIssues] PRIMARY KEY NONCLUSTERED ([IssueID] ASC)
);


GO
PRINT N'Creating [Console].[Employees]...';


GO
CREATE TABLE [Console].[Employees] (
    [Initials]       NVARCHAR (10) NULL,
    [ConsultantName] NVARCHAR (80) NULL,
    [Active]         BIT           NULL,
    [Email]          NVARCHAR (80) NULL
);


GO

INSERT INTO Console.Employees values('AJC','Tony Carrino',1,'ajc@solomononline.com');
INSERT INTO Console.Employees values('CAW','Cliff Watson',1,'caw@solomononline.com');
INSERT INTO Console.Employees values('WEP','Ed Platt',1,'wep@solomononline.com');

PRINT N'Creating [Console].[SANumbers]...';


GO
CREATE TABLE [Console].[SANumbers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SANumber] [int] NOT NULL,
	[SiteID] [nvarchar](10) NULL,
 CONSTRAINT [PK_[SANumbers] PRIMARY KEY CLUSTERED 
(
	[SANumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (1,'0AA1CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (2,'0AA5PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (3,'0AA7PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (4,'0AABPN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (5,'0AADCC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (6,'0AAMPN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (7,'0AI1CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (8,'0AI2CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (9,'0AI3CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (10,'0AI4CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (11,'0AI5CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (12,'0AI6CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (13,'0AI7CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (14,'0AI8CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (15,'0AI9CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (16,'0AJ1PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (17,'0AJ2PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (18,'0AKPN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (19,'0ALCC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (20,'0AM1PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (21,'0AM2PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (22,'0AM3PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (23,'0AN1CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (24,'0AN2CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (25,'0IC1CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (26,'0IC2CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (27,'0IC3CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (28,'0VCCC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (29,'0VCPN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (30,'100PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (31,'100PN16X');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (32,'101PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (33,'1054CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (34,'108CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (35,'110PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (36,'1150PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (37,'1151PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (38,'120CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (39,'121CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (40,'129PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (41,'136PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (42,'137CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (43,'138PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (44,'139PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (45,'141PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (46,'143CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (47,'144CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (48,'145CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (49,'146CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (50,'147PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (51,'148PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (52,'149CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (53,'152CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (54,'153PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (55,'154PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (56,'155PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (57,'156PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (58,'157PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (59,'158CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (60,'159CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (61,'166PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (62,'167CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (63,'170CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (64,'171CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (65,'173CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (66,'175CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (67,'176CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (68,'177CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (69,'178CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (70,'180CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (71,'182PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (72,'190CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (73,'201PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (74,'202PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (75,'203PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (76,'204PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (77,'205PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (78,'210PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (79,'211PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (80,'212PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (81,'214PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (82,'216PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (83,'217CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (84,'218ACC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (85,'218CPN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (86,'219CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (87,'225PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (88,'226PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (89,'227PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (90,'229PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (91,'243PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (92,'245PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (93,'246PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (94,'248PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (95,'255PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (96,'280PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (97,'281PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (98,'282PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (99,'283PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (100,'284PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (101,'285CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (102,'286CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (103,'288CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (104,'289CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (105,'290CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (106,'291CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (107,'292CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (108,'293CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (109,'294CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (110,'295CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (111,'296CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (112,'297PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (113,'298PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (114,'299PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (115,'299PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (116,'300PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (117,'300PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (118,'311PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (119,'315CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (120,'320CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (121,'321CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (122,'321CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (123,'322CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (124,'322CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (125,'323CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (126,'324CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (127,'324CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (128,'325CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (129,'325CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (130,'340CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (131,'344CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (132,'345CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (133,'355PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (134,'358CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (135,'359CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (136,'360CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (137,'361PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (138,'362CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (139,'363CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (140,'364CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (141,'364PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (142,'365CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (143,'365PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (144,'366PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (145,'367CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (146,'368PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (147,'369PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (148,'378CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (149,'378PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (150,'379CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (151,'379PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (152,'380CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (153,'381CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (154,'382CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (155,'383CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (156,'384CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (157,'385CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (158,'386CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (159,'387CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (160,'388CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (161,'389CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (162,'390PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (163,'391PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (164,'392CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (165,'393CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (166,'394CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (167,'395CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (168,'396CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (169,'397PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (170,'399PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (171,'409PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (172,'411CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (173,'412CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (174,'413CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (175,'414CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (176,'417PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (177,'420CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (178,'421CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (179,'422PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (180,'423PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (181,'424CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (182,'425PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (183,'426CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (184,'427PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (185,'428CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (186,'429CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (187,'430PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (188,'431PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (189,'433CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (190,'434PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (191,'435PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (192,'440PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (193,'441PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (194,'442PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (195,'467CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (196,'468CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (197,'481CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (198,'482CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (199,'483CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (200,'484PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (201,'490PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (202,'490PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (203,'493CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (204,'493CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (205,'501PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (206,'502PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (207,'502PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (208,'503PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (209,'504PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (210,'505PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (211,'505PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (212,'506PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (213,'507CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (214,'508CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (215,'509CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (216,'511CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (217,'512CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (218,'560PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (219,'561PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (220,'562CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (221,'564CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (222,'610PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (223,'681PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (224,'689CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (225,'699CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (226,'715PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (227,'716PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (228,'717PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (229,'718PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (230,'719PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (231,'801PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (232,'802PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (233,'803PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (234,'804PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (235,'806PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (236,'813PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (237,'814PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (238,'818PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (239,'820PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (240,'851CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (241,'855CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (242,'856CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (243,'857CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (244,'858CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (245,'901PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (246,'902PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (247,'903PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (248,'904PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (249,'905PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (250,'906PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (251,'907CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (252,'908CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (253,'909CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (254,'912PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (255,'913CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (256,'914CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (257,'915CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (258,'915CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (259,'920PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (260,'922CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (261,'923PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (262,'924PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (263,'925CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (264,'928CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (265,'933PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (266,'934CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (267,'934CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (268,'934PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (269,'935CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (270,'936CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (271,'937CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (272,'939CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (273,'943CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (274,'945CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (275,'946CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (276,'947CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (277,'951CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (278,'952CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (279,'953CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (280,'954CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (281,'955CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (282,'982CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (283,'AKS1PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (284,'CAP1CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (285,'CAP3CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (286,'CAP4CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (287,'CAP5CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (288,'CAP6CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (289,'CAP7CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (290,'CLB1PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (291,'CLB2CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (292,'CLB3CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (293,'CLB4CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (294,'CS121PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (295,'CS891CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (296,'CSIA3CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (297,'DON05PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (298,'DON15PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (299,'DON20PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (300,'EN5CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (301,'ENECC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (302,'FOR1PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (303,'FOR2CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (304,'FOR6CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (305,'GE1PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (306,'GENACC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (307,'GENBCC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (308,'GKMMPN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (309,'GPC1CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (310,'GPC2CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (311,'MAC1PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (312,'MAC3PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (313,'MAC4PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (314,'NATCC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (315,'OMV21CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (316,'ORDCC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (317,'ORI1PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (318,'ORLGCC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (319,'ORMLCC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (320,'ORMS3CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (321,'ORMSCC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (322,'ORQ5CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (323,'ORQCC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (324,'ORRCC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (325,'ORUCC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (326,'PET10CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (327,'PET11CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (328,'PET12CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (329,'PET13CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (330,'PET14CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (331,'PET1CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (332,'PET2CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (333,'PET3CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (334,'PET4CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (335,'PET5CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (336,'PET6CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (337,'PET7CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (338,'PET8CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (339,'PET9CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (340,'PGB01PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (341,'PGB02PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (342,'PKNPN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (343,'PSNMPN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (344,'PTT1CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (345,'PTT1CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (346,'SKNPCC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (347,'TAK0CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (348,'TAK1CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (349,'TAK2CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (350,'TJS1CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (351,'TJS2CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (352,'TJS4PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (353,'TJS5CC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (354,'TRAKCC16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (355,'VDM0PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (356,'VDM1PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (357,'VDM2PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (358,'VGE10PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (359,'VGE11PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (360,'VGE3CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (361,'VGE4CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (362,'VGE5PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (363,'VGE6PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (364,'VGE6PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (365,'VGE7PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (366,'VGE7PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (367,'VGE9PN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (368,'VGEAPN16P');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (369,'YYY177CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (370,'YYY276PN16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (371,'YYY507CC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (372,'YYYCC16');
INSERT INTO  [Console].[SANumbers] ( [SANumber],       [SiteID]  )    Values (373,'YYYPN16');


PRINT N'Creating [Console].[DF_ContinuingIssues_NoteDate]...';


GO
ALTER TABLE [Console].[ContinuingIssues]
    ADD CONSTRAINT [DF_ContinuingIssues_NoteDate] DEFAULT (getdate()) FOR [EntryDate];


GO
PRINT N'Creating [Console].[AddOrUpdateContinuingIssues]...';


GO

CREATE PROCEDURE [Console].[AddOrUpdateContinuingIssues]
	@ID int,
	@RefNum nvarchar(20),
	@Issue nvarchar(max),
	@EditedBy nvarchar(3)
AS
BEGIN
	IF EXISTS(SELECT IssueID FROM [Console].[ContinuingIssues] where IssueID=@ID)
		BEGIN
			UPDATE [Console].[ContinuingIssues] SET Deleted = 1, DeletedBy=@EditedBy where IssueID=@ID
		END
		
		INSERT INTO [Console].[ContinuingIssues] values(@RefNum,getdate(),@EditedBy,@Issue,0,null,@EditedBy)

		SELECT @@IDENTITY
END
GO
PRINT N'Creating [Console].[ChangeConsultant]...';


GO
CREATE PROCEDURE Console.ChangeConsultant
(
	@SiteId char(10),
	@Year int,
	@Consultant char(4)
)
AS
	UPDATE dbo.StudySites set Consultant = @Consultant 
	where StudyYear =@Year and SiteID=@SiteId
	AND EXISTS(SELECT * from Console.Employees E where E.Initials = RTRIM(@Consultant))
GO
PRINT N'Creating [Console].[ChangeCoord]...';


GO
CREATE PROCEDURE [Console].[ChangeCoord]
(
	 @SiteID nvarchar(15),
	 @PlantName char(50),
	 @CoordName char(40),
	 @CoordTitle char(50),
	 @CoordAddr1 char(50),
	 @CoordAddr2 char(50),
	 @CoordCity char(30),
	 @CoordState char(20),
	 @CoordZip char(15),
	 @CoordPhone char(30),
	 @CoordFax char(20),
	 @CoordEmail char(40)
)
AS

	IF( (select COUNT(SiteID) FROM [dbo].[ClientInfo] where SiteID=@SiteID)=0)
		INSERT INTO [dbo].[ClientInfo]( SiteID,PlantName,CoordName,CoordTitle,CoordAddr1,
		CoordAddr2,	CoordCity,CoordState,CoordZip,CoordPhone,CoordFax,CoordEMail) VALUES
		(@SiteID, @PlantName, @CoordName, @CoordTitle, @CoordAddr1, @CoordAddr2,
		@CoordCity, @CoordState, @CoordZip, @CoordPhone, @CoordFax, @CoordEmail)
	ELSE
		UPDATE[dbo].[ClientInfo] SET 
		CoordName = @CoordName ,
		CoordTitle = @CoordTitle, 
		CoordAddr1 = @CoordAddr1 ,
		CoordAddr2 = @CoordAddr2 ,
		CoordCity = @CoordCity ,
		CoordState = @CoordState, 
		CoordZip = @CoordZip ,
		CoordPhone = @CoordPhone ,
		CoordFax = @CoordFax ,
		CoordEMail = @CoordEMail 	
		where SiteID=@SiteID
GO
PRINT N'Creating [Console].[DeleteContinuingIssue]...';


GO

CREATE PROCEDURE [Console].[DeleteContinuingIssue]
	@IssueID int,
	@DeletedBy nvarchar(3)
AS
BEGIN
	UPDATE [Console].[ContinuingIssues] SET Deleted = 1, DeletedBy=@DeletedBy where IssueID=@IssueID
END
GO
PRINT N'Creating [Console].[GetAllConsultantsByYear]...';


GO


CREATE PROCEDURE Console.GetAllConsultantsByYear
(
	@Year int
)
AS
	SELECT DISTINCT Consultant from [dbo].[StudySites] 
	where StudyYear =@Year
	and SiteID not like '%P'
	and Consultant <> ''
	AND Consultant IS NOT NULL
GO

PRINT N'Creating [Console].[GetCompanynamesWithSiteIds]...';


GO
CREATE PROCEDURE [Console].[GetCompanynamesWithSiteIds]
(
	@Year int
)
AS
	SELECT CONCAT(RTRIM(SiteName)  , CHAR(10), CHAR(45),RTRIM(SiteID) ) As Site from [dbo].[StudySites] 
	where StudyYear = @Year and RTRIM(SiteID) NOT LIKE '%P' order by Site asc;
GO
PRINT N'Creating [Console].[GetConsultantBySiteId]...';


GO

CREATE PROCEDURE Console.GetConsultantBySiteId
(
	@SiteId nvarchar(12)
	--, @StudyYear int  --= 2016;
)
AS
select e.ConsultantName from Console.Employees e 
inner join dbo.StudySites s on s.Consultant = e.Initials
where SiteID = @SiteId
GO
PRINT N'Creating [Console].[GetConsultantEmail]...';


GO

CREATE PROCEDURE Console.GetConsultantEmail
(
	@SiteId nvarchar(12)
	--, @StudyYear int  --= 2016;
)
AS
select e.Email from Console.Employees e 
inner join dbo.StudySites s on s.Consultant = e.Initials
where SiteID = @SiteId
GO
PRINT N'Creating [Console].[GetConsultantInitials]...';


GO


CREATE PROCEDURE Console.GetConsultantInitials
(
	@SiteId char(10)
)
AS
	Select Consultant from dbo.StudySites where SiteId = @SiteId;
GO
PRINT N'Creating [Console].[GetConsultantName]...';


GO


CREATE PROCEDURE Console.GetConsultantName
(
	@ConsultantInitials nvarchar(10)
)
AS
	SELECT ConsultantName from Console.Employees  where Initials = @ConsultantInitials;
GO
PRINT N'Creating [Console].[GetConsultingOpportunities]...';


GO
CREATE PROCEDURE [Console].[GetConsultingOpportunities]
	@RefNum  nvarchar(12)
AS
BEGIN
	SELECT ConsultingOpportunities From ValidationNotes Where Refnum = @RefNum
END
GO
PRINT N'Creating [Console].[GetContinuingIssue]...';


GO
CREATE PROCEDURE Console.GetContinuingIssue
(
	@RefNum varchar(12),
	@IssueID int
)
AS
	SELECT IssueID, EntryDate,consultant,note,deleted,deleteddate,deletedby 
	From [Console].[ContinuingIssues] 
	Where RTRIM(RefNum) = @RefNum and IssueID = @IssueID and deleted = 0 order by EntryDate;
GO
PRINT N'Creating [Console].[GetContinuingIssueByConsultant]...';


GO
CREATE PROCEDURE Console.GetContinuingIssueByConsultant
(
	@RefNum varchar(12),
	@Consultant varchar(3)
)
AS
	SELECT IssueID, EntryDate,consultant,note,deleted,deleteddate,deletedby 
	From [Console].[ContinuingIssues] 
	Where RTRIM(RefNum) = @RefNum and Consultant = @Consultant and deleted = 0 order by EntryDate;
GO
PRINT N'Creating [Console].[GetCoord]...';


GO
CREATE PROCEDURE Console.GetCoord
(
	 @SiteID nvarchar(15)  --='007CC07   '; 	-- @SiteID nvarchar(15) ='PTTGC';
)
AS
	SELECT CoordName,CoordTitle,CoordAddr1,CoordAddr2,	CoordCity,
	CoordState,CoordZip,CoordPhone,CoordFax,CoordEMail
	from [dbo].[ClientInfo] where SiteID=@SiteID
GO
PRINT N'Creating [Console].[GetMFilesCompanyBenchmarkName]...';


GO

CREATE PROCEDURE  [Console].[GetMFilesCompanyBenchmarkName]
	@FacilityId nvarchar(10),
	@SiteId nvarchar(15)
AS
BEGIN
    DECLARE @Result nvarchar(200) = (SELECT CONCAT(RTRIM(c.Refnum), ' - ', RTRIM(c.CoLoc)) 
	As CompanyBenchmarkName FROM PowerWork.dbo.TSort t  LEFT JOIN MFilesSupport.dbo.CPAFacilities m ON m.FacilityID = @FacilityId  LEFT JOIN MFilesSupport.dbo.CPAParticipants p ON p.facilityid =  @FacilityId   LEFT JOIN (MFilesSupport.dbo.FacilityCompany fc INNER JOIN MFilesSupport.dbo.CPAParticipants c ON c.FacilityID = fc.CompanyID)  ON c.Study = 'Power' AND c.StudyYear = t.StudyYear AND fc.FacilityID =  @FacilityId   WHERE t.Siteid = @SiteId);
	SET @Result=RTRIM(@Result);
	SET @Result=LTRIM(@Result);
	IF LEN(@Result) = 1
		SET @Result = NULL;
	SELECT  @Result;
END
GO
PRINT N'Creating [Console].[GetRefNumsBySiteId]...';


GO
CREATE PROCEDURE [Console].[GetRefNumsBySiteId]
(
	@SiteId char(10),
	@Year int
)
AS
	SELECT Refnum from [dbo].[TSort] 
	where SiteID = @SiteId AND StudyYear = @Year AND RTRIM(Refnum) NOT LIKE '%P' order by refnum asc;
GO
PRINT N'Creating [Console].[GetSiteIds]...';


GO
CREATE PROCEDURE [Console].[GetSiteIds]
(
	@Year int
)
AS
	SELECT SiteID from [dbo].[StudySites] where StudyYear = @Year and RTRIM(SiteID) NOT LIKE '%P';
GO
PRINT N'Creating [Console].[GetTSortDataBySiteId]...';


GO

CREATE PROCEDURE Console.GetTSortDataBySiteId
(
	@SiteId nvarchar(12)
)
AS
select 
Refnum,SiteID,CompanyID,CompanyName,UnitName,UnitLabel,UnitID,CoLoc,StudyYear,
EvntYear,Continent,Country,Region,State,PricingHub,Regulated,CalcCommUnavail,
EGCRegion,EGCTechnology,EGCManualTech,CTGs,CTG_NDC,NDC,Coal_NDC,Oil_NDC,
Gas_NDC,PrecBagYN,ScrubbersYN,NumUnitsAtSite,FuelType,Metric,HHV,CurrencyID,
FormerCurrencyID,SingleShaft,NumSTs,DryCoolTower
FROM dbo.TSort 
where SiteID=@SiteId
GO
PRINT N'Creating [Console].[GetUnitnamesWithRefNums]...';


GO
CREATE PROCEDURE Console.GetUnitnamesWithRefNums
(
	@SiteId char(10),
	@Year int
)
AS
	SELECT CONCAT(RTRIM(UnitName)  , CHAR(10), CHAR(45),RTRIM(Refnum) ) As UnitnameRefnum from [dbo].[TSort] 
	where SiteID = @SiteId  AND RTRIM(SiteID) NOT LIKE '%P' AND StudyYear = @Year AND RTRIM(Refnum) NOT LIKE '%P' 
	order by UnitnameRefnum asc;
GO
PRINT N'Creating [Console].[InsertConsultingOpportunities]...';


GO

CREATE PROCEDURE [Console].[InsertConsultingOpportunities]

	@RefNum nvarchar(12),
	@ConsultingOpportunities text

AS
BEGIN
	INSERT INTO dbo.ValidationNotes (Refnum, ConsultingOpportunities) VALUES (@RefNum, @ConsultingOpportunities)
END
GO
PRINT N'Creating [Console].[UpdateCoContact]...';


GO
CREATE PROCEDURE [Console].[UpdateCoContact] 
	@SiteId nvarchar(12),
	@StudyYear int,
	@FirstName varchar(25),
	@LastName varchar(25),
	@JobTitle varchar(75),
	@Address1 varchar(75),
	@Address2 varchar(50),
	@Address3 nvarchar(50),
	@City nvarchar(30),
	@State nvarchar(30),
	@Zip nvarchar(30),
	@Country nvarchar(30),
	@Phone nvarchar(40),
	@Email nvarchar(255),
	@StAddress1 as nvarchar(75),
	@StAddress2 as nvarchar(50),
	@StAddress3 as nvarchar(50),
	@StAddress4 as nvarchar(30),
	@StAddress5 as nvarchar(30),
	@StAddress6 as nvarchar(30),
	@StAddress7 as nvarchar(30),
	@Fax as nvarchar(40),
	@ContactType as nvarchar(10),
	@Password as nvarchar(50)
AS
BEGIN
UPDATE PowerGlobal.dbo.Company_LU set Password = @Password where CompanyID = 
  (select DISTINCT CompanyID from dbo.TSort where SiteID = @SiteId);
IF EXISTS(
SELECT *
	FROM [dbo].[CoContactInfo] c 
	join [Console].[SANumbers] s on c.SANumber=s.SANumber
	join TSort t on s.SiteID=t.SiteID 
	WHERE t.SiteID = @SiteId
	AND t.StudyYear = @StudyYear 
	AND UPPER(c.ContactType)= @ContactType 
)
  BEGIN
   UPDATE C SET c.FirstName=@FirstName ,
		c.LastName=@LastName ,
		c.JobTitle = @JobTitle,
		c.Email =@Email,
		c.Phone = @Phone,
		c.Fax = @Fax ,
		c.MailAddr1 = @Address1 ,
		c.MailAddr2 = @Address2 ,
		c.MailAddr3 = @Address3 ,
		c.MailCity = @City  ,
		c.MailState = @State ,
		c.MailZip = @Zip ,
		c.MailCountry = @Country ,
		c.StrAddr1 = @StAddress1,
		c.StrAddr2 = @StAddress2,
		c.StrAdd3 = @StAddress3,
		c.StrCity = @StAddress4,
		c.StrState = @StAddress5,
		c.StrZip = @StAddress6,
		c.StrCountry = @StAddress7,
		c.contactType = @ContactType		
	FROM [dbo].[CoContactInfo] c 
	join [Console].[SANumbers] s on c.SANumber=s.SANumber
	join TSort t on s.SiteID=t.SiteID 
	WHERE t.SiteID= @SiteId
	AND t.StudyYear = @StudyYear 
	AND UPPER(c.ContactType)= @ContactType 	
  END
ELSE
  BEGIN
	DECLARE @SANumber int
	SET @SANumber = (SELECT DISTINCT SANumber from [Console].[SANumbers] s 
		join TSort t on s.SiteID=t.SiteID 
		WHERE t.SiteID = @SiteId 
		AND t.StudyYear = @StudyYear )
		INSERT INTO CoContactInfo (
			[SANumber],[ContactType] ,[FirstName] ,[LastName] ,[JobTitle] ,[Phone] ,[Fax] ,
			[Email],[StrAddr1] ,[StrAddr2] ,[StrAdd3] ,[StrCity] ,[StrState] ,[StrZip] ,
			[StrCountry] ,[MailAddr1] ,[MailAddr2] ,[MailAddr3] ,[MailCity] ,[MailState] ,
			[MailZip] ,[MailCountry] ,[CCAlt]
		) values(					
			@SANumber,
			@ContactType,
			@FirstName ,
			@LastName ,
			@JobTitle,
			@Phone,
			@Fax,
			@Email,
			@StAddress1,
			@StAddress2,
			@StAddress3,
			@StAddress4,
			@StAddress5,
			@StAddress6,
			@STAddress7,
			@Address1 ,
			@Address2 ,
			@Address3 ,
			@City  ,
			@State ,
			@Zip ,
			@Country,
			'N')
  END		
END

GO


PRINT N'Creating [Console].[UpdateConsultingOpportunities]...';


GO

CREATE PROCEDURE [Console].[UpdateConsultingOpportunities]
	@RefNum nvarchar(12),
	@ConsultingOpportunities text
AS
BEGIN
	UPDATE dbo.ValidationNotes SET ConsultingOpportunities = @ConsultingOpportunities
	WHERE Refnum=@RefNum 
END
GO
PRINT N'Creating [dbo].[UpdateClientInfo]...';


GO
CREATE PROCEDURE dbo.UpdateClientInfo
(
	@SiteID char(10)
	,
	@CorpName char(50),
	@AffName char(50),
	@PlantName char(50),
	@City char(30),
	@State char(20),
	@CoordName char(40),
	@CoordTitle char(50),
	@CoordAddr1 char(50),
	@CoordAddr2 char(50),
	@CoordCity char(30),
	@CoordState char(20),
	@CoordZip char(15),
	@CoordPhone char(30),
	@CoordFax char(20),
	@CoordEMail char(40),
	@RptUOM varchar(3),
	@RptHV varchar(3),
	@RptSteamMethod varchar(3)
	
) AS
	IF EXISTS(SELECT TOP 1 * from dbo.ClientInfo where SiteID=@SiteID) 
		BEGIN
			UPDATE dbo.ClientInfo 
			SET 
			SiteID =  RTRIM(@SiteID),
			CorpName =  RTRIM(@CorpName),
			AffName =  RTRIM(@AffName),
			PlantName =  RTRIM(@PlantName),
			City =  RTRIM(@City),
			State =  RTRIM(@State),
			CoordName =  RTRIM(@CoordName),
			CoordTitle =  RTRIM(@CoordTitle),
			CoordAddr1 =  RTRIM(@CoordAddr1),
			CoordAddr2 =  RTRIM(@CoordAddr2),
			CoordCity =  RTRIM(@CoordCity),
			CoordState =  RTRIM(@CoordState),
			CoordZip =  RTRIM(@CoordZip),
			CoordPhone =  RTRIM(@CoordPhone),
			CoordFax =  RTRIM(@CoordFax),
			CoordEMail =  RTRIM(@CoordEMail),
			RptUOM =  RTRIM(@RptUOM),
			RptHV =  RTRIM(@RptHV),
			RptSteamMethod =  RTRIM(@RptSteamMethod)
			where SiteID=@SiteID
		END
	ELSE
	BEGIN
		INSERT INTO dbo.ClientInfo (SiteID,CorpName,AffName,PlantName,City,State,CoordName,CoordTitle,CoordAddr1,CoordAddr2,CoordCity,CoordState,CoordZip,CoordPhone,CoordFax,CoordEMail,RptUOM,RptHV,RptSteamMethod)
		VALUES
		(RTRIM(@SiteID),
		RTRIM(@CorpName),
		RTRIM(@AffName),
		RTRIM(@PlantName),
		RTRIM(@City),
		RTRIM(@State),
		RTRIM(@CoordName),
		RTRIM(@CoordTitle),
		RTRIM(@CoordAddr1),
		RTRIM(@CoordAddr2),
		RTRIM(@CoordCity),
		RTRIM(@CoordState),
		RTRIM(@CoordZip),
		RTRIM(@CoordPhone),
		RTRIM(@CoordFax),
		RTRIM(@CoordEMail),
		RTRIM(@RptUOM),
		RTRIM(@RptHV),
		RTRIM(@RptSteamMethod)
		)
	END
GO
PRINT N'Creating [dbo].[UpdatePassword]...';


GO


CREATE PROCEDURE [dbo].[UpdatePassword]
(
	@RefNum nvarchar(12),
	@Password char(20) 
)
AS
BEGIN
	IF EXISTS(select * from dbo.Company_LU where CompanyID = (select CompanyID from dbo.TSort where Refnum=@RefNum))
	BEGIN
		UPDATE dbo.Company_LU 
		SET [Password] = @Password
		where CompanyID = (select CompanyID from dbo.TSort where Refnum=@RefNum)
	END
END
GO

--====================changes
DROP TABLE [Val].[Checklist];

GO

CREATE TABLE [Val].[Checklist] (
    [SiteId]     CHAR (10) NOT NULL,
    [IssueID]    CHAR (8)  NOT NULL,
    [IssueTitle] CHAR (50) NOT NULL,
    [IssueText]  TEXT      NULL,
    [PostedBy]   CHAR (3)  NOT NULL,
    [PostedTime] DATETIME  NOT NULL,
    [Completed]  CHAR (1)  NOT NULL,
    [SetBy]      CHAR (3)  NULL,
    [SetTime]    DATETIME  NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Checklist] PRIMARY KEY CLUSTERED ([SiteId] ASC, [IssueID] ASC)
);

GO


PRINT N'Creating [Console].[GetChecklist]...';


GO
CREATE PROCEDURE Console.GetChecklist
(
	@SiteId char(10)
)
AS
SELECT SiteId,IssueID,IssueTitle,IssueText,PostedBy,PostedTime,Completed,SetBy,SetTime FROM VAL.CHECKLIST WHERE RTRIM(SiteId) = @SiteId
GO



PRINT N'Altering [Console].[GetCompanyContactInfo]...';


GO
ALTER PROCEDURE  [Console].[GetCompanyContactInfo]
	@SiteId nvarchar(10),
	@StudyYear int,
	@ContactType char(5)
AS
BEGIN

	IF @SiteId is not null 
	BEGIN
	DECLARE @CompanyId char(15);
	DECLARE @Country varchar(50);
	set @CompanyId = (SELECT Distinct CompanyID from dbo.TSort where SiteID=@SiteId AND StudyYear = @StudyYear );
	set @Country = (SELECT Distinct Country from dbo.TSort where SiteID=@SiteId AND StudyYear = @StudyYear );
	SELECT 	
		c.FirstName + ' ' + c.LastName as CoordName,
		c.[SANumber],
       [ContactType] ,
       c.[FirstName] ,
       c.[LastName] ,
       [JobTitle] ,
       [PersonalTitle] ,
       [Phone] ,
       [PhoneSecondary] ,
       [Fax] ,
       c.[Email] ,
       [StrAddr1],
       [StrAddr2] ,
       [StrAdd3] ,
       [StrCity] ,
       [StrState] ,
       [StrZip] ,
       [StrCountry] ,
       [MailAddr1] ,
       [MailAddr2] ,
       [MailAddr3] ,
       [MailCity] ,
       [MailState] ,
       [MailZip] ,
       [MailCountry],
       [AltFirstName],
       [AltLastName],
       [AltEmail] ,
       [CCAlt] ,
       [SendMethod] ,
       [Comment] ,
	   @Country,
		l.Password as CompanyPassword
	FROM [dbo].[CoContactInfo] c 
	join [Console].[SANumbers] s on c.SANumber=s.SANumber
	join PowerWork.dbo.Company_LU l on l.CompanyID = @CompanyId
	WHERE UPPER(c.ContactType)= @ContactType 
	 and s.SiteID = @SiteId
END
END
GO


PRINT N'Altering [Console].[GetCompanyPassword]...';


GO

ALTER PROCEDURE [Console].[GetCompanyPassword]
(
	@SiteID char(10), 
	@StudyYear int  
)
AS
	select l.Password as CompanyPassword 
	FROM TSort t 
	inner join Company_LU l on t.CompanyID = l.CompanyID
	WHERE t.SiteID=@SiteID
	AND t.StudyYear = @StudyYear
GO
PRINT N'Altering [Console].[GetValidationNotes]...';


GO
ALTER PROCEDURE [Console].[GetValidationNotes]
	@RefNum nvarchar(12)
AS
BEGIN
	SELECT ValidationNotes From Val.Notes Where RefNum = @RefNum
END
GO

