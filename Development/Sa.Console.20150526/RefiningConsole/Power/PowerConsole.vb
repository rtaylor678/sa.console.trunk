﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports Excel = Microsoft.Office.Interop.Excel
Imports Word = Microsoft.Office.Interop.Word
Imports Outlook = Microsoft.Office.Interop.Outlook
Imports System.ComponentModel
Imports System.Threading
Imports System.IO
Imports System.Text
Imports System.IO.Compression
Imports System.Runtime.InteropServices
Imports System.Reflection
Imports SA.Internal.Console.DataObject
Imports System.Text.RegularExpressions
Imports System
Imports System.Xml
Imports SA.Console.Common


Public Class PowerConsole

#Region "PRIVATE VARIABLES"

    Dim _dAL As DataObject

    Private mSpawn As Boolean
    Private mReturnPassword As String
    Private _studyType As String = ConsoleVertical.ToString() '  "POWER" 'maybe should be EUR or NSA etc instead????
    Private _lastStudyAndRefnum As String()
    'Private _yearRefnumsAndPlants As IYearRefnumsAndPlants
    Private _profileConsoleTempPath As String = ProfileConsolePath + "temp\"
    Private _userWindowsProfileName As String = String.Empty 'deprecated
    Private _pathToPriorDatachecksFile As String = String.Empty
    Private _pathToCurrentDatachecksFile As String = String.Empty
    Private Const _delim As Char = Chr(14)
    Private _docsAccess As DocsFileSystemAccess
    Private _useMFiles As Boolean = False
    Private _settingPath As String = _profileConsoleTempPath + "ConsoleSettings\"
    Private _settingsFileName As String = "RefiningSettings20160830.txt"
    Private _mfilesFailed As Boolean = False
    Private _settings As Dictionary(Of String, String) = New Dictionary(Of String, String)
    Private _xmlAppSettingsPath As String = _settingPath + "RefineryAppSettings.xml"
    Private _formIsLoaded As Boolean = False
    Private _profileConsoleExtractedPath As String = _profileConsoleTempPath + "Extracted Files\"
    Private _tracingFile As String = ProfileConsolePath + "ConsoleTracing.txt"
    Private _tracing As Boolean = False
    Private _mfilesBenchmarkingParticipantVPath As String = String.Empty
    Private _mfilesBenchmarkingCompanyVPath As String = String.Empty
    Private _mfilesRefiningGeneralVPath As String = String.Empty
    Private _dragDropMode As Integer = 0 '1 for dragdrop is processing files, 2 for dragdrop is processing an email
    Private _benchmarkingParticipantSiteFiles As Sa.Console.Common.ConsoleFilesInfo
    Private _benchmarkingParticipantCompanyFiles As Sa.Console.Common.ConsoleFilesInfo
    Private _refiningGeneralFiles As SA.Console.Common.ConsoleFilesInfo
    Private _powerSvc As PowerService.PowerManager
    Private _powerSettings As System.Collections.Generic.Dictionary(Of String, String) ' Dictionary <string,string>
    Private _siteId As String = String.Empty
    Private _refNum As String = String.Empty

    Private _checklistDataTable As DataTable = New DataTable()
    Private _checklistManager As PowerChecklistManager = Nothing
    Private _checklistNodeChecked As Boolean = False

    Private _ciID As Integer = -1
    Private _ciTextBeingEdited As String = String.Empty
    Private _vfTemplatesPath As String = String.Empty
    Private _corrPath As String = String.Empty
    Private _compCorrPath As String = String.Empty
    Private _drawingPath As String = String.Empty
    Private _mfilesDesktopUIPath As String = String.Empty
    Private _clientAttachmentsPath As String = String.Empty
    Private WithEvents _contactForm As ContactFormPopup
    Private WithEvents _powerCoordContactForm As PowerCoordContactForm
    Private _companyName As String = String.Empty
    Private _docClassForIdr As Integer = 52
    Private _deliverableForPresentersNotes As Integer = 17
    Private _dataFolderPath As String = String.Empty
    Private _templatesFolderPath As String = String.Empty
    Private _initializing As Boolean = False
#End Region

#Region "PUBLIC PROPERTIES"

    Private mVPN As Boolean


    Public Property VPN() As Boolean
        Get
            Return mVPN
        End Get
        Set(ByVal value As Boolean)
            mVPN = value
        End Set
    End Property

    Private _userName As String
    Public Property UserName As String
        'NOT the same as UserWindowsProfileName; this is the part without the DC1.
        Get
            Return _userName
        End Get
        Set(value As String)
            If value.ToUpper().Contains("DC1") OrElse value.Contains(".") Then
                Dim parts() As String = value.Split(".")
                For i As Integer = 0 To parts.Length - 1
                    If parts(i).ToUpper() <> "DC1" Then
                        _userName = parts(i)
                    End If
                Next
            Else
                _userName = value
            End If

        End Set
    End Property

    'Public Property DBConnection() As String
    '    Get
    '        Return mDBConnection
    '    End Get
    '    Set(ByVal value As String)
    '        mDBConnection = value
    '    End Set
    'End Property

    Private mCurrentStudyYear As String

    Public Property CurrentStudyYear() As String
        Get
            Return mCurrentStudyYear
        End Get
        Set(ByVal value As String)
            mCurrentStudyYear = value
        End Set
    End Property

    'Public Property StudyYear() As String
    '    Get
    '        Return mStudyYear
    '    End Get
    '    Set(ByVal value As String)
    '        mStudyYear = value
    '    End Set
    'End Property

    Public Property UserWindowsProfileName() As String
        'Required by login form. Not used in this class
        Get
            If (IsNothing(_userWindowsProfileName)) OrElse (_userWindowsProfileName.Length < 1) Then
                ' = My.User.Name + ".DC1"
                Dim fullName As String = My.User.Name
                If Not fullName.Contains("\") Then
                    _userWindowsProfileName = fullName
                Else
                    Dim temp As String() = My.User.Name.Split("\")

                    _userWindowsProfileName = temp(1) + "." + temp(0)
                End If
            End If

            Return _userWindowsProfileName
        End Get
        Set(ByVal value As String)
            _userWindowsProfileName = value
        End Set
    End Property

    Public Property ReturnPassword() As String
        Get
            Return mReturnPassword
        End Get
        Set(ByVal value As String)
            mReturnPassword = value
        End Set
    End Property

    'Public Property Password() As String
    '    Get
    '        Return mPassword
    '    End Get
    '    Set(ByVal value As String)
    '        mPassword = value
    '    End Set
    'End Property

    'Public Property StudyType() As String
    '    Get
    '        Return mStudyType
    '    End Get
    '    Set(ByVal value As String)
    '        mStudyType = value
    '    End Set
    'End Property

    'Public Property ReferenceNum() As String
    '    Get
    '        Return mRefNum
    '    End Get
    '    Set(ByVal value As String)
    '        mRefNum = value
    '    End Set
    'End Property

    Public Property Spawn() As Boolean
        Get
            Return mSpawn
        End Get
        Set(ByVal value As Boolean)
            mSpawn = value
        End Set
    End Property

#End Region

#Region "INITIALIZATION"

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

    End Sub

    Private Sub PowerConsole_DragDrop(sender As Object, e As DragEventArgs) _
                    Handles MyBase.DragDrop, ConsoleTabs.DragDrop
        T("PowerConsole_DragDrop")
        'DEBUG NOTE: If this isn't firing in the IDE, try closing VS and reopening but NOT "As Administrator"
        '  For further info, see https://stackoverflow.com/questions/68598/how-do-i-drag-and-drop-files-into-an-application

        Dim errors As String = DragDropRoutine(e.Data, e.Data.GetDataPresent("FileGroupDescriptor"),
                                               e.Data.GetDataPresent("Object Descriptor"),
                                               e.Data.GetData("Text"))

        If errors.Length > 0 Then MsgBox(errors)
    End Sub

    Private Sub PowerConsole_DragEnter(sender As Object, e As DragEventArgs) Handles MyBase.DragEnter, ConsoleTabs.DragEnter, tvwCorrespondence.DragEnter, tvwDrawings.DragEnter, tvwCompCorr.DragEnter, tvwCompCorr2.DragEnter, tvwClientAttachments.DragEnter, tvwCorrespondence2.DragEnter, tvwDrawings2.DragEnter, tvwClientAttachments2.DragEnter
        T("PowerConsole_DragEnter")
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.Copy
        ElseIf (e.Data.GetDataPresent("FileGroupDescriptor")) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub PowerConsole_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Initialize()
        _formIsLoaded = True
        T("Leaving MainConsole_Load")
    End Sub

    Private Sub Initialize()
        _initializing = True
        If File.Exists(_tracingFile) Then
            Using sr As New StreamWriter(_tracingFile, False) 'clear out old
                sr.WriteLine(DateTime.Now.ToString())
            End Using
            _tracing = True
        End If
        T("Initialize")
        Try
            If ConsoleVertical = VerticalType.POWER Then
                T("Going to set _dAL")
                _dAL = New DataObject(DataObject.StudyTypes.POWER, UserName, "")
            End If
            T("Going to instantiate new PowerManager")
            _powerSvc = New PowerService.PowerManager(_dAL)
            T("Going to Remove Bad File")
            Dim badConsoleFile As String = ProfileConsolePath.Remove(ProfileConsolePath.Length - 1) 'a CONSOLE file left by the batch file, need folder instead.
            If File.Exists(badConsoleFile) Then File.Delete(badConsoleFile)
            T("Going to Remove tabs")
            RemoveTab("tabClippy")
            RemoveTab("tabQA")
            RemoveTab("tabTimeGrade")
            T("Going to Create Console Folder 1")
            If Not CreateConsoleFolder(ProfileConsolePath) Then End
            T("Going to Create Console Folder 2")
            If Not CreateConsoleFolder(_profileConsoleTempPath) Then End
            T("Going to Create Console Folder 3")
            If Not CreateConsoleFolder(_profileConsoleTempPath & "SecureSend") Then End
            T("Going to Create Console Folder 4")
            If Not CreateConsoleFolder(_profileConsoleTempPath & "ConsoleSettings") Then End
            T("Going to Create Console Folder 5")
            If Not CreateConsoleFolder(_profileConsoleTempPath & "Extracted Files") Then End
            T("Going to Create Console Folder 6")
            If Not CreateConsoleFolder(_profileConsoleTempPath & "ZIP") Then End
            T("Going to clear temp path")
            If Directory.Exists(_profileConsoleTempPath) Then
                Dim dir As New DirectoryInfo(_profileConsoleTempPath)
                Dim files() As FileInfo = dir.GetFiles()
                For i As Integer = 0 To files.Count - 1
                    Try
                        Kill(files(i).FullName)
                    Catch
                    End Try
                Next
            End If
            'CleanFilesFromFolder(_profileConsoleTempPath + "SecureSend")
            'CleanFilesFromFolder(_profileConsoleTempPath + "Extracted Files")
            'CleanFilesFromFolder(_profileConsoleTempPath + "ZIP")
            'CleanFilesFromFolder(ProfileConsolePath)

            T("Going to call GetStudies")
            Dim studies As List(Of Integer) = _powerSvc.GetStudies()
            If Not IsNothing(studies) AndAlso studies.Count > 0 Then
                For Each study As Integer In studies
                    cboStudy.Items.Add(study.ToString)
                Next
            End If

            '_useMFiles = True 'start with current year

            _mfilesDesktopUIPath = ConfigurationManager.AppSettings("MfilesUIPath")

            T("Going to create DocsFileSystemAccess")
            _docsAccess = New DocsFileSystemAccess(VerticalType.POWER, False)

            'now check for last place they left off: study year, SiteID.

            'Check for existance of Settings file, if missing, copy default one
            Dim settingsFilePath As String = _profileConsoleTempPath & "ConsoleSettings\" & _studyType & "Settings.txt"
            If Not _docsAccess.FileExists(settingsFilePath) Then
                T("Going to call MakeDefaultPowerSettinsFile")
                _powerSvc.MakeDefaultPowerSettingsFile(settingsFilePath)
            End If
            T("Going to call GetPOwerSettings")
            _powerSettings = _powerSvc.GetPowerSettings(settingsFilePath)

            Dim lastStudyFilePath As String = _profileConsoleTempPath & "ConsoleSettings\" & _studyType & "LastStudy.txt"
            T("Going to call GetLastStudy")
            If Not File.Exists(lastStudyFilePath) Then
                Using sw As New StreamWriter(lastStudyFilePath)
                    'sw.WriteLine(_powerSettings.(0))
                    'Dim z = _powerSettings.Keys
                    'MsgBox(z(0))
                    sw.WriteLine(_powerSettings.Keys(0))
                End Using
            End If
            Dim lastStudy As String = _powerSvc.GetLastStudy(lastStudyFilePath)
            If lastStudy.Length > 0 Then
                For Each item In cboStudy.Items
                    If item.ToString() = lastStudy Then
                        cboStudy.SelectedItem = item
                    End If
                Next
            End If


            Dim lastStudyYear As Integer = 0
            If Int32.TryParse(lastStudy, lastStudyYear) Then
                T("Going to call GetCompanynamesWithSiteIds")
                Dim siteIds As List(Of String) = _powerSvc.GetCompanynamesWithSiteIds(lastStudyYear)
                For Each siteId As String In siteIds
                    cboSiteId.Items.Add(siteId)
                Next
            End If

            Dim lastSiteId As String = String.Empty
            T("Going to call TryGetValue")
            _powerSettings.TryGetValue(lastStudy, lastSiteId)
            If lastSiteId.Length > 0 Then
                For Each item In cboSiteId.Items
                    If item.ToString().Trim().ToUpper() = lastSiteId.Trim().ToUpper() Then
                        cboSiteId.SelectedItem = item
                        Exit For
                    End If
                Next
            End If
            T("Going to call LoadConsultantsToCbo")
            LoadConsultantsToCbo(lastStudyYear)

            If Not IsNothing(cboSiteId.SelectedItem) AndAlso cboSiteId.SelectedItem.ToString().Length > 0 Then
                T("Going to call GetUnitnamesWithRefNums")
                Dim refnums As List(Of String) = _powerSvc.GetUnitnamesWithRefNums(cboSiteId.SelectedItem.ToString(), lastStudyYear)
                For Each refnum In refnums
                    listViewRefnums.Items.Add(refnum) '& Environment.NewLine
                Next
                T("Going to GetConsultant")
                Dim c As String = _powerSvc.GetConsultant(cboSiteId.SelectedItem, lastStudyYear)
                If Not IsNothing(cboSiteId.SelectedItem) Then
                    T("    " & cboSiteId.SelectedItem.ToString())
                End If
                If Not IsNothing(lastStudyYear) Then
                    T("    " & lastStudyYear.ToString())
                End If
            End If

            If _profileConsoleTempPath.ToUpper().Contains("SFB") Then
                MsgBox("Per Mike: SiteId or Refnum must never end in a P for Console purposes")
            End If

            'If File.Exists(ProfilePath & "\DevConfigHelper.txt") Then
            '    btnDragTest.Visible = True
            'End If

        Catch ex As System.Exception
            _dAL.WriteLog("Initialize", ex)
            MessageBox.Show(ex.Message)
        End Try
        _initializing = False
    End Sub

#End Region

#Region "ClearForm"
    Private Function ClearAllControls() As Boolean
        T("ClearAllControls")
        'MsgBox("clear top of form, status, each control on each tab. EXCEPT cboStudy and cboRenum and cboCompany")
        _mfilesBenchmarkingParticipantVPath = String.Empty
        _mfilesBenchmarkingCompanyVPath = String.Empty
        _mfilesRefiningGeneralVPath = String.Empty
        ClearForm()
        ClearContactsTab()
        ClearCorrespondenceTab()
        ClearChecklistTab()
        ClearNotesTab()
        ClearContinuingIssuesTab()
        ClearSecureSendTab()
        ClearTimeGradeTab()
        'ConsoleTabs.TabPages("tabTimeGrade").Hide()
        'ClearDragDropTab()
        ClearQueryTab()
        'ConsoleTabs.TabPages("tabClippy").Hide()
        ClearQATab()
        'ConsoleTabs.TabPages("tabQA").Hide()
        ClearPaths()
        'btnMasterEventsFile.Text = String.Empty
        'ClearSummaryFileButtonTags()
        Try
            For Each frm As Form In My.Application.OpenForms
                If frm.Name = "SecureSend" Then
                    frm.Close()
                End If
            Next
        Catch
        End Try
        Return True
    End Function
    Private Sub ClearForm()
        T("ClearForm")
        'clears all except the tabs
        'btnFuelLube.Visible = False
        WebBrowser1.Visible = False
        btnMain.Text = String.Empty
        cboConsultant.Items.Clear()
        cboConsultant.Text = String.Empty
        btnCompanyPW.Text = String.Empty
        btnReturnPW.Text = String.Empty
        lblValidationStatus.Text = String.Empty
        lblLastFileSave.Text = String.Empty
        lblLastUpload.Text = String.Empty
        lblLastCalcDate.Text = String.Empty
        lblRedFlags.Text = String.Empty
        lblBlueFlags.Text = String.Empty
        lblItemsRemaining.Text = String.Empty
        btnFLValidation.Visible = False
        btnCreateMissingPNFiles.Visible = False
        btnSendPrelimPricing.Visible = False
        lblStatus.Text = String.Empty
    End Sub
    Private Sub ClearPaths()
        T("ClearPaths")
        'StudyRegion = String.Empty
        'Coloc = String.Empty
        'Company = String.Empty
        'mRefNum = String.Empty
        'RefineryPath = String.Empty
        'GeneralPath = String.Empty
        'ProjectPath = String.Empty
        '_corrPath = String.Empty
        'DataFolderPath = String.Empty
        'CompanyPath = String.Empty
        'CompCorrPath = String.Empty
        'TempPath = String.Empty
        'IDRPath = String.Empty
        'DrawingPath = String.Empty
        '_clientAttachmentsPath = String.Empty
        'TemplatePath = String.Empty
        'PolishPath = String.Empty
    End Sub
    Private Sub ClearContactsTab()
        T("ClearForm")
        'lblPricingContact.Text = String.Empty
        'lblPricingContactEmail.Text = String.Empty
        'lblPricingPhone.Text = String.Empty

        lblName.Text = String.Empty
        lblEmail.Text = String.Empty
        lblPhone.Text = String.Empty
        lblAltName.Text = String.Empty
        lblAltEmail.Text = String.Empty
        lblAltPhone.Text = String.Empty
        lblInterimName.Text = String.Empty
        lblInterimEmail.Text = String.Empty
        lblInterimPhone.Text = String.Empty
        'lblRefCoName.Text = String.Empty
        'lblRefCoEmail.Text = String.Empty
        'lblRefCoPhone.Text = String.Empty
        'lblRefAltName.Text = String.Empty
        'lblRefAltEmail.Text = String.Empty
        'lblRefAltPhone.Text = String.Empty
        'lblRefMgrName.Text = String.Empty
        'lblRefMgrEmail.Text = String.Empty
        'lblRefMgrPhone.Text = String.Empty
        'lblOpsMgrName.Text = String.Empty
        'lblOpsMgrEmail.Text = String.Empty
        'lblOpsMgrPhone.Text = String.Empty
        'lblMaintMgrName.Text = String.Empty
        'lblMaintMgrEmail.Text = String.Empty
        'lblMaintMgrPhone.Text = String.Empty
        'lblTechMgrName.Text = String.Empty
        'lblTechMgrEmail.Text = String.Empty
        'lblTechMgrPhone.Text = String.Empty
        'lblPCCName.Text = String.Empty
        'lblPCCEmail.Text = String.Empty
        'lblPCCPhone.Text = String.Empty



        'ClearLabelsPrefixedWith("lbl")
        'ClearLabelsPrefixedWith("lblAlt")
        'ClearLabelsPrefixedWith("lblInterim")
        'ClearLabelsPrefixedWith("lblRefCo")
        'ClearLabelsPrefixedWith("lblRefAlt")
        'ClearLabelsPrefixedWith("lblRefMgr")
        'ClearLabelsPrefixedWith("lblOpsMgr")
        'ClearLabelsPrefixedWith("lblMaintMgr")
        'ClearLabelsPrefixedWith("lblTechMgr")
        'ClearLabelsPrefixedWith("lblPCC")
    End Sub
    Private Sub ClearCorrespondenceTab()
        T("ClearCorrespondenceTab")
        lstVFNumbers.Items.Clear()
        listViewCorrespondenceVF.Items.Clear()
        listViewCorrespondenceVR.Items.Clear()
        listViewCorrespondenceVReturn.Items.Clear()
        lstVFFiles.Items.Clear()
        lstVRFiles.Items.Clear()
        lstReturnFiles.Items.Clear()
    End Sub
    Private Sub ClearChecklistTab()
        T("ClearChecklistTab")
        tvIssues.Nodes.Clear()
        txtIssueID.Text = String.Empty
        txtIssueName.Text = String.Empty
        txtDescription.Text = String.Empty
    End Sub
    Private Sub ClearNotesTab()
        T("ClearNotesTab")
        txtNotes.Text = String.Empty
        txtConsultingOpportunities.Text = String.Empty
    End Sub
    Private Sub ClearContinuingIssuesTab()
        T("ClearContinuingIssuesTab")
        dgContinuingIssues.Rows.Clear()
    End Sub
    Private Sub ClearSecureSendTab()
        T("ClearSecureSendTab")
        tvwCompCorr.Nodes.Clear()
        tvwCompCorr2.Nodes.Clear()
        tvwDrawings.Nodes.Clear()
        tvwDrawings2.Nodes.Clear()
        tvwCorrespondence.Nodes.Clear()
        tvwCorrespondence2.Nodes.Clear()
        tvwClientAttachments.Nodes.Clear()
        tvwClientAttachments2.Nodes.Clear()
    End Sub
    Private Sub ClearTimeGradeTab()
        T("ClearTimeGradeTab")
        Try
            dgGrade.Rows.Clear()
        Catch
        End Try
        Try
            dgSummary.Rows.Clear()
        Catch
        End Try
        Try
            dgHistory.Rows.Clear()
        Catch
        End Try

    End Sub
    'Private Sub ClearDragDropTab()
    '    T("ClearDragDropTab")
    '    txtMessageFilename.Text = String.Empty
    '    optCompanyCorr.Checked = False
    '    dgFiles.Rows.Clear()
    'End Sub
    Private Sub ClearQueryTab()
        T("ClearQueryTab")
        txtClippySearch.Text = String.Empty
        dgClippyResults.Rows.Clear()
    End Sub
    Private Sub ClearQATab()
        T("ClearQATab")
        QASearch.Text = String.Empty
        tv.Nodes.Clear()
    End Sub
#End Region

    Private Sub LoadConsultantsToCbo(studyYear As String)
        T("LoadConsultantsToCbo")
        Dim consultants As List(Of String) = _powerSvc.GetAllConsultants(studyYear)
        For Each c In consultants
            cboConsultant.Items.Add(c)
        Next
    End Sub

    Private Function PopulateForm() As Boolean
        T("PopulateForm")
        Try
            If cboSiteId.SelectedItem.ToString().Trim().Length < 1 Then Return False
            Try
                _pathToPriorDatachecksFile = System.Configuration.ConfigurationManager.AppSettings("RefiningPathToPriorDatachecksFile").ToString()
                _pathToCurrentDatachecksFile = System.Configuration.ConfigurationManager.AppSettings("RefiningPathToCurrentDatachecksFile").ToString()
            Catch exDataCheckConfig As System.Exception
                _dAL.WriteLog("MainConsole.Initialize(): Can't find app setting for either RefiningPathToPriorDatachecksFile  or  RefiningPathToCurrentDatachecksFile", exDataCheckConfig)
            End Try
            Dim assembly As Assembly = System.Reflection.Assembly.GetExecutingAssembly()
            Dim version As System.Version = assembly.GetName().Version
            txtVersion.Text = String.Format("Version {0}", version)
            
            'lblStatus is the status label at bottom of form
            lblStatus.ForeColor = Color.DarkGreen
            lblStatus.Text = My.Settings.News

            Return True
        Catch ex As Exception
            MsgBox("Console Error: " + ex.Message)
            Return False
        End Try
    End Function

    Private Function PopulateTabs() As Boolean
        T("PopulateTabs")
        If Not PopulateDragDropTab() Then Return False
        'If Not PopulateTimeGradeTab() Then Return False

        RemoveTab("tabClippy")
        RemoveTab("tabQA")

        RemoveTab("tabTimeGrade")
        If Not PopulateSecureSendTab() Then Return False
        'If Not PopulateContinuingIssuesTab() Then Return False
        If Not PopulateNotesTab() Then Return False
        'If Not PopulateChecklistTab() Then Return False
        If Not PopulateCorrespondenceTab() Then Return False
        If Not PopulateContactsTab() Then Return False
        ' ConsoleTabs.SelectTab(tabContacts)
        'RemoveTab("")
        'RemoveTab("")
        Return True
    End Function
    Private Sub RemoveTab(tabname As String)
        T("RemoveTab")
        Dim tab As TabPage
        tab = ConsoleTabs.TabPages(tabname)
        If Not IsNothing(tab) Then


            ConsoleTabs.TabPages.Remove(tab)
        End If
    End Sub
    Private Function PopulateContactsTab() As Boolean
        T("PopulateContactsTab")
        'If _refNum.Length < 1 Then Return True
        Try
            PopulateCoord()
            PopulateAlt()
            PopulateInterim()
            PopulateGenCorresp()


            'BuildContactTab()
            Return True
        Catch ex As Exception
            MsgBox("Console Error: " + ex.Message)
            Return False
        End Try
    End Function
    Private Function PopulateCoord() As Boolean
        T("PopulateCoord")
        Try
            Dim coord As SA.Console.Common.PowerClientInfo = _powerSvc.GetClientInfo(_siteId)
            lblName.Text = coord.CoordName
            lblEmail.Text = coord.CoordEMail
            lblPhone.Text = coord.CoordPhone
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Private Function PopulateAlt() As Boolean
        T("PopulateAlt")
        Try
            Dim coord As SA.Console.Common.Contact = _powerSvc.GetCompanyContactInfo(_siteId, GetStudyYearFromCbo(), "ALT")
            lblAltName.Text = coord.FirstName & " " & coord.LastName
            lblAltEmail.Text = coord.Email
            lblAltPhone.Text = coord.Phone
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function PopulateInterim() As Boolean
        T("PopulateInterim")
        Try
            Dim coord As SA.Console.Common.Contact = _powerSvc.GetCompanyContactInfo(_siteId, GetStudyYearFromCbo(), "2PLNT")
            lblInterimName.Text = coord.FirstName & " " & coord.LastName
            lblInterimEmail.Text = coord.Email
            lblInterimPhone.Text = coord.Phone
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Private Function PopulateGenCorresp() As Boolean
        T("PopulateGenCorresp")
        Try
            Dim coord As SA.Console.Common.Contact = _powerSvc.GetCompanyContactInfo(_siteId, GetStudyYearFromCbo(), "2GCOR")
            lblPCCName.Text = coord.FirstName & " " & coord.LastName
            lblPCCEmail.Text = coord.Email
            txtGCorr.Text = coord.Phone
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Private Function PopulateCorrespondenceTab() As Boolean
        T("PopulateCorrespondenceTab")
        Try
            BuildCorrespondenceTab()
            Return True
        Catch ex As Exception
            MsgBox("Console Error: " + ex.Message)
            Return False
        End Try
    End Function
    Private Function PopulateChecklistTab() As Boolean
        T("PopulateChecklistTab")
        Try
            _checklistManager = New PowerChecklistManager(_dAL, _siteId, UserName)
            BuildCheckListTab("A")
            ' Y for completed, N for not completed, A for all
            cboCheckListView.SelectedIndex = 0
            Return True
        Catch ex As Exception
            MsgBox("Console Error: " + ex.Message)
            Return False
        End Try
    End Function
    Private Function PopulateNotesTab() As Boolean
        T("PopulateNotesTab")
        BuildNotesTab()
    End Function
    Private Function PopulateContinuingIssuesTab() As Boolean
        T("PopulateContinuingIssuesTab")
        Try
            Dim params = New List(Of String)
            dgContinuingIssues.Rows.Clear()
            dgContinuingIssues.Height = 385
            btnSaveCI.Enabled = False
            btnNewIssue.Enabled = True

            'this is refnum based, won't show anything until you click on a refnum!!!!

            If _refNum.Length < 1 Then Exit Function
            params.Add("RefNum/" + _refNum)
            Dim dt As DataTable = _powerSvc.GetContinuingIssues(_refNum)
            If Not IsNothing(dt) AndAlso dt.Rows.Count > 0 Then
                For Each dr In dt.Rows
                    If dr("note").ToString().Trim().Length > 0 Then
                        dgContinuingIssues.Rows.Add({dr("IssueID").ToString, Convert.ToDateTime(dr("EntryDate")), dr("note").ToString, dr("consultant").ToString, dr("deletedby").ToString, dr("deletedDate").ToString, dr("deleted").ToString})
                    End If
                Next
                btnExportCI.Enabled = True
            End If
            
        Catch ex As Exception
            MsgBox("Console Error: " + ex.Message)
            Return False
        End Try
    End Function
    Private Function PopulateSecureSendTab() As Boolean
        T("PopulateSecureSendTab")
        Try
            BuildSecureSendTab()
            Return True
        Catch ex As Exception
            MsgBox("Console Error: " + ex.Message)
            Return False
        End Try
    End Function
    Private Function PopulateTimeGradeTab() As Boolean
        T("PopulateTimeGradeTab")
        Try
            'ShowTimeGrade()
            Return True
        Catch ex As Exception
            MsgBox("Console Error: " + ex.Message)
            Return False
        End Try
    End Function
    Private Function PopulateDragDropTab() As Boolean
        T("PopulateDragDropTab")
        Try
            'this really needs to be empty until someone drags and drops.
            'So there is nothing to do to prep this tab
            Return True
        Catch ex As Exception
            MsgBox("Console Error: " + ex.Message)
            Return False
        End Try
    End Function
    Private Function CreateConsoleFolder(path As String) As Boolean
        T("CreateConsoleFolder")
        Try
            If Not Directory.Exists(path) Then
                Directory.CreateDirectory(path)
            End If
            Return True
        Catch exSecureSendPath As Exception
            MsgBox("Console tried to create a folder at " + path + ", but was unable to. Please create this folder manually and then try running Console again.")
            Return False
        End Try
    End Function

    Private Sub BuildNotesTab()
        T("BuildNotesTab")
        txtNotes.Text = _powerSvc.GetNotes(_refNum)
        If txtNotes.Text.StartsWith("~") Then
            txtNotes.Enabled = False
            btnCreatePN.Text = "Open PN File"
        Else
            txtNotes.Enabled = True
            btnCreatePN.Text = "Create PN File"
        End If
        txtConsultingOpportunities.Text = _powerSvc.GetConsultingOpportunities(_refNum)
    End Sub


    Private Sub BuildCheckListTab(strYesNoAll As String) ' Y for completed, N for not completed, A for all
        T("BuildCheckListTab")
        ClearChecklist()

        Dim yesNoAllLetter As String = String.Empty

        Select Case strYesNoAll
            Case "Complete"
                yesNoAllLetter = "Y"
            Case "Incomplete"
                yesNoAllLetter = "N"
            Case Else
                yesNoAllLetter = "A"
        End Select
        _checklistManager = New PowerChecklistManager(_dAL, _siteId, UserName) ' shortUserName(0))
        UpdateCheckList(yesNoAllLetter)

    End Sub
    Private Sub ClearChecklist()
        T("ClearChecklist")
        tvIssues.Nodes.Clear()
        txtIssueID.Text = ""
        txtIssueName.Text = ""
        txtDescription.Text = ""
    End Sub
    Private Sub UpdateCheckList(status As String)
        T("UpdateCheckList")
        Try
            Dim checklistTable As DataTable = _powerSvc.GetChecklistItems(_siteId, status)

            Dim row As DataRow
            Dim node As TreeNode
            RemoveHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
            RemoveHandler tvIssues.AfterSelect, AddressOf tvIssues_AfterSelect
            tvIssues.Nodes.Clear()

            Dim chosenView As String = cboCheckListView.Text
            If Not IsNothing(checklistTable) Then
                For I = 0 To checklistTable.Rows.Count - 1
                    row = checklistTable.Rows(I)
                    node = tvIssues.Nodes.Add(Utilities.Pad(row("IssueID"), 8) & "-" & row("IssueTitle"))
                    Dim done = row("Completed").ToString()
                    Select Case done.ToUpper()
                        Case "Y"
                            node.Checked = True
                        Case Else
                            node.Checked = False
                    End Select
                Next
            End If
            AddHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
            AddHandler tvIssues.AfterSelect, AddressOf tvIssues_AfterSelect
        Catch ex As Exception
            MsgBox("Error in UpdateCheckList():" & ex.Message)
            Try
                AddHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
            Catch
            End Try
            Try
                AddHandler tvIssues.AfterSelect, AddressOf tvIssues_AfterSelect
            Catch
            End Try
        End Try
    End Sub
    Private Sub BuildCorrespondenceTab()
        T("BuildCorrespondenceTab")
        If _useMFiles Then
            If _mfilesFailed Then Exit Sub
            'RefreshMFilesCollections(0)
        End If
        SetSummaryFileButtons() ' this hanldes the buttons on the right
        lstVFFiles.Items.Clear()
        lstVRFiles.Items.Clear()
        lstReturnFiles.Items.Clear()
        lstVFNumbers.Items.Clear()

        listViewCorrespondenceVF.Clear()
        listViewCorrespondenceVR.Clear()
        listViewCorrespondenceVReturn.Clear()

        If Not _useMFiles Then
            _corrPath = _powerSvc.BuildCorrPath(GetStudyYearFromCbo, _siteId)
            CorrPath = _corrPath
            If (Not _useMFiles) And _corrPath = Nothing Then Exit Sub
            _compCorrPath = _powerSvc.BuildCompanyCorrPath(GetStudyYearFromCbo, _siteId)
        End If
        If _siteId.Length > 0 Then
            Dim vNums As New List(Of String)
            If _useMFiles Then
                Dim errors As String = _powerSvc.GetDataFor_lstVFNumbersMFiles("VF*", vNums, False, _benchmarkingParticipantSiteFiles)
                If errors.Length > 0 Then
                    MsgBox(errors)
                    Return
                End If
                ' Now look for any orphan VR files
                errors = _powerSvc.GetDataFor_lstVFNumbersMFiles("VR*", vNums, False, _benchmarkingParticipantSiteFiles)
                If errors.Length > 0 Then
                    MsgBox(errors)
                    Return
                End If
                errors = _powerSvc.GetDataFor_lstVFNumbersMFiles("VA*", vNums, False, _benchmarkingParticipantSiteFiles)
                If errors.Length > 0 Then
                    MsgBox(errors)
                    Return
                End If
                errors = _powerSvc.GetDataFor_lstVFNumbersMFiles("F_Return*", vNums, True, _benchmarkingParticipantSiteFiles)
                If errors.Length > 0 Then
                    MsgBox(errors)
                    Return
                End If
                'For Each item As String In vNums
                '    lstVFNumbers.Items.Add(item)
                'Next
            Else
                If Not IsNothing(_corrPath) AndAlso _corrPath.Length > 0 Then
                    T("BuildCorrespondenceTab - GetRefNumCboText len > 0")
                    vNums = _docsAccess.GetDataFor_lstVFNumbers("VF*", _corrPath, vNums, Nothing, Nothing)
                    T("BuildCorrespondenceTab - vNums VF")
                    ' Now look for any orphan VR files
                    vNums = _docsAccess.GetDataFor_lstVFNumbers("VR*", _corrPath, vNums, Nothing, Nothing)
                    T("BuildCorrespondenceTab - vNums VR")
                    vNums = _docsAccess.GetDataFor_lstVFNumbers("VA*", _corrPath, vNums, Nothing, Nothing)
                    T("BuildCorrespondenceTab - vNums VA")
                    vNums = _docsAccess.GetDataFor_lstVFNumbers("F_Return*", _corrPath, vNums, Nothing, Nothing, True)
                    T("BuildCorrespondenceTab - vNums F_Return")
                    vNums = _docsAccess.GetDataFor_lstVFNumbers("L_Return*", _corrPath, vNums, Nothing, Nothing, True)
                    T("BuildCorrespondenceTab - vNums L_Return")
                End If
            End If
            For Each item As String In vNums
                lstVFNumbers.Items.Add(item)
            Next
        End If
        lstVFNumbers.SelectedIndex = lstVFNumbers.Items.Count - 1
        If lstVFNumbers.SelectedIndex > -1 Then
            SetCorr(_benchmarkingParticipantSiteFiles) ' populates the 3 listviews in the middle
        Else
            If _useMFiles Then
                listViewCorrespondenceVF.Visible = True
                listViewCorrespondenceVR.Visible = True
                listViewCorrespondenceVReturn.Visible = True
            End If
        End If

        _dataFolderPath = BuildDataFolderPath()
        _templatesFolderPath = ConfigurationManager.AppSettings("PowerTemplatesPath").ToString()

    End Sub


    Sub SetCorr()
        T("SetCorr")
        Dim strDirResult() As String
        Dim choice As String = String.Empty
        If lstVFNumbers.Items.Count > 0 Then
            choice = lstVFNumbers.SelectedItem.ToString()
        End If
        lstVFFiles.Items.Clear()
        lstVRFiles.Items.Clear()
        lstReturnFiles.Items.Clear()

        If _useMFiles Then
            lstVFFiles.Visible = False
            lstVRFiles.Visible = False
            lstReturnFiles.Visible = False

            listViewCorrespondenceVF.Visible = True
            listViewCorrespondenceVR.Visible = True
            listViewCorrespondenceVReturn.Visible = True

            Dim items As New List(Of ConsoleFile)
            Try
                'need a VF file in correspondence folder for this to get populated


                'at some point, call _docsAccess.GetFilesForCorrespondenceTab() instead, so can 
                'add mfiles id to ListViewItem's .Tag

                'PopulateVFFilesMFiles("", "", lstVFNumbers.SelectedItem.ToString(), items)
                PopulateVFFilesMFiles("VF", lstVFNumbers.SelectedItem.ToString(), items, False)
                PopulateVFFilesMFiles("VFFL", lstVFNumbers.SelectedItem.ToString(), items, False)
                PopulateVFFilesMFiles("VR", lstVFNumbers.SelectedItem.ToString(), items, False)
                PopulateVFFilesMFiles("VRFL", lstVFNumbers.SelectedItem.ToString(), items, False)
                PopulateVFFilesMFiles("VA", lstVFNumbers.SelectedItem.ToString(), items, False)
                PopulateVFFilesMFiles("VAFL", lstVFNumbers.SelectedItem.ToString(), items, False)
                PopulateVFFilesMFiles("Return", lstVFNumbers.SelectedItem.ToString(), items, True)

            Catch exlstVRFilesItems As System.Exception

            End Try
            PrepCorrListview2(listViewCorrespondenceVF, "VF", items, lstVFNumbers.SelectedItem, False)
            PrepCorrListview2(listViewCorrespondenceVR, "VR", items, lstVFNumbers.SelectedItem, False)
            PrepCorrListview2(listViewCorrespondenceVR, "VA", items, lstVFNumbers.SelectedItem, True)
            PrepCorrListview2(listViewCorrespondenceVReturn, "Return", items, lstVFNumbers.SelectedItem, False)
        Else

            lstVFFiles.Visible = True
            lstVRFiles.Visible = True
            lstReturnFiles.Visible = True

            listViewCorrespondenceVF.Visible = False
            listViewCorrespondenceVR.Visible = False
            listViewCorrespondenceVReturn.Visible = False

            strDirResult = Directory.GetFiles(_corrPath, "VF" & choice & "*")
            For Each f In strDirResult
                f = Utilities.ExtractFileName(f)
                If lstVFNumbers.SelectedItem = Mid(f, 3, 1) Then
                    lstVFFiles.Items.Add(Format(FileDateTime(_corrPath & f).ToString("dd-MMM-yy") & " | " & f))
                End If
            Next

            strDirResult = Directory.GetFiles(_corrPath, "VR" & choice & "*")
            For Each f In strDirResult
                f = Utilities.ExtractFileName(f)
                If lstVFNumbers.SelectedItem = Mid(f, 3, 1) Then
                    lstVRFiles.Items.Add(Format(FileDateTime(_corrPath & f).ToString("dd-MMM-yy") & " | " & f))
                End If
            Next

            strDirResult = Directory.GetFiles(_corrPath, "VA" & choice & "*")
            For Each f In strDirResult
                f = Utilities.ExtractFileName(f)
                If lstVFNumbers.SelectedItem = Mid(f, 3, 1) Then
                    lstVRFiles.Items.Add(Format(FileDateTime(_corrPath & f).ToString("dd-MMM-yy") & " | " & f))
                End If
            Next

            strDirResult = Directory.GetFiles(_corrPath, "*Return" & choice & "*")
            For Each f In strDirResult
                f = Utilities.ExtractFileName(f)
                If lstVFNumbers.SelectedItem = "" Then
                    lstReturnFiles.Items.Add(Format(FileDateTime(_corrPath & f).ToString("dd-MMM-yy") & " | " & f))
                Else
                    If (lstVFNumbers.SelectedItem = Mid(f, Len(f) - 4, 1) _
                            Or InStr(1, f, "Return" & lstVFNumbers.SelectedItem) > 0) Then
                        lstReturnFiles.Items.Add(Format(FileDateTime(_corrPath & f).ToString("dd-MMM-yy") & " | " & f))
                    End If
                End If
            Next
        End If

    End Sub
    Private Sub PrepCorrListview2(listView As ListView, startsWith As String, items As List(Of ConsoleFile), _
                                vfNumber As String, append As Boolean)
        T("PrepCorrListview2")
        Dim heading As String = String.Empty
        Select Case startsWith.ToUpper()
            Case "VF" ', "VFFL"
                heading = "IDR Fax Files (VF*.*)"
            Case "VR" ', "VRFL"
                heading = "IDR Reply Files (VR*.*)"
            Case Else
                heading = "Return Files"
                If ConsoleVertical = VerticalType.REFINING And startsWith.ToUpper() = "VA" Then
                    heading = "IDR Reply Files (VR*.*)"
                End If
        End Select
        If Not append Then
            If listView.Columns.Count < 1 Then listView.Columns.Add(heading)
            listView.Items.Clear()
            listView.View = Windows.Forms.View.Details
            listView.FullRowSelect = True
            listView.Columns(0).Width = listView.Width - 4 - SystemInformation.VerticalScrollBarWidth
        End If
        For Each item As ConsoleFile In items
            Dim itemText As String = item.FileName.ToString()
            Dim checkedOut As Boolean = itemText.Contains("(Checked out to")
            itemText = itemText.Replace("Checked out to ", "")
            Dim itemParts() As String = Split(itemText, " | ")
            Dim fileName As String = itemParts(1)
            If heading <> "Return Files" Then
                If vfNumber = Mid(fileName, 3, 1) And fileName.StartsWith(startsWith) Then ' OrElse vfNumber = Mid(fileName, 5, 1) And fileName.StartsWith(startsWith & "FL") Then
                    Dim listViewItem As New ListViewItem(itemText)
                    If checkedOut Then
                        listViewItem.BackColor = Color.Red
                    End If
                    listView.Items.Add(listViewItem)
                    'it seems we must do this after adding item to list else tag will be null
                    If item.Id > 0 Then listView.Items(listView.Items.Count - 1).Tag = item.Id.ToString()
                End If
            Else 'return files box
                If fileName.Contains("Return" + vfNumber) Then
                    Dim listViewItem As New ListViewItem(itemText)
                    If checkedOut Then
                        listViewItem.BackColor = Color.Red
                    End If
                    listView.Items.Add(listViewItem)
                    'it seems we must do this after adding item to list else tag will be null
                    If item.Id > 0 Then listView.Items(listView.Items.Count - 1).Tag = item.Id.ToString()
                End If
            End If
        Next
    End Sub


    Sub SetCorr(consoleFilesInfo As ConsoleFilesInfo)
        T("SetCorr(consoleFilesInfo)")
        Dim strDirResult() As String
        Dim choice As String = String.Empty
        If lstVFNumbers.Items.Count > 0 Then
            choice = lstVFNumbers.SelectedItem.ToString()
        End If
        lstVFFiles.Items.Clear()
        lstVRFiles.Items.Clear()
        lstReturnFiles.Items.Clear()

        If _useMFiles Then
            lstVFFiles.Visible = False
            lstVRFiles.Visible = False
            lstReturnFiles.Visible = False

            listViewCorrespondenceVF.Visible = True
            listViewCorrespondenceVR.Visible = True
            listViewCorrespondenceVReturn.Visible = True

            'prior to SteCorr, call _docsAccess.GetFilesForCorrespondenceTab() instead, so can 
            'add mfiles id to ListViewItem's .Tag

            If Not PrepCorrListview(listViewCorrespondenceVF, "VF", consoleFilesInfo, lstVFNumbers.SelectedItem, False) Then Exit Sub
            If Not PrepCorrListview(listViewCorrespondenceVR, "VR", consoleFilesInfo, lstVFNumbers.SelectedItem, False) Then Exit Sub
            If Not PrepCorrListview(listViewCorrespondenceVR, "VA", consoleFilesInfo, lstVFNumbers.SelectedItem, True) Then Exit Sub
            If Not PrepCorrListview(listViewCorrespondenceVReturn, "Return", consoleFilesInfo, lstVFNumbers.SelectedItem, False) Then Exit Sub
        Else

            lstVFFiles.Visible = True
            lstVRFiles.Visible = True
            lstReturnFiles.Visible = True

            listViewCorrespondenceVF.Visible = False
            listViewCorrespondenceVR.Visible = False
            listViewCorrespondenceVReturn.Visible = False

            strDirResult = Directory.GetFiles(_corrPath, "VF" & choice & "*")

            For Each f In strDirResult
                f = Utilities.ExtractFileName(f)
                If lstVFNumbers.SelectedItem = Mid(f, 3, 1) Then
                    lstVFFiles.Items.Add(Format(FileDateTime(_corrPath & f).ToString("dd-MMM-yy") & " | " & f))
                End If
            Next

            strDirResult = Directory.GetFiles(_corrPath, "VR" & choice & "*")
            For Each f In strDirResult
                f = Utilities.ExtractFileName(f)
                If lstVFNumbers.SelectedItem = Mid(f, 3, 1) Then
                    lstVRFiles.Items.Add(Format(FileDateTime(_corrPath & f).ToString("dd-MMM-yy") & " | " & f))
                End If
            Next

            strDirResult = Directory.GetFiles(_corrPath, "VA" & choice & "*")
            For Each f In strDirResult
                f = Utilities.ExtractFileName(f)
                If lstVFNumbers.SelectedItem = Mid(f, 3, 1) Then
                    lstVRFiles.Items.Add(Format(FileDateTime(_corrPath & f).ToString("dd-MMM-yy") & " | " & f))
                End If
            Next

            strDirResult = Directory.GetFiles(_corrPath, "*Return" & choice & "*")
            For Each f In strDirResult
                f = Utilities.ExtractFileName(f)
                If lstVFNumbers.SelectedItem = "" Then
                    lstReturnFiles.Items.Add(Format(FileDateTime(_corrPath & f).ToString("dd-MMM-yy") & " | " & f))
                Else
                    If (lstVFNumbers.SelectedItem = Mid(f, Len(f) - 4, 1) _
                            Or InStr(1, f, "Return" & lstVFNumbers.SelectedItem) > 0) Then
                        lstReturnFiles.Items.Add(Format(FileDateTime(_corrPath & f).ToString("dd-MMM-yy") & " | " & f))
                    End If
                End If
            Next
        End If

    End Sub

    Private Function PrepCorrListview(listView As ListView, startsWith As String, files As ConsoleFilesInfo, _
                                 vfNumber As String, append As Boolean) As Boolean
        T("PrepCorrListview")
        Try
            Dim heading As String = String.Empty
            Select Case startsWith.ToUpper()
                Case "VF"
                    heading = "IDR Fax Files (VF*.*)"
                Case "VR"
                    heading = "IDR Reply Files (VR*.*)"
                Case Else
                    heading = "Return Files"
                    If ConsoleVertical = VerticalType.REFINING And startsWith.ToUpper() = "VA" Then
                        heading = "IDR Reply Files (VR*.*)"
                    End If
            End Select
            If Not append Then
                If listView.Columns.Count < 1 Then listView.Columns.Add(heading)
                listView.Items.Clear()
                listView.View = Windows.Forms.View.Details
                listView.FullRowSelect = True
                listView.Columns(0).Width = listView.Width - 4 - SystemInformation.VerticalScrollBarWidth
            End If
            For Each cf As ConsoleFile In files.Files
                Dim checkedOut As Boolean = Not String.IsNullOrEmpty(cf.CheckedOutToUserName)
                Dim itemText As String = String.Empty
                If checkedOut Then
                    itemText = "(" & cf.CheckedOutToUserName & ") "
                End If
                itemText = itemText & cf.FileName & "." & cf.FileExtension
                If heading <> "Return Files" Then
                    If vfNumber = Mid(cf.FileName, 3, 1) And cf.FileName.StartsWith(startsWith) Then
                        Dim listViewItem As New ListViewItem(itemText)
                        If cf.Id > 0 Then
                            listViewItem.Tag = cf.Id.ToString()
                        End If
                        If checkedOut Then
                            listViewItem.BackColor = Color.Red
                        End If
                        listView.Items.Add(listViewItem)
                    End If
                Else 'return files box
                    If cf.FileName.Length > 4 Then
                        If vfNumber = Mid(cf.FileName, Len(cf.FileName) - 4, 1) Or _
                            cf.FileName.Contains("Return" + vfNumber) Then
                            Dim listViewItem As New ListViewItem(itemText)
                            If cf.Id > 0 Then
                                listViewItem.Tag = cf.Id.ToString()
                            End If
                            If checkedOut Then
                                listViewItem.BackColor = Color.Red
                            End If
                            listView.Items.Add(listViewItem)
                        End If
                    End If
                End If
            Next
            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function


    Private Sub T(methodName As String) 'short for Trace
        If Not _tracing Then Exit Sub
        Try
            Using sr As New StreamWriter(_tracingFile, True)
                sr.WriteLine(methodName)
            End Using
        Catch
        End Try
    End Sub



    Private Sub cboSiteId_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboSiteId.SelectionChangeCommitted
        T("cboSiteId_SelectionChangeCommitted")
        SiteIdChanged()
    End Sub
    Private Sub SiteIdChanged()
        T("SiteIdChanged")
        _refNum = String.Empty
        _siteId = GetSiteIdFromCbo()
        'ClearForm() gets done in ClearAllControls()
        ClearAllControls()
        ChangeCboSiteId(_siteId)
        LoadConsultantsToCbo(GetStudyYearFromCbo().ToString())
        Dim consultant As String = _powerSvc.GetConsultantInitials(_siteId)
        ChangeCboConsultant(consultant)
        btnCompanyPW.Text = _powerSvc.GetCompanyPassword(_siteId, GetStudyYearFromCbo().ToString())
        btnReturnPW.Text = _powerSvc.GetReturnFilePassword(_siteId)

        'ClearAllControls()

        cboCheckListView.SelectedIndex = 0
        cboCheckListView.Text = "All"  'to get checklist to populate after clearing out controls
        PopulateChecklistTab()
        PopulateContactsTab()
        PopulateCorrespondenceTab()
        ClearContinuingIssuesTab()
        PopulateContinuingIssuesTab()
        _clientAttachmentsPath = "K:\STUDY\" & _studyType & "\" & GetStudyYearFromCbo() & "\Plant\" & _siteId & "\"
        _corrPath = _powerSvc.BuildCorrPath(GetStudyYearFromCbo(), _siteId) ' "K:\STUDY\" & _studyType & "\" & GetStudyYearFromCbo() & "\Plant\" & _siteId & "\"
        CorrPath = _corrPath
        _compCorrPath = _powerSvc.BuildCompanyCorrPath(GetStudyYearFromCbo(), _siteId)
        _drawingPath = "K:\STUDY\" & _studyType & "\" & GetStudyYearFromCbo() & "\New Participants Set Up\Process Diagrams\" & _siteId & "\"
        PopulateSecureSendTab()
        'PopulateTabs()
        'GetContactInfo()
        'PopulateNotesTab()
        'PopulateContinuingIssuesTab()


    End Sub

    Private Function GetSiteIdFromCbo() As String
        T("GetSiteIdFromCbo")
        Dim result As String = String.Empty
        Dim parts As String() = cboSiteId.SelectedItem.ToString().Split(Chr(10))
        Return parts(1).Remove(0, 1)
    End Function

    Private Function GetStudyYearFromCbo() As Integer
        T("GetStudyYearFromCbo")
        Dim result As Integer = -1
        Dim part As String = cboStudy.SelectedItem.ToString()
        If part.Length > 0 Then
            Return Int32.Parse(part)
        Else
            Return -1
        End If
    End Function

    Private Function ChangeCboSiteId(newSiteID As String)
        T("ChangeCboSiteId")
        Try
            listViewRefnums.Clear()
            If Not IsNothing(cboSiteId.SelectedItem) AndAlso cboSiteId.SelectedItem.ToString().Length > 0 AndAlso GetStudyYearFromCbo() > 0 Then
                Dim parts As String() = cboSiteId.SelectedItem.ToString().Split(Chr(10))
                Dim site As String = parts(1).TrimStart(Chr(45))
                Dim refnums As List(Of String) = _powerSvc.GetUnitnamesWithRefNums(site, GetStudyYearFromCbo())
                If Not IsNothing(refnums) Then
                    For Each refnum In refnums
                        listViewRefnums.Items.Add(refnum) '& Environment.NewLine
                    Next
                End If
                _companyName = _powerSvc.GetCompanyName(site).Trim()
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub GetContactInfo()
        T("GetContactInfo")
        Dim clientInfo As SA.Console.Common.PowerClientInfo = _powerSvc.GetClientInfo(GetSiteIdFromCbo())
        If Not IsNothing(clientInfo) Then
            lblName.Text = clientInfo.CoordName
            lblEmail.Text = clientInfo.CoordEMail
            lblPhone.Text = clientInfo.CoordPhone
        End If

    End Sub

    Private Sub ChangeCboConsultant(newConsultant As String)
        T("ChangeCboConsultant")
        For Each item In cboConsultant.Items
            If item.ToString().Trim().ToUpper = newConsultant.Trim().ToUpper() Then
                cboConsultant.SelectedItem = item
                Return
            End If
        Next
        'End If
    End Sub

    Private Sub btnShow_Click(sender As Object, e As EventArgs) Handles btnShow.Click
        T("btnShow_Click")
        _powerCoordContactForm = New PowerCoordContactForm(_siteId, _dAL)
        _powerCoordContactForm.Show()
    End Sub

    Private Sub btnShowAltCo_Click(sender As Object, e As EventArgs) Handles btnShowAltCo.Click
        T("btnShowAltCo_Click")
        _contactForm = New ContactFormPopup(UserName, "ALT", _siteId, cboSiteId.SelectedItem.ToString().Trim(), String.Empty, GetStudyYearFromCbo().ToString(), _dAL)
        _contactForm.Show()
        lblAltName.Text = String.Empty
        lblAltEmail.Text = String.Empty
        lblAltPhone.Text = String.Empty
        'PopulateAlt()
    End Sub

    Private Sub btnShowIntCo_Click(sender As Object, e As EventArgs) Handles btnShowIntCo.Click
        T("btnShowIntCo_Click")
        _contactForm = New ContactFormPopup(UserName, "2PLNT", _siteId, cboSiteId.SelectedItem.ToString().Trim(), String.Empty, GetStudyYearFromCbo().ToString(), _dAL)
        _contactForm.Show()
        lblInterimName.Text = String.Empty
        lblInterimEmail.Text = String.Empty
        lblInterimPhone.Text = String.Empty
    End Sub

    Private Sub btnGeneralCorrespondence_Click(sender As Object, e As EventArgs) Handles btnGeneralCorrespondence.Click
        T("btnGeneralCorrespondence_Click")
        _contactForm = New ContactFormPopup(UserName, "2GCOR", _siteId, cboSiteId.SelectedItem.ToString().Trim(), String.Empty, GetStudyYearFromCbo().ToString(), _dAL)
        _contactForm.Show()
        lblPCCName.Text = String.Empty
        lblPCCEmail.Text = String.Empty
        txtGCorr.Text = String.Empty
        'PopulateAlt()
    End Sub

    Private Sub listViewRefnums_ItemSelectionChanged(sender As Object, e As ListViewItemSelectionChangedEventArgs) Handles listViewRefnums.ItemSelectionChanged
        T("listViewRefnums_ItemSelectionChanged")
        If listViewRefnums.Items.Count < 1 Then
            _refNum = String.Empty
        Else
            Select Case listViewRefnums.SelectedItems.Count
                Case 0
                    _refNum = String.Empty
                Case 1
                    Dim temp As String = listViewRefnums.SelectedItems(0).Text
                    Dim temps As String() = temp.Split(Chr(10))
                    _refNum = temps(1).Trim().Replace("-", "")
                    BuildNotesTab()
                    PopulateContinuingIssuesTab()
                    PopulateNotesTab()
                    'PopulateChecklistTab()
                    'PopulateContactsTab()
                Case Else
                    _refNum = String.Empty
                    MsgBox("Please select only one refnum and try again")
            End Select
        End If
    End Sub

    Private Function ParseIssueId(nodetext As String) As String
        T("ParseIssueId")
        Dim idx As Integer = nodetext.IndexOf(" -")
        Dim result As String = nodetext.Substring(0, idx).Trim()
        Return result
    End Function
    Private Sub tvIssues_AfterCheck(sender As Object, e As TreeViewEventArgs) Handles tvIssues.AfterCheck
        T("tvIssues_AfterCheck")
        Try
            _checklistNodeChecked = True
            Dim issueId As String = ParseIssueId(e.Node.Text).Trim()

            Dim results As PowerChecklistResults = _
            _checklistManager.GetResults(cboCheckListView.Text, "V", issueId, e.Node.Checked, Nothing)
            'RemoveHandler cboConsultant.SelectedIndexChanged, AddressOf cboConsultant_SelectedIndexChanged
            RemoveHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
            e.Node.Checked = results.IsChecked
            AddHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
            txtIssueID.Text = results.BoxesContent(0)
            txtIssueName.Text = results.BoxesContent(1)
            txtDescription.Text = results.BoxesContent(2)
            RebuildChecklist(_dAL, _siteId, cboCheckListView.Text)
            ShadeChecklist(results)
        Catch ex As Exception
            MsgBox("Error in tvIssues_AfterCheck(): " & ex.Message)
            Try
                AddHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
            Catch
            End Try
        End Try
    End Sub
    Private Sub ShadeChecklist(results As PowerChecklistResults)
        T("ShadeChecklist")
        If Not IsNothing(results.ShadedSelectedNodeText) AndAlso results.ShadedSelectedNodeText.Length > 1 Then
            For Each n As TreeNode In tvIssues.Nodes
                If ParseIssueId(n.Text).Trim() = ParseIssueId(results.ShadedSelectedNodeText) Then
                    n.BackColor = Color.LightGray
                    tvIssues.SelectedNode = n
                Else
                    n.BackColor = Color.Empty
                End If
            Next
        End If
    End Sub

    Private Sub RebuildChecklist(db As DataObject, _siteId As String, AIC As String)
        T("RebuildChecklist")
        Try
            tvIssues.Nodes.Clear() '
            _checklistManager = New PowerChecklistManager(db, _siteId, UserName)
            _checklistDataTable = _powerSvc.GetAllChecklistItems(_siteId)
            'db.SQL = "SELECT SiteId,IssueID,IssueTitle,IssueText,PostedBy,PostedTime,Completed,SetBy,SetTime FROM VAL.CHECKLIST WHERE RTRIM(SiteId) = '" & _siteId.Trim() & "'"
            If Not IsNothing(_checklistDataTable) Then
                Select Case AIC.Substring(0, 1).ToUpper()
                    Case "A"
                        For Each row As DataRow In _checklistDataTable.Rows
                            If row("Completed") = "Y" Then
                                Dim item As String = row("IssueId") & " - " & row("IssueTitle")
                                Dim n As TreeNode = New TreeNode(item)
                                n.Checked = True
                                tvIssues.Nodes.Add(n)
                            End If
                        Next
                        For Each row As DataRow In _checklistDataTable.Rows

                            If row("Completed") <> "Y" Then
                                Dim item2 As String = row("IssueId") & " - " & row("IssueTitle")
                                Dim n2 As TreeNode = New TreeNode(item2)
                                n2.Checked = False
                                tvIssues.Nodes.Add(n2)
                            End If
                        Next
                    Case Else
                        For Each row As DataRow In _checklistDataTable.Rows
                            Dim item As String = row("IssueId") & " - " & row("IssueTitle")
                            Dim n As TreeNode = New TreeNode(item)
                            If row("Completed") = "Y" Then n.Checked = True Else n.Checked = False
                            If (AIC.StartsWith("C") AndAlso n.Checked) Or (AIC.StartsWith("I") AndAlso Not n.Checked) Then
                                tvIssues.Nodes.Add(n)
                            End If
                        Next
                End Select
            End If
            'cboCheckListView.SelectedIndex = 0
            radChecklistAddNew.Checked = False
            radChecklistEditIssue.Checked = True
        Catch ex As Exception
            MsgBox("Error in RebuildChecklist(): " & ex.Message())
        Finally
            'db.SQL = String.Empty
        End Try
    End Sub
    Private Sub tvIssues_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles tvIssues.AfterSelect
        T("tvIssues_AfterSelect")
        If _checklistNodeChecked Then
            _checklistNodeChecked = False
            If txtIssueID.Text.Trim().Length > 0 Then
                txtIssueID.ReadOnly = True
            End If
            Exit Sub
        End If
        Dim issueId As String = ParseIssueId(tvIssues.SelectedNode.Text).Trim()

        Dim v As String = "VIEW"
        'If radChecklistAddNew.Checked Then v = "ADD"

        radChecklistAddNew.Checked = False
        radChecklistEditIssue.Checked = True

        Dim results As PowerChecklistResults = _
        _checklistManager.GetResults(cboCheckListView.Text, v, issueId, Nothing, Nothing)
        txtIssueID.Text = results.BoxesContent(0)
        txtIssueName.Text = results.BoxesContent(1)
        txtDescription.Text = results.BoxesContent(2)
        ShadeChecklist(results)
        If txtIssueID.Text.Trim().Length > 0 Then
            txtIssueID.ReadOnly = True
        End If
    End Sub


    Private Sub btnUpdateIssue_Click(sender As Object, e As EventArgs) Handles btnUpdateIssue.Click
        T("btnUpdateIssue_Click")
        Dim v As String = "VIEW"
        If radChecklistAddNew.Checked Then v = "ADD"
        Dim ckd As Boolean = False
        Dim issueId As String = txtIssueID.Text.Trim()
        Dim changes As New List(Of String)
        changes.Add(txtIssueID.Text.Trim())
        changes.Add(txtIssueName.Text.Trim())
        changes.Add(txtDescription.Text.Trim())
        If IsNothing(_checklistManager) Then
            _checklistManager = New PowerChecklistManager(_dAL, _siteId, UserName)
        End If
        Dim results As PowerChecklistResults = _
        _checklistManager.GetResults(cboCheckListView.Text, v, issueId, Nothing, changes)
        txtIssueID.Text = results.BoxesContent(0)
        txtIssueName.Text = results.BoxesContent(1)
        txtDescription.Text = results.BoxesContent(2)

        If v = "ADD" Then
            If txtIssueID.Text.Trim().Length < 1 Then
                MsgBox("Issue ID must not be blank")
                Exit Sub
            End If
            Dim n2 As New TreeNode(results.BoxesContent(0) & " - " & results.BoxesContent(1))
            n2.Checked = False
            tvIssues.Nodes.Add(n2)
            tvIssues.Refresh()
        End If
        RebuildChecklist(_dAL, _siteId, cboCheckListView.Text)
        ShadeChecklist(results)

        radChecklistEditIssue.Checked = True
    End Sub

    Private Sub radChecklistAddNew_Click(sender As Object, e As EventArgs) Handles radChecklistAddNew.Click
        T("radChecklistAddNew_Click")
        radChecklistEditIssue.Checked = False
        txtIssueID.ReadOnly = False
        txtIssueID.Text = String.Empty
        txtIssueName.Text = String.Empty
        txtDescription.Text = String.Empty
        tvIssues.SelectedNode = Nothing
        Dim results As New PowerChecklistResults()
        For Each n As TreeNode In tvIssues.Nodes
            n.BackColor = Color.Empty
        Next
        tvIssues.SelectedNode = Nothing
    End Sub

    Private Sub radChecklistEditIssue_Click(sender As Object, e As EventArgs) Handles radChecklistEditIssue.Click
        T("radChecklistEditIssue_Click")
        radChecklistAddNew.Checked = False
        txtDescription.ReadOnly = False
        txtIssueID.ReadOnly = True
    End Sub

    Private Sub btnNewIssue_Click(sender As Object, e As EventArgs) Handles btnNewIssue.Click
        T("btnNewIssue_Click")
        If IsNothing(_refNum) OrElse _refNum.Length < 1 Then
            MsgBox("Please select a refnum and try again.")
            Return
        End If
        dgContinuingIssues.Height = 220
        txtCI.Text = ""
        btnSaveCI.Text = "Save Issue"
        btnSaveCI.Enabled = True
        btnNewIssue.Enabled = False
    End Sub

    Private Sub btnSaveCI_Click(sender As Object, e As EventArgs) Handles btnSaveCI.Click
        T("btnSaveCI_Click")
        Try
            Dim testText As String = Utilities.FixQuotes(txtCI.Text.Replace("/", "-"))
            Me.Cursor = Cursors.WaitCursor
            Dim rowToUse As Integer = 0
            If dgContinuingIssues.Rows.Count < 1 Then
                If Not SaveCI() Then
                    MsgBox("Continuing Issue not saved")
                    Exit Sub
                End If
            Else
                dgContinuingIssues.Height = 385
                'no need to save if both blank.
                If _ciTextBeingEdited.Trim().Length < 1 AndAlso testText.Trim().Length < 1 Then
                    Return
                End If
                If _ciTextBeingEdited.ToUpper() <> testText.ToUpper() Then
                    If Not SaveCI() Then
                        MsgBox("Continuing Issue not saved")
                        Exit Sub
                    End If
                End If
            End If
            txtCI.Text = String.Empty
            ClearContinuingIssuesTab()
            PopulateContinuingIssuesTab()
        Catch ex As Exception
            MsgBox("Error in btnSaveCI_Click: " & ex.Message)
        Finally
            btnSaveCI.Enabled = False
            btnNewIssue.Enabled = True
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Function SaveCI() As Boolean 'Optional CalledByLostFocus As Boolean = False) As Boolean
        T("SaveCI")
        Dim whatToSave As String = Utilities.FixQuotes(txtCI.Text.Replace("/", "-"))
        Dim ds As New DataSet()
        Dim params As New List(Of String)
        params = New List(Of String)
        params.Add("RefNum/" + _refNum)

        params.Add("EditedBy/" + UserName)
        params.Add("Issue/" & whatToSave)

        If btnSaveCI.Text = "Update Issue" Then
            params.Add("ID/" + _ciID.ToString())
            ds = _dAL.ExecuteStoredProc("Console.AddOrUpdateContinuingIssues", params)
            Try
                _ciID = ds.Tables(0).Rows(0)(0)
                'confirm save
                ds = _powerSvc.GetContinuingIssue(_refNum, _ciID)
            Catch exSave As Exception
                Dim errMsg As String = exSave.Message
            End Try
        Else
            'save new issue
            params.Add("ID/" + "-100")
            ds = _dAL.ExecuteStoredProc("Console.AddOrUpdateContinuingIssues", params)

            '_mgr.GetContinuingIssueByConsultant("YYYNA1C16", "SFB");
            ds = _powerSvc.GetContinuingIssueByConsultant(_refNum, UserName)
            'Dim sql As String = "select Note from [Console].[ContinuingIssues] where RefNum = '" & _refNum &
            '    "' and Consultant = '" & UserName & "' and Deleted = 0 order by IssueID Desc"
            '_dAL.SQL = sql
            'ds = _dAL.Execute()
            '_dAL.SQL = ""
        End If

        Dim match As Boolean = False
        If Not ds Is Nothing AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 Then
            For Each r As DataRow In ds.Tables(0).Rows
                Dim note As String = r("note").ToString().Trim()
                If note = whatToSave.Trim() Then
                    match = True
                End If
            Next
        End If
        If match Then
            Return True
        Else
            Dim descriptiveError As String = String.Empty
            If ds Is Nothing OrElse ds.Tables.Count = 0 OrElse ds.Tables(0).Rows.Count = 0 Then
                descriptiveError = " No data was saved to the database."
            ElseIf ds.Tables(0).Rows(0)("Note").ToString() <> whatToSave Then
                descriptiveError = " Note was found in the database, but it is not an exact match with what you were trying to save."
            End If
        End If
        Return False
    End Function

    Private Sub dgContinuingIssues_Click(sender As Object, e As EventArgs) Handles dgContinuingIssues.Click
        T("dgContinuingIssues_Click")

        Dim row As DataGridViewRow
        dgContinuingIssues.Height = 240
        txtCI.Text = ""
        If dgContinuingIssues.SelectedRows.Count > 0 Then
            row = dgContinuingIssues.SelectedRows(0)
            _ciID = Int32.Parse(row.Cells(0).Value)
            txtCI.Text = row.Cells(2).Value.ToString
            _ciTextBeingEdited = row.Cells(2).Value.ToString
            'CIConsultant = row.Cells(3).Value.ToString
            'CIIssueDate = row.Cells(1).Value.ToString
            'CIDeletedBy = row.Cells(4).Value.ToString
            'CIDeletedDate = row.Cells(5).Value.ToString
            'CIDeleted = row.Cells(6).Value.ToString
            btnSaveCI.Text = "Update Issue"
            btnNewIssue.Enabled = False
        End If
    End Sub

    Private Sub DeleteContinuingIssue(id As Integer)
        T("DeleteContinuingIssue")
        Dim params As New List(Of String)
        params = New List(Of String)
        params.Add("IssueID/" + _ciID)
        params.Add("DeletedBy/" + UserName)
        _dAL.ExecuteStoredProc("Console.DeleteContinuingIssue", params)
    End Sub
    Private Sub dgContinuingIssues_KeyDown(sender As Object, e As KeyEventArgs) Handles dgContinuingIssues.KeyDown
        T("dgContinuingIssues_KeyDown")
        If dgContinuingIssues.SelectedRows.Count > 0 Then
            Dim Row As DataGridViewRow = dgContinuingIssues.SelectedRows(0)
            If e.KeyCode = Keys.Delete Then
                Dim dr As DialogResult = MessageBox.Show("This will remove this issue from list, are you sure you want to delete?", "Wait", MessageBoxButtons.YesNo)
                If dr = System.Windows.Forms.DialogResult.Yes Then
                    DeleteContinuingIssue(Row.Cells(0).Value)
                    'BuildContinuingIssuesTab()
                    PopulateContinuingIssuesTab()
                    txtCI.Text = ""
                End If
            End If
        End If
    End Sub

    Private Sub btnIssueCancel_Click(sender As Object, e As EventArgs) Handles btnIssueCancel.Click
        T("btnIssueCancel_Click")
        txtCI.Text = ""
        dgContinuingIssues.Height = 385
        btnSaveCI.Text = "Update Issue"
        btnSaveCI.Enabled = False
    End Sub

    Private Sub txtCI_TextChanged(sender As Object, e As EventArgs) Handles txtCI.TextChanged
        T("txtCI_TextChanged")
        If txtCI.Text.Length > 1 Then
            btnSaveCI.Enabled = True
        End If
    End Sub

    Private Sub cboCheckListView_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboCheckListView.SelectionChangeCommitted
        T("cboCheckListView_SelectionChangeCommitted")
        RebuildChecklist(_dAL, _siteId, cboCheckListView.SelectedItem.ToString())
    End Sub

    Private Sub PowerCoordContactForm_UpdateCoord() Handles _powerCoordContactForm.UpdateCoord
        T("PowerCoordContactForm_UpdateCoord")
        PopulateCoord()
        Try
            Dim coord As SA.Console.Common.Contact = _powerSvc.GetCoord(_siteId)
            If Not IsNothing(coord) Then
                If Not IsNothing(coord.FullName) Then lblName.Text = coord.FullName ' Row("CoordName").ToString().Trim()
                If Not IsNothing(coord.Email) Then lblEmail.Text = coord.Email ' Row("CoordEMail").ToString().Trim()
                If Not IsNothing(coord.Phone) Then lblPhone.Text = coord.Phone ' Row("CoordPhone").ToString().Trim()
            End If
        Catch ex As Exception
            MsgBox("Error in UpdateCoord event: " & ex.Message)
        End Try
    End Sub

    Private Sub ContactForm_UpdateAltContact() Handles _contactForm.UpdateAltContact
        T("ContactForm_UpdateAltContact")
        'ClearContactsTab()
        'lblAltName.Text = String.Empty
        'lblAltEmail.Text = String.Empty
        'lblAltPhone.Text = String.Empty
        PopulateAlt()
        PopulateInterim()
        PopulateGenCorresp()
    End Sub

    Private Sub CCEmail1_Click(sender As Object, e As EventArgs) Handles CCEmail1.Click
        T("CCEmail1_Click")
        OpenEmailExtended()
    End Sub
    Private Sub OpenEmailExtended()
        T("OpenEmailExtended")
        Dim toList As String = String.Empty
        Dim ccList As String = String.Empty
        Dim bccList As String = String.Empty

        PrepEmailToCcBcc(cboCCEmail1, lblEmail, toList, ccList, bccList)
        PrepEmailToCcBcc(cboACEmail2, lblAltEmail, toList, ccList, bccList)


        'PrepEmailToCcBcc(cboICEmail, lblInterimEmail, toList, ccList, bccList)
        'PrepEmailToCcBcc(cboRCEmail, lblRefCoEmail, toList, ccList, bccList)
        'PrepEmailToCcBcc(cboRACEmail, lblRefAltEmail, toList, ccList, bccList)
        'PrepEmailToCcBcc(cboPCEmail, lblPricingContactEmail, toList, ccList, bccList)
        'PrepEmailToCcBcc(cboRMEmail, lblRefMgrEmail, toList, ccList, bccList)
        'PrepEmailToCcBcc(cboOMEmail, lblOpsMgrEmail, toList, ccList, bccList)
        'PrepEmailToCcBcc(cboMMEmail, lblMaintMgrEmail, toList, ccList, bccList)
        'PrepEmailToCcBcc(cboTMEmail, lblTechMgrEmail, toList, ccList, bccList)
        'PrepEmailToCcBcc(cboPCCEmail, lblPCCEmail, toList, ccList, bccList)
        Utilities.SendEmail(toList, ccList, "", "", Nothing, True, bccList)
    End Sub

    Private Sub PrepEmailToCcBcc(cbo As ComboBox, emailLabel As TextBox, ByRef toList As String,
                                      ByRef ccList As String, ByRef bccList As String)
        T("PrepEmailToCcBcc")
        If Not IsNothing(cbo) AndAlso cbo.Text.Trim().Length > 0 Then
            If emailLabel.Text.Length > 0 AndAlso emailLabel.Text.Contains("@") Then
                Select Case cbo.Text.Trim().ToUpper()
                    Case "TO"
                        toList += emailLabel.Text + ";"
                    Case "CC"
                        ccList += emailLabel.Text + ";"
                    Case "BCC"
                        bccList += emailLabel.Text + ";"
                    Case Else

                End Select
            End If
        End If
    End Sub

    Private Sub ACEmail2_Click(sender As Object, e As EventArgs)
        T("ACEmail2_Click")
        Dim toList As String = String.Empty
        Dim ccList As String = String.Empty
        Dim bccList As String = String.Empty

        PrepEmailToCcBcc(cboACEmail2, lblAltEmail, toList, ccList, bccList)

        Utilities.SendEmail(toList, ccList, "", "", Nothing, True, bccList)
    End Sub

    Private Sub btnBug_Click(sender As Object, e As EventArgs) Handles btnBug.Click
        T("btnBug_Click")
        If _refNum.Length < 1 Then
            MsgBox("Please click on a RefNum and try again.")
            Return
        End If
        Dim bug As New frmBug(Main.ConsoleVertical, _refNum, UserName, _dAL.ConnectionString.ToString())
        bug.Show()

    End Sub

    Private Sub btnKillProcesses_Click(sender As Object, e As EventArgs) Handles btnKillProcesses.Click
        T("btnKillProcesses_Click")
        Dim dr As New DialogResult()
        dr = MessageBox.Show("This will kill all OPEN Excel and Word Documents.  Please <SAVE> any Word or Excel documents you have open and then click OK when ready", "Warning", MessageBoxButtons.OKCancel)
        If dr = DialogResult.OK Then
            Utilities.KillProcesses("WinWord")
            Utilities.KillProcesses("Excel")
        End If
    End Sub

    Private Sub btnCompanyPW_Click(sender As Object, e As EventArgs) Handles btnCompanyPW.Click
        T("btnCompanyPW_Click")
        Dim pw As String = _powerSvc.GetCompanyPassword(GetSiteIdFromCbo(), GetStudyYearFromCbo())
        If Not IsNothing(pw) AndAlso pw.Length > 0 Then
            Clipboard.SetText(pw)
        Else
            MsgBox("Password is blank!")
        End If
    End Sub

    Private Sub btnReturnPW_Click(sender As Object, e As EventArgs) Handles btnReturnPW.Click
        T("btnReturnPW_Click")
        Dim pw As String = _powerSvc.GetReturnFilePassword(GetSiteIdFromCbo())
        Clipboard.SetText(pw)
    End Sub

    Private Sub btnSavePN_Click(sender As Object, e As EventArgs) Handles btnSavePN.Click
        T("btnSavePN_Click")
        If _refNum.Length < 1 Then
            MsgBox("Please choose a refnum and try again.")
            Return
        End If
        If Not _powerSvc.UpdateNotes(_refNum, txtNotes.Text) Then
            MsgBox("Notes not saved!")
        End If

        If Not _powerSvc.UpdateConsultingOpportunities(_refNum, txtConsultingOpportunities.Text) Then
            MsgBox("Consulting Opportunities not saved!")
        End If
    End Sub


    Private Sub cboConsultant_KeyDown(sender As Object, e As KeyEventArgs) Handles cboConsultant.KeyDown
        T("cboConsultant_KeyDown")
        If e.KeyCode = Keys.Enter Then
            ChangeCboConsultant(cboConsultant.Text)
            _powerSvc.ChangeConsultant(GetSiteIdFromCbo(), GetStudyYearFromCbo(), cboConsultant.Text)
            Dim found As Boolean = False
            For Each itm In cboConsultant.Items
                If itm.ToString().ToUpper = cboConsultant.Text.ToUpper() Then
                    cboConsultant.SelectedItem = itm
                    found = True
                    Exit For
                End If
            Next
            If Not found Then
                cboConsultant.Items.Add(cboConsultant.Text)
            End If
        End If
    End Sub

    Private Sub cboConsultant_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboConsultant.SelectionChangeCommitted
        T("cboConsultant_SelectionChangeCommitted")
        _powerSvc.ChangeConsultant(GetSiteIdFromCbo(), GetStudyYearFromCbo(), cboConsultant.SelectedItem.ToString())
        For Each item In cboConsultant.Items
            If item.ToString() = cboConsultant.SelectedItem.ToString() Then
                cboConsultant.SelectedItem = item
                Exit Sub
            End If
        Next
    End Sub

    Private Sub btnValidate_Click(sender As Object, e As EventArgs) Handles btnValidate.Click
        T("btnValidate_Click")
        Dim path As String = _powerSvc.GetIdrXlsPath(Int32.Parse(GetStudyYearFromCbo()), _siteId)
        If path.Length < 1 Then
            MsgBox("Unable to find IDR file.")
        Else
            Dim xl As Excel.Application = Utilities.GetExcelProcess()
            Dim w As Excel.Workbook = xl.Workbooks.Open(path, , False)
            xl.Visible = True
        End If
    End Sub

    Private Sub btnManageGadsData_Click(sender As Object, e As EventArgs) Handles btnManageGadsData.Click
        T("btnManageGadsData_Click")
        Try
            Dim manageGadsDataPath As String = ConfigurationManager.AppSettings("ManageGADSDataUniversalLink")
            If manageGadsDataPath.Length > 0 Then
                Process.Start(manageGadsDataPath)
            Else
                MsgBox("Unable to find ManageGadsData at " & manageGadsDataPath)
            End If
        Catch ex As Exception
            MsgBox("ManageGadsData error: " & ex.Message)
        End Try
    End Sub

    Private Sub btnValFax_Click(sender As Object, e As EventArgs) Handles btnValFax.Click
        If _siteId.Length < 1 Then
            MsgBox("Please choose a site and try again.")
            Return
        End If
        T("btnValFax_Click")
        _vfTemplatesPath = "K:\Study\" & _studyType & "\" & GetStudyYearFromCbo().ToString & "\Templates\"
        ValFax_Build()

    End Sub

    Private Function ValFax_Build() As Boolean
        T("ValFax_Build")
        Dim frmTemp = New frmTemplates()
        Dim intNumDays = -1
        Dim templateChosen As String = String.Empty
        Try
            Dim vfTextFileName As String = "ValFax.txt"
            If File.Exists(_profileConsoleTempPath & vfTextFileName) Then
                Try
                    File.Delete(_profileConsoleTempPath + vfTextFileName)
                Catch
                End Try
            End If
            Dim fileNames As New List(Of String)
            Dim templatesPath As String = ""
            Dim errMsg As String = _powerSvc.GetVFTemplates(_vfTemplatesPath, fileNames)
            If errMsg.Length > 0 Then
                MsgBox(errMsg)
                Return False
            End If
            If fileNames.Count = 1 Then
                templateChosen = _vfTemplatesPath & "\" & fileNames(0)
            Else
                frmTemp.cboTemplates.Items.Clear()
                For Each nm As String In fileNames
                    frmTemp.cboTemplates.Items.Add(nm)
                Next
                frmTemp.cboTemplates.SelectedIndex = 0
                frmTemp.Text = "Select a Cover Template"
                frmTemp.ShowDialog()
                If Not frmTemp.OKClick Then Exit Function
                templateChosen = _vfTemplatesPath & "\" & frmTemp.cboTemplates.SelectedItem
            End If

            If Dir(_corrPath & "vf*") = "" Then
                intNumDays = 10
            Else
                intNumDays = 5
            End If

            Dim deadline As Date = Utilities.ValFaxDateDue(InputBox("How many business days do you want to give them to respond?", "Deadline", intNumDays))
            Dim consultantInitials As String = _powerSvc.GetConsultantInitials(_siteId) ', GetStudyYearFromCbo())
            If consultantInitials.Length < 1 Then
                consultantInitials = InputBox("I cannot find you in the TSort table, please give me your initials: ", "Consultant Initials")
            End If

            Dim consultantName As String = _powerSvc.GetConsultantName(cboConsultant.Text)
            'An error here probably means the consultant is not in the Console.Employees table.
            If consultantName.Length < 1 Then
                consultantName = InputBox("I cannot find you in the employee table, please give me your name: ", "Consultant Name")
            End If

            Dim coord As SA.Console.Common.Contact = _powerSvc.GetCoord(_siteId)
            Dim gotCoordInfo As Boolean = False
            Dim coordNameToUse As String = String.Empty
            Dim coordEmail As String = String.Empty

            Try
                coordEmail = coord.Email
                coordNameToUse = coord.FirstName & " " & coord.LastName
                If coordNameToUse.Trim().Length < 1 Then
                    coordNameToUse = Trim(coord.FullName)
                End If
                If coordEmail.Length > 0 OrElse coordNameToUse.Length > 0 Then
                    gotCoordInfo = True
                End If
            Catch
            End Try
            If IsNothing(coord) Then
                gotCoordInfo = False
            End If
            If Not gotCoordInfo Then
                If MsgBox("The contact information for this SiteID is not available. Do you want to continue?", vbYesNo, "Coordinator Info") = vbNo Then
                    Exit Function
                End If
            Else
                If IsNothing(coordNameToUse) Then coordNameToUse = String.Empty
                If IsNothing(coordEmail) Then coordEmail = String.Empty
            End If
            'no more input so offload the rest of this work to the Service
            Dim templateValues As New SA.Console.Common.WordTemplate()
            Dim coloc As String = String.Empty
            errMsg = _powerSvc.PrepVF(templateChosen, _profileConsoleTempPath + vfTextFileName,
                                      deadline, consultantInitials, consultantName, coordNameToUse,
                                      coordEmail, templateValues, _siteId, _corrPath, coloc)
            If errMsg.Length > 0 Then
                MsgBox(errMsg)
                Return False
            End If
            Dim vfName As String = String.Empty
            Try
                If File.Exists(_profileConsoleTempPath & Utilities.ExtractFileName(templateChosen)) Then
                    Kill(_profileConsoleTempPath & Utilities.ExtractFileName(templateChosen))
                End If
                File.Copy(templateChosen, _profileConsoleTempPath & Utilities.ExtractFileName(templateChosen), True)

                vfName = _docsAccess.NextV("VF", _corrPath, _siteId)
                vfName = "VF" & vfName & " " & coloc & ".docx"

                vfName = InputBox("Here is the suggested name for this New VF document." & Chr(10) & _
                            "Click OK to accept it, or change, then click OK.", "SaveAs", vfName)
                If vfName.Length < 1 Then Return True 'not really an error so don't return false
            Catch ex As Exception
                MsgBox("Error in ValFax_Build_Legacy during file save: " & ex.Message)
                Return False
            End Try
            Dim finalPathToWordDoc As String = String.Empty
            Dim wordApp As Microsoft.Office.Interop.Word.Application = Nothing

            'if word is closed, then the byref WordApp object will be null
            'the 2nd to last arg is path to the .doc file
            Utilities.WordTemplateReplace2(_profileConsoleTempPath & Utilities.ExtractFileName(templateChosen),
                                           templateValues, _corrPath & vfName, 1, finalPathToWordDoc, wordApp)
            'No need to close the wordapp object (undo the Utilities.GetWordProcess() ) later if it's not null; just close the doc object.

            'need to save and close else get file-in-use error on uploading to mFiles
            wordApp.ActiveDocument.Save()
            wordApp.ActiveDocument.Close()
            Dim siteIdInsteadOfRefnum As String = _siteId
            If _useMFiles Then
                'RefreshMFilesCollections(0)
                Dim benchmarkingParticipantId As Integer = _benchmarkingParticipantSiteFiles.BenchmarkingParticipantId
                Dim deliverableType As Integer = 12
                'done above:   check for name clash
                _docsAccess.UploadDocToMfiles(finalPathToWordDoc, _docClassForIdr, siteIdInsteadOfRefnum,
                                              deliverableType, benchmarkingParticipantId)
                _docsAccess.OpenFileFromMfiles(Utilities.FileNameWithoutExtension(vfName), _siteId, _siteId)
            Else
                Process.Start(_corrPath & vfName)
            End If


        Catch ex As Exception
            MsgBox("Error in ValFax_Build(): " + ex.Message)
        End Try
    End Function

    Private Sub cboStudy_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboStudy.SelectionChangeCommitted
        T("cboStudy_SelectionChangeCommitted")
        If _initializing Then Return

        ClearAllControls()
        cboSiteId.SelectedItem = Nothing
        cboSiteId.Text = String.Empty
        listViewRefnums.Items.Clear()


        Dim chosenStudyYear As String = cboStudy.Text
        Dim thisStudyYear As Integer = 0
        If Int32.TryParse(chosenStudyYear, thisStudyYear) Then
            T("Going to call GetCompanynamesWithSiteIds")
            Dim siteIds As List(Of String) = _powerSvc.GetCompanynamesWithSiteIds(thisStudyYear)
            cboSiteId.Items.Clear()
            For Each siteId As String In siteIds
                cboSiteId.Items.Add(siteId)
            Next
        End If

        Dim lastSiteId As String = String.Empty
        T("Going to call TryGetValue")
        _powerSettings.TryGetValue(thisStudyYear, lastSiteId)
        If Not IsNothing(lastSiteId) AndAlso lastSiteId.Length > 0 Then
            For Each item In cboSiteId.Items
                If item.ToString().Trim().ToUpper() = lastSiteId.Trim().ToUpper() Then
                    cboSiteId.SelectedItem = item
                    Exit For
                End If
            Next
        End If
        T("Going to call LoadConsultantsToCbo")
        LoadConsultantsToCbo(thisStudyYear)

        If Not IsNothing(cboSiteId.SelectedItem) AndAlso cboSiteId.SelectedItem.ToString().Length > 0 Then
            T("Going to call GetUnitnamesWithRefNums")
            Dim refnums As List(Of String) = _powerSvc.GetUnitnamesWithRefNums(cboSiteId.SelectedItem.ToString(), thisStudyYear)
            For Each refnum In refnums
                listViewRefnums.Items.Add(refnum) '& Environment.NewLine
            Next
            T("Going to GetConsultant")
            Dim c As String = _powerSvc.GetConsultant(cboSiteId.SelectedItem, thisStudyYear)
            If Not IsNothing(cboSiteId.SelectedItem) Then
                T("    " & cboSiteId.SelectedItem.ToString())
            End If
            If Not IsNothing(thisStudyYear) Then
                T("    " & thisStudyYear.ToString())
            End If
        End If

        If _profileConsoleTempPath.ToUpper().Contains("SFB") Then
            MsgBox("Per Mike: SiteId or Refnum must never end in a P for Console purposes")
        End If



        'If GetStudyYearFromCbo() >= 2016 Then
        '    _useMFiles = True
        'Else
        '    _useMFiles = False
        'End If
    End Sub

    Private Sub PopulateVFFilesMFiles(ByVal nameFilter As String,
                        ByVal SelectedItemString As String, ByRef VFFiles As List(Of ConsoleFile),
                        Optional ByVal containsNotStartsWith As Boolean = False)
        T("PopulateVFFilesMFiles")
        For Each fil As SA.Console.Common.ConsoleFile In _benchmarkingParticipantSiteFiles.Files
            Dim filter = nameFilter.Replace("*", "")
            filter = filter & SelectedItemString
            Dim match = False
            If Not containsNotStartsWith Then
                If (fil.FileName.StartsWith(filter)) Then
                    match = True
                End If
            Else
                If (fil.FileName.Contains(filter)) Then
                    match = True
                End If
            End If

            If match Then
                Dim formatDocumentDate As String = String.Empty
                If fil.CheckedOutTo > 0 Then
                    formatDocumentDate = "(Checked out to " + fil.CheckedOutToUserName + "):  "
                End If
                formatDocumentDate = formatDocumentDate & fil.LastModifiedUtcDate.ToString("dd-MMM-yy")
                Dim cfile As New ConsoleFile(True)
                cfile.Id = fil.Id
                cfile.FileName = Format(formatDocumentDate & " | " & fil.FileName & "." & fil.FileExtension)
                VFFiles.Add(cfile)
            End If
        Next
    End Sub

    Private Sub BuildSecureSendTab()
        T("BuildSecureSendTab")
        If _useMFiles Then
            If _mfilesFailed Then Exit Sub
            'RefreshMFilesCollections(0)
            'RefreshMFilesCollections(1)
            'RefreshMFilesCollections(2)
        End If
        Try
            BuildSecureSendListBoxes()
        Catch ex As Exception
            MsgBox("Error in BuildSecureSendTab(): " & ex.Message)
        End Try
        If Not ConsoleTabs.TabPages.Contains(tabSS) Then ConsoleTabs.TabPages.Add(tabSS)
    End Sub
    Private Sub BuildSecureSendListBoxes()
        T("BuildDirectoriesTab")
        cboDir.Items.Clear()
        If Not _useMFiles Then
            cboDir.Items.Add("Site Correspondence")
            cboDir.Items.Add("Company Correspondence")
            cboDir.Items.Add("Drawings")
        End If
        cboDir.Items.Add("Client Attachments")
        If cboDir2.Items.Count < 1 Then
            cboDir2.Items.Add("Site Correspondence")
            cboDir2.Items.Add("Company Correspondence")
            'If Not _useMFiles Then
            '    cboDir2.Items.Add("Client Attachments")  'these are not in MFiles het
            'End If
            cboDir2.Items.Add("Drawings")
        End If
        tvwCompCorr.Nodes.Clear()
        tvwCorrespondence.Nodes.Clear()
        tvwDrawings.Nodes.Clear()
        tvwClientAttachments.Nodes.Clear()
        tvwCorrespondence2.Nodes.Clear()
        tvwCompCorr2.Nodes.Clear()
        tvwDrawings2.Nodes.Clear()
        tvwClientAttachments2.Nodes.Clear()
        PopulateTreeView(tvwCompCorr, _compCorrPath)
        PopulateTreeView(tvwCorrespondence, _corrPath)
        PopulateTreeView(tvwCompCorr2, _compCorrPath)
        PopulateTreeView(tvwCorrespondence2, _corrPath)
        PopulateTreeView(tvwDrawings, _drawingPath)
        PopulateTreeView(tvwClientAttachments, _clientAttachmentsPath)
        PopulateTreeView(tvwDrawings2, _drawingPath)
        'PopulateTreeView(tvwClientAttachments2, _clientAttachmentsPath)
        Try
            cboDir.SelectedIndex = 0
            tvwClientAttachments.Visible = True
            tvwCorrespondence.Visible = False
            tvwDrawings.Visible = False
            tvwCompCorr.Visible = False
            '-----------------
            cboDir2.SelectedIndex = 1
        Catch
        End Try
        If _useMFiles Then
            lblSecureSendDirectory2.Text = "M-Files:"
        Else
            lblSecureSendDirectory2.Text = "Directory:"
        End If
    End Sub
    Private Sub PopulateTreeView(tvw As TreeView, path As String)
        'Ultimately, this uses wshom.ocx; so doesn't make sense to move it to a service which is a dll 
        T("PopulateTreeView")
        'Left treeviews-not using MFiles:
        If tvw.Name <> "tvwCorrespondence2" AndAlso tvw.Name <> "tvwCompCorr2" AndAlso tvw.Name <> "tvwDrawings2" AndAlso tvw.Name <> "tvwClientAttachments2" Then
            If Directory.Exists(path) Then
                SetTree(tvw, path)
                If _useMFiles And (tvw.Name.Contains("tvwCorrespondence") Or tvw.Name.Contains("tvwCompCorr")) Then
                    tvw.Enabled = False
                Else
                    tvw.Enabled = True
                End If
            End If
        Else ' right treeviews. These MIGHT use MFiles depending on year
            If _useMFiles Then
                Dim consoleFilesInfo As ConsoleFilesInfo = Nothing
                'orig: but now only use for tvwCorrespondence2
                'NEED to do diff lookup for tvwCompCorr2
                If tvw.Name = "tvwCorrespondence2" AndAlso _siteId.Length > 0 Then
                    consoleFilesInfo = _benchmarkingParticipantSiteFiles
                    consoleFilesInfo = _powerSvc.SortConsoleFilesByDate(consoleFilesInfo, _useMFiles)
                ElseIf tvw.Name = "tvwCompCorr2" And _powerSvc.GetCompanyName(_siteId).Length > 0 Then
                    Try
                        consoleFilesInfo = _benchmarkingParticipantCompanyFiles
                        consoleFilesInfo = _powerSvc.SortConsoleFilesByDate(consoleFilesInfo, _useMFiles)
                    Catch ex As Exception
                        'reset to no data so we don't try to populate list or err while trying
                        consoleFilesInfo = Nothing
                    End Try
                    _dAL.SQL = String.Empty
                ElseIf tvw.Name = "tvwDrawings2" AndAlso _siteId.Length > 0 Then
                    consoleFilesInfo = _refiningGeneralFiles
                End If
                'This doesn't change the image from a FOlder to anything else
                'If IsNothing(tvw.ImageList) Then
                '    Dim i As New System.Windows.Forms.ImageList
                '    Dim r As New Icon("C:\tfs\CPA\Sa.Console.Trunk\Development\Sa.Console.20150526\RefiningConsole\Images\Refinery.ico")
                '    i.Images.Add("MFiles", r)
                '    tvw.ImageList = i
                'End If  
                If Not IsNothing(consoleFilesInfo) AndAlso Not IsNothing(consoleFilesInfo.Files) Then
                    tvw.Nodes.Clear()
                    tvw.ImageList = ImageList1
                    For Each fil As ConsoleFile In consoleFilesInfo.Files
                        tvw.Nodes.Add(_powerSvc.PopulateTreeViewNodeWithMfilesFiles(fil))
                        tvw.Refresh()
                    Next
                End If
            Else 'not using MFiles
                If Directory.Exists(path) Then
                    SetTree(tvw, path)
                End If
            End If
        End If
    End Sub

    Public Sub SetTree(tvw As TreeView, mRootPath As String)
        T("SetTree")
        Try
            tvw.Nodes.Clear()
            Dim files() As String = Directory.GetFiles(mRootPath)
            Dim nf As String
            Dim mRootNode As New TreeNode
            For Each f In files
                nf = Path.GetFileName(f)
                If f.Substring(f.Length - 9, 9) <> "Thumbs.db" Then
                    If f.Substring(f.Length - 3, 3) = "lnk" Then
                        f = Utilities.GetFileFromLink(f)
                    End If
                    mRootNode = tvw.Nodes.Add(nf.Replace(" - Shortcut.lnk", "").Replace(".lnk", ""))
                    mRootNode.ImageKey = CacheShellIcon(f)
                    mRootNode.SelectedImageKey = mRootNode.ImageKey & "-open"
                    mRootNode.Tag = f
                End If
            Next
            tvw.ExpandAll()
            tvw.Refresh()
        Catch ex As System.Exception
            _dAL.WriteLog("SetTree", ex)
        End Try
    End Sub
    Function CacheShellIcon(ByVal argPath As String) As String
        T("CacheShellIcon")
        Dim mKey As String = Nothing
        ' determine the icon key for the file/folder specified in argPath
        If IO.Directory.Exists(argPath) = True Then
            Return mKey
        ElseIf IO.File.Exists(argPath) = True Then
            mKey = IO.Path.GetExtension(argPath)
        End If
        ' check if an icon for this key has already been added to the collection
        If ImageList1.Images.ContainsKey(mKey) = False Then
            ImageList1.Images.Add(mKey, GetShellIconAsImage(argPath))
            If mKey = "folder" Then ImageList1.Images.Add(mKey & "-open", GetShellOpenIconAsImage(argPath))
        End If
        Return mKey
    End Function


    'Private Function RefreshMFilesCollections(Optional forceRefresh As Integer = -1) As Boolean
    '    T("RefreshMFilesCollections")
    '    Dim companyBenchmarkName As String = _powerSvc.GetCompanyBenchmarkName(_siteId, (2000 - GetStudyYearFromCbo()).ToString(), _mfilesDesktopUIPath)
    '    Return _docsAccess.RefreshMFilesCollections(_siteId, companyBenchmarkName, _benchmarkingParticipantSiteFiles,
    '                                             _benchmarkingParticipantCompanyFiles, _refiningGeneralFiles, forceRefresh)
    'End Function

    Private Sub SetSummaryFileButtons()
        'see Refining for sample
    End Sub

    Private Sub ClearDragDropTab()
        T("ClearDragDropTab")
        Dim di As New DirectoryInfo(_profileConsoleTempPath)
        Dim fs As FileInfo() = di.GetFiles()
        For i As Integer = 0 To fs.Count - 1
            Try
                fs(i).Delete()
            Catch exdelete As Exception
                Dim z As String = "breakpoint"
            End Try
        Next

        di = New DirectoryInfo(_profileConsoleTempPath & "\Extracted Files\")
        If di.Exists() Then
            fs = di.GetFiles()
            For i As Integer = 0 To fs.Count - 1
                Try
                    fs(i).Delete()
                Catch exdelete As Exception
                    Dim z As String = "breakpoint"
                End Try
            Next
        End If
        _dragDropMode = 0
        dgFiles.RowCount = 1
        dgFiles.Rows.Clear()
        txtMessageFilename.Text = ""
        btnFilesSave.Enabled = False
    End Sub
    Private Function DragDropNonEmailToMFiles(filesPaths As List(Of String)) As Boolean
        T("DragDropNonEmailToMFiles")
        Dim currentFileName As String = String.Empty
        Dim failedFiles As New List(Of String)
        Dim rowHasBeenAdded As Boolean = True
        Dim result As Boolean = False
        For Each path As String In filesPaths
            Try
                Dim zext As String = String.Empty
                Dim ext As String = String.Empty
                rowHasBeenAdded = False
                currentFileName = Utilities.ExtractFileName(path)

                Try
                    ext = Utilities.GetFileExtension(path)
                Catch
                    ext = ""
                End Try
                Dim invalidMsg As String = String.Empty
                Select Case ext.ToUpper()
                    Case "LNK"
                        invalidMsg = "We are not currently saving .lnk files to M-Files"
                    Case "URL"
                        invalidMsg = "We are not currently saving .url files to M-Files"
                    Case "INI"
                        invalidMsg = "We are not currently saving .ini files to M-Files"
                    Case "EXE"
                        invalidMsg = "We are not currently saving .exe files to M-Files"
                    Case "DLL"
                        invalidMsg = "We are not currently saving .dll files to M-Files"
                    Case "ZIP"
                        invalidMsg = "We are not currently saving .zip files to M-Files. Please extract the files from the zip and drag them into Console."
                    Case "ZIPX"
                        invalidMsg = "We are not currently saving .zipx files to M-Files"
                    Case Else

                End Select
                If invalidMsg.Length > 0 Then
                    MsgBox(invalidMsg)
                    failedFiles.Add(currentFileName)
                    result = False
                    'ClearDragDropTab()
                    'Return False
                Else
                    'If ext.ToUpper = "ZIP" Then
                    '    If Dir(_profileConsoleTempPath & "Extracted Files\") = "" Then Directory.CreateDirectory(_profileConsoleTempPath & "\Extracted Files\")
                    '    Dim zfilenames() As String = Directory.GetFiles(_profileConsoleTempPath & "Extracted Files", "*.*")
                    '    For Each sFile In zfilenames
                    '        Try
                    '            File.Delete(sFile)
                    '        Catch ex As System.Exception
                    '            db.WriteLog("DragDrop", ex)
                    '            Debug.Print(ex.Message)
                    '        End Try
                    '    Next
                    '    att.SaveAsFile(_profileConsoleTempPath & "Extracted Files\" & att.FileName)
                    '    Utilities.UnZipFiles(_profileConsoleTempPath & "Extracted Files\" & att.FileName, _profileConsoleTempPath, CompanyPassword)
                    '    File.Delete(_profileConsoleTempPath & "Extracted Files\" & att.FileName)
                    '    OldTempPath = _profileConsoleTempPath & "Extracted Files\"
                    'End If
                    'For Each mfile In Directory.GetFiles(OldTempPath)
                    Dim fileName As String = Utilities.ExtractFileName(path)
                    Dim proposedName As String = fileName
                    'strFile = mfile
                    Dim zr = dgFiles.Rows.Add()
                    rowHasBeenAdded = True
                    Dim zcbcell = New DataGridViewComboBoxCell
                    zcbcell.Items.Add("Do Not Save")
                    zcbcell.Items.Add("Correspondence")
                    zcbcell.Items.Add("Company Correspondence")
                    zcbcell.Items.Add("Drawings")
                    'zcbcell.Items.Add("General")
                    zcbcell.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton
                    dgFiles.Rows(zr).Cells(2) = zcbcell
                    zext = Utilities.GetFileExtension(fileName) ' GetExtension(mfile)
                    'Select Case (zext.ToUpper)
                    '    Case "DOC", "XLS", "PDF", "XL", "TXT", "OCX", "XLSX", "DOCX", "DOCM", "XLSB"
                    '        zcbcell.Value = "Correspondence"
                    '    Case "JPG", "GIF", "BMP", "JPEG", "PSD", "TIF", "PNG", "VSO"
                    '        zcbcell.Value = "Drawings"
                    '    Case Else
                    '        zcbcell.Value = "General"
                    'End Select
                    File.Copy(path, _profileConsoleTempPath & fileName, True)
                    dgFiles.Rows(zr).Cells(0).Value = fileName
                    dgFiles.Rows(zr).Cells(1).Value = proposedName

                    If fileName.StartsWith("VF") Then
                        'strFile = mfile
                        proposedName = "VR" & _docsAccess.NextReturnFile(_siteId, _siteId).ToString() + fileName.Substring(3, fileName.Length - 3)
                        dgFiles.Rows(zr).Cells(1).Value = proposedName '"VR" & NextVR() & "-" & mfile.Substring(4, Len(mfile) - 4)
                    ElseIf fileName.ToUpper.Contains("RETURN") Then
                        Dim replace As Integer = fileName.ToUpper().IndexOf("RETURN") + 6
                        proposedName = fileName.Substring(0, replace)
                        proposedName += _docsAccess.NextReturnFile(_siteId, _siteId).ToString
                        proposedName += fileName.Substring(replace, fileName.Length - replace)
                        dgFiles.Rows(zr).Cells(1).Value = proposedName 'mfile.Substring(0, mfile.Length - 4) & strNextRetNum & "." & zext
                    Else
                        'same as fileName
                        'strFile = mfile.Substring(0, mfile.Length - 4) & "." & zext
                    End If

                    'If mfile.ToUpper.Contains("RETURN") Then
                    '    dgFiles.Rows(zr).Cells(1).Value = mfile.Substring(0, mfile.Length - 4) & strNextRetNum & "." & zext
                    'Else
                    '    dgFiles.Rows(zr).Cells(1).Value = mfile.Substring(0, mfile.Length - 4) & "." & zext
                    'End If
                    ''Next

                End If
            Catch ex As Exception
                result = False
                MsgBox("Error in DragDropNonEmail(): " & ex.Message)
                failedFiles.Add(currentFileName)
                If rowHasBeenAdded Then
                    dgFiles.Rows.RemoveAt(dgFiles.Rows.Count - 1)
                End If
                'ClearDragDropTab()
            End Try
        Next
        txtMessageFilename.Text = String.Empty
        txtMessageFilename.Enabled = False
        optCompanyCorr.Checked = False
        optCompanyCorr.Enabled = False
        If dgFiles.Rows.Count > 0 Then
            btnFilesSave.Enabled = True
        Else
            btnFilesSave.Enabled = False
        End If


        If failedFiles.Count > 0 Then
            Dim msg As String = "These attachments had problems, and were not included in Drag-and-Drop: " & Environment.NewLine
            For Each item As String In failedFiles
                msg = msg & "    " & item & ", " & Environment.NewLine
            Next
            msg = msg.Remove(msg.Length - 4, 4) 'trailing comma
            msg = msg & Environment.NewLine
            msg = msg & "To start over, please press the Clear button and try Drag-and-Drop again."
            MsgBox(msg)
        End If
        Return result
    End Function

    Private Function HandleDragDropZipAttachmentFiles(ByRef localstrFile As String,
       ByRef localOldTempPath As String, ByRef localTempPath As String,
       ByRef localstrNextRetNum As String) As Boolean
        T("HandleDragDropZipAttachmentFiles")
        Try
            For Each localmfile In Directory.GetFiles(localOldTempPath)
                localmfile = Utilities.ExtractFileName(localmfile)

                localstrFile = localmfile

                Dim zr = dgFiles.Rows.Add()
                Dim zcbcell = New DataGridViewComboBoxCell
                zcbcell.Items.Add("Do Not Save")
                zcbcell.Items.Add("Correspondence")
                zcbcell.Items.Add("Company Correspondence")
                zcbcell.Items.Add("Drawings")
                zcbcell.Items.Add("General")
                zcbcell.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton
                dgFiles.Rows(zr).Cells(2) = zcbcell
                Dim zext As String = Utilities.GetFileExtension(localmfile)
                Select Case (zext.ToUpper)
                    Case "DOC", "XLS", "PDF", "XL", "TXT", "OCX", "XLSX", "DOCX", "DOCM"
                        zcbcell.Value = "Correspondence"
                    Case "JPG", "GIF", "BMP", "JPEG", "PSD", "TIF", "PNG", "VSO"
                        'zcbcell.Value = "Drawings"
                    Case Else
                        'zcbcell.Value = "General"
                End Select

                File.Copy(localOldTempPath & "\" & localstrFile, localTempPath & localstrFile)
                dgFiles.Rows(zr).Cells(0).Value = localmfile

                If localmfile.Substring(0, 2).ToUpper() = "VF" Then
                    localstrFile = localmfile
                    dgFiles.Rows(zr).Cells(1).Value = "VR" & _docsAccess.NextReturnFile(_siteId, _siteId) & "-" & localmfile.Substring(4, Len(localmfile) - 4)
                Else
                    localstrFile = localmfile.Substring(0, localmfile.Length - 4) & "." & zext
                End If


                If localmfile.ToUpper.Contains("RETURN") Then
                    dgFiles.Rows(zr).Cells(1).Value = localmfile.Substring(0, localmfile.Length - 4) & localstrNextRetNum & "." & zext
                    'Else   populated above
                    'dgFiles.Rows(zr).Cells(1).Value = localmfile.Substring(0, localmfile.Length - 4) & "." & zext
                End If
            Next

            Return True
        Catch ex As Exception

        End Try
    End Function

    Private Function ProcessDragDropAttachment(ByRef currentFileName As String,
                                               ByRef ext As String,
                                               ByRef strFile As String,
                                               ByRef OldTempPath As String,
                                               ByRef TempPath As String,
                                               ByRef strNextRetNum As String,
                                               ByRef att As Microsoft.Office.Interop.Outlook.Attachment,
                                               ByRef failedFiles As List(Of String),
                                               ByRef ReturnFile As Boolean,
                                               ByRef companyPw As String,
                                               ByRef Location As String) As String
        T("ProcessDragDropAttachment")
        Try
            Dim thisIsARealAttachment As Boolean = True
            Try
                currentFileName = att.FileName
            Catch ex As Exception
                'something embedded in email, not a true attachment.
                thisIsARealAttachment = False
            End Try

            If thisIsARealAttachment Then
                currentFileName = Utilities.CleanFileName(currentFileName)
                btnFilesSave.Enabled = True
                If currentFileName.Substring(0, 5).ToUpper <> "IMAGE" Then
                    Try
                        ext = currentFileName.Substring(currentFileName.LastIndexOf(".") + 1, currentFileName.Length - currentFileName.LastIndexOf(".") - 1)
                    Catch
                        ext = ""
                    End Try

                    If ext.ToUpper = "ZIP" Then
                        If Dir(_profileConsoleTempPath & "Extracted Files\") = "" Then Directory.CreateDirectory(_profileConsoleTempPath & "\Extracted Files\")
                        Dim zfilenames() As String = Directory.GetFiles(_profileConsoleTempPath & "Extracted Files", "*.*")
                        For Each sFile In zfilenames
                            Try
                                File.Delete(sFile)
                            Catch ex As System.Exception
                                _dAL.WriteLog("DragDrop", ex)
                                Debug.Print(ex.Message)
                            End Try
                        Next

                        att.SaveAsFile(_profileConsoleTempPath & "Extracted Files\" & currentFileName)
                        Utilities.UnZipFiles(_profileConsoleTempPath & "Extracted Files\" & currentFileName, _profileConsoleTempPath, _powerSvc.GetCompanyPassword(_siteId, GetStudyYearFromCbo()))
                        File.Delete(_profileConsoleTempPath & "Extracted Files\" & currentFileName)
                        OldTempPath = _profileConsoleTempPath & "Extracted Files\"

                        Dim extractedFilesDirectory As New DirectoryInfo(OldTempPath)
                        'For Each zipfolder In Directory.GetDirectories(OldTempPath)
                        Dim zipfolders As DirectoryInfo() = extractedFilesDirectory.GetDirectories()
                        For folderCount As Integer = 0 To zipfolders.Length - 1
                            If Not HandleDragDropZipAttachmentFiles(strFile,
                            zipfolders(folderCount).FullName, _profileConsoleTempPath, strNextRetNum) Then
                                failedFiles.Add(strFile)
                                dgFiles.Rows.RemoveAt(dgFiles.Rows.Count - 1)
                                'Exit Sub
                            End If
                        Next

                        If Not HandleDragDropZipAttachmentFiles(strFile,
                            OldTempPath, _profileConsoleTempPath, strNextRetNum) Then
                            failedFiles.Add(strFile)
                            dgFiles.Rows.RemoveAt(dgFiles.Rows.Count - 1)
                            'Exit Sub
                        End If

                    Else
                        Dim mfile As String
                        Dim r = dgFiles.Rows.Add()
                        dgFiles.Rows(r).Cells(0).Value = currentFileName

                        mfile = currentFileName
                        Dim cbcell = New DataGridViewComboBoxCell

                        cbcell.Items.Add("Do Not Save")
                        cbcell.Items.Add("Correspondence") ' changing causes dataerror
                        cbcell.Items.Add("Company Correspondence")
                        cbcell.Items.Add("Drawings")
                        'cbcell.Items.Add("General")
                        cbcell.Value = "Correspondence"
                        cbcell.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton
                        'cbcell.Value = "Do Not Save"
                        dgFiles.Rows(r).Cells(2) = cbcell

                        Select (ext.ToUpper.Substring(0, 3))
                            '    Case "DOC", "XLS", "PDF", "XL", "TXT", "OCX", "XLSX", "DOCX", "DOCM"
                            '        cbcell.Value = "Correspondence"
                            Case "JPG", "GIF", "BMP", "JPEG", "PSD", "TIF", "PNG"
                                cbcell.Value = "Drawings"
                                '    Case Else
                                '        cbcell.Value = "General"
                        End Select

                        If File.Exists(_profileConsoleTempPath & mfile) Then
                            Try
                                File.Delete(_profileConsoleTempPath & mfile)
                            Catch ex As Exception
                                'MsgBox "Console tried to clear out this file, but failed. Please 
                                Try
                                    File.Move(_profileConsoleTempPath & mfile, _profileConsoleTempPath & "DELETE" + DateTime.Now.ToString())
                                Catch
                                End Try
                            End Try
                        End If
                        att.SaveAsFile(_profileConsoleTempPath & mfile)


                        If mfile.Substring(0, 4).ToUpper().Contains("VF") Then
                            ReturnFile = True
                            strFile = "VR" & _docsAccess.NextV("VR", _siteId, _siteId) & "-" & mfile.Substring(4, Len(mfile) - 4)
                            dgFiles.Rows(r).Cells(1).Value = strFile
                        Else
                            strFile = mfile.Substring(0, mfile.Length - ext.Length - 1) & "." & ext
                            If Not _useMFiles Then
                                If (strFile.ToUpper.Contains("_RETURN") And ext.ToUpper.Substring(0, 3) = "XLS") Then
                                    ReturnFile = True
                                    dgFiles.Rows(r).Cells(1).Value = strFile.Substring(0, mfile.Length - ext.Length - 1) & strNextRetNum & "." & ext
                                Else
                                    dgFiles.Rows(r).Cells(1).Value = strFile.Substring(0, mfile.Length - ext.Length - 1) & "." & ext
                                End If
                            Else
                                If (strFile.ToUpper.Contains("_RETURN") And ext.ToUpper.Substring(0, 3) = "XLS") Then
                                    ReturnFile = True
                                    'If Not strFile.ToUpper().Contains(Replace(Location.ToUpper(), ".", "")) Then
                                    '    MsgBox("Possible Refinery Mismatch - the Return File does not have the correct Location (" + Location + ") in its name. Please contact Sung for resolution of this problem")
                                    '    ClearDragDropTab()
                                    '    Return "Possible Refinery Mismatch - the Return File does not have the correct Location (" + Location + ") in its name. Please contact Sung for resolution of this problem"
                                    'End If
                                    dgFiles.Rows(r).Cells(1).Value = strFile.Substring(0, mfile.Length - ext.Length - 1) & strNextRetNum & "." & ext
                                Else
                                    dgFiles.Rows(r).Cells(1).Value = strFile.Substring(0, mfile.Length - ext.Length - 1) & "." & ext
                                End If
                            End If
                        End If

                        mfile = dgFiles.Rows(r).Cells(1).Value
                        'Per Penny, we aren't saving anything to MFiles in an encrypted state
                        'moving this below to decrypt before saving:  att.SaveAsFile(TempPath & dgFiles.Rows(r).Cells(0).Value)
                        'NEW: unprotect here
                        Dim fileName As String = dgFiles.Rows(r).Cells(0).Value
                        Dim filePath As String = _profileConsoleTempPath & fileName
                        'already done above, right?  att.SaveAsFile(filePath)
                        If Utilities.GetFileExtension(fileName).ToUpper().StartsWith("XLS") Then
                            If Utilities.ExcelWorkbookIsEncrypted(filePath) Then
                                Dim u As New Utilities()
                                If Not u.RemovePassword(filePath, _powerSvc.GetReturnFilePassword(_siteId), companyPw) Then
                                    MsgBox("Unable to remove password on " & fileName)
                                    'ClearDragDropTab()
                                    failedFiles.Add(currentFileName)
                                    dgFiles.Rows.RemoveAt(dgFiles.Rows.Count - 1)
                                    'Exit Sub
                                End If
                            End If
                        ElseIf Utilities.GetFileExtension(fileName).ToUpper().StartsWith("DOC") Then
                            If Utilities.WordDocIsEncrypted(filePath) Then
                                Dim u As New Utilities()
                                If Not u.RemovePassword(filePath, _powerSvc.GetReturnFilePassword(_siteId), companyPw) Then
                                    MsgBox("Unable to remove password on " & fileName)
                                    'ClearDragDropTab()
                                    failedFiles.Add(currentFileName)
                                    dgFiles.Rows.RemoveAt(dgFiles.Rows.Count - 1)
                                    'Exit Sub
                                End If
                            End If
                        End If
                    End If
                End If
            End If
            Return String.Empty
        Catch ex As Exception
            Return "Error in ProcessDragDropAttachment(): " & ex.Message
        End Try
    End Function

    Private Function DragDropRoutine(args As System.Windows.Forms.DataObject,
                                     fileGroupDescriptor As Boolean,
                                     objectDescriptor As Boolean,
                                     headerText As String) As String
        T("DragDropRoutine")
        Try

            Dim failedFiles As New List(Of String)
            If _useMFiles And _mfilesFailed Then Return String.Empty
            If _siteId.Length < 1 Then Return "Please select a Site first."
            If Not ConsoleTabs.TabPages.Contains(tabDD) Then ConsoleTabs.TabPages.Add(tabDD)
            ConsoleTabs.SelectTab(tabDD)
            txtMessageFilename.Text = String.Empty
            txtMessageFilename.Enabled = True
            optCompanyCorr.Enabled = True
            '// ? is one or one char wildcard. *  is 0 or more
            Dim strNextNum As String = String.Empty
            Dim strNextRetNum As String = String.Empty

            strNextNum = _docsAccess.NextV("VF", _siteId, _siteId)
            strNextRetNum = _docsAccess.NextV("VR", _siteId, _siteId)

            Dim companyPw As String = _powerSvc.GetCompanyPassword(_siteId, GetStudyYearFromCbo())

            'Dim strNumModifier = FindModifier(strNextNum)
            Dim mailItem As Outlook._MailItem
            Try
                Me.ConsoleTabs.SelectedIndex = 9
                'check for Drag-Drop Files instead of Email
                Dim filePaths As List(Of String) = Nothing
                Dim DragDropForFilesCheckErrors As String = String.Empty
                'Select Case _powerSvc.DragDropForFilesCheck(fileNames, fileGroupDescriptor, _useMFiles, filePaths, DragDropForFilesCheckErrors)
                Dim fileNames As System.Collections.Specialized.StringCollection
                Try
                    fileNames = args.GetFileDropList()
                Catch exFiles As Exception
                    'not a file
                End Try


                Select Case _powerSvc.DragDropForFilesCheck(fileNames, fileGroupDescriptor, _useMFiles, DragDropForFilesCheckErrors)
                    Case 0 'error
                        Return "Error in DragDropForFilesCheck."
                    Case 1 'yes, drag drop for files,
                        Dim fullPath As String = fileNames(0)
                        filePaths = New List(Of String)()
                        filePaths.Add(fullPath)
                        If Not _dragDropMode = 1 Then
                            ClearDragDropTab()
                        End If
                        _dragDropMode = 1
                        DragDropNonEmailToMFiles(filePaths)
                        Return String.Empty
                    Case 2 'no, drag drop for email,
                        'Fall through and continue below
                        'unless already have an email in the grid
                        If _dragDropMode = 2 Then
                            ClearDragDropTab()
                            Return String.Empty
                        End If
                        _dragDropMode = 2
                End Select
                ClearDragDropTab()
                Dim currentFileName As String = String.Empty

                If dgFiles.Rows.Count = 0 Then
                    If objectDescriptor Then 'they are dragging the attachment
                        Dim app As New Microsoft.Office.Interop.Outlook.Application() ' // get current selected items
                        Dim selection As Microsoft.Office.Interop.Outlook.Selection
                        Dim myText As String = ""
                        selection = app.ActiveExplorer.Selection
                        If selection IsNot Nothing Then
                            For i As Integer = 0 To selection.Count - 1
                                mailItem = TryCast(selection.Item(i + 1), Outlook._MailItem) 'Outlook._MailItem
                                'Dim docsMFilesAccess As New DocsMFilesAccess(ConsoleVertical, GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
                                If mailItem IsNot Nothing Then
                                    Try
                                        myText = headerText & vbCrLf & vbCrLf & mailItem.Body  'Plain Text Body Message
                                        'now save the attachments with the same file name and then 1,2,3 next to it
                                        Dim test As String = mailItem.Subject
                                        For Each att As Attachment In mailItem.Attachments
                                            Dim ext As String = String.Empty
                                            Dim strFile As String = String.Empty
                                            Dim oldTempPath As String = String.Empty
                                            Dim tempPath As String = String.Empty
                                            Dim returnFile As Boolean = False
                                            Dim location As String = String.Empty
                                            Dim errors As String = ProcessDragDropAttachment(currentFileName, ext,
                                              strFile, oldTempPath, tempPath, strNextNum, att, failedFiles, _
                                                returnFile, companyPw, location)
                                            If errors.Length > 0 Then Return errors
                                        Next
                                        'continue with:
                                        dgFiles.EditMode = DataGridViewEditMode.EditOnEnter
                                        Dim subject As String = String.Empty
                                        If IsNothing(mailItem.Subject) OrElse mailItem.Subject.Length < 1 Then
                                            subject = _siteId & " " & DateTime.Today().ToShortDateString().Replace("/", "-")
                                        Else
                                            subject = mailItem.Subject
                                            subject = Utilities.CleanFileName(subject)
                                        End If

                                        Dim strFileName As String = _powerSvc.GetCompanyName(_siteId) ' cboCompany.Text
                                        Dim emailExt As String = ".txt"
                                        emailExt = ".msg"
                                        If strNextNum = "" Then
                                            strFileName = _powerSvc.GetCompanyName(_siteId) ' cboCompany.Text
                                        Else
                                            ' More often, use this technique to name the text file.
                                            If strFileName.Contains("VF") Then
                                                'txtMessageFilename.Text = Utilities.CleanFileName("VR" &  NextVR() & " " & subject.Trim.Replace(":", " ") & emailExt) '".txt"
                                                txtMessageFilename.Text = Utilities.CleanFileName("VR" & _docsAccess.NextV("VR", _corrPath, _siteId, Nothing) & " " & subject.Trim.Replace(":", " ") & emailExt) '".txt"
                                            Else
                                                'If OrigFile Then 'SB NOTE: OrigFile is always false
                                                'txtMessageFilename.Text = Utilities.CleanFileName(subject.Trim.Replace(":", " ") & "-" & NextLetter & emailExt) '".txt"
                                                'Else
                                                txtMessageFilename.Text = Utilities.CleanFileName(subject.Trim.Replace(":", " ") & emailExt) '".txt"
                                                'End If
                                            End If
                                        End If
                                        currentFileName = "Outlook Email (.msg)"
                                        If _useMFiles Then
                                            mailItem.SaveAs(_profileConsoleTempPath + _siteId & emailExt)
                                        Else
                                            mailItem.SaveAs(_profileConsoleTempPath + "EMail" & emailExt)
                                        End If
                                    Catch ex As Exception
                                        failedFiles.Add(currentFileName)
                                        If currentFileName = "Outlook Email (.msg)" Then
                                            txtMessageFilename.Text = String.Empty
                                        Else
                                            dgFiles.Rows.RemoveAt(dgFiles.Rows.Count - 1)
                                        End If
                                        MsgBox("DragDrop error on file " & currentFileName & ": " & ex.Message)
                                        _dAL.WriteLog("DragDrop", ex)
                                    End Try
                                End If
                            Next
                        End If
                    End If
                Else
                    ClearDragDropTab()
                End If
            Catch ex As Exception
                _dAL.WriteLog("DragDrop", ex)
                Return "Error during Drag_Drop: " & ex.Message
            Finally
                mailItem = Nothing
                Try
                    Marshal.ReleaseComObject(mailItem)
                Catch
                End Try
            End Try
            Return String.Empty
        Catch ex As Exception
            Return "Error in DragDropRoutine(): " & ex.Message
        End Try
    End Function

    Private Sub cboDir2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboDir2.SelectedIndexChanged
        T("cboDir2_SelectedIndexChanged")
        Select Case cboDir2.Text

            Case "Site Correspondence"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = True

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = False

            Case "Client Attachments"
                tvwClientAttachments2.Visible = True
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = False

            Case "Drawings"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = True
                tvwCompCorr2.Visible = False
            Case "Company Correspondence"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = True
        End Select

    End Sub

    Private Sub btnGVClear_Click(sender As Object, e As EventArgs) Handles btnGVClear.Click
        ClearDragDropTab()
    End Sub

    Private Sub btnDragTest_Click(sender As Object, e As EventArgs) Handles btnDragTest.Click
        For i As Integer = 0 To cboSiteId.Items.Count - 1
            If cboSiteId.Items(i).ToString().Contains("YYYCC16") Then
                cboSiteId.SelectedIndex = i
                Exit For
            End If
        Next
        _siteId = "YYYCC16"
        SiteIdChanged()
    End Sub




    Private Sub btnSecureSend_Click(sender As Object, e As EventArgs) Handles btnSecureSend.Click
        T("btnSecureSend_Click")
        _vfTemplatesPath = "K:\Study\" & _studyType & "\" & GetStudyYearFromCbo().ToString & "\Templates\"
        Dim networkTemplatePath As String = String.Empty
        If _useMFiles Then
            tvwCorrespondence2.Tag = "USE_MFILES"
            tvwCompCorr2.Tag = "USE_MFILES"
            tvwDrawings2.Tag = "USE_MFILES"
            tvwClientAttachments2.Tag = "USE_MFILES"
        Else
            tvwCorrespondence2.Tag = ""
            tvwCompCorr2.Tag = ""
            tvwDrawings2.Tag = ""
            tvwClientAttachments2.Tag = ""
        End If
        Dim ss As New SecureSend(_dAL, "Power", tvwCorrespondence, tvwDrawings, tvwClientAttachments, tvwCompCorr, _
                                 tvwCorrespondence2, tvwDrawings2, tvwClientAttachments2, tvwCompCorr2, Nothing, Nothing)

        ss.IDRFile = ""
        networkTemplatePath = _vfTemplatesPath.Trim() & "\IDR Instructions.docx"
        If chkIDR.Checked Then

            Dim NewIDR As String = String.Empty
            'If _useMFiles Then
            '    localTemplatePath = TemplatePath & cboStudy.Text.Trim() & " IDR Instructions.docx"
            'Else

            'End If
            If Not File.Exists(networkTemplatePath) Then
                networkTemplatePath = networkTemplatePath.Replace(".docx", ".doc")
            End If
            NewIDR = BuildIDREmail(networkTemplatePath)
            If System.IO.File.Exists(NewIDR) Then
                'seems to be done already-File.Copy(localTemplatePath, _profileConsoleTempPath + "SecureSend\" + Utilities.ExtractFileName(localTemplatePath), True)
                'NewIDR = BuildIDREmail(StudyRegion & StudyYear.Substring(2, 2) & " IDR Instructions.doc")
                ss.IDRFile = NewIDR ' _profileConsoleTempPath & "SecureSend\" & GetStudyRegionFromCboStudy() & Get2DigitYearFromCboStudy().ToString() & " IDR Instructions.doc"
            Else
                MessageBox.Show(NewIDR + " is missing.", "Missing")
                Exit Sub
            End If
        End If

        ss.CompanyContactName = lblName.Text
        ss.RefNum = String.Empty
        ss.PowerSiteId = _siteId
        'ss.LubeRefNum = LubRefNum
        ss.Study = String.Empty
        ss.TemplatePath = networkTemplatePath.Replace(Utilities.ExtractFileName(networkTemplatePath), String.Empty)
        ss.CorrPath = _corrPath
        ss.DrawingPath = DrawingPath
        ss.ClientAttachmentsPath = _clientAttachmentsPath

        ss.Loc = _powerSvc.GetLocation(_siteId) ' Company

        ss.User = UserName
        ss.PCCEmail = lblPCCEmail.Text
        ss.StudyType = cboStudy.Text
        ss.CompCorrPath = _compCorrPath
        ss.CorrPath2 = _corrPath
        ss.DrawingPath2 = DrawingPath
        ss.ClientAttachmentsPath2 = _clientAttachmentsPath
        ss.SendIDR = chkIDR.Checked
        ss.CompCorrPath2 = _compCorrPath
        ss.TempPath = _profileConsoleTempPath + "SecureSend\" '   TempPath
        ss.CompanyPassword = _powerSvc.GetCompanyPassword(_siteId, GetStudyYearFromCbo()) ' CompanyPassword
        ss.ShortRefNum = String.Empty ' GetRefNumCboText()
        ss.LongRefNum = String.Empty 'Utilities.GetLongRefnum(GetRefNumCboText())
        ss.StudyYear = GetStudyYearFromCbo() ' Get4DigitYearFromCboStudy.ToString()
        'ss.UseMFiles = _useMFiles

        'If chkAddFLMacros.Checked Then
        '    Dim filePath As String = GetFLMacrosPath()
        '    ss.FLMacrosPath = ss.TempPath & Utilities.ExtractFileName(filePath)
        '    If _docsAccess.FileExists(ss.TempPath & Utilities.ExtractFileName(filePath)) Then
        '        _docsAccess.FileDelete(ss.TempPath & Utilities.ExtractFileName(filePath))
        '    End If
        '    File.Copy(filePath, ss.TempPath & Utilities.ExtractFileName(filePath))
        'Else
        ss.FLMacrosPath = String.Empty
        'End If
        ss.Show()

    End Sub
    Private Function BuildIDREmail(filePath As String) As String
        T("BuildIDREmail")
        Dim TemplateValues As New SA.Console.Common.WordTemplate()

        Dim Coloc As String

        TemplateValues.Field.Add("_TodaysDate")
        TemplateValues.RField.Add(Format(Now, "dd-MMM-yy"))


        Dim Row As DataRow
        Dim params As List(Of String)

        TemplateValues.Field.Add("_Company")
        TemplateValues.RField.Add(_powerSvc.GetCompanyName(_siteId))
        TemplateValues.Field.Add("_Refinery")
        TemplateValues.RField.Add(_powerSvc.GetLocation(_siteId))
        'Coloc = Row("Coloc").ToString

        TemplateValues.Field.Add("_Contact")
        TemplateValues.RField.Add(lblName.Text)
        ' Contact Email Address

        TemplateValues.Field.Add("_EMail")
        TemplateValues.RField.Add(lblEmail.Text)

        Dim strName As String = String.Empty
        Dim found As Boolean = _powerSvc.GetConsultantInfo(UserName, strName, String.Empty, String.Empty)
        If Not found OrElse IsNothing(strName) OrElse strName.Length < 1 Then
            strName = InputBox("I cannot find you in the employee table, please give me your name: ", "Console.ConsultantInfo table")
        End If

        TemplateValues.Field.Add("_ConsultantName")
        TemplateValues.RField.Add(strName)
        ' Consultant Initials

        TemplateValues.Field.Add("_Initials")
        TemplateValues.RField.Add(UserName.ToUpper())


        'don't create this in  "SecureSend\" folder; SecureSend deletes everything in that folder when it loads.
        'Utilities.WordTemplateReplace(mfile, TemplateValues, _profileConsoleTempPath & "SecureSend\" & Utilities.ExtractFileName(mfile), 0)
        Utilities.WordTemplateReplace(filePath, TemplateValues, _profileConsoleTempPath & Utilities.ExtractFileName(filePath), 0)


        Return _profileConsoleTempPath & Utilities.ExtractFileName(filePath)
    End Function


    Private Sub btnSecureSendRefresh_Click(sender As Object, e As EventArgs) Handles btnSecureSendRefresh.Click
        T("btnSecureSendRefresh_Click")
        Me.Cursor = Cursors.WaitCursor
        btnSecureSendRefreshClick()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnSecureSendRefreshClick()
        ClearSecureSendTab()
        PopulateSecureSendTab()
    End Sub
    
    Private Sub btnFilesSave_Click(sender As Object, e As EventArgs) Handles btnFilesSave.Click
        T("btnFilesSave_Click")
        If _useMFiles Then
            If txtMessageFilename.Enabled = True Then 'came from email, not files dropped onto form
                If txtMessageFilename.Text.Trim().Length < 1 Then
                    txtMessageFilename.Text = _siteId.Trim() & ".msg"
                End If
                Dim ext As String = Utilities.GetFileExtension(txtMessageFilename.Text)
                If ext.ToUpper() <> "MSG" Then
                    txtMessageFilename.Text = Utilities.FileNameWithoutExtension(txtMessageFilename.Text) + ".msg"
                End If
            End If
            btnFilesSaveMFiles()
            _dragDropMode = 0
        Else
            btnFilesSaveLegacy()
        End If
        btnSecureSendRefreshClick()
    End Sub
    Private Function FixFilename(ext As String, strFile As String)
        T("FixFilename")
        Dim strNewFilename As String
        Dim NewExt As String = Utilities.GetFileExtension(strFile)
        strNewFilename = strFile.Replace(NewExt, ext)
        Return strNewFilename
    End Function
    Private Sub btnFilesSaveLegacy()
        T("btnFilesSaveLegacy")
        Dim CopyFile As String
        Dim Dest As String
        Dim DestFile As String
        Dim ext As String
        Dim success As Boolean = True

        For i As Integer = 0 To dgFiles.Rows.Count - 1
            Dim val As String = dgFiles.Rows(i).Cells(2).Value
            If val.Trim().Length < 1 Then
                MsgBox("Please ensure all attachments have a destination and try again.")
                Return
            End If
        Next

        Try
            '======================================================================================================
            'Commented out the company password checking. Power is not using it now. Uncommented below when needed. by LW 8/24/2018
            '======================================================================================================
            'Dim CompanyPassword As String = btnCompanyPW.Text
            For I = 0 To dgFiles.Rows.Count - 1

                Dest = dgFiles.Rows(I).Cells(2).Value
                If Dest <> "Do Not Save" Then
                    lblStatus.Text = "Saving " & dgFiles.Rows(I).Cells(0).Value & "...."
                    Try
                        ext = Utilities.GetFileExtension(dgFiles.Rows(I).Cells(0).Value)
                    Catch
                        ext = ""
                    End Try

                    CopyFile = _profileConsoleTempPath & FixFilename(ext, dgFiles.Rows(I).Cells(0).Value)

                    DestFile = FixFilename(ext, dgFiles.Rows(I).Cells(1).Value)
                    Dim pathToUse As String = String.Empty
                    Select Case (Dest)
                        Case "Correspondence"
                            pathToUse = _corrPath
                        Case "Company Correspondence"
                            pathToUse = _compCorrPath
                        Case "Drawings"
                            pathToUse = _drawingPath
                    End Select
                    'Create the path if not existed [by LW 8/24/2018]
                    If Not Directory.Exists(pathToUse) Then
                        Directory.CreateDirectory(pathToUse)
                    End If
                    '======================================================================================================
                    'Commented out the company password checking. Power is not using it now. Uncommented below when needed. by LW 8/24/2018
                    '======================================================================================================

                    'If (ext.ToUpper = "DOC" Or ext.ToUpper = "DOCX" Or ext.ToUpper.Substring(0, 3) = "XLS") Then
                    'If CompanyPassword.Length = 0 Then
                    '    MessageBox.Show("Must have a Company Password to Encrypt files", "Save Error")
                    '    Exit Sub
                    'End If

                    'success = RefiningRemovePassword(CopyFile, DestFile, _powerSvc.GetReturnFilePassword(_siteId), CompanyPassword)
                    'If success Then
                    'File.Copy(CopyFile, pathToUse & DestFile, True)
                    'File.Delete(CopyFile)
                    'Else
                    '    Dim company As String = _powerSvc.GetCompanyName(_siteId)
                    '    If company.Length > 0 Then
                    '        company = ": " & company
                    '    End If
                    '    MessageBox.Show("Appears the password is incorrect for " & CopyFile & ".  Make sure this is the correct company " & company)
                    '    Exit Sub
                    'End If
                    'Else
                    File.Copy(CopyFile, pathToUse & DestFile, True)
                    File.Delete(CopyFile)
                    'End If
                End If
            Next

            If optCompanyCorr.Checked Then
                File.Copy(_profileConsoleTempPath & "Email.msg", _compCorrPath & Utilities.CleanFileName(txtMessageFilename.Text), True)
            Else
                File.Copy(_profileConsoleTempPath & "Email.msg", _corrPath & Utilities.CleanFileName(txtMessageFilename.Text), True)
            End If

            File.Delete(_profileConsoleTempPath & "Email.msg")

            dgFiles.Rows.Clear()

            'Dim d As DialogResult = MessageBox.Show("Email and attachments saved.  Would you like to open the correspondence directory?", "Open Folder", MessageBoxButtons.YesNo)
            'If d = System.Windows.Forms.DialogResult.Yes Then Process.Start(_corrPath)
            txtMessageFilename.Text = ""
            btnFilesSave.Enabled = False
            'If OldTempPath.Length > 0 Then TempPath = OldTempPath
            lblStatus.Text = "Attachments Saved"
            'ConsoleTabs.TabPages.Remove(tabDD)
            ConsoleTabs.SelectedIndex = 1
        Catch ex As System.Exception
            _dAL.WriteLog("btnSaveFiles", ex)
            lblStatus.Text = "Error saving files: " & ex.Message
        End Try
    End Sub
    Private Function RefiningRemovePassword(File As String, dFile As String, password As String, companypassword As String) As Boolean
        T("RefiningRemovePassword")
        Dim success As Boolean = True
        Dim ext As String = Utilities.GetFileExtension(File)
        If ext.Length > 2 AndAlso ext.ToUpper.Substring(0, 3) = "XLS" Then
            If File.ToUpper.Contains("RETURN") Or dFile.ToUpper.Contains("RETURN") Then
                success = Utilities.ExcelPassword(File, password, 1)
            Else
                success = Utilities.ExcelPassword(File, companypassword, 1)
            End If
        End If
        If File.Substring(File.Length - 3, 3).ToUpper = "DOC" Or File.Substring(File.Length - 3, 3).ToUpper = "OCX" Then
            success = Utilities.WordPassword(File, companypassword, 1)
        End If
        Return success
    End Function
    Private Sub btnFilesSaveMFiles()
        T("btnFilesSaveMFiles")
        'need to check names and see if clash with MFiles docs.

        'get list of to-be filenames 
        Dim consoleCorrespFiles As New List(Of ConsoleFile)
        Dim consoleCompanyCorrespFiles As New List(Of ConsoleFile)
        Dim consoleGeneralDrawingsFiles As New List(Of ConsoleFile)
        Dim consoleGeneralFiles As New List(Of ConsoleFile)

        Dim companyBenchmark As String = String.Empty
        Dim correspondenceBenchmarkingId As Integer = -1
        Dim companyCorrBenchmarkingId As Integer = -1
        Dim nameClash As Boolean = False

        'EUR16Co-EXAMPLE
        companyBenchmark = _powerSvc.GetCompanyBenchmarkName(_siteId, 2000 - GetStudyYearFromCbo(), _mfilesDesktopUIPath) ' GetCompanyBenchmarkName()
        'Dim tempConsoleFileInfo = _docsAccess.GetDocInfoByNameAndRefnum(companyBenchmarkRefnum, companyBenchmarkRefnum, companyBenchmarkRefnum, False)

        'HttpContext.Current.Server.HtmlEncode(longRefNum);
        'Dim encoded As String = companyBenchmarkRefnum.Replace("&", "&amp;")

        'm encodedRefnum As String = System.Web.HttpContext.Current.Server.HtmlEncode(encoded)

        'DON'T USE the instance-level mfiles cached-docs objects. We want to get a fresh list of mfiles docs for the name clash check
        Dim tempConsoleFileInfo As ConsoleFilesInfo ' = _docsAccess.GetDocInfoByNameAndRefnum(companyBenchmarkRefnum, companyBenchmarkRefnum, companyBenchmarkRefnum, False)
        Dim companyMfiles As DocsMFilesAccess '(ConsoleVertical, companyBenchmarkRefnum, companyBenchmarkRefnum)
        Try
            tempConsoleFileInfo = _docsAccess.GetDocInfoByNameAndBenchmarkingParticipantName(companyBenchmark, companyBenchmark, False)
            companyMfiles = New DocsMFilesAccess(ConsoleVertical, companyBenchmark)
        Catch ex As Exception
            If Not ex.Message.Contains("Unable to find Benchmarking Participant for") Then
                MsgBox(ex.Message)
                Return
            End If
        End Try
        If Not IsNothing(companyMfiles) AndAlso companyMfiles.BenchmarkingParticipantId > -1 Then
            companyCorrBenchmarkingId = companyMfiles.BenchmarkingParticipantId
        End If

        For i = 0 To dgFiles.Rows.Count - 1
            If dgFiles.Rows(i).Cells(2).Value <> "Do Not Save" Then
                Dim gridFileName As String = dgFiles.Rows(i).Cells(1).Value
                If dgFiles.Rows(i).Cells(2).Value = "Correspondence" Then
                    'DON'T USE the instance-level mfiles cached-docs objects. We want to get a fresh list of mfiles docs for the name clash check
                    Dim mfilesForCorresp As New DocsMFilesAccess(VerticalType.POWER, _siteId, _siteId)
                    If Not IsNothing(mfilesForCorresp) Then
                        correspondenceBenchmarkingId = mfilesForCorresp.BenchmarkingParticipantId
                    End If
                    Dim consoleFileInfo As ConsoleFilesInfo = _docsAccess.GetDocInfoByNameAndRefnum( _
                    Utilities.FileNameWithoutExtension(gridFileName), _siteId, _siteId, False)
                    If Not IsNothing(consoleFileInfo) AndAlso Not IsNothing(consoleFileInfo.Files) Then
                        If Not IsNothing(consoleFileInfo) Then
                            For Each consoleFile As ConsoleFile In consoleFileInfo.Files
                                If consoleFile.FileExtension.ToUpper = Utilities.GetFileExtension(gridFileName).ToUpper() Then '.Substring(gridFileName.Length - 3, 3).ToUpper() Then
                                    nameClash = True
                                    consoleCorrespFiles.Add(consoleFile)
                                End If
                            Next
                        End If
                        '//consoleFiles.Add(consoleFileInfo.Files(0)) 'prospectiveFileName + _delim + FileInfo.Files(0).Id.ToString)
                    Else
                        Dim newFile As New ConsoleFile(True)
                        newFile.FileName = Utilities.FileNameWithoutExtension(gridFileName)
                        newFile.FileExtension = Utilities.GetFileExtension(gridFileName)
                        consoleCorrespFiles.Add(newFile)
                    End If
                ElseIf dgFiles.Rows(i).Cells(2).Value = "Company Correspondence" Then
                    Dim consoleFileInfo As ConsoleFilesInfo = Nothing
                    If companyBenchmark.Length > 0 Then
                        'DON'T USE the instance-level mfiles cached-docs objects. We want to get a fresh list of mfiles docs for the name clash check
                        consoleFileInfo = _docsAccess.GetDocInfoByNameAndBenchmarkingParticipantName( _
                        Utilities.FileNameWithoutExtension(gridFileName), companyBenchmark, False)
                        If Not IsNothing(consoleFileInfo) Then
                            companyCorrBenchmarkingId = consoleFileInfo.BenchmarkingParticipantId
                        End If
                    Else
                        consoleFileInfo = New ConsoleFilesInfo(True, Common.VerticalType.POWER)
                    End If
                    If Not IsNothing(consoleFileInfo) AndAlso Not IsNothing(consoleFileInfo.Files) Then
                        For Each consoleFile As ConsoleFile In consoleFileInfo.Files
                            If consoleFile.FileExtension.ToUpper = Utilities.GetFileExtension(gridFileName).ToUpper() Then
                                nameClash = True
                                consoleCompanyCorrespFiles.Add(consoleFile)
                            End If
                        Next
                    Else
                        Dim newFile As New ConsoleFile(True)
                        newFile.FileName = Utilities.FileNameWithoutExtension(gridFileName)
                        newFile.FileExtension = Utilities.GetFileExtension(gridFileName)
                        consoleCompanyCorrespFiles.Add(newFile)
                    End If
                End If
            End If
        Next
        If nameClash Then
            Dim message As String = "You are about to save files with these names: " + Environment.NewLine
            For Each consoleFile As ConsoleFile In consoleCorrespFiles
                If consoleFile.Id > 0 Then
                    message += "       " + consoleFile.FileName + "." + consoleFile.FileExtension + "," + Environment.NewLine
                End If
            Next
            For Each consoleFile As ConsoleFile In consoleCompanyCorrespFiles
                If consoleFile.Id > 0 Then
                    message += "       " + consoleFile.FileName + "." + consoleFile.FileExtension + "," + Environment.NewLine
                End If
            Next
            For Each consoleFile As ConsoleFile In consoleGeneralDrawingsFiles
                If consoleFile.Id > 0 Then
                    message += "       " + consoleFile.FileName + "." + consoleFile.FileExtension + "," + Environment.NewLine
                End If
            Next
            For Each consoleFile As ConsoleFile In consoleGeneralFiles
                If consoleFile.Id > 0 Then
                    message += "       " + consoleFile.FileName + "." + consoleFile.FileExtension + "," + Environment.NewLine
                End If
            Next
            message += " but files with these names already exist in MFiles." + Environment.NewLine + Environment.NewLine
            message += "Click 'Yes' to overwrite the MFiles, making your file the latest version." + Environment.NewLine
            message += "Click 'No' to go back and make changes to the file names"
            If MsgBox(message, MsgBoxStyle.YesNo, "Console - MFiles") <> vbYes Then Exit Sub
        End If
        'separate code from UI
        DragNDropSaveMFiles(consoleCorrespFiles, correspondenceBenchmarkingId, consoleCompanyCorrespFiles, companyCorrBenchmarkingId, companyBenchmark, consoleGeneralDrawingsFiles, consoleGeneralFiles)
    End Sub
    Private Sub DragNDropSaveMFiles(ByRef consoleFiles As List(Of ConsoleFile), correspondenceBenchmarkingId As Integer,
                                ByRef companyCorrespondenceFiles As List(Of ConsoleFile), companyCorrBenchmarkingId As Integer,
                                companyCorrespondenceRefnum As String,
                                ByRef consoleGeneralDrawingsFiles As List(Of ConsoleFile),
                                ByRef consoleGeneralFiles As List(Of ConsoleFile))
        T("DragNDropSaveMFiles")
        Dim Dest As String
        Dim success As Boolean = False

        Try
            Dim docsMFilesAccessCorrespondence As DocsMFilesAccess = Nothing
            docsMFilesAccessCorrespondence = New DocsMFilesAccess(ConsoleVertical, _siteId, _siteId)
            Dim docsMFilesAccessCompany As DocsMFilesAccess = Nothing
            Try
                docsMFilesAccessCompany = New DocsMFilesAccess(ConsoleVertical, companyCorrespondenceRefnum)
            Catch ex As Exception
                If Not ex.Message.Contains("Unable to find Benchmarking Participant for") Then
                    MsgBox(ex.Message)
                    Return
                End If
            End Try

            For i = 0 To dgFiles.Rows.Count - 1
                Dest = dgFiles.Rows(i).Cells(2).Value
                Dim fileName As String = dgFiles.Rows(i).Cells(0).Value
                If Dest <> "Do Not Save" Then
                    'txtDragDropStatus.Text = "Saving " & fileName & "...."
                    If Dest = "Correspondence" Then
                        If Not ProcessDragDropMFile(fileName, dgFiles.Rows(i).Cells(1).Value.Trim(), consoleFiles, correspondenceBenchmarkingId, docsMFilesAccessCorrespondence) Then
                            MsgBox("Error occurred in DragNDropSaveMFiles," + dgFiles.Rows(i).Cells(1).Value.Trim() + " was not saved.")
                        End If
                    ElseIf Dest = "Company Correspondence" AndAlso Not IsNothing(docsMFilesAccessCompany) Then
                        If Not ProcessDragDropMFile(fileName, dgFiles.Rows(i).Cells(1).Value.Trim(), companyCorrespondenceFiles, companyCorrBenchmarkingId, docsMFilesAccessCompany) Then
                            MsgBox("Error occurred in DragNDropSaveMFiles," + dgFiles.Rows(i).Cells(1).Value.Trim() + " was not saved.")
                        End If
                        'ElseIf Dest = "Drawings" Then
                        '    If Not ProcessDragDropMFileGeneralItem(fileName, dgFiles.Rows(i).Cells(1).Value.Trim(), consoleGeneralDrawingsFiles, 2) Then
                        '        MsgBox("Error occurred in DragNDropSaveMFiles," + dgFiles.Rows(i).Cells(1).Value.Trim() + " was not saved.")
                        '    End If
                        'ElseIf Dest = "General" Then
                        '    If Not ProcessDragDropMFileGeneralItem(fileName, dgFiles.Rows(i).Cells(1).Value.Trim(), consoleGeneralFiles, 0) Then
                        '        MsgBox("Error occurred in DragNDropSaveMFiles," + dgFiles.Rows(i).Cells(1).Value.Trim() + " was not saved.")
                        '    End If
                    End If
                Else
                    Try
                        If File.Exists(_profileConsoleTempPath + fileName) Then
                            File.Delete(_profileConsoleTempPath + fileName)
                        ElseIf File.Exists(_profileConsoleExtractedPath + fileName) Then
                            File.Delete(_profileConsoleExtractedPath + fileName)
                        End If
                    Catch
                    End Try
                End If  'end of  If Dest <> "Do Not Save" Then
            Next

            'save email
            'mailItem.SaveAs(_profileConsoleTempPath + GetRefNumCboText() + ".msg")
            If txtMessageFilename.Enabled Then
                If Not optCompanyCorr.Checked Then
                    'save email to REfinery corr
                    If Not ProcessDragDropMFileEmail(docsMFilesAccessCorrespondence, correspondenceBenchmarkingId) Then
                        MsgBox("Error occured; email not saved.")
                    End If
                Else
                    'save email to Company  corresp
                    If Not ProcessDragDropMFileEmail(docsMFilesAccessCompany, companyCorrBenchmarkingId) Then
                        MsgBox("Error occured; email not saved.")
                    End If
                End If
            End If
            'start move to new method---------------------------
            'Dim consoleMFiles As ConsoleFilesInfo = DocsMFilesAccess.GetDocNamesAndIdsByBenchmarkingParticipant(Utilities.GetLongRefnum(cboRefNum.Text))
            'Dim emailFileId As Integer = -1
            'For Each consoleMFile As ConsoleFile In consoleMFiles.Files
            '    If consoleMFile.FileName.ToUpper().Trim() = Utilities.FileNameWithoutExtension(txtMessageFilename.Text.ToUpper.Trim()) Then
            '        emailFileId = consoleMFile.Id
            '        Exit For
            '    End If
            'Next
            'File.Move(_profileConsoleTempPath + cboRefNum.Text + ".msg", _profileConsoleTempPath + txtMessageFilename.Text)
            'If emailFileId > -1 Then 'name clash
            '    DocsMFilesAccess.UploadNewVersionOfDocToMFiles(_profileConsoleTempPath + txtMessageFilename.Text, emailFileId)
            'Else
            '    DocsMFilesAccess.UploadNewDocToMFiles(_profileConsoleTempPath + txtMessageFilename.Text, 12, -1, Nothing)
            'End If
            ''end move to new method--------------------------------

            dgFiles.Rows.Clear()
            txtMessageFilename.Text = ""
            'txtDragDropStatus.Text = "Attachments Saved"
            DeleteExtractedFolder()

            Dim zfilenames() As String = Directory.GetFiles(_profileConsoleTempPath, "*.*")
            For Each sFile In zfilenames
                Try
                    File.Delete(sFile)
                Catch 'it's not that important. Will get cleared out during next Drag-N-Drop
                End Try
            Next
        Catch ex As System.Exception
            _dAL.WriteLog("btnSaveFiles", ex)
            'txtDragDropStatus.Text = "Error saving files: " & ex.Message
        Finally
            'RefreshMFilesCollections(0)
            'RefreshMFilesCollections(1)
            'RefreshMFilesCollections(2)
        End Try
    End Sub
    Private Sub DeleteExtractedFolder()
        If Directory.Exists(_profileConsoleTempPath) = False Then Directory.CreateDirectory(_profileConsoleTempPath)
        Dim extractedFilesFolderPath As String = _profileConsoleTempPath + "Extracted Files\"

        System.Threading.Thread.Sleep(2000)
        If Directory.Exists(extractedFilesFolderPath) Then
            If Dir(extractedFilesFolderPath) <> "" Then
                Dim zfilenames() As String = Directory.GetFiles(extractedFilesFolderPath, "*.*")
                For Each sFile In zfilenames
                    Try
                        File.Delete(sFile)
                    Catch
                        'it's not that important. Will get cleared out during next Drag-N-Drop
                    End Try
                Next
                Try
                    Directory.Delete(extractedFilesFolderPath)
                Catch
                    'it's not that important. Will get cleared out during next Drag-N-Drop
                End Try
            End If
        End If
    End Sub
    Private Function ProcessDragDropMFile(fileName As String, saveAsFileName As String, _
                                      ByRef filesList As List(Of ConsoleFile), benchmark As Integer,
                                      ByRef mFilesAccess As DocsMFilesAccess) As Boolean
        T("ProcessDragDropMFile")
        Try
            Dim ext As String = String.Empty
            Try
                ext = Utilities.GetFileExtension(fileName)
            Catch  'ext = ""
            End Try

            '##### NOTE: FixFilename below will change file extension!!!
            Dim localPath As String = String.Empty
            If File.Exists(_profileConsoleTempPath + fileName) Then
                localPath = _profileConsoleTempPath
            ElseIf File.Exists(_profileConsoleExtractedPath + fileName) Then
                localPath = _profileConsoleExtractedPath
            Else
                MsgBox("Unable to determine localPath in ProcessDragDropMFile()")
                Return False
            End If
            Dim CopyFile As String = FixFilename(ext, fileName.Trim()) ' will change file extension!!!
            Dim DestFile As String = FixFilename(ext, saveAsFileName)  'will change file extension!!!
            'If Not CopyFile.EndsWith(".msg") Then 'handle it below
            Dim nameClash As Boolean = False
            Dim consoleFileId As Integer = 0
            Dim found As Boolean = False

            For Each consoleFile As ConsoleFile In filesList 'ths could be Corresp or Company
                '1 try to match up Existing Filename with consoleFile
                '2 if not find, then is handled below this For
                'If CopyFile.ToUpper() = consoleFile.FileName.ToUpper() + _
                If DestFile.ToUpper() = consoleFile.FileName.ToUpper() + _
                    "." + consoleFile.FileExtension.ToUpper() Then
                    found = True
                    If consoleFile.Id > 0 Then
                        nameClash = True ' will save as new version of existing file
                        consoleFileId = consoleFile.Id
                    Else
                        'new file to be uploadd to MFiles. No other action needed.
                    End If
                    Exit For
                End If
            Next

            If Not found Then
                'User must have changed to name in 2nd column so rename and save as that.
                If File.Exists(localPath + fileName) Then
                    File.Move(localPath + fileName, localPath + DestFile.Trim())
                    CopyFile = DestFile.Trim()
                End If
            End If
            If (File.Exists(localPath + fileName)) And (Not File.Exists(localPath + DestFile.Trim())) Then
                'User must have changed to name in 2nd column so rename and save as that.
                File.Move(localPath + fileName, localPath + DestFile.Trim())
                CopyFile = DestFile.Trim()
            End If


            If nameClash Then
                mFilesAccess.UploadNewVersionOfDocToMFiles(localPath & CopyFile, consoleFileId)
            Else 'is new doc, benchmark could be for corresp or for Company.
                mFilesAccess.UploadNewDocToMFiles(localPath & CopyFile, 12, benchmark, Nothing)
            End If
            Try
                File.Delete(localPath + CopyFile)
            Catch exDelete As Exception
                Dim discard As String = String.Empty
            End Try
            Return True
        Catch ex As Exception
            _dAL.WriteLog("Error in ProcessDragDropMFile().", ex)
            Return False
        End Try
    End Function
    Private Function ProcessDragDropMFileEmail(ByRef mfilesAccess As DocsMFilesAccess, benchmark As Integer) As Boolean
        T("ProcessDragDropMFileEmail")
        Dim emailCorrespFileId As Integer = -1
        Try
            Dim consoleEmailCorresp As ConsoleFilesInfo '= mfilesAccess.GetDocNamesAndIdsByBenchmarkingParticipant(_siteId)
            Try
                consoleEmailCorresp = mfilesAccess.GetDocNamesAndIdsByBenchmarkingParticipant(_siteId)
                For Each consoleMFile As ConsoleFile In consoleEmailCorresp.Files
                    If consoleMFile.FileName.ToUpper().Trim() = Utilities.FileNameWithoutExtension(txtMessageFilename.Text.ToUpper.Trim()) Then
                        emailCorrespFileId = consoleMFile.Id
                        Exit For
                    End If
                Next
            Catch exEmailCorresp As Exception

            End Try
            File.Move(_profileConsoleTempPath + _siteId + ".msg", _profileConsoleTempPath + txtMessageFilename.Text)
            If emailCorrespFileId > -1 Then 'name clash
                mfilesAccess.UploadNewVersionOfDocToMFiles(_profileConsoleTempPath + txtMessageFilename.Text, emailCorrespFileId)
            Else
                mfilesAccess.UploadNewDocToMFiles(_profileConsoleTempPath + txtMessageFilename.Text, 12, benchmark, Nothing)
            End If
            Return True
        Catch ex As Exception
            Dim msg As String = String.Empty
            If emailCorrespFileId < 0 Then 'no way to save it to a company folder, probably
                msg = "Error saving Email msg file; no M-Files virtual folder exists to save the file to."
            Else
                msg = "Error in ProcessDragDropMFileEmail(): " + ex.Message
            End If
            MsgBox(msg)
            _dAL.WriteLog("Error in ProcessDragDropMFileEmail().", ex)
            Return False
        End Try
    End Function


    Private Sub btnCorrespRefresh_Click(sender As Object, e As EventArgs) Handles btnCorrespRefresh.Click
        T("btnCorrespRefresh_Click")
        CorrespRefresh()
    End Sub

    Private Sub CorrespRefresh()
        Me.Cursor = Cursors.WaitCursor
        ClearCorrespondenceTab()
        PopulateCorrespondenceTab()
        Me.Cursor = Cursors.Default
    End Sub
 
    Private Sub lstVFNumbers_Click(sender As Object, e As EventArgs) Handles lstVFNumbers.Click
        T("lstVFNumbers_Click")
        Me.Cursor = Cursors.WaitCursor
        SetCorr()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnReceiptAck_Click(sender As Object, e As EventArgs) Handles btnReceiptAck.Click
        T("btnReceiptAck_Click")
        Dim strDirResult As String
        Dim strDateTimeToPrint As String

        'strDateTimeToPrint = InputBox("Enter the date/time that you want to show. Click OK to use present.", "Now or earlier?", Now())

        'If strDateTimeToPrint = "" Then
        '    MsgBox("Receipt Acknowledgement was canceled.")
        '    Exit Sub
        'End If

        If _useMFiles Then
            Dim fullPath As String = _profileConsoleTempPath & "VA" & Me.lstVFNumbers.Text & ".txt"
            Using objWriter As New System.IO.StreamWriter(fullPath, True)
                objWriter.WriteLine(UserName)
                objWriter.WriteLine(strDateTimeToPrint)
                objWriter.Close()
            End Using
            Try
                Dim deliverable As Integer = 12
                Dim siteIdInsteadOfRefnum As String = _siteId
                Dim benchmarkingParticipantId As Integer = _benchmarkingParticipantSiteFiles.BenchmarkingParticipantId
                '_docsAccess.UploadDocToMfiles(finalPathToWordDoc, docClassForIdr, siteIdInsteadOfRefnum,
                '              deliverableType, benchmarkingParticipantId)
                'If _docsAccess.UploadDocToMfiles(fullPath, 52, GetRefNumCboText(), deliverable, -1, DateTime.Today) Then

                If _docsAccess.UploadDocToMfiles(fullPath, _docClassForIdr, siteIdInsteadOfRefnum,
                                                 deliverable, benchmarkingParticipantId, DateTime.Today) Then
                    MsgBox("VA" & Me.lstVFNumbers.Text & " was saved to M-Files")
                Else
                    MsgBox("VA" & Me.lstVFNumbers.Text & " was NOT saved to M-Files")
                End If
                Try
                    File.Delete(fullPath)
                Catch
                End Try
            Catch ex As Exception
                MsgBox("VA" & Me.lstVFNumbers.Text & " was NOT saved to M-Files: " + ex.Message)
            End Try
            'RefreshMFilesCollections(0)
        Else
            strDirResult = Dir(_corrPath & "VA" & Me.lstVFNumbers.Text & ".txt")
            If strDirResult <> "" Then
                MsgBox("Receipt of VF" & Me.lstVFNumbers.Text & " has already been noted.")
                Exit Sub
            End If

            strDateTimeToPrint = InputBox("Enter the date/time that you want to show. Click OK to use present.", "Now or earlier?", Now())

            If strDateTimeToPrint <> "" Then

                Dim objWriter As New System.IO.StreamWriter(_corrPath & "VA" & Me.lstVFNumbers.Text & ".txt", True)

                objWriter.WriteLine(UserName)
                objWriter.WriteLine(strDateTimeToPrint)
                objWriter.Close()
                CorrespRefresh()
                MsgBox("VA" & Me.lstVFNumbers.Text & ".txt has been built.")
            Else
                MsgBox("Receipt Acknowledgement was canceled.")
            End If
        End If
    End Sub

    Private Function BuildDataFolderPath() As String
        T("BuildDataFolderPath")
        'NOTE: this is for Plant Correspondence.
        Dim path As String = Nothing
        Dim proposedPath As String = "K:\Study\Power\" & cboStudy.Text.Trim() & "\Plant\" & _siteId.Trim() & "\" ' StudyDrive & fourDigitYear.ToString() & "\" & thisStudyRegion & "\Refinery\" & thisRefnum & "\"
        If Directory.Exists(proposedPath) Then
            T("BuildDataFolderPath - path exists")
            T("-proposedPath=" & proposedPath)
            path = proposedPath
        End If
        Return path
    End Function

    Private Sub btnDataFolder_Click(sender As Object, e As EventArgs) Handles btnDataFolder.Click
        T("btnDataFolder_Click")
        Try
            Process.Start(_dataFolderPath)
        Catch
        End Try
    End Sub

    Private Sub OpenNetworkFile(path As String)

        Try
            Process.Start(path)
        Catch ex As Exception
            MsgBox("Console was unable to open the path '" & path & "' for you. Please try opening path to the file, and try clicking this button again.")
        End Try

    End Sub

    Private Sub btnOpenCompanyCorresp_Click(sender As Object, e As EventArgs) Handles btnOpenCompanyCorresp.Click
        T("btnOpenCompanyCorresp_Click")
        If Not _useMFiles Then
            Try
                Process.Start(_compCorrPath)
            Catch ex As Exception
                MsgBox("Path " & _compCorrPath & " does not exist")
            End Try
        Else
            If _mfilesFailed Then
                MsgBox("There was a failure to get MFiles information. Console is unable to open MFiles for you.")
                Exit Sub
            End If

            Dim vpath As String = GetMfilesBenchmarkingCompanyVPath()
            Try
                Process.Start(vpath)
            Catch ex As Exception
                MsgBox("Console was unable to open the M-Files path '" & vpath & "' for you. Please try opening M-Files, making sure it is not Offline, and try clicking this button again.")
            End Try
        End If
    End Sub
    Private Function GetMfilesBenchmarkingCompanyVPath() As String
        T("GetMfilesBenchmarkingCompanyVPath")
        PopulateMFilesVPaths()
        Return _mfilesBenchmarkingCompanyVPath
    End Function
    Private Sub PopulateMFilesVPaths()
        T("PopulateMFilesVPaths")
        Dim trimmedSiteId As String = _siteId.Trim()
        If trimmedSiteId.Length < 1 Then Return
        If _mfilesBenchmarkingParticipantVPath.Length < 1 Or
            _mfilesBenchmarkingCompanyVPath.Length < 1 Then 'Or
            '_mfilesRefiningGeneralVPath.Length < 1 Then
            Try
                If _powerSvc.LookupMFilesVPaths(_mfilesDesktopUIPath, trimmedSiteId,
                                                _mfilesBenchmarkingParticipantVPath,
                                                _mfilesBenchmarkingCompanyVPath) Then
                    'Return True
                End If
                'Dim ds As DataSet = _dAL.Execute()
                'If Not IsNothing(ds) AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then

                '    If Not IsDBNull(_mfilesBenchmarkingParticipantVPath = ds.Tables(0).Rows(0)("RefStudyFolder")) Then
                '        _mfilesBenchmarkingParticipantVPath = ds.Tables(0).Rows(0)("RefStudyFolder").ToString()
                '    End If
                '    If Not IsDBNull(_mfilesBenchmarkingCompanyVPath = ds.Tables(0).Rows(0)("CoStudyFolder")) Then
                '        _mfilesBenchmarkingCompanyVPath = ds.Tables(0).Rows(0)("CoStudyFolder").ToString()
                '    End If
                '    'If Not IsDBNull(_mfilesRefiningGeneralVPath = ds.Tables(0).Rows(0)("RefineryGeneral")) Then
                '    '    _mfilesRefiningGeneralVPath = ds.Tables(0).Rows(0)("RefineryGeneral").ToString()
                '    'End If

                'End If
            Catch ex As Exception
                _dAL.WriteLog("PopulateMFilesVPaths: " & _siteId, ex)
            Finally
                '_dAL.SQL = String.Empty
            End Try
        End If
    End Sub


    Private Sub btnCreatePN_Click(sender As Object, e As EventArgs) Handles btnCreatePN.Click
        T("btnCreatePN_Click")
        If _refNum.Length < 1 Then
            MsgBox("Please select a Refnum and try again.")
            Return
        End If
        If Not _useMFiles Then
            PresentersNotesLegacy()
        Else
            PresentersNotesMFiles()
        End If
    End Sub
    Private Sub PresentersNotesLegacy()
        T("PresentersNotesLegacy")
        If File.Exists(_corrPath & "PN_" & _companyName & ".doc") Then
            Process.Start(_corrPath & "PN_" & _companyName & ".doc")
        Else
            Dim dr As DialogResult = MessageBox.Show("Are you sure you want to create the PN Word file?   This will lock the field.", "Wait", MessageBoxButtons.YesNo)
            If dr = System.Windows.Forms.DialogResult.Yes Then
                Try
                    Dim myText As String = "Presenter Notes for" & vbCrLf & _companyName & vbCrLf & vbCrLf
                    Dim params = New List(Of String)
                    Dim row As DataRow

                    lblStatus.Text = "Creating Presenter's Word file..."
                    Dim strw As New StreamWriter(_profileConsoleTempPath & "PN_" & _companyName & ".txt")
                    strw.Write(_powerSvc.GetNotes(_refNum))
                    strw.Close()
                    strw.Dispose()
                    Dim doc As String = Utilities.ConvertToDoc(_profileConsoleTempPath, _profileConsoleTempPath & "PN_" & _companyName & ".txt")
                    File.Move(_profileConsoleTempPath & "PN_" & _companyName & ".doc", _corrPath & "PN_" & _companyName & ".doc")
                    Try
                        File.Delete(_profileConsoleTempPath & "PN_" & _companyName & ".txt")
                    Catch
                    End Try
                    lblStatus.Text = "Presenter's Word file created and locked..."
                    txtNotes.Text = "~" & txtNotes.Text
                    If Not _powerSvc.UpdateNotes(_refNum, txtNotes.Text) Then
                        MsgBox("Notes not updated!")
                    End If
                    BuildNotesTab()
                    Process.Start(_corrPath & "PN_" & _companyName & ".doc")
                    btnCreatePN.Text = "Open PN File"
                Catch ex As System.Exception
                    lblStatus.Text = "Error: " & ex.Message
                End Try
            End If
        End If
    End Sub

    Private Sub PresentersNotesMFiles()
        T("PresentersNotesMFiles")
        Dim myText As String = "Presenter Notes for" & vbCrLf & _companyName & vbCrLf & vbCrLf
        'pull from MFiles if exists there. Else create.
        'Dim docs As ConsoleFilesInfo = _docsAccess.GetDocNamesAndIdsByBenchmarkingParticipant(_refNum, _refNum)
        Dim docs As ConsoleFilesInfo = _docsAccess.GetDocNamesAndIdsByBenchmarkingParticipant(_siteId, _siteId)
        For Each item As ConsoleFile In docs.Files
            If item.DeliverableType = "Presenters Notes" Then 'deliverable = 17  'Presenters Notes" Then
                _docsAccess.OpenFileFromMfiles(item.Id, _siteId, _siteId)
                Exit Sub
            End If
        Next
        Dim pnTemplatePath As String = String.Empty
        Dim dr As DialogResult = MessageBox.Show("Are you sure you want to create the PN Word file?   This will push the notes to MFiles.", "Wait", MessageBoxButtons.YesNo)
        If dr = System.Windows.Forms.DialogResult.Yes Then
            Try
                Dim strw As New StreamWriter(_profileConsoleTempPath & "PN_" & _companyName & ".txt")
                strw.Write(_powerSvc.GetNotes(_refNum))
                strw.Close()
                strw.Dispose()
                Dim docPath As String = Utilities.ConvertToDoc(_profileConsoleTempPath, _profileConsoleTempPath & "PN_" & _companyName & ".txt")
                Dim strfilename As String = Utilities.ExtractFileName(docPath)
                If _docsAccess.UploadDocToMfiles(_profileConsoleTempPath + strfilename, 52, _siteId, _deliverableForPresentersNotes, -1, DateTime.Today) Then
                    'then reopen from MFIles and check out, delete local file at end of this method
                    Dim mFilesFilename As String = Utilities.FileNameWithoutExtension(strfilename)
                    '_docsAccess.OpenFileFromMfiles(mFilesFilename, GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
                    Dim result As String = _docsAccess.OpenFileFromMfiles(mFilesFilename, _siteId, _siteId)
                    If result.Length > 0 Then
                        Throw New Exception(result)
                    End If
                End If
                Try
                    File.Delete(_profileConsoleTempPath + strfilename)
                Catch 'not critical that it get deleted at this second
                End Try
                Try
                    File.Delete(_profileConsoleTempPath & "PN_" & _companyName & ".txt")
                Catch 'not critical that it get deleted at this second
                End Try

                lblStatus.Text = "Presenter's Word file created and locked..."
                txtNotes.Text = "~" & txtNotes.Text
                If Not _powerSvc.UpdateNotes(_refNum, txtNotes.Text) Then
                    MsgBox("Notes not updated!")
                End If
                BuildNotesTab()
                btnCreatePN.Text = "Open PN File"
                'RefreshMFilesCollections(0)
            Catch ex As System.Exception
                lblStatus.Text = "Error: " & ex.Message
            End Try
            'txtNotes.ReadOnly = True
        End If
    End Sub

    Private Sub btnMasterEventsFile_Click(sender As Object, e As EventArgs) Handles btnMasterEventsFile.Click
        OpenNetworkFile(_templatesFolderPath & ConfigurationManager.AppSettings("MasterEventsFileName").ToString())
    End Sub

    Private Sub btnStartReliabilityFile_Click(sender As Object, e As EventArgs) Handles btnStartReliabilityFile.Click
        OpenNetworkFile(_templatesFolderPath & ConfigurationManager.AppSettings("StartReliabilityFileName").ToString())
    End Sub

    Private Sub btnValidatedInputFile_Click(sender As Object, e As EventArgs) Handles btnValidatedInputFile.Click
        OpenNetworkFile(_templatesFolderPath & ConfigurationManager.AppSettings("ValidatedInputFileName").ToString())
    End Sub

    
    Private Sub lstVFFiles_DoubleClick(sender As Object, e As EventArgs) Handles lstVFFiles.DoubleClick
        Dim file As String
        Dim FindTab As String = lstVFFiles.SelectedItem.ToString.IndexOf("|")
        file = lstVFFiles.SelectedItem.ToString.Substring(FindTab + 1, lstVFFiles.SelectedItem.ToString.Length - FindTab - 1)
        Process.Start(_corrPath & file.Trim)
    End Sub

    Private Sub lstVRFiles_DoubleClick(sender As Object, e As EventArgs) Handles lstVRFiles.DoubleClick
        Dim file As String
        Dim FindTab As String = lstVRFiles.SelectedItem.ToString.IndexOf("|")
        file = lstVRFiles.SelectedItem.ToString.Substring(FindTab + 1, lstVRFiles.SelectedItem.ToString.Length - FindTab - 1)
        Process.Start(_corrPath & file.Trim)
    End Sub

    Private Sub lstReturnFiles_DoubleClick(sender As Object, e As EventArgs) Handles lstReturnFiles.DoubleClick
        Dim file As String
        Dim FindTab As String = lstReturnFiles.SelectedItem.ToString.IndexOf("|")
        file = lstReturnFiles.SelectedItem.ToString.Substring(FindTab + 1, lstReturnFiles.SelectedItem.ToString.Length - FindTab - 1)
        Process.Start(_corrPath & file.Trim)
    End Sub

    Private Sub cboDir_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboDir.SelectedIndexChanged
        T("cboDir_SelectedIndexChanged")
        Select Case cboDir.Text
            Case "Site Correspondence"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = True

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = False

            Case "Client Attachments"
                tvwClientAttachments.Visible = True
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = False

            Case "Drawings"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = True
                tvwCompCorr.Visible = False
            Case "Company Correspondence"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = True
        End Select
    End Sub


End Class
