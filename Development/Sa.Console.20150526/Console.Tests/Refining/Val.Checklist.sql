﻿USE [Refining]
GO
/*
drop Schema [Val]
GO

Create Schema [Val]
GO

GRANT ALTER ON [Val] TO PUBLIC
GO
*/
/*
drop TABLE [Val].[Checklist]
GO
*/
/*
CREATE TABLE [Val].[Checklist](
	[Refnum] [char](9) NOT NULL,[IssueID] [char](8) NOT NULL,
	[IssueTitle] [char](50) NOT NULL,
	[IssueText] [text] NULL,
	[PostedBy] [char](3) NOT NULL,
	[PostedTime] [datetime] NOT NULL,
	[Completed] [char](1) NOT NULL,
	[SetBy] [char](3) NULL,
	[SetTime] [datetime] NULL,
	[IsCustom] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_Checklist] PRIMARY KEY CLUSTERED 
(
	[Refnum] ASC,
	[IssueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
*/
GRANT SELECT, INSERT, UPDATE, DELETE, ALTER ON [Refining].[Val].[Checklist] TO PUBLIC
GO

/*
DROP Schema [Console]
GO
*/
/*
Create Schema [Console]
GO

GRANT ALTER ON [Console] TO PUBLIC
GO
*/

/*
DROP PROCEDURE [Console].[GetAnIssue] 
GO
*/
/*
CREATE PROCEDURE [Console].[GetAnIssue] 

	@RefNum varchar(10),
	@IssueTitle nvarchar(50) = null,
	@IssueID nvarchar(50) = null

AS
BEGIN

Select * from Val.CheckList
Where Refnum = @RefNum And ((@IssueTitle IS NULL) OR (IssueTitle = @IssueTitle)) and 
((@IssueID IS NULL) OR (IssueID = @IssueID))
END
GO


GRANT EXECUTE, ALTER on  [Console].[GetAnIssue] 
GO
*/