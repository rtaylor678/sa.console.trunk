﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Data;
using System.Configuration;

using Sa.Console.OlefinsService;

namespace Console.Tests.Olefins
{
    [TestClass]
    public class OlefinsTest1
    {
        string _dbId = string.Empty;
        string _dbPw = string.Empty;
        private sa.Internal.Console.DataObject.DataObject _dAL;
        private int d = 14;
        private char _delim = Convert.ToChar(14);

        [TestInitialize]
        public void Startup()
        {
            _dbId = DevConfigHelper.ReadConfig("TestDbId");
            _dbPw = DevConfigHelper.ReadConfig("TestDbPassword");
            _dAL = new sa.Internal.Console.DataObject.DataObject(sa.Internal.Console.DataObject.DataObject.StudyTypes.OLEFINS, _dbId, _dbPw);
        }

        [TestMethod]
        public void GetStudiesTest()
        {
            OlefinsManager ole = new OlefinsManager(_dAL);
            Assert.IsTrue(ole.GetStudies().Count == 5);
        }



        //===============================================
        #region "Data methods"
        private List<T> GetDataFromColumn<T>(string columnName, DataTable dt)
        {
            List<T> list = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<T>();
                foreach (DataRow row in dt.Rows)
                {
                    try
                    {
                        if (typeof(T) == typeof(System.String))
                        {
                            string val = row[columnName].ToString().Trim();
                            list.Add((T)Convert.ChangeType(val, typeof(String))); //Convert.ChangeType(row[columnName], typeof(System.String));
                        }
                        else
                        {
                            list.Add((T)Convert.ChangeType(row[columnName], typeof(T)));
                        }
                    }
                    catch (Exception ex)
                    {
                        string debugHere = ex.Message;
                    }
                }
            }
            return list;
        }

        private DataTable Select(string sql)
        {
            DataSet result = new DataSet();
            try
            {
                _dAL.SQL = sql;
                result = _dAL.Execute();
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                _dAL.SQL = string.Empty;
            }
            return result.Tables[0];
        }

        private DataTable Exec(string procName, List<string> parameters)
        {
            DataSet result = new DataSet();
            try
            {
                result = _dAL.ExecuteStoredProc(procName, parameters);
            }
            catch (Exception ex)
            {
                string breakpoint = ex.Message;
                return null;
            }
            if (result == null)
            {
                return null;
            }
            else
            {
                if (result.Tables.Count > 0)
                {
                    return result.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        #endregion

    }
}
