﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Collections.Generic;
using System.Configuration;
using SA.Console;
using System.Windows.Forms;

namespace Console.Tests.Olefins
{
    [TestClass]
    public class DocsAccessTests
    {
        private string GetDevConfigHelperSetting(string settingName)
        {
            string value = string.Empty;
            try
            {
                value = Console.Tests.DevConfigHelper.ReadConfig(settingName);
            }
            catch (NullReferenceException nullReferenceExceptionId)
            {
                try
                {
                    value = ConfigurationManager.AppSettings[settingName].ToString();
                }
                catch (Exception exId)
                {
                    Assert.Fail("Can't find setting " + settingName);
                }
            }
            return value;
        }
        [TestMethod]
        public void lstVRFilesItemsTest()
        {
            string path = (@"C:\");
            SA.Console.DocsFileSystemAccess docsFileSystemAccess = new SA.Console.DocsFileSystemAccess(Main.VerticalType.OLEFINS, true);
            PrepTestDocsFileSystemAccessTestFiles(true, path + "VF1TEST.txt");
            PrepTestDocsFileSystemAccessTestFiles(true, path + "VR1TEST.txt");
            var items = docsFileSystemAccess.lstVRFilesItems(@"C:\", "1", "2015PCH998");
            docsFileSystemAccess = null;
            Assert.IsTrue(items.Count > 0);
            //cleanup
            PrepTestDocsFileSystemAccessTestFiles(false, path + "VF1TEST.txt");
            PrepTestDocsFileSystemAccessTestFiles(false, path + "VR1TEST.txt");
        }

        private void PrepTestDocsFileSystemAccessTestFiles(bool CreateElseDelete, string FilePath)
        {
            if (CreateElseDelete)
            {
                StreamWriter writer = new StreamWriter(FilePath);
                writer.Close();
            }
            else
            {
                File.Delete(FilePath);
            }
        }

        [TestMethod]
        public void btnReceiptAck_ClickTest()
        {
            //Can't test due to MsgBoxes and InputBox
            Assert.Inconclusive("SKIPPING");
        }

        //[TestMethod]  this just calls the MFiles object
        public void CheckInTest()
        {
            return;
        }

        //[TestMethod]  this just calls the MFiles object
        public void CheckOutToMeTest()
        {
            return;
        }

        //[TestMethod]  this just calls the MFiles object
        public void DocExistsTest()
        {
            return;
        }

        //[TestMethod]  this just calls the MFiles object
        public void DownloadFileTest()
        {
            return;
        }

        //[TestMethod]  this just calls the MFiles object
        public void GetCheckedOutStatusTest()
        {
            return;
        }

        //[TestMethod]  this just calls the MFiles object
        public void GetDocsInfoByNameAndRefnumTest()
        {
            return;
        }

        //[TestMethod]  this just calls the MFiles object
        public void GetDocNamesByIdsAndBenchmarkingParticipantTest()
        {
            return;
        }

        //[TestMethod]  this just calls the MFiles object
        public void NextReturnFileTest()
        {
            return;
        }

        //[TestMethod]  this just calls the MFiles object
        public void NextVTest()
        {
            return;
        }

        //[TestMethod]  this just calls the MFiles object
        public void OpenFileFromMfiles_DocIdTest()
        {
            return;
        }

        //[TestMethod]  this just calls the MFiles object
        public void OpenFileFromMfiles_FileNameTest()
        {
            return;
        }

        [TestMethod] 
        public void ValFax_BuildTest()
        {
            //Can't test due to inputbox
            Assert.Inconclusive("Skipping");

            //NON OLEFINS for testing network code
            //DocsFileSystemAccess docsAccess = new DocsFileSystemAccess(Main.VerticalType.RAM);
            return;          
        }

        [TestMethod] 
        public void BuildVRFileTest()
        {

            //NON OLEFINS for testing network code
            string v  = "1";
            DocsFileSystemAccess docsAccess = new DocsFileSystemAccess(Main.VerticalType.RAM,false);
            string result = docsAccess.BuildVRFile("SFB", "TEST VR FILE", @"C:\TEmp\", v, "15PCH998");
            Assert.IsTrue(result.Contains("VR" + v + ".txt has been built."));
        }

        [TestMethod] 
        public void GetDataFor_lstVFNumbersTest()
        {
            //NON OLEFINS for testing network code
            string vfPath = @"c:\temp\VF1.txt";
            StreamWriter sw = new StreamWriter(vfPath);
            sw.Close();
            DocsFileSystemAccess docsAccess = new DocsFileSystemAccess(Main.VerticalType.RAM,false);
            List<string> result = docsAccess.GetDataFor_lstVFNumbers("VF1.txt", @"C:\TEmp\", null, "15PCH998", "2015PCH998");
            File.Delete(vfPath);
            Assert.IsTrue(result.Count>0);
        }

        //[TestMethod]  //this just calls Mfiles method, nothing to test.
        public void lstVFFiles_DoubleClickTest()
        {
          return;
        }

        //[TestMethod]  //this just calls Mfiles method, nothing to test.
        public void UploadDocToMfilesTest()
        {
            return; 
        }
        


    }
}
