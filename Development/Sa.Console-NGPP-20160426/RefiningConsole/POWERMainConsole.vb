﻿
Imports System.Data.SqlClient
Imports System.Configuration
Imports Excel = Microsoft.Office.Interop.Excel
Imports Word = Microsoft.Office.Interop.Word
Imports Outlook = Microsoft.Office.Interop.Outlook
Imports System.ComponentModel
Imports System.Threading
Imports System.IO
Imports System.Text
Imports System.IO.Compression
Imports System.Runtime.InteropServices
Imports System.Reflection
Imports SA.Internal.Console.DataObject
Imports iwantedue.Windows.Forms




Public Class PowerMainConsole
    Public Class WordTemplate
        Public Field As New List(Of String)
        Public RField As New List(Of String)
    End Class

#Region "PRIVATE VARIABLES"
    Dim SavedFile As String = Nothing
    Dim CompleteIssue As String = "N"
    Dim CurrentTab As Integer = 0
    Dim ValidationIssuesIsDirty As Boolean = False
    Dim Time As Integer = 0
    Dim LastTime As Integer = 0
    Dim CompanyContact As Contacts
    Dim OlefinsContact As Contacts
    Dim RefineryContact As Contacts
    Dim InterimContact As Contacts
    Dim PlantContact As Contacts
    Dim PowerContact As Contacts
    Dim OpsContact As Contacts
    Dim TechContact As Contacts
    Dim PricingContact As Contacts
    Dim DataContact As Contacts
    Dim CompanyPassword As String = ""

    Dim TabDirty(9) As Boolean
    Dim ds As DataSet
    'Dim db As New DataAccess()
    Dim db As DataObject
    Dim CurrentSortMode As Integer = 1

    Dim sDir(10) As String
    Dim StudyRegion As String = ""
    Dim Study As String = ""

    Dim SANumber As String

    Dim CurrentConsultant As String
    Dim CurrentRefNum As String
    Dim ValidationFile As String
    Dim ValidatePath As String
    Dim JustLooking As Boolean = False
    Dim CurrentCompany As String
    Dim Company As String
    Dim CompCorrPath As String
    Dim GeneralPath As String
    Dim ProjectPath As String
    Dim TempPath As String
    Dim OldTempPath As String = ""
    Dim TemplatePath As String
    Dim DirResult As String

    Dim StudyDrive As String
    Dim TempDrive As String = "C:\USERS\"
    Dim CurrentPath As String
    Dim RefNum As String
    Dim SiteID As String
    Dim bJustLooking As Boolean = True
    Dim RefNumPart As String
    Dim LubRefNum As String
    Dim StudySave(20) As String
    Dim RefNumSave(20) As String
    Dim FirstTime As Boolean = True
    Dim KeyEntered As Boolean = False

    Private mDBConnection As String
    Private mStudyYear As String
    Private mPassword As String
    Private mRefNum As String
    Private mSpawn As Boolean
    Private mStudyType As String
    Private mUserName As String
    Private mReturnPassword As String

    Private Err As ErrorLog
#End Region

#Region "PUBLIC PROPERTIES"

    Private mVPN As Boolean
    Public Property VPN() As Boolean
        Get
            Return mVPN
        End Get
        Set(ByVal value As Boolean)
            mVPN = value
        End Set
    End Property

    Public Property DBConnection() As String
        Get
            Return mDBConnection
        End Get
        Set(ByVal value As String)
            mDBConnection = value
        End Set
    End Property
    Private mCurrentStudyYear As String
    Public Property CurrentStudyYear() As String
        Get
            Return mCurrentStudyYear
        End Get
        Set(ByVal value As String)
            mCurrentStudyYear = value
        End Set
    End Property
    Public Property StudyYear() As String
        Get
            Return mStudyYear
        End Get
        Set(ByVal value As String)
            mStudyYear = value
        End Set
    End Property

    Public Property UserName() As String

        Get
            Return mUserName
        End Get
        Set(ByVal value As String)
            mUserName = value
        End Set

    End Property
    Public Property ReturnPassword() As String
        Get
            Return mReturnPassword
        End Get
        Set(ByVal value As String)
            mReturnPassword = value
        End Set
    End Property

    Public Property Password() As String
        Get
            Return mPassword
        End Get
        Set(ByVal value As String)
            mPassword = value
        End Set
    End Property


    Public Property StudyType() As String
        Get
            Return mStudyType
        End Get
        Set(ByVal value As String)
            mStudyType = value
        End Set
    End Property

    Public Property ReferenceNum() As String
        Get
            Return mRefNum
        End Get
        Set(ByVal value As String)
            mRefNum = value
        End Set
    End Property

    Public Property Spawn() As Boolean
        Get
            Return mSpawn
        End Get
        Set(ByVal value As Boolean)
            mSpawn = value
        End Set
    End Property
#End Region

#Region "INITIALIZATION"
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

    End Sub

    Private Sub Initialize()
        Try


            Dim assembly As Assembly = System.Reflection.Assembly.GetExecutingAssembly()
            Dim version As System.Version = assembly.GetName().Version
            txtVersion.Text = String.Format("Version {0}", version)

            'Populate Consultant Tab with login

            SetTabDirty(True)


            'Populate the Form Title
            Me.Text = "Console." & StudyType & " Validation Console"



            'Fill Study Combo Box

            Dim TheYear As Integer = Year(DateTime.Now)
            For i = TheYear - 1 To TheYear - 4 Step -1
                cboStudy.Items.Add("POWER" & i)
            Next



            StudyDrive = "K:\STUDY\POWER\"
            db = New DataObject(DataObject.StudyTypes.POWER, UserName, Password)



            'Setup Database connection for ErrorLog
            'db.DBConnection = mDBConnection
            Err = New ErrorLog(StudyType, UserName, Password)
            'Assigned Combo Box


            ds = db.ExecuteStoredProc("Console." & "GetAllConsultants")

            If ds.Tables.Count > 0 Then
                For Each Row In ds.Tables(0).Rows
                    cboConsultant.Items.Add(Row("Consultant").ToString.ToUpper.Trim)

                Next
            End If
            'Delivery Methods


            'Fill Directory Drop Down

            cboCheckListView.SelectedIndex = 0
            cboDir.Items.Add("Plant Correspondence")
            cboDir2.Items.Add("Plant Correspondence")

            cboDir.Items.Add("Company Correspondence")
            cboDir.Items.Add("Client Attachments")

            cboDir2.Items.Add("Company Correspondence")
            cboDir2.Items.Add("Client Attachments")



            cboDir.SelectedIndex = 0
            cboDir2.SelectedIndex = 2




            'Choose first Checklist item in drop down



            'Set up Tabs and buttons per Study Type


            'btnValidate.Visible = False








            'Check for existance of Settings file, if missing, copy default one

            GetSettingsFile()

            'If not spawned off an existing instance of the program, read the settings file and set the drop downs accordingly


            If Not Spawn Then
                ReadLastStudy()
            Else
                If ReferenceNum <> "" Then
                    cboStudy.SelectedIndex = cboStudy.FindString(ParseRefNum(ReferenceNum, 2) & StudyYear)
                Else
                    cboStudy.SelectedValue = "POWER" & StudyYear
                End If

            End If

            BuildSummarySupportTab()

        Catch ex As System.Exception
            Err.WriteLog("Initialize", ex)
            MessageBox.Show(ex.Message)
        End Try

    End Sub
#End Region

#Region "BUILD TABS"

    Private Sub BuildSummaryTab()
        Dim SiteID As String = ""
        Dim params = New List(Of String)
        Try
            SiteID = Utilities.GetSite(cboRefNum.Text, 0)
            SetSummaryFileButtons(SiteID)
            lblInHolding.Visible = False
            lblLastCalcDate.Text = "Not Calculated"
            lblLastUpload.Text = "Not Uploaded"
            txtValidationIssues.Text = ""
            lblFileModify.Visible = True
            lblLU.Visible = True
            lblOU.Visible = True
            lblFileModify.Text = ""
            lblPricingHub.Text = ""




            If Dir("K:\Study\Power\" & StudyYear & "\Plant\" & SiteID & "\" & SiteID & ".xls") <> "" Then
                lblFileModify.Text = FileDateTime("K:\Study\Power\" & StudyYear & "\Plant\" & SiteID & "\" & SiteID & ".xls")

                lblValidationStatus.Text = "In Progress"
            Else
                lblFileModify.Text = "Undetermined"
                lblValidationStatus.Text = "Not Received"
            End If

            params.Add("SiteID/" + SiteID)

            ds = db.ExecuteStoredProc("Console.GetLastUpload", params)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                lblLastUpload.Text = ds.Tables(0).Rows(0)("MessageTime").ToString
            End If

            params.Clear()

            params.Add("SiteID/" + SiteID)

            ds = db.ExecuteStoredProc("Console.GetLastCalcDate", params)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                lblLastCalcDate.Text = ds.Tables(0).Rows(0)("MessageTime").ToString
            End If

            params.Clear()

            params.Add("RefNum/" + SiteID)

            ds = db.ExecuteStoredProc("Console.GetPricingHub", params)
            If ds.Tables.Count > 1 Then
                lblPricingHub.Text = "Multiple!"
            Else
                If ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    lblPricingHub.Text = ds.Tables(0).Rows(0)(0).ToString
                End If
            End If

        Catch ex As System.Exception
            Err.WriteLog("BuildSummaryTab", ex)
        End Try


        params.Clear()

        Dim row As DataRow

        params = New List(Of String)
        params.Add("RefNum/" + SiteID)
        ds = db.ExecuteStoredProc("Console." & "GetValidationNotes", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                row = ds.Tables(0).Rows(0)
                txtValidationIssues.Text = row("ValidationNotes").ToString
            Else
                txtValidationIssues.Text = ""
            End If
        End If


        If txtValidationIssues.TextLength > 0 Then
            If txtValidationIssues.Text.Substring(0, 1) = "~" Then
                chkLockPN.Checked = True
                chkLockPN.Enabled = False
                txtValidationIssues.ReadOnly = True
            Else
                chkLockPN.Enabled = True
            End If
        End If

        mUserName = mUserName.ToUpper



    End Sub

    Private Sub BuildCheckListTab(strYesNoAll As String) ' Y for completed, N for not completed, A for all

        txtIssueID.Text = ""
        txtIssueName.Text = ""
        txtCompletedBy.Text = ""
        txtCompletedOn.Text = ""
        txtPostedBy.Text = ""
        txtPostedOn.Text = ""
        txtDescription.Text = ""


        Select Case strYesNoAll
            Case "Complete"
                strYesNoAll = "Y"

            Case "Incomplete"
                strYesNoAll = "N"

            Case Else
                strYesNoAll = "A"

        End Select

        UpdateCheckList(strYesNoAll)


    End Sub





    Private Sub BuildDirectoriesTab()
        SetPaths()
        PopulateCompCorrTreeView(tvwCompCorr)
        PopulateCorrespondenceTreeView(tvwCorrespondence)
        PopulateCompCorrTreeView(tvwCompCorr2)
        PopulateCorrespondenceTreeView(tvwCorrespondence2)


    End Sub

#End Region

#Region "UTILITIES"

    Private Function IsInHolding() As Boolean
        Dim mRefNum As String = Utilities.GetDataSetID(cboRefNum.Text, CurrentSortMode).Trim
        Dim strHoldPathName As String = ""
        Select Case StudyYear
            Case 2002
                strHoldPathName = "P:\2003\03P\Validate\Holding\"
            Case 2003
                strHoldPathName = "P:\2004\04P\Validate\Holding\"
            Case 2004
                strHoldPathName = "P:\2005\05P\Validate\Holding\"
            Case 2005
                strHoldPathName = "P:\2006\06P\Validate\Holding\"
            Case 2006
                strHoldPathName = "P:\2007\07P\7PM006S - Power CPA\Validate\Holding\"
            Case 2007
                strHoldPathName = "P:\2008\08P\8PM006S - Power CPA\Validate\Holding\"
            Case 2008
                strHoldPathName = "P:\2009\Power\9PM006S - Power CPA\Validate\Holding\"
            Case 2009
                strHoldPathName = "P:\2010\Power\0PM006S - Power CPA\Validate\Holding\"
            Case 2010
                strHoldPathName = "P:\2011\Power\1PM006S - Power CPA\Validate\Holding\"
            Case 2011
                strHoldPathName = "P:\2012\Power\2PM006S - Power CPA\Validate\Holding\"
            Case 2012
                strHoldPathName = "P:\2013\Power\3PM006S - Power CPA\Validate\Holding\"
        End Select



        If File.Exists(strHoldPathName & mRefNum & ".xls") Then
            lblInHolding.Text = "In Holding"
            lblInHolding.Visible = True
        Else

            lblInHolding.Visible = False
        End If

        If Dir(strHoldPathName & mRefNum & ".xls") <> "" Then
            lblFileModify.Text = FileDateTime(strHoldPathName & mRefNum & ".xls")
            lblValidationStatus.Text = "In Holding"

        End If
        Return lblInHolding.Visible
    End Function
    Private Function StripEmail(email As String) As String
        Return email.Substring(email.IndexOf("-") + 2, email.Length - (email.IndexOf("-") + 2))
    End Function

    Private Sub SetTabDirty(status As Boolean)
        For i = 0 To 8
            TabDirty(i) = status
        Next
    End Sub
    Private Sub LoadTab(tab As Integer)

        Select Case tab


            Case 0 'Refining/Plant and Company Tab
                If TabDirty(0) Then
                    GetPlantContacts()
                    GetCompanyContacts()
                    FillContacts()
                    TabDirty(0) = False
                End If

            Case 1 'Summary Tab
                If TabDirty(1) Then
                    BuildSummaryTab()
                    TabDirty(1) = False
                End If

            Case 2  'CheckList Tab
                If TabDirty(2) Then
                    BuildCheckListTab("Incomplete")
                    TabDirty(2) = False
                End If

            Case 3 'Directory Tab

                If TabDirty(3) Then
                    BuildDirectoriesTab()
                    TabDirty(3) = False
                End If

            Case 4

                If TabDirty(4) Then
                    BuildSummarySupportTab()
                    TabDirty(4) = False
                End If


        End Select
    End Sub

    Private Function RemovePassword(File As String, password As String, companypassword As String) As Boolean
        Dim success As Boolean = True
        If File.Substring(File.Length - 3, 3).ToUpper = "XLS" Then
            If File.ToUpper.Contains("RETURN") Then
                success = Utilities.ExcelPassword(File, password, 1)
            Else
                success = Utilities.ExcelPassword(File, companypassword, 1)
            End If
        End If
        If File.Substring(File.Length - 3, 3).ToUpper = "DOC" Or File.Substring(File.Length - 3, 3).ToUpper = "OCX" Then
            success = Utilities.WordPassword(File, companypassword, 1)
        End If
        Return success
    End Function
    Private Function FixFilename(ext As String, strFile As String)
        Dim strNewFilename As String
        Dim NewExt As String = GetExtension(strFile)
        strNewFilename = strFile.Replace(NewExt, ext)
        Return strNewFilename
    End Function
    Private Function GetExtension(strFile As String)
        Dim ext As String
        Try
            ext = strFile.Substring(strFile.LastIndexOf(".") + 1, 3)
        Catch
            ext = ""
        End Try
        Return ext
    End Function


    Public Function PlugLetterIfNeeded(strFileName As String) As String
        Dim strWorkFileName As String
        Dim strDirResult As String
        Dim strCorrPath As String
        Dim strABC(10) As String
        Dim I As Integer
        Dim intInsertPoint As Integer

        strABC(0) = ""
        strABC(1) = "A"
        strABC(2) = "B"
        strABC(3) = "C"
        strABC(4) = "D"
        strABC(5) = "E"
        strABC(6) = "F"
        strABC(7) = "G"
        strABC(8) = "H"
        strABC(9) = "I"

        strCorrPath = CorrPath
        If strFileName.Substring(0, 6).ToUpper = "RETURN" Then
            intInsertPoint = 7
        Else
            intInsertPoint = 3
        End If

        For I = 0 To 9
            strWorkFileName = strFileName.Substring(0, intInsertPoint) & strABC(I) & Mid(strFileName, intInsertPoint + 1, 99)
            strDirResult = Dir(CorrPath & strWorkFileName)
            If strDirResult = "" Then
                Return strABC(I)
                Exit Function
            End If
        Next I

        MsgBox("More than 9 iterations of same file?", vbOKOnly, "Check with Joe Waters (JDW)?")

        PlugLetterIfNeeded = strFileName

    End Function
    Function CleanFileName(strFileName As String) As String
        Dim strBadChar As String
        Dim strRepChar As String
        Dim I As Integer
        Dim J As Integer
        Dim blnChanged As Boolean
        blnChanged = False

        strBadChar = "\/:*?""<>|%"
        ' A
        strBadChar = strBadChar & Chr(192)
        strBadChar = strBadChar & Chr(193)
        strBadChar = strBadChar & Chr(194)
        strBadChar = strBadChar & Chr(195)
        strBadChar = strBadChar & Chr(196)
        strBadChar = strBadChar & Chr(197)
        strBadChar = strBadChar & Chr(198)
        ' E
        strBadChar = strBadChar & Chr(200)
        strBadChar = strBadChar & Chr(201)
        strBadChar = strBadChar & Chr(202)
        strBadChar = strBadChar & Chr(203)
        ' I
        strBadChar = strBadChar & Chr(204)
        strBadChar = strBadChar & Chr(205)
        strBadChar = strBadChar & Chr(206)
        strBadChar = strBadChar & Chr(207)
        ' O
        strBadChar = strBadChar & Chr(210)
        strBadChar = strBadChar & Chr(211)
        strBadChar = strBadChar & Chr(212)
        strBadChar = strBadChar & Chr(213)
        strBadChar = strBadChar & Chr(214)
        ' U
        strBadChar = strBadChar & Chr(217)
        strBadChar = strBadChar & Chr(218)
        strBadChar = strBadChar & Chr(219)
        strBadChar = strBadChar & Chr(220)

        ' a
        strBadChar = strBadChar & Chr(224)
        strBadChar = strBadChar & Chr(225)
        strBadChar = strBadChar & Chr(226)
        strBadChar = strBadChar & Chr(227)
        strBadChar = strBadChar & Chr(228)
        strBadChar = strBadChar & Chr(229)
        ' e
        strBadChar = strBadChar & Chr(232)
        strBadChar = strBadChar & Chr(233)
        strBadChar = strBadChar & Chr(234)
        strBadChar = strBadChar & Chr(235)
        ' i
        strBadChar = strBadChar & Chr(236)
        strBadChar = strBadChar & Chr(237)
        strBadChar = strBadChar & Chr(238)
        strBadChar = strBadChar & Chr(239)
        ' o
        strBadChar = strBadChar & Chr(242)
        strBadChar = strBadChar & Chr(243)
        strBadChar = strBadChar & Chr(244)
        strBadChar = strBadChar & Chr(245)
        strBadChar = strBadChar & Chr(246)
        ' u
        strBadChar = strBadChar & Chr(249)
        strBadChar = strBadChar & Chr(250)
        strBadChar = strBadChar & Chr(251)
        strBadChar = strBadChar & Chr(252)

        strRepChar = "  -      AAAAAAAEEEEIIIIOOOOOUUUUaaaaaaeeeeiiiiooooouuuu"
        For I = 1 To Len(strBadChar)
            For J = 1 To Len(strFileName)
                If Mid(strFileName, J, 1) = Mid(strBadChar, I, 1) Then
                    Mid(strFileName, J, 1) = Mid(strRepChar, I, 1)
                    blnChanged = True
                End If
            Next J
        Next I
        CleanFileName = Trim(strFileName)


    End Function
    Private Sub GetSettingsFile()
        Dim yr As String
        Try
            TempPath = TempDrive & mUserName & "\Console\Temp\"
            If Directory.Exists(TempDrive & mUserName & "\Console") = False Then Directory.CreateDirectory(TempDrive & mUserName & "\Console")
            If Directory.Exists(TempDrive & mUserName & "\Console\Temp") = False Then Directory.CreateDirectory(TempDrive & mUserName & "\Console\Temp")
            If Directory.Exists(TempDrive & mUserName & "\Console\Temp\ConsoleSettings") = False Then Directory.CreateDirectory(TempDrive & mUserName & "\Console\Temp\ConsoleSettings")

            If File.Exists(TempPath & "ConsoleSettings\" & StudyType & "Settings.txt") = False Then
                Dim objWriter As New System.IO.StreamWriter(TempPath & "ConsoleSettings\" & StudyType & "Settings.txt", False)

                Dim TheYear As Integer = Convert.ToInt32((Year(DateTime.Now) - 1))

                For i = TheYear To TheYear - 4 Step -1
                    yr = i.ToString
                    objWriter.WriteLine("POWER" & yr)
                    objWriter.WriteLine()
                Next


                objWriter.Close()
            End If

        Catch ex As System.Exception
            Err.WriteLog("GetSettingsFile", ex)
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ReadLastStudy()
        Try
            Dim objReader As New IO.StreamReader(TempPath & "ConsoleSettings\" & StudyType & "settings.txt")

            For i = 0 To 3
                StudySave(i) = objReader.ReadLine()
                RefNumSave(i) = objReader.ReadLine()
            Next


            Dim lastStudy As String = objReader.ReadLine()
            cboStudy.Text = lastStudy
            Dim rn As String = objReader.ReadLine()
            cboRefNum.Text = rn
            RefNum = rn
            objReader.Close()
            GetRefNumRecord(rn)
        Catch ex As System.Exception
            Err.WriteLog("ReadLastStudy", ex)
        End Try

    End Sub

    Private Sub SaveLastStudy()
        Try
            If cboRefNum.Text <> "" Then
                If Directory.Exists(TempPath & "ConsoleSettings\") = False Then GetSettingsFile()
                Dim objWriter As New System.IO.StreamWriter(TempPath & "ConsoleSettings\" & StudyType & "Settings.txt", False)


                For i = 0 To 3
                    objWriter.WriteLine(StudySave(i))
                    objWriter.WriteLine(RefNumSave(i))
                Next


                objWriter.WriteLine(cboStudy.Text)
                objWriter.WriteLine(cboRefNum.Text)
                objWriter.Close()
            End If
        Catch ex As System.Exception
            Err.WriteLog("SaveLastStudy", ex)
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ClearFields()

        txtValidationIssues.Text = ""
        txtCompletedBy.Text = ""
        txtCompletedOn.Text = ""
        txtDescription.Text = ""
        txtIssueName.Text = ""

    End Sub

    Public Sub WordTemplateReplace(strfile As String, fields As WordTemplate, strfilename As String)

        Dim oWord As Object
        Dim oDoc As Object
        Dim range As Object
        Dim DoubleCheck As String = "These are the template fields: " & vbCrLf
        For count = 0 To fields.Field.Count - 1
            DoubleCheck += fields.Field(count) & " --> " & fields.RField(count) & vbCrLf
        Next
        MessageBox.Show(DoubleCheck, "Template Field Check")

        oWord = CreateObject("Word.Application")
        oWord.Visible = True
        oDoc = oWord.Documents.Open(strfile)
        range = oDoc.Content
        Try
            For count = 0 To fields.Field.Count - 1
                With oWord.Selection.Find
                    .Text = fields.Field(count)
                    .Replacement.Text = fields.RField(count)
                    .Forward = True
                    .Wrap = Microsoft.Office.Interop.Word.WdFindWrap.wdFindContinue
                    .Format = False
                    .MatchCase = False
                    .MatchWholeWord = False
                    .MatchWildcards = False
                    .MatchSoundsLike = False
                    .MatchAllWordForms = False
                End With
                oWord.Selection.Find.Execute(Replace:=Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll)
            Next

            If File.Exists(strfilename) Then
                strfilename = InputBox("CAUTION: The suggested file name already exists. If possible, contact FRS, if not, change the file name to avoid overwriting an existing document.", "CAUTION", strfilename)
            Else
                'Stop
                strfilename = InputBox("Here is the suggested name for this New Validation Fax document." & Chr(10) & "Click OK to accept it, or change, then click OK.", "SaveAs", strfilename)
            End If
            ' If they hit cancel, just stop.
            If strfilename = "" Then Exit Sub
            '
            oDoc.SaveAs(strfilename, ADDTORECENTFILES:=True)
            MessageBox.Show("Name is now " & oDoc.Name)
        Catch ex As System.Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Function NextVF()

        Dim strDirResult As String
        Dim I As Integer
        For I = 1 To 9
            strDirResult = Dir(CorrPath & "\vf" & CStr(I) & "*.*")
            If strDirResult = "" Then
                Return I
                Exit For
            End If
        Next I
        Return Nothing
    End Function
    Function NextVR()

        Dim strDirResult As String
        Dim I As Integer
        For I = 0 To 9
            strDirResult = Dir(CorrPath & "\vr" & CStr(I) & "*.*")
            If strDirResult = "" Then
                Return I
                Exit For
            End If
        Next I
        Return Nothing
    End Function
    Function NextReturnFile()
        Dim strDirResult As String
        Dim I As Integer
        For I = 0 To 9
            strDirResult = Dir(CorrPath & "*F_Return" & CStr(I) & "*.xls")
            If strDirResult = "" Then
                Return I
                Exit For
            End If
        Next I
        Return Nothing

    End Function


    Function CacheShellIcon(ByVal argPath As String) As String
        Dim mKey As String = Nothing
        ' determine the icon key for the file/folder specified in argPath
        If IO.Directory.Exists(argPath) = True Then
            mKey = "folder"
        ElseIf IO.File.Exists(argPath) = True Then
            mKey = IO.Path.GetExtension(argPath)
        End If
        ' check if an icon for this key has already been added to the collection
        If ImageList1.Images.ContainsKey(mKey) = False Then
            ImageList1.Images.Add(mKey, GetShellIconAsImage(argPath))
            If mKey = "folder" Then ImageList1.Images.Add(mKey & "-open", GetShellOpenIconAsImage(argPath))
        End If
        Return mKey
    End Function




    Function DatePulledFromVAFile(ByVal strDirAndFileName As String) As Date

        Dim strWork As String = ""
        ' Read the second line of the VA file. Get the date from there.
        Dim objReader As New System.IO.StreamReader(strDirAndFileName, True)

        ' Read and toss the first line
        strWork = objReader.ReadLine()
        ' Read the date-time line
        strWork = objReader.ReadLine()

        DatePulledFromVAFile = strWork

    End Function


    Private Function VChecked(mRefNum As String, mode As String) As Boolean
        Dim ret As Boolean = False
        Dim params As New List(Of String)
        params.Add("RefNum/" + mRefNum)
        params.Add("IssueID/" + mode)
        Dim ds As DataSet = db.ExecuteStoredProc("Console." & "IssueCheckV", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)(0).ToString = "Y" Then Return True
            End If
        End If
        Return False
    End Function
    Private Sub GetDirectories(ByVal subDirs() As DirectoryInfo, ByVal nodeToAddTo As TreeNode)

        Dim aNode As TreeNode
        Dim subSubDirs() As DirectoryInfo
        Dim subDir As DirectoryInfo
        For Each subDir In subDirs
            aNode = New TreeNode(subDir.Name, 0, 0)
            aNode.Tag = subDir
            aNode.ImageKey = "folder"
            subSubDirs = subDir.GetDirectories()
            If subSubDirs.Length <> 0 Then
                GetDirectories(subSubDirs, aNode)
            End If
            nodeToAddTo.Nodes.Add(aNode)
        Next subDir

    End Sub
    'Validation
    Private Sub Validation(JustLooking As Boolean)
        Dim strFLMacroDev As String = ""
        Dim AlreadyOpen As Boolean = False
        Dim templatePath As String
        'Launch Excel Validation
        ' Create new Application.
        Dim xl As Excel.Application

        'Dim obj As Object = Interaction.GetObject(TempPath & ValidationFile)


        xl = Utilities.GetExcelProcess()

        Try
            If Not Directory.Exists(TempDrive & UserName & "\VAL12\") Then Directory.CreateDirectory(TempDrive & UserName & "\VAL12\")
            If Not Directory.Exists(TempDrive & UserName & "\VAL12\temp\") Then Directory.CreateDirectory(TempDrive & UserName & "\VAL12\temp\")

            templatePath = TempDrive & UserName & "\VAL12\temp\"
            For Each wb In xl.Workbooks
                If wb.Name = ValidationFile Then
                    AlreadyOpen = True
                    Exit For
                End If
            Next wb

            Dim w As Excel.Workbook
            ' Open Excel spreadsheet.
            If Not AlreadyOpen Then
                If UserName = "DBB" Or UserName = "JDW" Or UserName = "SWP" Or UserName = "GLC" Then
                    If MsgBox("Use FLMacros from Development?", vbYesNoCancel, "Careful!") = vbYes Then
                        'gstrValidateFolder = gstrValidateFolder & "Development\"
                        strFLMacroDev = "Development\"
                    End If
                End If

                If Not IO.File.Exists(templatePath & ValidationFile) Then File.Delete(templatePath & ValidationFile)
                FileCopy(ValidatePath & strFLMacroDev & ValidationFile, templatePath & ValidationFile)
                If (VPN) Then
                    Dim b As DialogResult = MessageBox.Show("Since you are on VPN, we are going to pause to give the file enough time to load", "Wait", MessageBoxButtons.OK)
                End If
                System.Threading.Thread.Sleep(3000)
                w = xl.Workbooks.Open(templatePath & ValidationFile)
                xl.Visible = True
                w.RunAutoMacros(1)


            End If

            If xl.Workbooks(ValidationFile).Windows(ValidationFile).Visible = False Then xl.Workbooks(ValidationFile).Windows(ValidationFile).Visible = True
            If bJustLooking Then
                'Run Validation Macro
                xl.Run("JustLookin", CurrentRefNum)
            Else
                xl.Run("DelayLoadRefinery", CurrentRefNum)
            End If

        Catch ex As System.Exception
            Err.WriteLog("Validation: " & CurrentRefNum.ToString, ex)
            MessageBox.Show("Trouble launching " & ValidationFile & ".  Make sure your Macro File is not already opened" & vbCrLf & ex.Message)
        End Try

    End Sub

    'Set all directory paths
    Private Sub SetPaths()
        Dim Site As String

        Site = Utilities.GetSite(cboRefNum.Text, CurrentSortMode)
        Dim mRefNum As String = Utilities.GetSite(cboRefNum.Text, IIf(CurrentSortMode = 1, 0, 1))
        StudyRegion = cboStudy.Text.Substring(0, 3)

        Dim params As List(Of String)
        GeneralPath = StudyDrive & StudyYear
        ProjectPath = StudyDrive & StudyYear & "\Project\"

        If Not Directory.Exists(TempDrive & mUserName & "\Console\Temp\") Then Directory.CreateDirectory(TempDrive & mUserName & "\Console\Temp\")
        TempPath = TempDrive & mUserName & "\Console\Temp\"

        Try


            Company = GetCompanyName(cboRefNum.Text)


            PlantPath = StudyDrive & StudyYear & "\Plant\" & Site & "\"
            CorrPath = StudyDrive & StudyYear & "\Correspondence\" & mRefNum & "\"
            CompanyPath = StudyDrive & StudyYear & "\Company\" & Company & "\"
            CompCorrPath = CompanyPath


            TemplatePath = StudyDrive & StudyYear & "\software\validation\template\"


        Catch ex As System.Exception
            'Err.WriteLog("SetPaths: " & RefNum, ex)

        End Try

    End Sub
    'Method to remove specific tab from tabcontrol
    Private Sub RemoveTab(tabname As String)
        Dim tab As TabPage
        tab = ConsoleTabs.TabPages(tabname)
        ConsoleTabs.TabPages.Remove(tab)
    End Sub

    Private Function ParseRefNum(mRefNum As String, mode As Integer) As String
        Dim parsed As String = Utilities.GetRefNumPart(mRefNum, mode)
        Return parsed
    End Function

    'Method to save last refnum visited per Study for settings file
    Private Sub SaveRefNum(strStudy As String, strRefNum As String)

        For i = 0 To 2
            If StudySave(i) = strStudy Then
                RefNumSave(i) = strRefNum
            End If
        Next


    End Sub
#End Region

#Region "DATABASE LOOKUPS"






    Private Function IsValidRefNum(refnum As String) As String
        Dim ret As String = ""
        Dim params = New List(Of String)

        params.Add("@RefNum/" + refnum)
        params.Add("@StudyYear/" + StudyYear.Substring(2, 2))

        Dim c As DataSet = db.ExecuteStoredProc("Console." & "IsValidRefNum", params)
        If c.Tables.Count > 0 Then
            If c.Tables(0).Rows.Count > 0 Then
                ret = c.Tables(0).Rows(0)(0).ToString
            End If
        End If
        Return ret

    End Function
    Private Function FindString(lb As ComboBox, str As String) As Integer
        Dim count As Integer = 0
        For Each item As String In lb.Items
            If item.Contains(str) Then
                Return count
            End If
            count += 1
        Next
    End Function


    Private Sub UpdateConsultant()
        Dim params = New List(Of String)

        params.Add("RefNum/" + Utilities.GetSite(cboRefNum.Text, 0))
        params.Add("StudyYear/" + StudyYear.ToString)
        params.Add("Consultant/" + cboConsultant.Text)
        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateConsultant", params)
        If c = 0 Then
            MessageBox.Show("Consultant did not save.")
        End If

    End Sub
    Private Sub UpdateIssue(status As String)
        Dim params As List(Of String)
        If Me.tvIssues.SelectedNode IsNot Nothing Then
            Dim node As String = tvIssues.SelectedNode.Text.ToString.Trim
            params = New List(Of String)
            params.Add("RefNum/" + Utilities.GetSite(cboRefNum.Text, CurrentSortMode))
            params.Add("IssueTitle/" & node.Substring(0, node.IndexOf("-") - 1).Trim)
            params.Add("Status/" & status)
            params.Add("UpdatedBy/" & UserName)
            Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateIssue", params)
        End If
        UpdateCheckList()

    End Sub


    Private Function FillRefNums() As Integer
        Dim params As List(Of String)
        params = New List(Of String)
        params.Add("StudyYear/" + StudyYear)
        params.Add("Mode/" + CurrentSortMode.ToString)

        ds = db.ExecuteStoredProc("Console." & "GetRefNums", params)
        Dim row As DataRow 'Represents a row of data in Systems.data.datatable
        For Each row In ds.Tables(0).Rows
            If CurrentSortMode = 0 Then
                cboRefNum.Items.Add(row("RefNum").ToString.Trim & " - " & row("Coloc").ToString.Trim)
            Else
                cboRefNum.Items.Add(row("Coloc").ToString.Trim & " - " & row("RefNum").ToString.Trim)
            End If

        Next
        Return ds.Tables(0).Rows.Count
    End Function
    'Method to populate refnums combo box
    Private Sub GetRefNums(Optional strRefNum As String = "")
        Dim NoRefNums As Integer = 0
        Dim selectItem As String = cboStudy.SelectedItem


        StudyYear = selectItem.Substring(selectItem.Length - 2, 2)
        If StudyYear <> CurrentStudyYear Then
            cboStudy.BackColor = Color.Yellow
            lblWarning.Visible = True
        Else
            cboStudy.BackColor = Color.White
            lblWarning.Visible = False
        End If

        Study = selectItem.Substring(0, selectItem.Length - 2)
        If (StudyYear > 50) Then
            StudyYear = "19" + StudyYear
        Else
            StudyYear = "20" + StudyYear
        End If

        If Not strRefNum Is Nothing Then
            cboRefNum.Items.Clear()


            'Study RefNum Box
            NoRefNums = FillRefNums()

            If NoRefNums = 0 Then
                MessageBox.Show("There are no records for " & StudyYear & " study.", "Not Found")
                RemoveHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
                cboStudy.Text = "POWER12"
                StudyYear = "2012"
                NoRefNums = FillRefNums()
                AddHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
            End If



            If Not strRefNum Is Nothing And strRefNum.Length > 5 Then

                cboRefNum.SelectedIndex = cboRefNum.FindString(strRefNum)
            Else
                If ReferenceNum Is Nothing Then
                    'RefNum = cboRefNum.Items(0).ToString
                    cboRefNum.SelectedIndex = 0
                Else
                    RefNum = ReferenceNum.Trim
                    cboRefNum.SelectedIndex = cboRefNum.FindString(ReferenceNum)
                End If
            End If
            If cboRefNum.SelectedIndex = -1 Then cboRefNum.SelectedIndex = 0
        End If
    End Sub
    'Method to see if there is a combo refinery and show and populate the button


    'Method to retrieve and populate consultants drop down box
    Private Sub GetConsultants()

        Dim params As List(Of String)

        params = New List(Of String)
        params.Add("@StudyYear/" + StudyYear)
        params.Add("@RefNum/" + Utilities.GetDataSetID(cboRefNum.Text, 1).Trim)
        ds = db.ExecuteStoredProc("Console." & "GetConsultants", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                CurrentConsultant = ds.Tables(0).Rows(0).Item("Consultant").ToString.ToUpper.Trim
            End If
        End If

        cboConsultant.Text = CurrentConsultant


    End Sub
    Private Function GetCompanyByRefNum(strRefNum As String) As String

        Dim mCurrentCompany As String = ""
        Dim params As List(Of String)

        params = New List(Of String)

        params.Add("RefNum/" + Utilities.GetSite(cboRefNum.Text, CurrentSortMode))
        ds = db.ExecuteStoredProc("Console." & "GetCompanyName", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                mCurrentCompany = ds.Tables(0).Rows(0).Item("Coloc").ToString.ToUpper.Trim
            End If
        End If

        Return mCurrentCompany


    End Function

    'Function to retrieve Company name by RefNum
    Private Function GetCompanyName(str As String) As String
        Dim CompanyName As String
        ''Get Company Name
        If str = "" Then Return ""
        Dim FirstDash As Integer = str.IndexOf("-")
        Dim SecondDash As Integer = str.LastIndexOf("-")
        If CurrentSortMode = 1 Then
            CompanyName = str.Substring(0, FirstDash - 1)
        Else
            CompanyName = str.Substring(FirstDash + 2, (SecondDash - FirstDash - 3))
        End If
        ClearFields()
        Return CompanyName

    End Function

    'If exists, this will retrieve Interim Contact info
    Private Sub GetInterimContactInfo()
        InterimContact = New Contacts()
        Dim params As New List(Of String)

        'Check for Interim Contact Info
        params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        ds = db.ExecuteStoredProc("Console." & "GetInterimContactInfo", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                InterimContact.FirstName = ds.Tables(0).Rows(0).Item("FirstName").ToString
                InterimContact.LastName = ds.Tables(0).Rows(0).Item("LastName").ToString
                InterimContact.Email = ds.Tables(0).Rows(0).Item("Email").ToString
                InterimContact.Phone = ds.Tables(0).Rows(0).Item("Phone").ToString
            End If
        Else
            InterimContact.Clear()
        End If

    End Sub

    'Method to retrieve Company Contact Information


    'Method to retrieve Refinery Contact information
    Private Sub GetPlantContacts()
        Dim Site As String = Utilities.GetSite(cboRefNum.Text, CurrentSortMode)
        Dim row As DataRow
        Dim params As New List(Of String)
        params.Add("RefNum/" + Site)
        ds = db.ExecuteStoredProc("Console." & "GetContactInfo", params)

        PlantContact = New Contacts()
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                row = ds.Tables(0).Rows(0)

                PlantContact.FullName = row("CoordName").ToString.Trim
                PlantContact.Email = row("CoordEmail").ToString
                PlantContact.Phone = row("CoordPhone").ToString
            Else
                PlantContact.Clear()
            End If
        End If



    End Sub
    Private Sub GetCompanyContacts()
        Dim Site As String = Utilities.GetSite(cboRefNum.Text, CurrentSortMode)
        Dim row As DataRow
        Dim params As New List(Of String)
        params.Add("CompanyID/" + GetCompanyName(cboRefNum.Text))
        ds = db.ExecuteStoredProc("Console." & "GetCompanyContactInfo", params)

        CompanyContact = New Contacts()
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                row = ds.Tables(0).Rows(0)

                CompanyContact.FullName = row("CoordName").ToString.Trim
                CompanyContact.Email = row("CoordEmail").ToString
                CompanyPassword = row("Password").ToString.Trim
                txtReturnPassword.Text = CompanyPassword


            Else
                CompanyContact.Clear()
            End If
        Else
            CompanyContact.Clear()
        End If
    End Sub
    'Method to retrieve all infomation per Ref Num
    Public Sub GetARefNumRecord(mRefNum As String)

        ConsoleTabs.Enabled = True
        If mRefNum = "" Then mRefNum = RefNum

        CurrentRefNum = mRefNum


        'If Refnum is lubes, change variable according to need
        If CurrentRefNum.Contains("LIV") Then
            LubRefNum = CurrentRefNum
            CurrentRefNum = CurrentRefNum.Replace("LIV", "LUB")
        End If
        If CurrentRefNum.Contains("LEV") Then
            LubRefNum = CurrentRefNum
            CurrentRefNum = CurrentRefNum.Replace("LEV", "LUB")
        End If

        StudyRegion = ParseRefNum(mRefNum, 2)
        cboRefNum.Text = CurrentRefNum
        'Main


        GetConsultants()
        SetPaths()
        SetTabDirty(True)
        LoadTab(CurrentTab)

    End Sub
    'Method to retrieve all infomation per Ref Num
    Public Sub GetRefNumRecord(mRefNum As String)

        ConsoleTabs.Enabled = True
        If mRefNum = "" Then mRefNum = RefNum

        CurrentRefNum = mRefNum


        StudyRegion = ParseRefNum(mRefNum, 2)
        Company = GetCompanyByRefNum(mRefNum)
        If Company.Length > 0 Then CurrentCompany = Company


        cboRefNum.Text = CurrentRefNum
        'Main 
        IsInHolding()
        GetConsultants()
        SetPaths()
        SetTabDirty(True)
        LoadTab(CurrentTab)

    End Sub





#End Region

#Region "UI METHODS"


    Private Sub ConsoleTabs_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ConsoleTabs.SelectedIndexChanged
        CurrentTab = ConsoleTabs.SelectedIndex
        LoadTab(CurrentTab)
    End Sub


    Private Sub txtValidationIssues_LostFocus(sender As Object, e As System.EventArgs) Handles txtValidationIssues.LostFocus
        Dim mRefNum As String = Utilities.GetSite(RefNum, CurrentSortMode)
        Dim params = New List(Of String)


        If ValidationIssuesIsDirty Then
            params.Add("RefNum/" + mRefNum)
            params.Add("Notes/" + Utilities.FixQuotes(txtValidationIssues.Text))
            Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes", params)
            If c = 1 Then

                ValidationIssuesIsDirty = False
            Else
                MessageBox.Show("Validation Notes did not update")
            End If
        End If

    End Sub


    Private Sub txtValidationIssues_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtValidationIssues.TextChanged
        ValidationIssuesIsDirty = True
    End Sub

    Private Sub tvwClientAttachments2_DragDrop(sender As Object, e As System.Windows.Forms.DragEventArgs) Handles tvwClientAttachments2.DragDrop
        TreeviewDragDrop(sender, e, ClientAttachmentsPath)
    End Sub

    Private Sub tvIssues_AfterCheck(sender As Object, e As System.Windows.Forms.TreeViewEventArgs) Handles tvIssues.AfterCheck

        txtIssueID.Text = ""
        txtIssueName.Text = ""
        txtCompletedBy.Text = ""
        txtCompletedOn.Text = ""
        txtPostedBy.Text = ""
        txtPostedOn.Text = ""
        txtDescription.Text = ""

        Try
            tvIssues.SelectedNode = e.Node

            If e.Node.Checked Then
                If tvIssues.SelectedNode.Text.Contains("C 0.1") Then
                    If IsInHolding() Then
                        MessageBox.Show("You cannot check this off if it is in a HOLDING status", "Refinery in Holding")
                        e.Node.Checked = False
                        Exit Sub
                    End If

                End If
                UpdateIssue("Y")
            Else

                UpdateIssue("N")
            End If

        Catch ex As System.Exception
            Err.WriteLog("tvIssues_AfterCheck:" & RefNum, ex)
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub tvIssues_AfterSelect(sender As System.Object, e As System.Windows.Forms.TreeViewEventArgs) Handles tvIssues.AfterSelect
        Dim mRefNum As String = Utilities.GetSite(RefNum, CurrentSortMode)
        Dim IssueID As String = Nothing
        If Not tvIssues.SelectedNode Is Nothing Then


            IssueID = tvIssues.SelectedNode.Text
            IssueID = IssueID.Substring(0, IssueID.IndexOf("-")).Trim
            Dim row As DataRow
            Dim params As List(Of String)

            params = New List(Of String)
            params.Add("RefNum/" + mRefNum)
            params.Add("IssueID/" & IssueID)
            ds = db.ExecuteStoredProc("Console." & "GetAnIssue", params)


            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    row = ds.Tables(0).Rows(0)

                    txtIssueID.Text = row("IssueID").ToString
                    txtIssueName.Text = row("IssueTitle").ToString
                    txtPostedBy.Text = row("PostedBy").ToString
                    txtPostedOn.Text = row("PostedTime").ToString
                    txtCompletedBy.Text = row("SetBy").ToString
                    txtCompletedOn.Text = row("SetTime").ToString
                    txtDescription.Text = row("IssueText").ToString

                End If


            End If

        End If
    End Sub



    Dim WithEvents ContactForm As ContactFormPopup

    'Show Contact Popup form


    Private Sub btnPlantCoordinator_Click(sender As System.Object, e As System.EventArgs) Handles btnPlantCoordinator.Click
        ContactForm = New ContactFormPopup(UserName, "PowerPlant", Utilities.GetSite(RefNum, CurrentSortMode), GetCompanyName(RefNum), StudyType)
        ContactForm.Show()
    End Sub





    Private Sub btnCopyRetPassword_Click(sender As System.Object, e As System.EventArgs)
        Clipboard.SetText(txtReturnPassword.Text)
    End Sub



    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Clipboard.SetText(txtReturnPassword.Text)
    End Sub

    Private Sub tvwClientAttachments_DragDrop(sender As Object, e As System.Windows.Forms.DragEventArgs) Handles tvwClientAttachments.DragDrop
        TreeviewDragDrop(sender, e, ClientAttachmentsPath)
    End Sub

    Private Sub tvwCorrespondence_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles tvwCorrespondence.KeyDown, tvwDrawings.KeyDown, tvwClientAttachments.KeyDown, tvwCompCorr.KeyDown, tvwCorrespondence2.KeyDown, tvwDrawings2.KeyDown, tvwClientAttachments2.KeyDown, tvwCompCorr2.KeyDown
        If e.KeyCode = Keys.Space Then
            Process.Start("C:\")
        End If
    End Sub

    Private Sub btnOpenCorrFolder_Click(sender As System.Object, e As System.EventArgs)
        Process.Start(CorrPath)
    End Sub

    Private Sub btnUpdatePresenterNotes_Click(sender As System.Object, e As System.EventArgs)
        Dim mRefNum As String = Utilities.GetSite(RefNum, CurrentSortMode)
        Dim params = New List(Of String)

        params.Add("RefNum/" + mRefNum)
        params.Add("Notes/" + txtValidationIssues.Text)
        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes", params)
        If c = 1 Then
            MessageBox.Show("Validation Notes updated successfully")
        Else
            MessageBox.Show("Validation Notes did not update")
        End If
    End Sub


    Private Sub btnUpdateIssue_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdateIssue.Click
        Dim mRefNum As String = Utilities.GetSite(RefNum, CurrentSortMode)
        Dim params As List(Of String)
        ' Be sure they gave us an IssueID and an IssueTitle
        If txtIssueID.Text = "" Or txtIssueName.Text = "" Then
            MsgBox("Please enter both an ID and a Title")
            Exit Sub
        End If

        ' Drop any single quotes in ID, Title, and Text.
        txtIssueID.Text = Utilities.DropQuotes(txtIssueID.Text)
        txtIssueName.Text = Utilities.DropQuotes(txtIssueName.Text)
        txtDescription.Text = Utilities.DropQuotes(txtDescription.Text)

        params = New List(Of String)
        params.Add("RefNum/" + mRefNum)
        params.Add("IssueID/" + txtIssueID.Text)
        ' Be sure it does not already exist.
        ds = db.ExecuteStoredProc("Console." & "GetAnIssue", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                MsgBox("That IssueID already exists for this Refnum.", vbOKOnly)
                Exit Sub
            End If

        End If

        ' Everything looks OK, add it here.
        'Study Combo Box
        params = New List(Of String)
        params.Add("RefNum/" + mRefNum)
        params.Add("IssueID/" + txtIssueID.Text)
        params.Add("IssueTitle/" + txtIssueName.Text)
        params.Add("IssueText/" + txtDescription.Text)
        params.Add("UserName/" + UserName)
        ds = db.ExecuteStoredProc("Console." & "AddIssue", params)
        BuildCheckListTab("Incomplete")
        btnAddIssue.Enabled = True
        txtDescription.ReadOnly = True
    End Sub

    Private Sub btnCancelIssue_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelIssue.Click
        btnAddIssue.Enabled = True
    End Sub


    Private Sub btnSecureSend_Click_1(sender As System.Object, e As System.EventArgs) Handles btnSecureSend.Click
        Dim ss As New SecureSend(StudyType, tvwCorrespondence, tvwDrawings, tvwClientAttachments, tvwCompCorr, tvwCorrespondence2, tvwDrawings2, tvwClientAttachments2, tvwCompCorr2, Nothing, Nothing, UserName, Password)

        ss.RefNum = RefNum
        ss.LubeRefNum = LubRefNum
        ss.Study = StudyType
        ss.TemplatePath = TemplatePath
        ss.CorrPath = CorrPath
        ss.DrawingPath = DrawingPath
        ss.ClientAttachmentsPath = ClientAttachmentsPath
        ss.Loc = Company
        ss.User = UserName

        ss.CompCorrPath = CompCorrPath
        ss.CorrPath2 = CorrPath
        ss.DrawingPath2 = DrawingPath
        ss.ClientAttachmentsPath2 = ClientAttachmentsPath

        ss.CompCorrPath2 = CompCorrPath

        ss.TempPath = TempPath
        ss.SendTo = PlantCoordEmail.Text.Trim
        ss.SendCC = CompanyContactEmail.Text.Trim


        ss.Show()
    End Sub




    Private Sub cboRefNum_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cboRefNum.KeyDown
        Dim rmRefNum As String = CurrentRefNum

        If e.KeyCode = Keys.Enter Then
            KeyEntered = True

            cboRefNum.SelectedIndex = FindString(cboRefNum, cboRefNum.Text)

            If cboRefNum.SelectedIndex <> -1 Then
                RefNumChanged(cboRefNum.Text)
            Else
                cboRefNum.SelectedIndex = cboRefNum.FindString(CurrentRefNum)
            End If
        End If

    End Sub

    Private Sub btnPA_Click(sender As System.Object, e As System.EventArgs)
        Dim csm As Integer = 0
        If CurrentSortMode = 0 Then csm = 1 Else csm = 0
        Process.Start("K:\study\power\2013\plant\" & Utilities.GetSite(RefNum, csm))
    End Sub

    Private Sub BuildSummarySupportTab()

        clbSummaryCalcFiles.Enabled = True
        clbSummaryCalcFiles.Items.Clear()
        Dim csm As Integer = 0
        Dim files() As String
        If CurrentSortMode = 0 Then csm = 1 Else csm = 0


        files = Directory.GetFiles("K:\study\power\2013\plant\" & Utilities.GetSite(cboRefNum.Text, csm), "*_SC.*")



        For Each f As String In files
            clbSummaryCalcFiles.Items.Add(f, True)
        Next
        If files.Count = 0 Then
            clbSummaryCalcFiles.Items.Add("NO SUMMARY CALCS FOUND", False)
            clbSummaryCalcFiles.Enabled = False
        End If
    End Sub

    Private Sub tvwCompCorr_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles tvwCompCorr.DragDrop

        TreeviewDragDrop(sender, e, CompCorrPath)

    End Sub
    Private Sub tvwCompCorr2_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles tvwCompCorr2.DragDrop

        TreeviewDragDrop(sender, e, CompCorrPath)

    End Sub
    Private Sub tvwCorrespondence2_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles tvwCorrespondence2.DragDrop

        TreeviewDragDrop(sender, e, CorrPath)

    End Sub

    Private Sub tvwDrawings2_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles tvwDrawings2.DragDrop

        TreeviewDragDrop(sender, e, DrawingPath)

    End Sub


    Private Sub tvwCorrespondence_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles tvwCorrespondence.DragDrop

        TreeviewDragDrop(sender, e, CorrPath)

    End Sub


    Private Sub tvwCompany_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs)
        TreeviewDragDrop(sender, e, CompanyPath)
    End Sub
    Private Sub TreeviewDragDrop(sender As Object, e As System.Windows.Forms.DragEventArgs, sDir As String)
        Dim dataObject As OutlookDataObject = New OutlookDataObject(e.Data)

        If cboRefNum.Text <> "" Then
            'get the names and data streams of the files dropped
            Dim filenames() As String = CType(dataObject.GetData("FileGroupDescriptorW"), String())
            Dim ans As String
            ans = InputBox(ValidatePath & " : ", "Save File to:", filenames(0).Trim)
            If ans <> "" Then
                Dim filestreams() As MemoryStream = CType(dataObject.GetData("FileContents"), MemoryStream())

                For fileIndex = 0 To filenames.Length - 1

                    'use the fileindex to get the name and data stream
                    Dim filename As String = filenames(fileIndex)
                    Dim filestream As MemoryStream = filestreams(fileIndex)

                    'save the file stream using its name to the application path
                    Dim outputStream As FileStream = File.Create(sDir & "\" & ans)
                    filestream.WriteTo(outputStream)
                    outputStream.Close()
                    SetTree(tvwCompCorr, CompCorrPath)
                Next
            End If
        End If
    End Sub


    Private Sub btnSecureSend_Click(sender As System.Object, e As System.EventArgs)

    End Sub


    Private Sub cboDir_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboDir.SelectedIndexChanged
        Select Case cboDir.Text

            Case "Plant Correspondence"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = True

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = False

            Case "Client Attachments"
                tvwClientAttachments.Visible = True
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = False

            Case "Drawings"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = True
                tvwCompCorr.Visible = False
            Case "Company Correspondence"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = True
        End Select
    End Sub
    Private Sub cboDir2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboDir2.SelectedIndexChanged
        Select Case cboDir2.Text

            Case "Refinery Correspondence"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = True

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = False

            Case "Client Attachments"
                tvwClientAttachments2.Visible = True
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = False

            Case "Drawings"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = True
                tvwCompCorr2.Visible = False
            Case "Company Correspondence"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = True
        End Select
    End Sub

    Private Sub tvwDrawings_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles tvwDrawings.DragDrop
        TreeviewDragDrop(sender, e, DrawingPath)
    End Sub


    Private Sub btnFLComp_Click(sender As System.Object, e As System.EventArgs)
        File.Copy(ValidatePath & "Compare Fuels and Lubes " & StudyYear.Substring(2, 2) & ".xls", CorrPath & "Compare Fuels and Lubes " & StudyYear.Substring(2, 2) & ".xls")
        Process.Start(CorrPath & "Compare Fuels and Lubes " & StudyYear.Substring(2, 2) & ".xls")
    End Sub

    Private Sub btnTP_Click(sender As System.Object, e As System.EventArgs)
        File.Copy(ValidatePath & "Transfer Pricing " & StudyYear.Substring(2, 2) & ".xls", CorrPath & "Transfer Pricing " & StudyYear.Substring(2, 2) & ".xls")
        Process.Start(CorrPath & "Transfer Pricing " & StudyYear.Substring(2, 2) & ".xls")
    End Sub


    Private Sub btnOpenWord_Click(sender As System.Object, e As System.EventArgs)
        Dim msword As New Word.Application
        Dim docPN As New Word.Document
        Try
            If Dir(CorrPath & "PN_" & Company & ".doc") = "" Then
                If MsgBox("Do you want to create PN_" & Company & " .doc and stop updating the Validation Notes / Presenter's Notes field through Console?", vbYesNoCancel + vbDefaultButton2, "Click Yes to create the file.") = vbYes Then
                    'MsgBox "Create the file, lock the field, open the file with word."

                    Dim strWork As String = "~ " & Now & " The Presenters Notes document has been built so this field is locked. Please use the Presenters Notes button to open the file. " & Chr(13) & Chr(10) & "----------" & Chr(13) & Chr(10)

                    ' Add the tilde to the Issues Notes field
                    ' First, see if a record exists for this refnum
                    Dim row As DataRow
                    Dim params As New List(Of String)
                    params.Add("RefNum/" + RefNum)
                    ds = db.ExecuteStoredProc("Console." & "GetValidationNotes", params)

                    If ds.Tables.Count > 0 Then
                        row = ds.Tables(0).Rows(0)

                        txtValidationIssues.Text = row("ValidationNotes").ToString


                    Else

                        params = New List(Of String)
                        params.Add("RefNum/" + RefNum)
                        params.Add("Notes/" + txtValidationIssues.Text)

                        Dim Count As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes", params)

                    End If

                    Dim objWriter As New System.IO.StreamWriter(TempPath & "\PNotes.txt", True)
                    objWriter.WriteLine(RefNum)
                    objWriter.WriteLine(txtValidationIssues.Text)
                    objWriter.Close()

                    ' Create the document.
                    File.Copy(TemplatePath & "PN_Template.doc", TempPath & "PN_Template.doc", True)
                    msword = New Word.Application
                    'Set docPN = MSWord.Documents.Open(gstrtempfolder & "PNotes.txt")

                    docPN = msword.Documents.Open(TempPath & "PN_Template.doc")

                    docPN.SaveAs(CorrPath & "PN_" & Company & ".doc")

                    ' Put the CoLoc and the Notes into the document.
                    msword.Run("SubsAuto")
                End If
            Else

                msword = New Word.Application
                'Set docPN = MSWord.Documents.Open(gstrtempfolder & "PNotes.txt")

                docPN = msword.Documents.Open(CorrPath & "PN_" & Company & ".doc")
                msword.Visible = True
            End If

        Catch Ex As System.Exception
            Err.WriteLog("btnOpenWord", Ex)
        End Try


    End Sub



    Private Sub MainConsole_DragDrop(ByVal sender As Object, _
                        ByVal e As System.Windows.Forms.DragEventArgs) _
                        Handles MyBase.DragDrop, ConsoleTabs.DragDrop

        Dim strFile As String = String.Empty
        Dim ext As String
        Dim SavedFiles As New List(Of String)
        Dim strFileName As String
        Dim strDocFileName As String
        Dim Solomon As Boolean = False
        Dim mailItem As Outlook._MailItem = Nothing
        If cboRefNum.Text <> "" Then
            lblStatus.ForeColor = Color.DarkGreen
            lblStatus.Text = "Saving..."

            Dim filenames() As String = Directory.GetFiles(TempPath, "*.*")
            For Each sFile In filenames
                Try
                    File.Delete(sFile)
                Catch ex As System.Exception
                    Debug.Print(ex.Message)
                End Try
            Next




            If Not e.Data.GetDataPresent(DataFormats.Text) = False Then
                'check if this is an outlook message. The outlook messages, all contain a FileContents attribute. If not, exit.
                Dim formats() As String = e.Data.GetFormats()

                If formats.Contains("FileContents") = False Then Exit Sub

                'they are dragging the attachment
                If (e.Data.GetDataPresent("Object Descriptor")) Then
                    Dim app As New Microsoft.Office.Interop.Outlook.Application() ' // get current selected items
                    Dim selection As Microsoft.Office.Interop.Outlook.Selection
                    Dim myText As String = ""
                    selection = app.ActiveExplorer.Selection
                    If selection IsNot Nothing Then


                        For i As Integer = 0 To selection.Count - 1

                            mailItem = TryCast(selection.Item(i + 1), Outlook._MailItem)

                            If mailItem IsNot Nothing Then

                                'If mailItem.Recipients.Count > 0 Then Solomon = mailItem.Recipients(1).Address.Contains("SOLOMON")
                                myText = ""
                                myText = e.Data.GetData("Text")  'header text
                                myText += vbCrLf + vbCrLf
                                myText += mailItem.Body  'Plain Text Body Message

                                If mailItem.SenderEmailAddress.ToUpper.Contains("SOLOMON") Then Solomon = True

                                'now save the attachments with the same file name and then 1,2,3 next to it
                                For Each att As Attachment In mailItem.Attachments
                                    If att.FileName.Substring(0, 5).ToUpper <> "IMAGE" Then
                                        Try
                                            ext = att.FileName.Substring(att.FileName.LastIndexOf(".") + 1, att.FileName.Length - att.FileName.LastIndexOf(".") - 1)
                                        Catch
                                            ext = ""
                                        End Try

                                        If ext.ToUpper = "ZIP" Then
                                            If Dir(TempPath & "Extracted Files\") = "" Then Directory.CreateDirectory(TempPath & "Extracted Files\")
                                            Dim zfilenames() As String = Directory.GetFiles(TempPath & "Extracted Files", "*.*")
                                            For Each sFile In zfilenames
                                                Try
                                                    File.Delete(sFile)
                                                Catch ex As System.Exception
                                                    'Err.WriteLog("DragDrop", ex)
                                                    Debug.Print(ex.Message)
                                                End Try
                                            Next
                                            If Solomon Then
                                                att.SaveAsFile(TempPath & Date.Now.ToString("yyyy-MM-dd hh.mm") & " (SENT) - " & att.FileName)
                                            Else
                                                att.SaveAsFile(TempPath & Date.Now.ToString("yyyy-MM-dd hh.mm") & " (RECV) - " & att.FileName)
                                            End If

                                            Utilities.UnZipFiles(TempPath & att.FileName, TempPath, CompanyPassword)
                                            OldTempPath = TempPath
                                            TempPath = TempPath & "Extracted Files\"

                                        Else
                                            If Solomon Then
                                                att.SaveAsFile(TempPath & Date.Now.ToString("yyyy-MM-dd hh.mm") & " (SENT) - " & att.FileName)
                                            Else
                                                att.SaveAsFile(TempPath & Date.Now.ToString("yyyy-MM-dd hh.mm") & " (RECV) - " & att.FileName)
                                            End If
                                        End If
                                    End If
                                Next


                            End If




                        Next


                        strFileName = Utilities.GetSite(cboRefNum.Text, CurrentSortMode)

                        ' More often, use this technique to name the text file.
                        If mailItem.Subject Is Nothing Or mailItem.Subject = "" Then
                            strFileName = Utilities.GetSite(cboRefNum.Text, CurrentSortMode)
                            strFileName = strFileName & ".txt"
                        Else
                            strFileName = strFileName & " - " & mailItem.Subject.Replace(":", " ") & ".txt"
                        End If


                        If Solomon Then
                            strFileName = TempPath & Date.Now.ToString("yyyy-MM-dd hh.mm") & " (SENT) - " & strFileName
                        Else
                            strFileName = TempPath & Date.Now.ToString("yyyy-MM-dd hh.mm") & " (RECV) - " & strFileName
                        End If
                        Dim strw As New StreamWriter(strFileName)

                        strw.WriteLine(myText)
                        strw.Close()
                        strw.Dispose()



                        strDocFileName = Utilities.ConvertToDoc(TempPath, strFileName)
                        File.Delete(strFileName)

                    End If

                End If
            End If
        End If

        SaveFiles()
    End Sub
    Private Sub SaveFiles()
        Dim CopyFile As String

        Dim ext As String
        Dim success As Boolean = True
        Try



            For Each f In Directory.GetFiles(TempPath)
                ext = GetExtension(f)
                CopyFile = Utilities.ExtractFileName(f)

                CopyFile = FixFilename(ext, CopyFile)


                If ext.Substring(0, 3).ToUpper = "DOC" Or ext.Substring(0, 3).ToUpper = "XLS" Then

                    success = RemovePassword(TempPath & CopyFile, "", CompanyPassword)

                End If

                If Not success Then MessageBox.Show(CopyFile & " is password protected.", "Warning")

                File.Copy(TempPath & CopyFile, CorrPath & CopyFile, True)
                lblStatus.Text = "Saving " & CopyFile & "...."
                File.Delete(TempPath & CopyFile)

            Next
            lblStatus.Text = "Ready"
        Catch ex As System.Exception
            Err.WriteLog("btnSaveFiles", ex)
            lblStatus.ForeColor = Color.DarkRed
            lblStatus.Text = "Error saving files"
        End Try
    End Sub

    Private Sub tvwCorrespondence_BeforeExpand(sender As System.Object, e As System.Windows.Forms.TreeViewCancelEventArgs) Handles tvwCorrespondence.BeforeExpand, tvwDrawings.BeforeExpand, tvwClientAttachments.BeforeExpand, tvwCompCorr.BeforeExpand, tvwCorrespondence2.BeforeExpand, tvwDrawings2.BeforeExpand, tvwClientAttachments2.BeforeExpand, tvwCompCorr2.BeforeExpand
        e.Node.Nodes.Clear()
        ' get the directory representing this node
        Dim mNodeDirectory As IO.DirectoryInfo
        mNodeDirectory = New IO.DirectoryInfo(e.Node.Tag.ToString)

        ' add each subdirectory from the file system to the expanding node as a child node
        For Each mDirectory As IO.DirectoryInfo In mNodeDirectory.GetDirectories
            ' declare a child TreeNode for the next subdirectory
            Dim mDirectoryNode As New TreeNode
            ' store the full path to this directory in the child TreeNode's Tag property
            mDirectoryNode.Tag = mDirectory.FullName
            ' set the child TreeNodes's display text
            mDirectoryNode.Text = mDirectory.Name
            ' add a dummy TreeNode to this child TreeNode to make it expandable
            mDirectoryNode.Nodes.Add("*DUMMY*")
            mDirectoryNode.ImageKey = CacheShellIcon(mDirectoryNode.Tag)
            mDirectoryNode.SelectedImageKey = mDirectoryNode.ImageKey & "-open"
            ' add this child TreeNode to the expanding TreeNode
            e.Node.Nodes.Add(mDirectoryNode)
        Next

        ' add each file from the file system that is a child of the argNode that was passed in
        For Each mFile As IO.FileInfo In mNodeDirectory.GetFiles
            ' declare a TreeNode for this file
            Dim mFileNode As New TreeNode
            ' store the full path to this file in the file TreeNode's Tag property
            mFileNode.Tag = mFile.FullName
            ' set the file TreeNodes's display text
            mFileNode.Text = mFile.Name
            mFileNode.ImageKey = CacheShellIcon(mFileNode.Tag)
            mFileNode.SelectedImageKey = mFileNode.ImageKey
            mFileNode.SelectedImageKey = mFileNode.ImageKey & "-open"
            ' add this file TreeNode to the TreeNode that is being populated
            e.Node.Nodes.Add(mFileNode)
        Next

    End Sub

    Private Sub tvwCorrespondence_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tvwCorrespondence.NodeMouseDoubleClick, tvwDrawings.NodeMouseDoubleClick, tvwClientAttachments.NodeMouseDoubleClick, tvwCompCorr.NodeMouseDoubleClick, tvwCorrespondence2.NodeMouseDoubleClick, tvwDrawings2.NodeMouseDoubleClick, tvwClientAttachments2.NodeMouseDoubleClick, tvwCompCorr2.NodeMouseDoubleClick
        ' try to open the file
        Try
            Process.Start(e.Node.Tag)
        Catch ex As System.Exception
            Err.WriteLog("tvwCorrespondence_NodeMouseDoubleClick", ex)
            MessageBox.Show("Error opening file: " & ex.Message)
        End Try
    End Sub
    Private Sub MainConsole_DragEnter(ByVal sender As Object, _
                           ByVal e As System.Windows.Forms.DragEventArgs) _
                           Handles MyBase.DragEnter, ConsoleTabs.DragEnter, tvwCorrespondence.DragEnter, tvwDrawings.DragEnter, tvwCompCorr.DragEnter, tvwCompCorr2.DragEnter, tvwClientAttachments.DragEnter, tvwCorrespondence2.DragEnter, tvwDrawings2.DragEnter, tvwClientAttachments2.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.Copy
        ElseIf (e.Data.GetDataPresent("FileGroupDescriptor")) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub
    Private Sub btnAddIssue_Click(sender As System.Object, e As System.EventArgs) Handles btnAddIssue.Click

        txtIssueID.Text = ""
        txtIssueName.Text = ""
        txtCompletedBy.Text = ""
        txtCompletedOn.Text = ""
        txtPostedBy.Text = ""
        txtPostedOn.Text = ""
        txtDescription.Text = ""
        txtIssueID.Focus()
        btnAddIssue.Enabled = False

        'IssueForm.DBConn = mDBConnection
        'IssueForm.RefNum = RefNum
        'IssueForm.StudyType = StudyType
        'IssueForm.UserName = mUserName
        'IssueForm.Show()
        txtDescription.ReadOnly = False

    End Sub


    Private Sub btnHelp_Click(sender As System.Object, e As System.EventArgs) Handles btnHelp.Click



        Dim strHelpFilePath As String

        strHelpFilePath = "K:\STUDY\SAI Software\Console2012\"


        Process.Start(strHelpFilePath & "Console 2012.docx")

    End Sub



    Private Sub btnLog_Click(sender As System.Object, e As System.EventArgs)
        ' These are the "file" buttons on the right of the
        ' Validation Summary tab.
        ' Basically, here we just open the path/file stored in the tag
        ' property of the button.
        ' This property was set by SetFileButtons earlier when the Refnum
        ' was set/changed.
        Dim wb As Object
        Dim xl As New Excel.Application


        Try

            xl.Visible = True
            wb = xl.Workbooks.Open("K:\Study\" & "Console." & StudyType & "\" & StudyYear & "\" & cboStudy.SelectedItem.ToString.Substring(0, 3) & "\Refinery\" & RefNum & "\" & RefNum & "_Log.xls")
            xl.Visible = True

            ' Next line is important.
            ' If you don't do this, if the user closes Excel then clicks a file button
            ' again, you will get an Excel window with just the 'frame'. The inside
            ' of the 'window' will be empty!
            xl = Nothing

        Catch ex As System.Exception

            MsgBox("Unable to open file. If you have Excel open, and are editing a cell, get out of that mode and retry. ", vbOKOnly, "Unable to open file")
            Err.WriteLog("btnLOG_Click", ex)
        End Try
    End Sub


    Private Sub MainConsole_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Initialize()
    End Sub

    Private Sub MainConsole_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If Not Spawn Then SaveLastStudy()
        End
    End Sub




    Private Sub ValCheckList_ItemCheck(sender As Object, e As System.Windows.Forms.ItemCheckEventArgs) Handles ValCheckList.ItemCheck

        Try
            If e.CurrentValue = CheckState.Checked Then
                e.NewValue = CheckState.Unchecked
                UpdateIssue("N")
            Else
                e.NewValue = CheckState.Checked
                UpdateIssue("Y")
            End If

        Catch ex As System.Exception
            Err.WriteLog("ValCheckList_ItemCheck", ex)
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ValCheckList_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ValCheckList.SelectedIndexChanged

        If Not ValCheckList.SelectedItem Is Nothing Then

            Dim row As DataRow
            Dim params As List(Of String)

            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("IssueTitle/" & Me.ValCheckList.SelectedItem.ToString.Trim)
            ds = db.ExecuteStoredProc("Console." & "GetAnIssue", params)


            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    row = ds.Tables(0).Rows(0)

                    txtIssueID.Text = row("IssueID").ToString
                    txtIssueName.Text = row("IssueTitle").ToString
                    txtCompletedBy.Text = row("PostedBy").ToString
                    txtCompletedOn.Text = row("PostedTime").ToString
                    txtPostedBy.Text = row("SetBy").ToString
                    txtPostedOn.Text = row("SetTime").ToString
                    txtDescription.Text = row("IssueText").ToString

                End If


            End If

        End If



    End Sub


    Public Sub SetTree(tvw As TreeView, mRootPath As String)
        Try
            tvw.Nodes.Clear()
            Dim mRootNode As New TreeNode
            mRootNode.Text = mRootPath
            mRootNode.Tag = mRootPath
            mRootNode.ImageKey = CacheShellIcon(mRootPath)
            mRootNode.SelectedImageKey = mRootNode.ImageKey & "-open"
            mRootNode.Nodes.Add("*")
            tvw.Nodes.Add(mRootNode)
            tvw.ExpandAll()
            tvw.Refresh()
        Catch ex As System.Exception
            Err.WriteLog("SetTree", ex)
        End Try
    End Sub

    Private Sub btnUpdateNotes_Click(sender As System.Object, e As System.EventArgs)


        Dim bIssues As Boolean = False

        Dim params As New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetValidationNotes", params)

        If ds.Tables.Count = 0 Then
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("Notes/" & Utilities.FixQuotes(txtValidationIssues.Text))

            ds = db.ExecuteStoredProc("Console." & "InsertValidationNotes", params)

        Else
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("Notes/" & Utilities.FixQuotes(txtValidationIssues.Text))

            ds = db.ExecuteStoredProc("Console." & "UpdateValidationNotes", params)
        End If

        params = New List(Of String)
        params.Add("RefNum/" + RefNum)


        ds = db.ExecuteStoredProc("Console." & "UpdateComments", params)


        BuildSummaryTab()



    End Sub





    Private Sub cboCheckListView_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboCheckListView.SelectedIndexChanged
        BuildCheckListTab(cboCheckListView.SelectedItem)
    End Sub

    Private Sub UpdateCheckList(Optional status As String = "N")
        Dim mRefNum As String = Utilities.GetSite(cboRefNum.Text, CurrentSortMode)
        Dim I As Integer
        Dim row As DataRow
        Dim params As List(Of String)
        Dim intCompleteCount As Integer, intNotCompleteCount As Integer
        Dim node As TreeNode
        RemoveHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
        RemoveHandler cboCheckListView.SelectedIndexChanged, AddressOf cboCheckListView_SelectedIndexChanged
        ValCheckList.Items.Clear()
        tvIssues.Nodes.Clear()

        ' Load the Uncompleted Checklist Item count label
        params = New List(Of String)
        params.Add("RefNum/" + mRefNum)
        params.Add("Mode/Y")
        ds = db.ExecuteStoredProc("Console." & "GetIssueCounts", params)


        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                intCompleteCount = Convert.ToInt32(ds.Tables(0).Rows(0).Item("Issues"))
            End If
        End If

        params = New List(Of String)
        params.Add("RefNum/" + mRefNum)
        params.Add("Mode/N")
        ds = db.ExecuteStoredProc("Console." & "GetIssueCounts", params)


        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                intNotCompleteCount = Convert.ToInt32(ds.Tables(0).Rows(0).Item("Issues"))
            End If
        End If

        lblItemCount.Text = intCompleteCount & " Completed    " & intNotCompleteCount & " Not Complete."



        If status = "Y" Or status = "A" Then
            params = New List(Of String)
            params.Add("RefNum/" + mRefNum)
            params.Add("Completed/Y")
            ds = db.ExecuteStoredProc("Console." & "GetIssues", params)
            If ds.Tables(0).Rows.Count > 0 Then
                For I = 0 To ds.Tables(0).Rows.Count - 1
                    row = ds.Tables(0).Rows(I)

                    node = tvIssues.Nodes.Add(Utilities.Pad(row("IssueID"), 8) & "-" & row("IssueTitle"))

                    node.Checked = True

                Next

            End If
            cboCheckListView.SelectedIndex = 1
        End If

        If status = "N" Or status = "A" Then
            params = New List(Of String)
            params.Add("RefNum/" + mRefNum)
            params.Add("Completed/N")
            ds = db.ExecuteStoredProc("Console." & "GetIssues", params)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For I = 0 To ds.Tables(0).Rows.Count - 1
                        row = ds.Tables(0).Rows(I)

                        tvIssues.Nodes.Add(Utilities.Pad(row("IssueID"), 8) & "-" & row("IssueTitle"))

                    Next

                End If
            End If
            If status = "N" Then
                cboCheckListView.SelectedIndex = 0
            Else
                cboCheckListView.SelectedIndex = 2
            End If
        End If
        AddHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
        AddHandler cboCheckListView.SelectedIndexChanged, AddressOf cboCheckListView_SelectedIndexChanged
    End Sub
    Private Sub SetSummaryFileButtons(mSiteID As String)

        Dim mRefNums As New List(Of String)
        Dim params As New List(Of String)
        params.Add("SiteID/" + mSiteID)

        ds = db.ExecuteStoredProc("Console.GetRefNumBySiteID", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each Row In ds.Tables(0).Rows
                    mRefNums.Add(Row(0).ToString.Trim)
                Next
            End If
        End If


        If mRefNums.Count > 0 And (mRefNums(0) = mSiteID) Then


            If File.Exists(PlantPath & mSiteID & "_PT.xls") Then
                btnPT.Enabled = True
                btnPT.Tag = PlantPath & mSiteID & "_PT.xls"
            Else
                btnPT.Enabled = False
            End If





        Else
            If mRefNums.Count = 1 Then
                If File.Exists(PlantPath & mRefNums(0) & "_PT.xls") Then
                    btnPT.Enabled = True
                    btnPT.Tag = PlantPath & mRefNums(0) & "_PT.xls"
                Else
                    btnPT.Enabled = False
                End If




            Else
                btnPT.Enabled = True

                btnPT.Tag = PlantPath

            End If
        End If
    End Sub
    Private Sub PopulateCompCorrTreeView(tvw As TreeView)

        If Directory.Exists(CompCorrPath) Then
            SetTree(tvw, CompCorrPath)
        End If

    End Sub
    Private Sub PopulateCorrespondenceTreeView(tvw As TreeView)

        If Directory.Exists(CorrPath) Then
            SetTree(tvw, CorrPath)
        End If

    End Sub


    Private Sub PopulateDrawingTreeView(tvw As TreeView)

        If Directory.Exists(DrawingPath) Then
            SetTree(tvw, DrawingPath)
        End If

    End Sub
    Private Sub PopulateCATreeView(tvw As TreeView)

        If Directory.Exists(ClientAttachmentsPath) Then
            SetTree(tvw, ClientAttachmentsPath)
        End If

    End Sub
    Private Sub btnUpdate_Click(sender As System.Object, e As System.EventArgs)

        LaunchValidation(True)

    End Sub
    Private Sub btnValidate_Click(sender As System.Object, e As System.EventArgs) Handles btnValidate.Click

        LaunchValidation(False)

    End Sub

    Private Sub LaunchValidation(JustLooking As Boolean)
        Dim xl As Excel.Application
        Dim w As Excel.Workbook = Nothing
        Dim AlreadyOpen As Boolean = False
        Dim PowerPath As String = "C:\Users\" & UserName & "\Console\Temp\"

        'Dim xl As Object = CreateObject("Excel.Application")
        xl = Utilities.GetExcelProcess()
        Try
            Dim gstrStudyYearShort As String = StudyYear.ToString.Substring(2, 2)
            Dim strValidationFileOnly As String = ""
            Dim strValidationFile As String = ""
            Dim DirYear As Integer = Convert.ToInt16(StudyYear) + 1

            strValidationFileOnly = "Validate" & gstrStudyYearShort & ".xls"
            strValidationFile = "P:\" & DirYear.ToString & "\Power\" & (Convert.ToInt16(DirYear.ToString.Substring(3, 1))).ToString() & "PM006S - Power CPA\Validate\" & strValidationFileOnly
            If Not File.Exists(PowerPath & strValidationFileOnly) Then
                File.Copy(strValidationFile, PowerPath & strValidationFileOnly, True)
            End If

            For Each wb In xl.Workbooks
                If wb.Name = strValidationFileOnly Then
                    AlreadyOpen = True
                    Exit For
                End If
            Next wb


            ' Open Excel spreadsheet.
            If Not AlreadyOpen Then

                If (VPN) Then
                    Dim b As DialogResult = MessageBox.Show("Since you are on VPN, we are going to pause to give the file enough time to load", "Wait", MessageBoxButtons.OK)
                    System.Threading.Thread.Sleep(3000)
                End If

                w = xl.Workbooks.Open(PowerPath & strValidationFileOnly)
                xl.Visible = True


            End If

            If xl.Workbooks(strValidationFileOnly).Windows(strValidationFileOnly).Visible = False Then xl.Workbooks(strValidationFileOnly).Windows(strValidationFileOnly).Visible = True
            'Process.Start(strValidationFile & "!ValidateCall", Utilities.GetDataSetID(RefNum, 1).Trim)
            xl.Run("ValidateCall", Utilities.GetDataSetID(RefNum, 1).Trim)

            'w.Close(SaveChanges:=False)
            'xl = Nothing

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "LaunchValidation")
            Err.WriteLog("LaunchValidation", ex)
        End Try

    End Sub
    Private Sub ChangedStudy(Optional mRefNum As String = "")

        If mRefNum = "" Then

            For i = 0 To 3
                If StudySave(i) = cboStudy.Text Then
                    GetRefNums(RefNumSave(i))
                    Exit Sub
                End If
            Next

        End If
        GetRefNums(mRefNum)



    End Sub
    'If Study Changes, then re-populate RefNums
    Private Sub cboStudy_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboStudy.SelectedIndexChanged
        ChangedStudy()
    End Sub

    'Method to populate the labels for Contacts
    Private Sub FillContacts()

        ClearFields()
        PlantCoordName.Text = PlantContact.FullName.ToUpper
        PlantCoordEmail.Text = PlantContact.Email.ToUpper
        PlantCoordPhone.Text = PlantContact.Phone.ToUpper
        CompanyContactName.Text = CompanyContact.FullName.ToUpper
        CompanyContactEmail.Text = CompanyContact.Email.ToUpper




    End Sub
    <Obsolete("Been incorrectly implemented, change for 2013 Olefins and 2014 Fuels", False)>
    Private Function GetReturnPassword(RefNum As String, salt As String) As String
        Return Utilities.XOREncryption(Trim(UCase(RefNum)), salt)
    End Function
    Public Sub RefNumChanged(Optional mRefNum As String = "")

        SetTabDirty(True)
        BuildDirectoriesTab()
        cboRefNum.Text = mRefNum
        GetRefNumRecord(mRefNum)
        SaveRefNum(cboStudy.Text, mRefNum)


        If mRefNum.Length > 0 Then RefNum = mRefNum
        'TabControl1.SelectedIndex = 0
    End Sub
    Private Sub cboRefNum_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboRefNum.SelectedIndexChanged
        RefNumChanged(cboRefNum.Text)
    End Sub

    'Private Sub cboConsultant_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboConsultant.SelectedIndexChanged
    'End Sub


    Private Sub btnDirRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnDirRefresh.Click
        BuildDirectoriesTab()
    End Sub

    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        Dim t As New TimeSpan(0, 0, Time)
        Dim l As New TimeSpan(0, 0, LastTime)
        Dim hours As String
        Dim minutes As String
        Dim seconds As String
        Dim lhours As String
        Dim lminutes As String
        Dim lseconds As String
        hours = t.Hours.ToString()
        minutes = t.Minutes.ToString
        seconds = t.Seconds.ToString
        If hours.Length = 1 Then hours = "0" & hours
        If minutes.Length = 1 Then minutes = "0" & minutes
        If seconds.Length = 1 Then seconds = "0" & seconds
        lhours = l.Hours.ToString()
        lminutes = l.Minutes.ToString
        lseconds = l.Seconds.ToString
        If lhours.Length = 1 Then lhours = "0" & lhours
        If lminutes.Length = 1 Then lminutes = "0" & lminutes
        If lseconds.Length = 1 Then lseconds = "0" & lseconds
        Time += 1
        ConsoleTimer.Text = "Timer : " & hours & ":" & minutes & ":" & seconds & "   Last Time: " & lhours & ":" & lminutes & ":" & lseconds
    End Sub



    Private Sub ConsoleTimer_DoubleClick(sender As System.Object, e As System.EventArgs) Handles ConsoleTimer.DoubleClick
        LastTime = Time
        Time = 0
    End Sub

    Private Sub ConsoleTimer_Click_1(sender As System.Object, e As System.EventArgs) Handles ConsoleTimer.Click
        If Timer1.Enabled Then
            ConsoleTimer.ForeColor = Color.DarkRed
            Timer1.Enabled = False
        Else
            ConsoleTimer.ForeColor = Color.Green
            Timer1.Enabled = True
        End If
    End Sub





    Private Sub btnKillProcesses_Click(sender As System.Object, e As System.EventArgs) Handles btnKillProcesses.Click
        Dim dr As New DialogResult()
        dr = MessageBox.Show("This will kill all OPEN Excel and Word Documents.  Please <SAVE> any Word or Excel documents you have open and then click OK when ready", "Warning", MessageBoxButtons.OKCancel)
        If dr = DialogResult.OK Then
            Utilities.KillProcesses("WinWord")
            Utilities.KillProcesses("Excel")
        End If
    End Sub

    Private Sub chkLock_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkLockPN.CheckedChanged
        If chkLockPN.Checked = False Then
            txtValidationIssues.ReadOnly = False
        Else
            txtValidationIssues.ReadOnly = True
        End If
    End Sub


#End Region


    Private Sub cboConsultant_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboConsultant.SelectedIndexChanged
        UpdateConsultant()
    End Sub


    Private Sub btnLaunch_Click(sender As System.Object, e As System.EventArgs) Handles btnLaunch.Click
        Dim cbList As CheckedListBox.CheckedItemCollection = clbSummaryCalcFiles.CheckedItems
        For Each cb In cbList
            Process.Start(cb)
        Next
    End Sub
End Class
