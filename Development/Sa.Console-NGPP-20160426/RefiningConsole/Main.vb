﻿
Public Module Main
    Public CorrPath As String
    Public CompanyPath As String
    Public DrawingPath As String
    Public CompCorrPath As String
    Public ClientAttachmentsPath As String
    Public CorrPath2 As String
    Public CompanyPath2 As String
    Public DrawingPath2 As String
    Public CompCorrPath2 As String
    Public RefineryPath As String
    Public SiteDataPath As String
    Public PlantPath As String
    Public PolishCorrPath As String
    Public PolishCorrPath2 As String

    Public ProfilePath As String = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile).ToString() + "\"
    Public ProfileConsolePath As String = ProfilePath + "Console\"

    Private _mfilesPassword As String = String.Empty
    Public Property MfilesPassword() As String
        Get
            Return _mfilesPassword
        End Get
        Set(ByVal value As String)
            _mfilesPassword = value
        End Set
    End Property

    Private _vertical As VerticalType
    Public Property ConsoleVertical() As VerticalType
        Get
            Return _vertical
        End Get
        Set(ByVal value As VerticalType)
            _vertical = value
        End Set
    End Property

    Public Enum VerticalType
        REFINING
        OLEFINS
        POWER
        RAM
        PIPELINES
        NGPP
    End Enum

    Private _troubleCount As Integer
    Public Sub WP(Optional msg As String = "")
        Try
            If _troubleCount = 0 Then
                Kill("C:\temp\ConsoleTroubleshooting.txt")
            End If
            Dim sw As New StreamWriter("C:\temp\ConsoleTroubleshooting.txt", True)
            sw.WriteLine(DateTime.Now.ToShortTimeString() + "   " + _troubleCount.ToString() + "   " + msg)
            _troubleCount += 1
            sw.Close()
        Catch ex As System.Exception
            _troubleCount += 1
        End Try
    End Sub

End Module

