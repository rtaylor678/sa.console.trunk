﻿'This will be the new way to handle the settings.txt file

Imports System.IO
Imports System


Public Class RefnumsUsedHistory
    Implements IRefnumsUsedHistory

    Private _profilePath As String = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile).ToString() + "\"
    Private _profileConsolePath As String = _profilePath + "Console\"
    Private _profileConsoleTempPath As String = _profileConsolePath + "temp\"
    Private _profileConsoleLastRefnumPath As String = String.Empty
    Private _delim = "^"


    Public Sub New(vertical As VerticalType)
        Dim verticalName As String = [Enum].GetName(GetType(VerticalType), vertical)
        _profileConsoleLastRefnumPath = _profileConsoleTempPath & "ConsoleSettings\" & verticalName & "History.txt"
        If Not File.Exists(_profileConsoleLastRefnumPath) Then
            If Not MakeFirstFile(vertical) Then
                Throw New Exception("Problem creating new Settings file in class RefnumsUsedHistory")
            End If
        End If
    End Sub

    Private Function MakeFirstFile(vertical As VerticalType) As Boolean
        Dim writer As StreamWriter = Nothing
        Try
            writer = New StreamWriter(_profileConsoleLastRefnumPath)
            If vertical = VerticalType.OLEFINS Then
                For i As Integer = 9 To 15 Step 2
                    Dim yr As String = i.ToString
                    If yr.Length = 1 Then yr = "0" & yr
                    writer.WriteLine("PCH" & yr + _delim + "PCH003")
                Next
            End If
        Catch ex As Exception
            Return False
        Finally
            writer.Close()
        End Try
        Return True
    End Function

    Public Function GetLastRefnum(study As String) As String Implements IRefnumsUsedHistory.GetLastRefnum
        'expect study to be soemtihgn like PCH15
        Dim reader As StreamReader = Nothing
        Dim result As String = String.Empty
        Try
            reader = New StreamReader(_profileConsoleLastRefnumPath)
            While Not reader.EndOfStream
                Dim line As String = reader.ReadLine()
                Dim lineParts() As String = line.Split(_delim)
                If lineParts(0) = study Then
                    result = lineParts(1)
                    Exit While
                End If
            End While
        Catch ex As Exception
        Finally
            reader.Close()
        End Try
        Return result
    End Function

    Public Sub UpdateRefnum(study As String, newRefNum As String) Implements IRefnumsUsedHistory.UpdateRefnum
        'expect study to be soemtihgn like PCH15
        Dim reader As StreamReader = Nothing
        Dim lines As List(Of String) = New List(Of String)
        Try
            reader = New StreamReader(_profileConsoleLastRefnumPath)
            While Not reader.EndOfStream
                Dim line As String = reader.ReadLine()
                Dim lineParts() As String = line.Split(_delim)
                If lineParts(0) = study Then
                    lineParts(1) = newRefNum
                End If
                lines.Add(lineParts(0) + _delim + lineParts(1))
            End While
        Catch exRead As Exception
        Finally
            reader.Close()
        End Try

        Dim writer As StreamWriter = Nothing
        Try
            writer = New StreamWriter(_profileConsoleLastRefnumPath)
            For Each line As String In lines
                writer.WriteLine(line)
            Next
        Catch exWrite As Exception
        Finally
            writer.Close()
        End Try

    End Sub

End Class
