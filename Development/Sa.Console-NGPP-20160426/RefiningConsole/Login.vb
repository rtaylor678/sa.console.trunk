﻿Imports System.Data.SqlClient
Imports SA.Internal.Console.DataObject
Imports System.Configuration

Public Class Login
    Dim db As DataObject
    Dim RefNum As String
    Dim UserName As String
    Dim Password As String
    'Dim StudyType As String
    Dim StudyYear As String
    Dim Entered As Boolean = False
    Dim Console As Object
    Dim VPN As Boolean = False
    Dim ScreenName As String = ""


    Private Sub LoginClick()
        WP("1")
        If cboVertical.Text.Trim().Length < 1 Then
            MsgBox("Please select a Vertical and login again")
            Exit Sub
        End If
        WP("2")
        Try
            lblError.Visible = False
            Dim tpw As String = txtLogin.Text.ToUpper
            WP("3")
            If txtMfilesPassword.Text.Trim().Length < 1 Then
                txtMfilesPassword.Text = DevConfigHelper.ReadConfig("MFilesPw")
            End If
            WP("4")
            Select Case ConsoleVertical
                'Case  "REFININGDEV"
                '   db = New DataObject(DataObject.StudyTypes.REFININGDEV, txtLogin.Text, txtPassword.Text)
                '  Console = New MainConsole()
                Case VerticalType.REFINING
                    db = New DataObject(DataObject.StudyTypes.REFINING, txtLogin.Text, txtPassword.Text)
                    Console = New MainConsole()
                Case VerticalType.OLEFINS
                    db = New DataObject(DataObject.StudyTypes.OLEFINS, txtLogin.Text, txtPassword.Text)
                    Console = New OlefinsMainConsole()
                    If txtMfilesPassword.Text.Trim().Length < 1 Then
                        MsgBox("Please enter your Outlook password. This is needed in order to access MFiles.")
                        Exit Sub
                    End If
                    Main.MfilesPassword = txtMfilesPassword.Text.Trim()
                Case VerticalType.POWER
                    db = New DataObject(DataObject.StudyTypes.POWER, txtLogin.Text, txtPassword.Text)
                    Console = New PowerMainConsole()
                Case VerticalType.RAM
                    If CheckRAMPassword(txtLogin.Text, txtPassword.Text) Then
                        db = New DataObject(DataObject.StudyTypes.RAM, txtLogin.Text, txtPassword.Text)
                        UserName = ScreenName
                        Console = New RAMMainConsole()
                    Else
                        lblError.Text = "Invalid Login"
                        lblError.Visible = True
                        Exit Sub
                    End If
                Case VerticalType.PIPELINES
                    db = New DataObject(DataObject.StudyTypes.PIPELINES, txtLogin.Text, txtPassword.Text)
                    Console = New TPMainConsole()
                Case VerticalType.NGPP
                    WP("5")
                    db = New DataObject(DataObject.StudyTypes.NGPP, txtLogin.Text, txtPassword.Text)
                    WP("6")
                    Console = New NgppConsole(db)
                    WP("7")
            End Select

            If db.DBError IsNot Nothing Then
                MsgBox(db.DBError.ToString())
                lblError.Text = "Database Error"
                lblError.Visible = True
                Exit Sub
            End If
            WP("8")
            If Not IsNothing(RefNum) AndAlso RefNum <> "" Then
                Console.ReferenceNum = RefNum
                Console.Spawn = True
            End If
            WP("9")
            Console.Password = txtPassword.Text
            WP("10")
            Console.UserName = IIf(UserName Is Nothing, txtLogin.Text, UserName)
            WP("11")
            If ConsoleVertical = VerticalType.OLEFINS Then
                Console.UserWindowsProfileName = UserName ' = Environment.UserName + "." + Environment.UserDomainName
            End If
            WP("12")
            Console.CurrentStudyYear = My.Settings.Item("CurrentStudyYear")
            WP("13")
            Console.VPN = VPN
            WP("14")
            Console.Show()
            WP("15")
            Me.Hide()
            WP("16")
            Me.WindowState = FormWindowState.Minimized
            WP("17")
            If txtLogin.Text <> "DBB" And txtLogin.Text <> "JDW" And txtLogin.Text <> "EJB" Then Me.Close()
            WP("18")
        Catch ex As System.Exception
            MessageBox.Show(ex.Message)
            lblError.Text = ex.Message
            lblError.Visible = True
        End Try


    End Sub

    Private Sub btnLogin_Click(sender As System.Object, e As System.EventArgs) Handles btnLogin.Click
        If ConsoleVertical = VerticalType.OLEFINS Then
            If txtMfilesPassword.Text.Trim().Length < 1 Then
                MsgBox("Please enter your Outlook password, this is needed to access MFiles.")
                Exit Sub
            End If
        End If
        Entered = True
        Me.Cursor = Cursors.WaitCursor
        LoginClick()
        Me.Cursor = Cursors.Default
    End Sub

    Private Function CheckRAMPassword(RAMUserName As String, RAMPassword As String) As Boolean

        Dim strPassword As String = ""
        Dim CompanyID As String = "000SAI"
        Dim pw As String = ""

        Dim params As New List(Of String)
        params.add("UserID/" & RAMUserName)


        Dim ds As DataSet = db.ExecuteStoredProc("Console.GetRAMPassword", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                strPassword = ds.Tables(0).Rows(0)("Password")
                ScreenName = ds.Tables(0).Rows(0)("ScreenName")
                CompanyID = ds.Tables(0).Rows(0)("CompanyID")
            End If
            Dim salt As String = "dog" + ScreenName.ToLower + "butt" & CompanyID.Trim & "steelers"
            pw = Utilities.EncryptedPassword(txtPassword.Text, salt)
        End If

        If pw = strPassword Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub Login_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        'For Each z In cboVertical.Items
        '    If z.ToString().ToUpper.Trim() = StudyType Then
        '        cboVertical.SelectedItem = z
        '        Exit For
        '    End If
        'Next
    End Sub


    Private Sub Login_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Dim weekStart As DayOfWeek = DayOfWeek.Monday
        Dim startingDate As DateTime = DateTime.Today
        Dim endingDate As DateTime = DateTime.MinValue

        While startingDate.DayOfWeek <> weekStart
            startingDate = startingDate.AddDays(-1)
        End While
        endingDate = startingDate.AddDays(4)

        Dim iteration = "\" + startingDate.Year.ToString() + "\"
        iteration += startingDate.Month.ToString() + "-" + startingDate.Day.ToString() + " ~ "
        iteration += endingDate.Month.ToString() + "-" + endingDate.Day.ToString()


        For Each enumValue In System.Enum.GetValues(GetType(Main.VerticalType))
            cboVertical.Items.Add(enumValue.ToString())
        Next

        Try
            Me.Height = 263 '180
            If CBool(ConfigurationManager.AppSettings("DbTesting").ToString()) Then
                Me.Text = " **Testing **"
                'Me.Height = 260
            End If
        Catch
        End Try

        VPN = False

        'Dim s() As String = System.Environment.GetCommandLineArgs()
        'If s.Length > 1 Then
        '    If s(1).Length > 2 Then
        '        StudyType = s(1)
        '        StudyYear = s(2)
        '        If s.Length > 3 Then VPN = s(3)
        '    End If
        'Else
        '    StudyType = "REFINING"
        '    StudyYear = "14"
        'End If
        'Me.Text = Me.Text + " " + StudyType & " 20" & StudyYear

        Me.Show()
        If UserName = "" Then
            If ConsoleVertical = VerticalType.RAM Then ' StudyType = "RAM" Then
                txtLogin.Text = Environment.UserName & "@sa"
            Else
                txtLogin.Text = Environment.UserName
            End If
        End If

        If txtLogin.Text <> "" Then txtPassword.Focus()

        Me.Left = (Screen.PrimaryScreen.WorkingArea.Width - Me.Width) / 2
        Me.Top = (Screen.PrimaryScreen.WorkingArea.Height - Me.Height) / 2
        'Me.Height = 252

        ' If My.User.Name.ToUpper() = "DC1\SFB" Then
        ' cboVertical.SelectedIndex = 5
        ' LoginClick()
        ' End If


    End Sub

    Private Sub txtPassword_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress, txtLogin.KeyPress
        If e.KeyChar = Chr(13) Then LoginClick()
    End Sub

    Private Sub cboVertical_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboVertical.SelectedIndexChanged
        lblMfilesPassword.Visible = False
        txtMfilesPassword.Enabled = False
        txtMfilesPassword.Visible = False

        For count As Integer = 0 To System.Enum.GetValues(GetType(VerticalType)).Length - 1
            If cboVertical.Text.Trim().ToUpper() = System.Enum.GetName(GetType(VerticalType), count) Then
                Main.ConsoleVertical = count
                Exit For
            End If
        Next

        Select Case Main.ConsoleVertical
            Case VerticalType.REFINING
            Case VerticalType.OLEFINS
                lblMfilesPassword.Visible = True
                txtMfilesPassword.Enabled = True
                txtMfilesPassword.Visible = True
                ConsoleVertical = VerticalType.OLEFINS
            Case VerticalType.RAM
                txtLogin.Enabled = True
                txtPassword.Enabled = True
                ConsoleVertical = VerticalType.RAM
            Case VerticalType.POWER
            Case VerticalType.PIPELINES ' "PL && T"
            Case VerticalType.NGPP
            Case Else
        End Select
    End Sub

End Class

