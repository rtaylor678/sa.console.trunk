﻿Public Class DropdownForm

    Public Delegate Sub ReturnChoice(sender As String) 'System.Object)
    Public choice As ReturnChoice

    Private _choice As String = String.Empty

    Public Function btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        Me.Close()
        choice(_choice)
        'NgppConsole.SaveDropdownChoice(_choice)
        'Return _choice
    End Function

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub


    Public Sub New(labelText As String, dropDownList As List(Of String))

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Label1.Text = labelText
        For Each valu As String In dropDownList
            ComboBox1.Items.Add(valu)
        Next

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        _choice = ComboBox1.Text
    End Sub
End Class