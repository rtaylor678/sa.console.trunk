﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddIssue
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnAddIssue = New System.Windows.Forms.Button()
        Me.txtIssueTitle = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.txtIssueText = New System.Windows.Forms.TextBox()
        Me.txtIssueID = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnAddIssue
        '
        Me.btnAddIssue.Location = New System.Drawing.Point(153, 258)
        Me.btnAddIssue.Name = "btnAddIssue"
        Me.btnAddIssue.Size = New System.Drawing.Size(102, 23)
        Me.btnAddIssue.TabIndex = 3
        Me.btnAddIssue.Text = "Update"
        Me.btnAddIssue.UseVisualStyleBackColor = True
        '
        'txtIssueTitle
        '
        Me.txtIssueTitle.Location = New System.Drawing.Point(25, 69)
        Me.txtIssueTitle.Name = "txtIssueTitle"
        Me.txtIssueTitle.Size = New System.Drawing.Size(352, 20)
        Me.txtIssueTitle.TabIndex = 1
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(25, 53)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(55, 13)
        Me.Label49.TabIndex = 28
        Me.Label49.Text = "Issue Title"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(25, 94)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(60, 13)
        Me.Label47.TabIndex = 27
        Me.Label47.Text = "Description"
        '
        'txtIssueText
        '
        Me.txtIssueText.Location = New System.Drawing.Point(25, 113)
        Me.txtIssueText.Multiline = True
        Me.txtIssueText.Name = "txtIssueText"
        Me.txtIssueText.Size = New System.Drawing.Size(352, 132)
        Me.txtIssueText.TabIndex = 2
        '
        'txtIssueID
        '
        Me.txtIssueID.Location = New System.Drawing.Point(25, 26)
        Me.txtIssueID.Name = "txtIssueID"
        Me.txtIssueID.Size = New System.Drawing.Size(94, 20)
        Me.txtIssueID.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(25, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 31
        Me.Label1.Text = "Issue ID"
        '
        'AddIssue
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(407, 299)
        Me.Controls.Add(Me.txtIssueID)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnAddIssue)
        Me.Controls.Add(Me.txtIssueTitle)
        Me.Controls.Add(Me.Label49)
        Me.Controls.Add(Me.Label47)
        Me.Controls.Add(Me.txtIssueText)
        Me.Name = "AddIssue"
        Me.Text = "Add New Issue"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnAddIssue As System.Windows.Forms.Button
    Friend WithEvents txtIssueTitle As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents txtIssueText As System.Windows.Forms.TextBox
    Friend WithEvents txtIssueID As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
