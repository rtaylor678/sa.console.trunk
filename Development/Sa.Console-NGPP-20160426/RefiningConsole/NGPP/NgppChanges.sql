﻿USE [NGPP14]
GO

/****** Object:  StoredProcedure [Console].[GetConsultants]    Script Date: 4/25/2016 11:29:05 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[TSort] ADD "Consultant" VARCHAR(5) NULL
GO
alter table tsort add CompanyPassword char(25)
GO

CREATE SCHEMA [Console]
Go

CREATE TABLE [Console].[Roles](
	[Initials] [nvarchar](3) NULL,
	[Role] [nvarchar](20) NULL
) ON [PRIMARY]

GO

INSERT INTO [Console].[Roles] (Initials,Role) Values ('SFB','ADMIN');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('SFB','CheckV2');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('SFB','CONSULTANT');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('SFB','DB');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('SFB','DEV');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('SFB','FL');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('SFB','FLMacrosDev');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('SFB','MissingPNs');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('SFB','PrelimPricing');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('SFB','QA');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('SFB','UnlockPN');
go

INSERT INTO [Console].[Roles] (Initials,Role) Values ('DEJ','ADMIN');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('DEJ','CheckV2');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('DEJ','CONSULTANT');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('DEJ','DB');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('DEJ','DEV');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('DEJ','FL');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('DEJ','FLMacrosDev');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('DEJ','MissingPNs');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('DEJ','PrelimPricing');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('DEJ','QA');
INSERT INTO [Console].[Roles] (Initials,Role) Values ('DEJ','UnlockPN');
go

create procedure [Console].[GetRoles]
	@UserID as nvarchar(3)
as
	SELECT Role from Console.Roles where Initials = @UserID
GO

CREATE PROCEDURE [Console].[GetRefNumsByConsultant]
@Consultant nvarchar(3),
@Study varchar(3),
@StudyYear nvarchar(4)

AS
BEGIN
	SELECT DISTINCT RefID as Refnum, Coloc FROM dbo.TSort WHERE Consultant=@Consultant /*AND  Study = @Study*/ and StudyYear = @StudyYear  and refnum not like '%xA%'
END
GO

CREATE PROCEDURE [Console].[GetRefNums]
	@StudyYear dbo.StudyYear,
	@Study varchar(3)
AS
BEGIN
	SELECT DISTINCT RefID, CoLoc, Consultant, RefNum FROM dbo.TSort
	WHERE StudyYear = @StudyYear AND Study = @Study and refnum not like '%xA%'
	ORDER BY Refnum ASC
END
GO

CREATE procedure [Console].[GetMainConsultant]
	@StudyYear dbo.StudyYear,
	@Study dbo.Study,
	@RefNum dbo.Refnum
as
select consultant from Tsort where Refnum = @RefNum and Study = @Study and StudyYear = @StudyYear
GO

CREATE TABLE [dbo].[MessageLog](
	[Refnum] [dbo].[Refnum] NULL,
	[Scenario] [dbo].[Scenario] NULL,
	[Source] [varchar](12) NULL,
	[Severity] [char](1) NOT NULL,
	[MessageID] [int] NOT NULL,
	[MessageText] [varchar](255) NULL,
	[SysAdmin] [bit] NOT NULL,
	[Consultant] [bit] NOT NULL,
	[Pricing] [bit] NOT NULL,
	[Audience4] [bit] NOT NULL,
	[Audience5] [bit] NOT NULL,
	[Audience6] [bit] NOT NULL,
	[Audience7] [bit] NOT NULL,
	[MessageTime] [datetime] NULL CONSTRAINT [DF_MessageLog_MessageTime1__22]  DEFAULT (getdate())
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[UploadQueue](
	[Refnum] [dbo].[Refnum] NOT NULL,
	[FileName] [varchar](max) NOT NULL,
	[Consultant] [varchar](5) NULL,
	[SubmitDate] [datetime] NOT NULL,
	[LastAttempt] [datetime] NULL,
	[NumErrors] [smallint] NULL,
 CONSTRAINT [PK_UploadQueue] PRIMARY KEY CLUSTERED 
(
	[Refnum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE FUNCTION [Console].[GetUploadStatus](@Refnum dbo.Refnum)
RETURNS varchar(255)
AS
BEGIN
	DECLARE @str varchar(255)
	IF EXISTS (SELECT * FROM UploadQueue WHERE Refnum = @Refnum AND LastAttempt IS NULL)
		SELECT @str = 'Requested on ' + CAST(SubmitDate as varchar(50)) FROM UploadQueue WHERE Refnum = @Refnum
	ELSE IF EXISTS (SELECT * FROM UploadQueue WHERE Refnum = @Refnum AND LastAttempt IS NOT NULL)
		SELECT @str = 'Failed on ' + CAST(LastAttempt as varchar(50)) FROM UploadQueue WHERE Refnum = @Refnum 
	ELSE IF EXISTS (SELECT * FROM RefStatus WHERE Refnum = @Refnum AND Upload IS NOT NULL)
		SELECT @str = 'Successful on ' + CAST(Upload as varchar(50)) FROM RefStatus WHERE Refnum = @Refnum
	ELSE
		SELECT @str = ''
	RETURN @str
END
GO

CREATE PROCEDURE [Console].[GetMessageLog]
    @MessageType int,
    @RefNum dbo.Refnum
AS
BEGIN
IF @MessageType = 53
	BEGIN
		SELECT [Console].[GetUploadStatus](@Refnum)
	END
ELSE
	BEGIN
	select messagetime  from MessageLog
		WHERE Refnum = @RefNum
		AND MessageID = @MessageType 
	END
END
GO

CREATE TABLE [dbo].[MiscInput](
	[Refnum] [dbo].[Refnum] NOT NULL,
	[UnitsExcl] [dbo].[YorN] NULL,
	[UtlyAlloc] [dbo].[YorN] NULL,
	[OffsitesAlloc] [dbo].[YorN] NULL,
	[Napthenic] [dbo].[YorN] NULL,
	[DiagramIncluded] [dbo].[YorN] NULL,
	[SpecFracIncluded] [dbo].[YorN] NULL,
	[CDUChargeBbl] [float] NULL,
	[CDUChargeMT] [float] NULL,
	[RPFRXVSulf] [real] NULL,
	[RPFRXVCS] [real] NULL,
	[RPFRXVTemp] [real] NULL,
	[NapCurrRoutCostLocal] [real] NULL,
	[NapCurrRoutCostUS] [real] NULL,
	[NapCurrRoutMatlLocal] [real] NULL,
	[NapCurrRoutMatlUS] [real] NULL,
	[NapPrevRoutCostLocal] [real] NULL,
	[NapPrevRoutCostUS] [real] NULL,
	[NapPrevRoutMatlLocal] [real] NULL,
	[NapPrevRoutMatlUS] [real] NULL,
	[VACOCCOperHrs] [real] NULL,
	[VACOCCMaintHrs] [real] NULL,
	[VACOCCTechHrs] [real] NULL,
	[VACOCCAdminHrs] [real] NULL,
	[VACMPSOperHrs] [real] NULL,
	[VACMPSMaintHrs] [real] NULL,
	[VACMPSTechHrs] [real] NULL,
	[VACMPSAdminHrs] [real] NULL,
	[VACThermEnergyMBTU] [float] NULL,
	[VACElecMWH] [float] NULL,
	[VACProdMBTU] [float] NULL,
	[VACTankEnergyMBTU] [float] NULL,
	[CRMRefMarine] [real] NULL,
	[CRMOthMarine] [real] NULL,
	[CRMLOOP] [real] NULL,
	[CRMBuoy] [real] NULL,
	[CRMPipeline] [real] NULL,
	[CRMTruck] [real] NULL,
	[PrcOpTimeOff] [char](1) NULL,
	[SiteAcres] [real] NULL,
	[SiteUnitsPcnt] [real] NULL,
	[SiteNonRefPcnt] [real] NULL,
	[SiteOffsitesPcnt] [real] NULL,
	[SiteUnusedPcnt] [real] NULL,
	[SiteTotPcnt] [real] NULL,
	[DCRedFlags] [int] NULL,
	[DCBlueFlags] [int] NULL,
	[OffsiteEnergyPcnt] [real] NULL,
	[EnergyMethod] [tinyint] NULL,
	[MiscEnergyPcnt] [real] NULL,
	[MaintSoftwareVendor] [varchar](20) NULL,
	[MaintSoftwareVendorOth] [nvarchar](50) NULL,
	[LimitingUnit] [nvarchar](50) NULL,
	[AirTemp] [real] NULL,
	[SulfurLT] [real] NULL,
	[InputFileVersion] [varchar](15) NULL,
	[M106PricePerBblLocal] [real] NULL,
	[M106PricePerBblUS] [real] NULL,
 CONSTRAINT [PK___6__15] PRIMARY KEY CLUSTERED 
(
	[Refnum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]
GO

 CREATE PROCEDURE [Console].[GetFlags]
	@RefNum dbo.Refnum
AS
BEGIN
Select DCRedFlags, DCBlueFlags from dbo.MiscInput Where Refnum = @RefNum 
END
GO

CREATE SCHEMA [Val]
Go

CREATE TABLE [Val].[Checklist](
	[Refnum] [char](9) NOT NULL,
	[IssueID] [char](8) NOT NULL,
	[IssueTitle] [char](50) NOT NULL,
	[IssueText] [text] NULL,
	[PostedBy] [char](3) NOT NULL,
	[PostedTime] [datetime] NOT NULL,
	[Completed] [char](1) NOT NULL,
	[SetBy] [char](3) NULL,
	[SetTime] [datetime] NULL,
 CONSTRAINT [PK_Checklist] PRIMARY KEY CLUSTERED 
(
	[Refnum] ASC,
	[IssueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE PROCEDURE [Console].[GetIssueCounts]
	@RefNum dbo.Refnum,
	@Mode varchar(1)
AS
BEGIN
	SELECT COUNT(IssueID) as Issues FROM Val.CheckList 
	WHERE Refnum = dbo.FormatRefNum(@RefNum,0)
	AND (completed = @Mode or @Mode='A')
END
GO

CREATE TABLE [dbo].[CoContactInfo](
	[RefNum] [char] (9) NOT NULL,
--	[SANumber] [int] NOT NULL,
	[ContactType] [char](5) NOT NULL,
	[FirstName] [varchar](25) NULL,
	[LastName] [varchar](25) NULL,
	[JobTitle] [varchar](75) NULL,
	[PersonalTitle] [varchar](5) NULL,
	[Phone] [varchar](40) NULL,
	[PhoneSecondary] [varchar](40) NULL,
	[Fax] [varchar](40) NULL,
	[Email] [varchar](255) NULL,
	[StrAddr1] [varchar](75) NULL,
	[StrAddr2] [varchar](50) NULL,
	[StrAdd3] [varchar](50) NULL,
	[StrCity] [varchar](30) NULL,
	[StrState] [varchar](30) NULL,
	[StrZip] [varchar](30) NULL,
	[StrCountry] [varchar](30) NULL,
	[MailAddr1] [varchar](75) NULL,
	[MailAddr2] [varchar](50) NULL,
	[MailAddr3] [varchar](50) NULL,
	[MailCity] [varchar](30) NULL,
	[MailState] [varchar](30) NULL,
	[MailZip] [varchar](30) NULL,
	[MailCountry] [varchar](30) NULL,
	[AltFirstName] [varchar](20) NULL,
	[AltLastName] [varchar](25) NULL,
	[AltEmail] [varchar](255) NULL,
	[CCAlt] [dbo].[YorN] NULL CONSTRAINT [DF_CoContactInfo_CCAlt]  DEFAULT (1),
	[SendMethod] [char](3) NULL,
	[Comment] [varchar](255) NULL
 )
GO

CREATE TABLE [dbo].[SAMaster](
	[SANumber] [int] IDENTITY(1,1) NOT NULL,
	[StudyAndYear] [char](5) NOT NULL,
	[Company] [char](50) NOT NULL,
	[Password] [char](25) NULL,
	[FileName] [char](50) NULL,
 CONSTRAINT [PK_SAMaster] PRIMARY KEY CLUSTERED 
(
	[StudyAndYear] ASC,
	[Company] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[CoRef](
	[SANumber] [int] NOT NULL,
	[Refnum] [char](10) NULL,
	[Location] [char](40) NULL
) ON [PRIMARY]

GO

Alter Table dbo.TSort add Comments Varchar(255)
go

CREATE PROCEDURE [Console].[GetCompanyContactInfo] 
	@RefNum dbo.Refnum = null,
	@ContactType nvarchar(10),
	@StudyYear int
AS
BEGIN
			SELECT FirstName,
			LastName,
			ContactType,
			cc.Email,
			cc.Phone,
			cc.Fax ,
			cc.MailAddr1 ,
			cc.MailAddr2 ,
			cc.MailAddr3 ,
			MailCity ,
			MailState ,
			MailZip ,
			MailCountry ,
			StrAddr1 ,
			StrAddr2 ,
			StrAdd3 ,
			StrCity ,
			StrState ,
			StrZip,
			StrCountry ,
			JobTitle,
			cc.SendMethod ,
			t.CompanyPassword ,
			cc.SendMethod as CompanySendMethod,
			--t.Comments as Comment,
			--s.SANumber as SANumber,
			cc.AltFirstName ,
			cc.AltLastName ,
			cc.AltEmail 
		 FROM CoContactInfo cc 
		 --join SAMaster s on cc.SANumber = s.SANumber
			--join CoRef cr on cr.SANumber = s.SANumber
			--join TSort t on cr.Refnum=t.RefID
			join TSort t on cc.RefNum=t.RefNum
			WHERE cc.RefNum=@RefNum
			and UPPER(ContactType)=@ContactType
		END
GO

CREATE FUNCTION [dbo].[GetSANumber](@Refnum dbo.Refnum)
RETURNS int
AS
BEGIN
	DECLARE @SANumber int
	
	SELECT @SANumber = SANumber
	FROM dbo.CoRef WHERE Refnum = dbo.FormatRefnum(@Refnum, 0)
		
	RETURN @SANumber
END
GO

CREATE PROCEDURE [Console].[GetCompanyPassword](@RefNum dbo.Refnum)
AS
BEGIN
	Select COmpanyPassword from dbo.Tsort where Refnum = @RefNum
END
GO


CREATE PROCEDURE [Console].[GetInterimContactInfo]
		@RefNum dbo.Refnum = null
AS
BEGIN
	IF @RefNum is not null -- AND @LubRefNum is null
	BEGIN
		IF SUBSTRING(@RefNum,LEN(@RefNum),1)='A'
		BEGIN
			SET @RefNum = SUBSTRING(@RefNum,1,LEN(@RefNum)-1)
		END
			
	SELECT cc.LastName, cc.FirstName, cc.Email, cc.Phone FROM CoContactInfo cc 
			--join SAMaster s on cc.SANumber = s.SANumber
			--join CoRef cr on cr.SANumber = s.SANumber
			--join TSort t on cr.Refnum=t.RefID
			join TSort t on cc.RefNum=t.RefNum
			WHERE cc.Refnum=@RefNum and ContactType='Inter'
	END
END
GO

ALTER TABLE TSort ADD PCC VARCHAR(10)
GO

create procedure [Console].[GetPrimaryCoordinator]
@refnum dbo.refnum
AS
Select PCC from TSort where RefNum = @refnum
GO

CREATE PROCEDURE [Console].[GetCompanyName] 
	@RefNum dbo.Refnum
AS
BEGIN
	SELECT CoLoc FROM dbo.TSort WHERE RefNum = @RefNum
END
GO

CREATE FUNCTION [dbo].[FormatRefNum] 
(
	@RefNum dbo.Refnum,
	@Mode int -- 0=Return Full RefNum, 1=Return RefNum Cycle, 2=Return Study, 3=Return Year
)
RETURNS varchar(12)
AS
BEGIN
	DECLARE @FirstPart varchar(4)
	DECLARE @SecondPart varchar(3)
	DECLARE @ThirdPart varchar(4)
	DECLARE @Part2Start tinyint, @Part2Size tinyint
	DECLARE @result varchar(12)
	SELECT @SecondPart = Study, @Part2Size = StudyCodeSize, @Part2Start = CHARINDEX(StudyCode, @Refnum, 1)
	FROM Console.RefnumStudyCodes
	WHERE CHARINDEX(StudyCode, @Refnum, 1) > 0
	SET @result = 'INVALID MODE'
	IF @Part2Start > 0
	BEGIN
		SELECT @FirstPart = LEFT(@Refnum, @Part2Start-1), @ThirdPart = SUBSTRING(@Refnum, @Part2Start + @Part2Size, 4)
	
		IF @Mode=0
			SET @result =  @FirstPart + @SecondPart + @ThirdPart
			
		ELSE IF @Mode=1
			SET @result =  @FirstPart
		
		ELSE IF @Mode=2
			SET @result =  @SecondPart
			
		ELSE IF @Mode=3
			SET @result =  @ThirdPart
	END
	ELSE
		SET @result = 'INVALID REF'
	RETURN @result
END
GO


CREATE PROCEDURE [Console].[GetIssues]
      @RefNum dbo.Refnum,
      @Completed nvarchar(1)
AS
BEGIN
      SELECT IssueID, IssueTitle, IssueText, Completed FROM Val.CheckList 
      WHERE Refnum = dbo.FormatRefNum(@RefNum,0) 
END
GO

CREATE TABLE [Console].[ContinuingIssues](
	[IssueID] [int] IDENTITY(1,1) NOT NULL,
	[RefNum] [varchar](9) NOT NULL,
	[EntryDate] [datetime] NOT NULL CONSTRAINT [DF_ContinuingIssues_NoteDate]  DEFAULT (getdate()),
	[Consultant] [varchar](3) NULL,
	[Note] [nvarchar](max) NOT NULL,
	[Deleted] [bit] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [varchar](3) NULL,
 CONSTRAINT [PK_ContinuingIssues] PRIMARY KEY NONCLUSTERED 
(
	[IssueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE PROCEDURE [Console].[GetContinuingIssues2014]
	@RefNum nvarchar(20)
AS
BEGIN
	SELECT  IssueID, EntryDate,consultant,note,deleted,deleteddate,deletedby From [Console].[ContinuingIssues] Where RefNum = @refnum  and deleted = 0 order by EntryDate
END
GO

CREATE PROCEDURE [Console].[UpdateCoContact] 
	@RefNum dbo.Refnum,
	@FirstName varchar(25),
	@LastName varchar(25),
	@JobTitle varchar(75),
	@Address1 varchar(75),
	@Address2 varchar(50),
	@Address3 nvarchar(50),
	@City nvarchar(30),
	@State nvarchar(30),
	@Zip nvarchar(30),
	@Country nvarchar(30),
	@Phone nvarchar(40),
	@Email nvarchar(255),
	@StAddress1 as nvarchar(75),
	@StAddress2 as nvarchar(50),
	@StAddress3 as nvarchar(50),
	@StAddress4 as nvarchar(30),
	@StAddress5 as nvarchar(30),
	@StAddress6 as nvarchar(30),
	@StAddress7 as nvarchar(30),
	@Fax as nvarchar(40),
	@ContactType as nvarchar(10),
	@Password as nvarchar(50)
AS
BEGIN
IF EXISTS(SELECT *
		 FROM CoContactInfo
		 /* cc join SAMaster s on cc.SANumber = s.SANumber
			join CoRef cr on cr.SANumber = s.SANumber
			join TSort t on cr.Refnum=t.RefID
		*/
			WHERE RefNum=@RefNum and  
			UPPER(ContactType)=@ContactType)
UPDATE C 
SET
		c.FirstName=@FirstName ,
		c.LastName=@LastName ,
		c.JobTitle = @JobTitle,
		c.Email =@Email,
		c.Phone = @Phone,
		c.Fax = @Fax ,
		c.MailAddr1 = @Address1 ,
		c.MailAddr2 = @Address2 ,
		c.MailAddr3 = @Address3 ,
		c.MailCity = @City  ,
		c.MailState = @State ,
		c.MailZip = @Zip ,
		c.MailCountry = @Country ,
		c.StrAddr1 = @StAddress1,
		c.StrAddr2 = @StAddress2,
		c.StrAdd3 = @StAddress3,
		c.StrCity = @StAddress4,
		c.StrState = @StAddress5,
		c.StrZip = @StAddress6,
		c.StrCountry = @StAddress7,
		c.contactType = @ContactType
	FROM CoContactInfo c 
	/*join SAMaster s on c.SANumber = s.SANumber
			join CoRef cr on cr.SANumber = s.SANumber
			WHERE cr.Refnum=@RefNum and 
	*/
	WHERE UPPER(ContactType)=@ContactType			
ELSE
	/*
	DECLARE @SANumber int

		SELECT @SaNumber = s.SaNumber from SAMaster s join CoRef cr on cr.SANumber = s.SANumber
		WHERE cr.Refnum=@RefNum 
	*/
		INSERT INTO CoContactInfo (RefNum,/* SANumber,*/ ContactType,  FirstName, LastName,
				 JobTitle, Phone,  Fax, Email, StrAddr1, StrAddr2, StrAdd3,StrCity,StrState,StrZip,
				 StrCountry,MailAddr1, MailAddr2, MailAddr3, MailCity, MailState, MailZip, MailCountry,CCAlt)
				 Values(
						@RefNum,					
						--@SANumber,
						@ContactType,
						@FirstName ,
						@LastName ,
						@JobTitle,
						@Phone,
						@Fax,
						@Email,
						@StAddress1,
						@StAddress2,
						@StAddress3,
						@StAddress4,
						@StAddress5,
						@StAddress6,
						@STAddress7,
						@Address1 ,
						@Address2 ,
						@Address3 ,
						@City  ,
						@State ,
						@Zip ,
						@Country,
						'N')
END
GO


CREATE PROCEDURE [Console].[UpdateInterimContactInfo]
		@RefNum dbo.Refnum = null,
		@FirstName nvarchar(80),
		@LastName nvarchar(80),
		@Email nvarchar(80),
		@Phone nvarchar(80)
AS
--DECLARE @SAMaster int
BEGIN
		IF SUBSTRING(@RefNum,LEN(@RefNum),1)='A'
		BEGIN
			SET @RefNum = SUBSTRING(@RefNum,1,LEN(@RefNum)-1)
		END			
IF EXISTS(SELECT * FROM CoContactInfo cc
			-- join SAMaster s on cc.SANumber = s.SANumber
			--join CoRef cr on cr.SANumber = s.SANumber
			--join ContactInfo c on c.Refnum = cr.Refnum
			--WHERE cr.Refnum=@RefNum and ContactType='Inter')
			WHERE RefNum=@RefNum and ContactType='Inter')
BEGIN  
	UPDATE cc
	SET cc.FirstName = @FirstName,
		cc.LastName = @LastName,
		cc.Email = @Email,
		cc.Phone = @Phone
		FROM CoContactInfo cc 
			--join SAMaster s on cc.SANumber = s.SANumber
			--join CoRef cr on cr.SANumber = s.SANumber
			--join ContactInfo c on c.Refnum = cr.Refnum
			--WHERE cr.Refnum=@RefNum and ContactType='Inter'
			WHERE RefNum=@RefNum and ContactType='Inter'
END
ELSE
BEGIN
--SELECT @SAMaster = SANumber from CoRef where Refnum=@RefNum
--INSERT INTO CoContactInfo (SANUmber, ContactType, FirstName, LastName, Email, Phone) VALUES(@SAMaster, 'Inter',@FirstName, @LastName,@Email, @Phone)
INSERT INTO CoContactInfo (RefNum, ContactType, FirstName, LastName, Email, Phone) VALUES(@RefNum, 'Inter',@FirstName, @LastName,@Email, @Phone)
END
END
GO

CREATE TABLE [dbo].[ContactInfo](
	[Refnum] [dbo].[Refnum] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Title] [nvarchar](50) NULL,
	[Phone] [char](40) NULL,
	[Fax] [char](40) NULL,
	[EMail] [varchar](255) NULL,
	[Name2] [nvarchar](50) NULL,
	[Title2] [nvarchar](50) NULL,
	[Phone2] [char](40) NULL,
	[Fax2] [char](40) NULL,
	[EMail2] [varchar](255) NULL,
	[StreetAddr1] [nvarchar](50) NULL,
	[StreetAddr2] [nvarchar](50) NULL,
	[StreetAddr3] [nvarchar](50) NULL,
	[StreetAddr4] [nvarchar](50) NULL,
	[StreetAddr5] [nvarchar](50) NULL,
	[StreetAddr6] [nvarchar](50) NULL,
	[MailAddr1] [nvarchar](50) NULL,
	[MailAddr2] [nvarchar](50) NULL,
	[MailAddr3] [nvarchar](50) NULL,
	[MailAddr4] [nvarchar](50) NULL,
	[MailAddr5] [nvarchar](50) NULL,
	[MailAddr6] [nvarchar](50) NULL,
	[GenPhone] [varchar](20) NULL,
	[RefMgrName] [nvarchar](50) NULL,
	[RefMgrEMail] [varchar](255) NULL,
	[OpsMgrName] [nvarchar](50) NULL,
	[OpsMgrEMail] [varchar](255) NULL,
	[MaintMgrName] [nvarchar](50) NULL,
	[MaintMgrEMail] [varchar](255) NULL,
	[TechMgrName] [nvarchar](50) NULL,
	[TechMgrEMail] [varchar](255) NULL,
	[PricingName] [nvarchar](50) NULL,
	[PricingTitle] [nvarchar](50) NULL,
	[PricingEMail] [varchar](255) NULL,
	[PricingPhone] [char](40) NULL,
	[PricingFax] [char](40) NULL,
 CONSTRAINT [PK___1__13] PRIMARY KEY CLUSTERED 
(
	[Refnum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE PROCEDURE [Console].[GetContactInfo]
	@RefNum dbo.Refnum
	--@LubRefNum dbo.Refnum = null
AS
BEGIN
	IF @RefNum is not null --AND @LubRefNum is null
	BEGIN
		IF SUBSTRING(@RefNum,LEN(@RefNum),1)='A'
		BEGIN
			IF NOT EXISTS(SELECT RefNum from TSort WHERE RefNum = @RefNum)
			BEGIN
				SET @RefNum = SUBSTRING(@RefNum,1,LEN(@RefNum)-1)
			END
		END	
	SELECT	Name as CoordName,
			Email as CoordEmail,
			Phone as CoordPhone,
			MaintMgrName, 
			MaintMgrEmail, 
			OpsMgrName, 
			OpsMgrEmail, 
			TechMgrName, 
			TechMgrEmail,  
			RefMgrName, 
			RefMgrEmail, 
			GenPhone as RefGenPhone, 
			Name as RefName, 
			Phone as RefPhone, 
			Fax as RefFax, 
			Email as RefEmail, 
			Phone2 as RefPhone2,
			Name2 as RefName2, 
			Email2 as RefEmail2, 
			Fax2 as RefFax2, 
			Title as RefTitle, 
			Title2 as RefTitle2,
			MailAddr1,
			MailAddr2,
			MailAddr3,
			MailAddr4,
			MailAddr5,
			MailAddr6,
			StreetAddr1,
			StreetAddr2,
			StreetAddr3,
			StreetAddr4,
			StreetAddr5,
			StreetAddr6,
			t.Country,
			--t.location + ', ' + t.[state] as Location
			t.[state] as Location
			 
	FROM [ContactInfo] c join TSort t on c.Refnum=t.Refnum  WHERE --t.RefineryID = @RefNum 
	t.RefNum =@RefNum -- dbo.FormatRefNum(@RefNum, 0)
END
END
GO
insert into dbo.ContactInfo
([Refnum]
      ,[Name]
      ,[Title]
      ,[Phone]
      ,[Fax]
      ,[EMail]
) values (
'777NGP14',
'Ref Cord',
'Refinery Coordinator',
'123-456-7890',
NULL,
'RefCord@gmail.com'
)
GO

update tsort set PCC = 'PCC Test' where Refnum = '777NGP14'
GO

CREATE TABLE [Console].[ValidationNotes](
	[NoteID] [int] IDENTITY(1,1) NOT NULL,
	[Refnum] [dbo].[Refnum] NOT NULL,
	[EntryDate] [datetime] NOT NULL CONSTRAINT [DF_ValidationNotes_NoteDate]  DEFAULT (getdate()),
	[Consultant] [varchar](3) NULL,
	[Note] [nvarchar](max) NOT NULL,
	[Deleted] [bit] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [varchar](3) NULL,
 CONSTRAINT [PK_ValidationNotes_1] PRIMARY KEY NONCLUSTERED 
(
	[NoteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE PROCEDURE [Console].[GetValidationNotes]
	@RefNum nvarchar(20)
AS
BEGIN
		--SET @Refnum = REPLACE(@Refnum,'LIV','LUB')
		--SET @Refnum = REPLACE(@Refnum,'LEV','LUB')
	SELECT  NoteID, EntryDate,consultant,note,deleted,deleteddate,deletedby From [Console].[ValidationNotes] Where Refnum = @Refnum and deleted = 0 order by EntryDate
END
GO

CREATE PROCEDURE [Console].[InsertValidationNotes]
	@RefNum dbo.Refnum,
	@ID nvarchar(20)=null,
	@Consultant nvarchar(3),
	@Issue nvarchar(max)
AS
BEGIN
	INSERT INTO [Console].[ValidationNotes] values(@RefNum,getdate(),@Consultant,@Issue,0,null,null)
END
GO

CREATE PROCEDURE [Console].[UpdateValidationNotes]
	@RefNum dbo.Refnum,
	@Issue nvarchar(max),
	@Consultant nvarchar(3)
AS
BEGIN
		--SET @RefNum = REPLACE(@Refnum,'LIV','LUB')
		--SET @RefNum = REPLACE(@Refnum,'LEV','LUB')
			IF EXISTS(SELECT * FROM [Console].[ValidationNotes] where RefNum = @RefNum)
				UPDATE [Console].[ValidationNotes] SET Deleted = 1, DeletedBy=@Consultant where refnum= @Refnum and Deleted = 0
			
			
				INSERT INTO [Console].[ValidationNotes] values(@RefNum,getdate(),@Consultant,@Issue,0,null,@Consultant)

		SELECT @@IDENTITY
END
GO

update tsort set CompanyPassword = 'nomolos' where Refnum = '777NGP14'
GO

CREATE PROCEDURE [Console].[GetTSortData]
	@RefNum dbo.Refnum
AS
BEGIN
SELECT * FROM TSort WHERE Refnum=@RefNum
END
GO

CREATE TABLE [Console].[Employees](
	[Initials] [nvarchar](3) NULL,
	[ConsultantName] [nvarchar](80) NULL,
	[StudyYear] [nvarchar](4) NULL,
	[StudyType] [nvarchar](10) NULL,
	[Active] [bit] NULL,
	[Email] [nvarchar](80) NULL
) ON [PRIMARY]
GO

INSERT INTO 
[Console].[Employees]
([Initials]
      ,[ConsultantName]
      ,[StudyYear]
      ,[StudyType]
      ,[Active]
      ,[Email]
	  )
	  values
	  ('SFB', 'Stuart Bard','2014','NGPP',1,'Stuart.Bard')
GO

CREATE PROCEDURE [Console].[GetConsultant]
@Initials varchar(4),
@StudyYear varchar(4),
@StudyType varchar(10)
AS
BEGIN
Select * from Console.Employees 
Where Initials = @Initials
AND StudyYear= @StudyYear 
AND StudyType = @StudyType
AND Active = 1
END
GO

CREATE PROCEDURE [Console].[GetContactEmails] 
	@RefNum dbo.Refnum
AS
		BEGIN
	SELECT 'Company' as [EmailType], FirstName + ' ' + LastName as [Name], cc.Email,s.CompanyPassword  
	FROM dbo.CoContactInfo cc 
	--INNER JOIN dbo.SAMaster s ON s.SANumber = cc.SANumber 
	--INNER JOIN dbo.CoRef c on c.SANumber = s.SANumber
	INNER JOIN dbo.Tsort s on s.Refnum=cc.RefNum
	--Where c.Refnum = dbo.FormatRefNum(@RefNum,0) and email is not null and ContactType = 'Coord'
	Where cc.Refnum = @RefNum and email is not null and ContactType = 'Coord'
	
	UNION
	
	SELECT 'Alt Company' as [EmailType], FirstName + ' ' + LastName as [Name], cc.Email,s.CompanyPassword
	FROM dbo.CoContactInfo cc 
	--INNER JOIN dbo.SAMaster s ON s.SANumber = cc.SANumber 
	--INNER JOIN dbo.CoRef c on c.SANumber = s.SANumber
	INNER JOIN dbo.Tsort s on s.Refnum=cc.RefNum
	--Where c.Refnum = dbo.FormatRefNum(@RefNum,0)  and ContactType = 'Alt'
	Where cc.Refnum = @RefNum  and ContactType = 'Alt'

	UNION
	
	SELECT 'Interim' as [EmailType], FirstName + ' ' + LastName as [Name], cc.Email,s.CompanyPassword
	FROM dbo.CoContactInfo cc 
	--INNER JOIN dbo.SAMaster s ON s.SANumber = cc.SANumber 
	--INNER JOIN dbo.CoRef c on c.SANumber = s.SANumber
	INNER JOIN dbo.Tsort s on s.Refnum=cc.RefNum
	--Where c.Refnum = dbo.FormatRefNum(@RefNum,0)  and ContactType='Inter'
	Where cc.Refnum = @RefNum  and ContactType='Inter'
	
	UNION
	SELECT 'Refinery' as [EmailType], Name, EMail, null 
	FROM ContactInfo WHERE Refnum = @RefNum and email is not null

	UNION
	
	SELECT 'Alt Refinery' as [EmailType], Name2, EMail2, null 
	FROM ContactInfo where Refnum = @refnum and email2 is not null
	
END
GO

CREATE PROCEDURE [Console].[GetMissingPNs]
@Study nvarchar(3),
@StudyYear nvarchar(4)
AS
select v.Refnum,coloc,note from TSort t 
join [Console].[ValidationNotes] v on t.RefNum = v.Refnum 
where Study = @Study 
and StudyYear=@StudyYear
and SUBSTRING(note,1,1) <>'~' 
ORDER BY v.refnum 
GO

CREATE TABLE [Console].[bugs](
	[RequestID] [int] IDENTITY(1,1) NOT NULL,
	[Study] [nchar](5) NULL,
	[Priority] [nchar](20) NULL,
	[Request] [nvarchar](max) NULL,
	[Status] [nchar](20) NULL,
	[Requestor] [nchar](3) NULL,
	[RequestDate] [datetime] NULL,
	[AssignedTo] [nchar](3) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE PROCEDURE [Console].[SubmitRequest]
@Study nchar(25), 
@Priority nchar(20), 
@Request nvarchar (max),
@Status nchar (20),
@Requestor nchar(3) 
AS

INSERT INTO Console.Bugs
(Study, Priority, Request,[Status],Requestor, RequestDate)
VALUES( @Study,@Priority, @Request, @Status, @Requestor, GETDATE())
SELECT SCOPE_IDENTITY()
GO

CREATE PROCEDURE [Console].[UpdateCompanyPassword]
(
	@RefNum dbo.Refnum,
	@NewPassword char(25)
)
AS
BEGIN
	UPDATE  dbo.Tsort 
	SET COmpanyPassword = @NewPassword
	where Refnum = @RefNum
END
GO

ALTER TABLE dbo.Tsort ADD ReturnFilePassword char(25) null
GO

insert into [Console].[Employees]
values
('DEJ','Don Jones',2015,'NGPP',1,'Don.Jones')
GO

update [dbo].[Tsort] set [ReturnFilePassword] = '^Q:2"PYRQC>+1_A' where refnum = '500NGP15';
update [dbo].[Tsort] set [ReturnFilePassword] = '^P:2"PYRQB>+1_A' where refnum = '501NGP15';
update [dbo].[Tsort] set [ReturnFilePassword] = '_Q:2"PYRPC>+1_A' where refnum = '510NGP15';
update [dbo].[Tsort] set [ReturnFilePassword] = '_P:2"PYRPB>+1_A' where refnum = '511NGP15';
update [dbo].[Tsort] set [ReturnFilePassword] = '_S:2"PYRPA>+1_A' where refnum = '512NGP15';
update [dbo].[Tsort] set [ReturnFilePassword] = '_R:2"PYRP@>+1_A' where refnum = '513NGP15';
update [dbo].[Tsort] set [ReturnFilePassword] = '_U:2"PYRPG>+1_A' where refnum = '514NGP15';
update [dbo].[Tsort] set [ReturnFilePassword] = '\Q:2"PYRSC>+1_A' where refnum = '520NGP15';
update [dbo].[Tsort] set [ReturnFilePassword] = ']Q:2"PYRRC>+1_A' where refnum = '530NGP15';
update [dbo].[Tsort] set [ReturnFilePassword] = ']P:2"PYRRB>+1_A' where refnum = '531NGP15';
update [dbo].[Tsort] set [ReturnFilePassword] = ']S:2"PYRRA>+1_A' where refnum = '532NGP15';
update [dbo].[Tsort] set [ReturnFilePassword] = 'ZQ:2"PYRUC>+1_A' where refnum = '540NGP15';
update [dbo].[Tsort] set [ReturnFilePassword] = 'ZP:2"PYRUB>+1_A' where refnum = '541NGP15';
update [dbo].[Tsort] set [ReturnFilePassword] = 'ZS:2"PYRUA>+1_A' where refnum = '542NGP15';
update [dbo].[Tsort] set [ReturnFilePassword] = 'ZR:2"PYRU@>+1_A' where refnum = '543NGP15';
update [dbo].[Tsort] set [ReturnFilePassword] = 'ZU:2"PYRUG>+1_A' where refnum = '544NGP15';
update [dbo].[Tsort] set [ReturnFilePassword] = 'YV:2"PYPVD>+1_A' where refnum = '777NGP15';

CREATE TABLE [dbo].[RefStatus](
	[Refnum] [dbo].[Refnum] NOT NULL,
	[Upload] [smalldatetime] NULL,
	[FullCalc] [smalldatetime] NULL,
	[PricingCalcs] [smalldatetime] NULL,
	[FactorCalcs] [smalldatetime] NULL,
	[Copy] [smalldatetime] NULL,
	[SumCalc] [smalldatetime] NULL,
	[ClientTable] [smalldatetime] NULL,
	[ReportCalc] [smalldatetime] NULL,
 CONSTRAINT [PK_RefStatus] PRIMARY KEY CLUSTERED 
(
	[Refnum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [Console].[RefnumStudyCodes](
	[StudyCode] [varchar](3) NOT NULL,
	[Study] [varchar](3) NOT NULL,
	[StudyCodeSize]  AS (len(rtrim([StudyCode]))),
 CONSTRAINT [PK_RefnumStudyCodes] PRIMARY KEY CLUSTERED 
(
	[StudyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE PROCEDURE [Console].[UpdateConsultant]

	@RefNum dbo.Refnum,
	@Consultant nvarchar(5)

AS
BEGIN
DECLARE @NewRefNum dbo.Refnum
BEGIN
UPDATE dbo.TSort SET Consultant = @Consultant Where Refnum = @RefNum
END
END
GO


CREATE TABLE [Console].[ConsoleLog](
	[ConsoleLogId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](20) NOT NULL,
	[LogType] [nvarchar](20) NOT NULL,
	[Message] [nvarchar](2000) NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[tsModified] [datetimeoffset](7) NOT NULL CONSTRAINT [DF_ConsoleLog_tsModified]  DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost] [nvarchar](168) NOT NULL CONSTRAINT [DF_ConsoleLog_tsModifiedHost]  DEFAULT (host_name()),
	[tsModifiedUser] [nvarchar](168) NOT NULL CONSTRAINT [DF_ConsoleLog_tsModifiedUser]  DEFAULT (suser_sname()),
	[tsModifiedApp] [nvarchar](168) NOT NULL CONSTRAINT [DF_ConsoleLog_tsModifiedApp]  DEFAULT (app_name()),
 CONSTRAINT [PK_ConsoleLog] PRIMARY KEY CLUSTERED 
(
	[ConsoleLogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [Console].[ConsoleLog]  WITH CHECK ADD  CONSTRAINT [CL_ConsoleLog_LogType] CHECK  (([LogType]<>''))
GO
ALTER TABLE [Console].[ConsoleLog] CHECK CONSTRAINT [CL_ConsoleLog_LogType]
GO
ALTER TABLE [Console].[ConsoleLog]  WITH CHECK ADD  CONSTRAINT [CL_ConsoleLog_Message] CHECK  (([Message]<>''))
GO
ALTER TABLE [Console].[ConsoleLog] CHECK CONSTRAINT [CL_ConsoleLog_Message]
GO
ALTER TABLE [Console].[ConsoleLog]  WITH CHECK ADD  CONSTRAINT [CL_ConsoleLog_UserName] CHECK  (([UserName]<>''))
GO
ALTER TABLE [Console].[ConsoleLog] CHECK CONSTRAINT [CL_ConsoleLog_UserName]
GO

CREATE PROC [Console].[WriteLog]
	@UserName nvarchar(20),
	@LogType nvarchar(20),
	@Message nvarchar(2000)	
AS
	INSERT INTO Console.ConsoleLog
	(UserName,LogType,Message,DateAdded)
	VALUES(@UserName,@LogType,@Message,GETDATE())
GO

insert into  Console.Employees values('DEJ','Don Jones',2014,'NGPP',1,'Don.Jones');
