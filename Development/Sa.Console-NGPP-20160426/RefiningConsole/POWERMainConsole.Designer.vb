﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PowerMainConsole
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PowerMainConsole))
        Me.cboStudy = New System.Windows.Forms.ComboBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.cboRefNum = New System.Windows.Forms.ComboBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lblWarning = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboConsultant = New System.Windows.Forms.ComboBox()
        Me.btnValidate = New System.Windows.Forms.Button()
        Me.lblInHolding = New System.Windows.Forms.Label()
        Me.btnHelp = New System.Windows.Forms.Button()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.tabSS = New System.Windows.Forms.TabPage()
        Me.btnDirRefresh = New System.Windows.Forms.Button()
        Me.btnSecureSend = New System.Windows.Forms.Button()
        Me.tvwCompCorr2 = New System.Windows.Forms.TreeView()
        Me.tvwCompCorr = New System.Windows.Forms.TreeView()
        Me.tvwCorrespondence2 = New System.Windows.Forms.TreeView()
        Me.tvwDrawings2 = New System.Windows.Forms.TreeView()
        Me.tvwClientAttachments2 = New System.Windows.Forms.TreeView()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cboDir2 = New System.Windows.Forms.ComboBox()
        Me.tvwClientAttachments = New System.Windows.Forms.TreeView()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboDir = New System.Windows.Forms.ComboBox()
        Me.tvwDrawings = New System.Windows.Forms.TreeView()
        Me.tvwCorrespondence = New System.Windows.Forms.TreeView()
        Me.tabCheckList = New System.Windows.Forms.TabPage()
        Me.tvIssues = New System.Windows.Forms.TreeView()
        Me.txtPostedOn = New System.Windows.Forms.TextBox()
        Me.txtPostedBy = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.btnCancelIssue = New System.Windows.Forms.Button()
        Me.btnUpdateIssue = New System.Windows.Forms.Button()
        Me.lblItemCount = New System.Windows.Forms.Label()
        Me.btnAddIssue = New System.Windows.Forms.Button()
        Me.cboCheckListView = New System.Windows.Forms.ComboBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.txtCompletedOn = New System.Windows.Forms.TextBox()
        Me.txtCompletedBy = New System.Windows.Forms.TextBox()
        Me.txtIssueName = New System.Windows.Forms.TextBox()
        Me.txtIssueID = New System.Windows.Forms.TextBox()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.ValCheckList = New System.Windows.Forms.CheckedListBox()
        Me.tabSummary = New System.Windows.Forms.TabPage()
        Me.lblPricingHub = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkLockPN = New System.Windows.Forms.CheckBox()
        Me.lblFileModify = New System.Windows.Forms.Label()
        Me.lblOU = New System.Windows.Forms.Label()
        Me.txtValidationIssues = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.lblLastCalcDate = New System.Windows.Forms.Label()
        Me.lblLastUpload = New System.Windows.Forms.Label()
        Me.lblValidationStatus = New System.Windows.Forms.Label()
        Me.lblLU = New System.Windows.Forms.Label()
        Me.lblLCD = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.ConsoleTabs = New System.Windows.Forms.TabControl()
        Me.tabPlant = New System.Windows.Forms.TabPage()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.CompanyContactEmail = New System.Windows.Forms.Label()
        Me.CompanyContactName = New System.Windows.Forms.Label()
        Me.btnCompany = New System.Windows.Forms.Button()
        Me.PlantCoordPhone = New System.Windows.Forms.Label()
        Me.PlantCoordEmail = New System.Windows.Forms.Label()
        Me.PlantCoordName = New System.Windows.Forms.Label()
        Me.btnPlantCoordinator = New System.Windows.Forms.Button()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.txtReturnPassword = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.tabSupport = New System.Windows.Forms.TabPage()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnPT = New System.Windows.Forms.Button()
        Me.btnCT = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnLaunch = New System.Windows.Forms.Button()
        Me.clbSummaryCalcFiles = New System.Windows.Forms.CheckedListBox()
        Me.txtVersion = New System.Windows.Forms.TextBox()
        Me.VersionToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ConsoleTimer = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.btnKillProcesses = New System.Windows.Forms.Button()
        Me.lblStatus = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSS.SuspendLayout()
        Me.tabCheckList.SuspendLayout()
        Me.tabSummary.SuspendLayout()
        Me.ConsoleTabs.SuspendLayout()
        Me.tabPlant.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.tabSupport.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cboStudy
        '
        Me.cboStudy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStudy.FormattingEnabled = True
        Me.cboStudy.Location = New System.Drawing.Point(254, 25)
        Me.cboStudy.Name = "cboStudy"
        Me.cboStudy.Size = New System.Drawing.Size(90, 21)
        Me.cboStudy.TabIndex = 1
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(183, 25)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(48, 17)
        Me.Label59.TabIndex = 21
        Me.Label59.Text = "Study:"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.Location = New System.Drawing.Point(183, 57)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(53, 17)
        Me.Label60.TabIndex = 23
        Me.Label60.Text = "Site ID:"
        '
        'cboRefNum
        '
        Me.cboRefNum.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboRefNum.FormattingEnabled = True
        Me.cboRefNum.Location = New System.Drawing.Point(254, 57)
        Me.cboRefNum.Name = "cboRefNum"
        Me.cboRefNum.Size = New System.Drawing.Size(228, 21)
        Me.cboRefNum.TabIndex = 22
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(27, 25)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(135, 96)
        Me.PictureBox1.TabIndex = 24
        Me.PictureBox1.TabStop = False
        '
        'lblWarning
        '
        Me.lblWarning.AutoSize = True
        Me.lblWarning.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWarning.ForeColor = System.Drawing.Color.Red
        Me.lblWarning.Location = New System.Drawing.Point(245, 9)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(108, 13)
        Me.lblWarning.TabIndex = 25
        Me.lblWarning.Text = "Not Current Study"
        Me.lblWarning.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(183, 90)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(70, 17)
        Me.Label8.TabIndex = 27
        Me.Label8.Text = "Assigned:"
        '
        'cboConsultant
        '
        Me.cboConsultant.FormattingEnabled = True
        Me.cboConsultant.Location = New System.Drawing.Point(255, 90)
        Me.cboConsultant.MaxLength = 3
        Me.cboConsultant.Name = "cboConsultant"
        Me.cboConsultant.Size = New System.Drawing.Size(90, 21)
        Me.cboConsultant.TabIndex = 26
        '
        'btnValidate
        '
        Me.btnValidate.Location = New System.Drawing.Point(498, 90)
        Me.btnValidate.Name = "btnValidate"
        Me.btnValidate.Size = New System.Drawing.Size(84, 23)
        Me.btnValidate.TabIndex = 29
        Me.btnValidate.Text = "Validate"
        Me.btnValidate.UseVisualStyleBackColor = True
        '
        'lblInHolding
        '
        Me.lblInHolding.AutoSize = True
        Me.lblInHolding.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInHolding.ForeColor = System.Drawing.Color.Red
        Me.lblInHolding.Location = New System.Drawing.Point(510, 116)
        Me.lblInHolding.Name = "lblInHolding"
        Me.lblInHolding.Size = New System.Drawing.Size(65, 13)
        Me.lblInHolding.TabIndex = 31
        Me.lblInHolding.Text = "In Holding"
        '
        'btnHelp
        '
        Me.btnHelp.Location = New System.Drawing.Point(733, 4)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(27, 23)
        Me.btnHelp.TabIndex = 32
        Me.btnHelp.Text = "&?"
        Me.btnHelp.UseVisualStyleBackColor = True
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'tabSS
        '
        Me.tabSS.Controls.Add(Me.btnDirRefresh)
        Me.tabSS.Controls.Add(Me.btnSecureSend)
        Me.tabSS.Controls.Add(Me.tvwCompCorr2)
        Me.tabSS.Controls.Add(Me.tvwCompCorr)
        Me.tabSS.Controls.Add(Me.tvwCorrespondence2)
        Me.tabSS.Controls.Add(Me.tvwDrawings2)
        Me.tabSS.Controls.Add(Me.tvwClientAttachments2)
        Me.tabSS.Controls.Add(Me.Label13)
        Me.tabSS.Controls.Add(Me.cboDir2)
        Me.tabSS.Controls.Add(Me.tvwClientAttachments)
        Me.tabSS.Controls.Add(Me.Label12)
        Me.tabSS.Controls.Add(Me.cboDir)
        Me.tabSS.Controls.Add(Me.tvwDrawings)
        Me.tabSS.Controls.Add(Me.tvwCorrespondence)
        Me.tabSS.Location = New System.Drawing.Point(4, 22)
        Me.tabSS.Name = "tabSS"
        Me.tabSS.Padding = New System.Windows.Forms.Padding(3)
        Me.tabSS.Size = New System.Drawing.Size(789, 420)
        Me.tabSS.TabIndex = 7
        Me.tabSS.Text = "Secure Send"
        Me.tabSS.UseVisualStyleBackColor = True
        '
        'btnDirRefresh
        '
        Me.btnDirRefresh.Location = New System.Drawing.Point(688, 386)
        Me.btnDirRefresh.Name = "btnDirRefresh"
        Me.btnDirRefresh.Size = New System.Drawing.Size(84, 23)
        Me.btnDirRefresh.TabIndex = 50
        Me.btnDirRefresh.Text = "Refresh"
        Me.btnDirRefresh.UseVisualStyleBackColor = True
        '
        'btnSecureSend
        '
        Me.btnSecureSend.Location = New System.Drawing.Point(598, 386)
        Me.btnSecureSend.Name = "btnSecureSend"
        Me.btnSecureSend.Size = New System.Drawing.Size(84, 23)
        Me.btnSecureSend.TabIndex = 49
        Me.btnSecureSend.Text = "Secure Send"
        Me.btnSecureSend.UseVisualStyleBackColor = True
        '
        'tvwCompCorr2
        '
        Me.tvwCompCorr2.AllowDrop = True
        Me.tvwCompCorr2.CheckBoxes = True
        Me.tvwCompCorr2.ImageIndex = 0
        Me.tvwCompCorr2.ImageList = Me.ImageList1
        Me.tvwCompCorr2.Location = New System.Drawing.Point(395, 34)
        Me.tvwCompCorr2.Name = "tvwCompCorr2"
        Me.tvwCompCorr2.SelectedImageIndex = 0
        Me.tvwCompCorr2.Size = New System.Drawing.Size(377, 346)
        Me.tvwCompCorr2.TabIndex = 48
        Me.tvwCompCorr2.Visible = False
        '
        'tvwCompCorr
        '
        Me.tvwCompCorr.AllowDrop = True
        Me.tvwCompCorr.CheckBoxes = True
        Me.tvwCompCorr.ImageIndex = 0
        Me.tvwCompCorr.ImageList = Me.ImageList1
        Me.tvwCompCorr.Location = New System.Drawing.Point(17, 34)
        Me.tvwCompCorr.Name = "tvwCompCorr"
        Me.tvwCompCorr.SelectedImageIndex = 0
        Me.tvwCompCorr.Size = New System.Drawing.Size(369, 346)
        Me.tvwCompCorr.TabIndex = 47
        Me.tvwCompCorr.Visible = False
        '
        'tvwCorrespondence2
        '
        Me.tvwCorrespondence2.AllowDrop = True
        Me.tvwCorrespondence2.CheckBoxes = True
        Me.tvwCorrespondence2.ImageIndex = 0
        Me.tvwCorrespondence2.ImageList = Me.ImageList1
        Me.tvwCorrespondence2.Location = New System.Drawing.Point(404, 35)
        Me.tvwCorrespondence2.Name = "tvwCorrespondence2"
        Me.tvwCorrespondence2.SelectedImageIndex = 0
        Me.tvwCorrespondence2.Size = New System.Drawing.Size(368, 345)
        Me.tvwCorrespondence2.TabIndex = 46
        '
        'tvwDrawings2
        '
        Me.tvwDrawings2.AllowDrop = True
        Me.tvwDrawings2.CheckBoxes = True
        Me.tvwDrawings2.ImageIndex = 0
        Me.tvwDrawings2.ImageList = Me.ImageList1
        Me.tvwDrawings2.Location = New System.Drawing.Point(404, 34)
        Me.tvwDrawings2.Name = "tvwDrawings2"
        Me.tvwDrawings2.SelectedImageIndex = 0
        Me.tvwDrawings2.Size = New System.Drawing.Size(368, 346)
        Me.tvwDrawings2.TabIndex = 45
        Me.tvwDrawings2.Visible = False
        '
        'tvwClientAttachments2
        '
        Me.tvwClientAttachments2.AllowDrop = True
        Me.tvwClientAttachments2.CheckBoxes = True
        Me.tvwClientAttachments2.ImageIndex = 0
        Me.tvwClientAttachments2.ImageList = Me.ImageList1
        Me.tvwClientAttachments2.Location = New System.Drawing.Point(395, 34)
        Me.tvwClientAttachments2.Name = "tvwClientAttachments2"
        Me.tvwClientAttachments2.SelectedImageIndex = 0
        Me.tvwClientAttachments2.Size = New System.Drawing.Size(377, 346)
        Me.tvwClientAttachments2.TabIndex = 43
        Me.tvwClientAttachments2.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(404, 9)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(69, 17)
        Me.Label13.TabIndex = 42
        Me.Label13.Text = "Directory:"
        '
        'cboDir2
        '
        Me.cboDir2.FormattingEnabled = True
        Me.cboDir2.Location = New System.Drawing.Point(479, 8)
        Me.cboDir2.Name = "cboDir2"
        Me.cboDir2.Size = New System.Drawing.Size(164, 21)
        Me.cboDir2.TabIndex = 41
        '
        'tvwClientAttachments
        '
        Me.tvwClientAttachments.AllowDrop = True
        Me.tvwClientAttachments.CheckBoxes = True
        Me.tvwClientAttachments.ImageIndex = 0
        Me.tvwClientAttachments.ImageList = Me.ImageList1
        Me.tvwClientAttachments.Location = New System.Drawing.Point(17, 35)
        Me.tvwClientAttachments.Name = "tvwClientAttachments"
        Me.tvwClientAttachments.SelectedImageIndex = 0
        Me.tvwClientAttachments.Size = New System.Drawing.Size(369, 345)
        Me.tvwClientAttachments.TabIndex = 40
        Me.tvwClientAttachments.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(14, 8)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(69, 17)
        Me.Label12.TabIndex = 39
        Me.Label12.Text = "Directory:"
        '
        'cboDir
        '
        Me.cboDir.FormattingEnabled = True
        Me.cboDir.Location = New System.Drawing.Point(89, 7)
        Me.cboDir.Name = "cboDir"
        Me.cboDir.Size = New System.Drawing.Size(164, 21)
        Me.cboDir.TabIndex = 38
        '
        'tvwDrawings
        '
        Me.tvwDrawings.AllowDrop = True
        Me.tvwDrawings.CheckBoxes = True
        Me.tvwDrawings.ImageIndex = 0
        Me.tvwDrawings.ImageList = Me.ImageList1
        Me.tvwDrawings.Location = New System.Drawing.Point(17, 34)
        Me.tvwDrawings.Name = "tvwDrawings"
        Me.tvwDrawings.SelectedImageIndex = 0
        Me.tvwDrawings.Size = New System.Drawing.Size(369, 346)
        Me.tvwDrawings.TabIndex = 36
        Me.tvwDrawings.Visible = False
        '
        'tvwCorrespondence
        '
        Me.tvwCorrespondence.AllowDrop = True
        Me.tvwCorrespondence.CheckBoxes = True
        Me.tvwCorrespondence.ImageIndex = 0
        Me.tvwCorrespondence.ImageList = Me.ImageList1
        Me.tvwCorrespondence.Location = New System.Drawing.Point(17, 35)
        Me.tvwCorrespondence.Name = "tvwCorrespondence"
        Me.tvwCorrespondence.SelectedImageIndex = 0
        Me.tvwCorrespondence.Size = New System.Drawing.Size(369, 345)
        Me.tvwCorrespondence.TabIndex = 0
        '
        'tabCheckList
        '
        Me.tabCheckList.Controls.Add(Me.tvIssues)
        Me.tabCheckList.Controls.Add(Me.txtPostedOn)
        Me.tabCheckList.Controls.Add(Me.txtPostedBy)
        Me.tabCheckList.Controls.Add(Me.Label21)
        Me.tabCheckList.Controls.Add(Me.Label22)
        Me.tabCheckList.Controls.Add(Me.btnCancelIssue)
        Me.tabCheckList.Controls.Add(Me.btnUpdateIssue)
        Me.tabCheckList.Controls.Add(Me.lblItemCount)
        Me.tabCheckList.Controls.Add(Me.btnAddIssue)
        Me.tabCheckList.Controls.Add(Me.cboCheckListView)
        Me.tabCheckList.Controls.Add(Me.Label52)
        Me.tabCheckList.Controls.Add(Me.txtCompletedOn)
        Me.tabCheckList.Controls.Add(Me.txtCompletedBy)
        Me.tabCheckList.Controls.Add(Me.txtIssueName)
        Me.tabCheckList.Controls.Add(Me.txtIssueID)
        Me.tabCheckList.Controls.Add(Me.txtDescription)
        Me.tabCheckList.Controls.Add(Me.Label51)
        Me.tabCheckList.Controls.Add(Me.Label50)
        Me.tabCheckList.Controls.Add(Me.Label49)
        Me.tabCheckList.Controls.Add(Me.Label48)
        Me.tabCheckList.Controls.Add(Me.Label47)
        Me.tabCheckList.Controls.Add(Me.ValCheckList)
        Me.tabCheckList.Location = New System.Drawing.Point(4, 22)
        Me.tabCheckList.Name = "tabCheckList"
        Me.tabCheckList.Padding = New System.Windows.Forms.Padding(3)
        Me.tabCheckList.Size = New System.Drawing.Size(789, 420)
        Me.tabCheckList.TabIndex = 3
        Me.tabCheckList.Text = "Checklist"
        Me.tabCheckList.UseVisualStyleBackColor = True
        '
        'tvIssues
        '
        Me.tvIssues.CheckBoxes = True
        Me.tvIssues.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tvIssues.Location = New System.Drawing.Point(3, 54)
        Me.tvIssues.Name = "tvIssues"
        Me.tvIssues.ShowLines = False
        Me.tvIssues.ShowPlusMinus = False
        Me.tvIssues.ShowRootLines = False
        Me.tvIssues.Size = New System.Drawing.Size(293, 365)
        Me.tvIssues.TabIndex = 33
        '
        'txtPostedOn
        '
        Me.txtPostedOn.Location = New System.Drawing.Point(678, 54)
        Me.txtPostedOn.Name = "txtPostedOn"
        Me.txtPostedOn.Size = New System.Drawing.Size(97, 20)
        Me.txtPostedOn.TabIndex = 30
        '
        'txtPostedBy
        '
        Me.txtPostedBy.Location = New System.Drawing.Point(596, 54)
        Me.txtPostedBy.Name = "txtPostedBy"
        Me.txtPostedBy.Size = New System.Drawing.Size(78, 20)
        Me.txtPostedBy.TabIndex = 29
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(677, 77)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(74, 13)
        Me.Label21.TabIndex = 30
        Me.Label21.Text = "Completed On"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(593, 77)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(72, 13)
        Me.Label22.TabIndex = 29
        Me.Label22.Text = "Completed By"
        '
        'btnCancelIssue
        '
        Me.btnCancelIssue.Location = New System.Drawing.Point(381, 6)
        Me.btnCancelIssue.Name = "btnCancelIssue"
        Me.btnCancelIssue.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelIssue.TabIndex = 32
        Me.btnCancelIssue.UseVisualStyleBackColor = True
        '
        'btnUpdateIssue
        '
        Me.btnUpdateIssue.Location = New System.Drawing.Point(301, 6)
        Me.btnUpdateIssue.Name = "btnUpdateIssue"
        Me.btnUpdateIssue.Size = New System.Drawing.Size(74, 23)
        Me.btnUpdateIssue.TabIndex = 26
        Me.btnUpdateIssue.Text = "Update"
        Me.btnUpdateIssue.UseVisualStyleBackColor = True
        '
        'lblItemCount
        '
        Me.lblItemCount.AutoSize = True
        Me.lblItemCount.Location = New System.Drawing.Point(6, 36)
        Me.lblItemCount.Name = "lblItemCount"
        Me.lblItemCount.Size = New System.Drawing.Size(63, 13)
        Me.lblItemCount.TabIndex = 26
        Me.lblItemCount.Text = "Issue Name"
        '
        'btnAddIssue
        '
        Me.btnAddIssue.Location = New System.Drawing.Point(194, 6)
        Me.btnAddIssue.Name = "btnAddIssue"
        Me.btnAddIssue.Size = New System.Drawing.Size(102, 23)
        Me.btnAddIssue.TabIndex = 25
        Me.btnAddIssue.Text = "Add New Issue"
        Me.btnAddIssue.UseVisualStyleBackColor = True
        '
        'cboCheckListView
        '
        Me.cboCheckListView.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCheckListView.FormattingEnabled = True
        Me.cboCheckListView.Items.AddRange(New Object() {"Incomplete", "Complete", "All"})
        Me.cboCheckListView.Location = New System.Drawing.Point(36, 8)
        Me.cboCheckListView.Name = "cboCheckListView"
        Me.cboCheckListView.Size = New System.Drawing.Size(152, 21)
        Me.cboCheckListView.TabIndex = 24
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(3, 11)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(30, 13)
        Me.Label52.TabIndex = 23
        Me.Label52.Text = "View"
        '
        'txtCompletedOn
        '
        Me.txtCompletedOn.Location = New System.Drawing.Point(678, 93)
        Me.txtCompletedOn.Name = "txtCompletedOn"
        Me.txtCompletedOn.Size = New System.Drawing.Size(97, 20)
        Me.txtCompletedOn.TabIndex = 22
        '
        'txtCompletedBy
        '
        Me.txtCompletedBy.Location = New System.Drawing.Point(596, 93)
        Me.txtCompletedBy.Name = "txtCompletedBy"
        Me.txtCompletedBy.Size = New System.Drawing.Size(78, 20)
        Me.txtCompletedBy.TabIndex = 21
        '
        'txtIssueName
        '
        Me.txtIssueName.Location = New System.Drawing.Point(396, 54)
        Me.txtIssueName.Name = "txtIssueName"
        Me.txtIssueName.Size = New System.Drawing.Size(192, 20)
        Me.txtIssueName.TabIndex = 28
        '
        'txtIssueID
        '
        Me.txtIssueID.Location = New System.Drawing.Point(301, 54)
        Me.txtIssueID.Name = "txtIssueID"
        Me.txtIssueID.Size = New System.Drawing.Size(89, 20)
        Me.txtIssueID.TabIndex = 27
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(301, 120)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ReadOnly = True
        Me.txtDescription.Size = New System.Drawing.Size(480, 270)
        Me.txtDescription.TabIndex = 31
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(675, 37)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(61, 13)
        Me.Label51.TabIndex = 18
        Me.Label51.Text = "Entered On"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(591, 37)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(59, 13)
        Me.Label50.TabIndex = 17
        Me.Label50.Text = "Entered By"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(393, 37)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(63, 13)
        Me.Label49.TabIndex = 16
        Me.Label49.Text = "Issue Name"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(299, 36)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(46, 13)
        Me.Label48.TabIndex = 15
        Me.Label48.Text = "Issue ID"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(300, 100)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(60, 13)
        Me.Label47.TabIndex = 14
        Me.Label47.Text = "Description"
        '
        'ValCheckList
        '
        Me.ValCheckList.FormattingEnabled = True
        Me.ValCheckList.HorizontalScrollbar = True
        Me.ValCheckList.Location = New System.Drawing.Point(0, 52)
        Me.ValCheckList.Name = "ValCheckList"
        Me.ValCheckList.Size = New System.Drawing.Size(294, 364)
        Me.ValCheckList.TabIndex = 0
        '
        'tabSummary
        '
        Me.tabSummary.Controls.Add(Me.lblPricingHub)
        Me.tabSummary.Controls.Add(Me.Label1)
        Me.tabSummary.Controls.Add(Me.chkLockPN)
        Me.tabSummary.Controls.Add(Me.lblFileModify)
        Me.tabSummary.Controls.Add(Me.lblOU)
        Me.tabSummary.Controls.Add(Me.txtValidationIssues)
        Me.tabSummary.Controls.Add(Me.Label46)
        Me.tabSummary.Controls.Add(Me.lblLastCalcDate)
        Me.tabSummary.Controls.Add(Me.lblLastUpload)
        Me.tabSummary.Controls.Add(Me.lblValidationStatus)
        Me.tabSummary.Controls.Add(Me.lblLU)
        Me.tabSummary.Controls.Add(Me.lblLCD)
        Me.tabSummary.Controls.Add(Me.Label35)
        Me.tabSummary.Location = New System.Drawing.Point(4, 22)
        Me.tabSummary.Name = "tabSummary"
        Me.tabSummary.Padding = New System.Windows.Forms.Padding(3)
        Me.tabSummary.Size = New System.Drawing.Size(789, 420)
        Me.tabSummary.TabIndex = 2
        Me.tabSummary.Text = "Summary"
        Me.tabSummary.UseVisualStyleBackColor = True
        '
        'lblPricingHub
        '
        Me.lblPricingHub.AutoSize = True
        Me.lblPricingHub.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPricingHub.Location = New System.Drawing.Point(105, 92)
        Me.lblPricingHub.Name = "lblPricingHub"
        Me.lblPricingHub.Size = New System.Drawing.Size(70, 13)
        Me.lblPricingHub.TabIndex = 33
        Me.lblPricingHub.Text = "000000000"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 92)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Pricing Hub:"
        '
        'chkLockPN
        '
        Me.chkLockPN.AutoSize = True
        Me.chkLockPN.Checked = True
        Me.chkLockPN.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLockPN.Location = New System.Drawing.Point(244, 129)
        Me.chkLockPN.Name = "chkLockPN"
        Me.chkLockPN.Size = New System.Drawing.Size(77, 17)
        Me.chkLockPN.TabIndex = 31
        Me.chkLockPN.Text = "Lock Entry"
        Me.chkLockPN.UseVisualStyleBackColor = True
        '
        'lblFileModify
        '
        Me.lblFileModify.AutoSize = True
        Me.lblFileModify.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFileModify.Location = New System.Drawing.Point(105, 69)
        Me.lblFileModify.Name = "lblFileModify"
        Me.lblFileModify.Size = New System.Drawing.Size(130, 13)
        Me.lblFileModify.TabIndex = 29
        Me.lblFileModify.Text = "6/8/2010 3:51:08 PM"
        Me.lblFileModify.Visible = False
        '
        'lblOU
        '
        Me.lblOU.AutoSize = True
        Me.lblOU.Location = New System.Drawing.Point(9, 69)
        Me.lblOU.Name = "lblOU"
        Me.lblOU.Size = New System.Drawing.Size(83, 13)
        Me.lblOU.TabIndex = 28
        Me.lblOU.Text = "Last File Modify:"
        Me.lblOU.Visible = False
        '
        'txtValidationIssues
        '
        Me.txtValidationIssues.Location = New System.Drawing.Point(15, 158)
        Me.txtValidationIssues.Multiline = True
        Me.txtValidationIssues.Name = "txtValidationIssues"
        Me.txtValidationIssues.ReadOnly = True
        Me.txtValidationIssues.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtValidationIssues.Size = New System.Drawing.Size(741, 208)
        Me.txtValidationIssues.TabIndex = 12
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(12, 129)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(209, 13)
        Me.Label46.TabIndex = 11
        Me.Label46.Text = "Validation Issues/Presenter's Notes"
        '
        'lblLastCalcDate
        '
        Me.lblLastCalcDate.AutoSize = True
        Me.lblLastCalcDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastCalcDate.Location = New System.Drawing.Point(105, 53)
        Me.lblLastCalcDate.Name = "lblLastCalcDate"
        Me.lblLastCalcDate.Size = New System.Drawing.Size(130, 13)
        Me.lblLastCalcDate.TabIndex = 7
        Me.lblLastCalcDate.Text = "6/8/2010 3:51:08 PM"
        '
        'lblLastUpload
        '
        Me.lblLastUpload.AutoSize = True
        Me.lblLastUpload.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastUpload.Location = New System.Drawing.Point(105, 37)
        Me.lblLastUpload.Name = "lblLastUpload"
        Me.lblLastUpload.Size = New System.Drawing.Size(130, 13)
        Me.lblLastUpload.TabIndex = 6
        Me.lblLastUpload.Text = "6/8/2010 3:51:08 PM"
        '
        'lblValidationStatus
        '
        Me.lblValidationStatus.AutoSize = True
        Me.lblValidationStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidationStatus.Location = New System.Drawing.Point(105, 14)
        Me.lblValidationStatus.Name = "lblValidationStatus"
        Me.lblValidationStatus.Size = New System.Drawing.Size(63, 13)
        Me.lblValidationStatus.TabIndex = 4
        Me.lblValidationStatus.Text = "Validating"
        '
        'lblLU
        '
        Me.lblLU.AutoSize = True
        Me.lblLU.Location = New System.Drawing.Point(9, 37)
        Me.lblLU.Name = "lblLU"
        Me.lblLU.Size = New System.Drawing.Size(67, 13)
        Me.lblLU.TabIndex = 3
        Me.lblLU.Text = "Last Upload:"
        '
        'lblLCD
        '
        Me.lblLCD.AutoSize = True
        Me.lblLCD.Location = New System.Drawing.Point(9, 53)
        Me.lblLCD.Name = "lblLCD"
        Me.lblLCD.Size = New System.Drawing.Size(80, 13)
        Me.lblLCD.TabIndex = 1
        Me.lblLCD.Text = "Last Calc Date:"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(9, 14)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(89, 13)
        Me.Label35.TabIndex = 0
        Me.Label35.Text = "Validation Status:"
        '
        'ConsoleTabs
        '
        Me.ConsoleTabs.AllowDrop = True
        Me.ConsoleTabs.Controls.Add(Me.tabPlant)
        Me.ConsoleTabs.Controls.Add(Me.tabSummary)
        Me.ConsoleTabs.Controls.Add(Me.tabCheckList)
        Me.ConsoleTabs.Controls.Add(Me.tabSS)
        Me.ConsoleTabs.Controls.Add(Me.tabSupport)
        Me.ConsoleTabs.Enabled = False
        Me.ConsoleTabs.Location = New System.Drawing.Point(0, 149)
        Me.ConsoleTabs.Name = "ConsoleTabs"
        Me.ConsoleTabs.SelectedIndex = 0
        Me.ConsoleTabs.Size = New System.Drawing.Size(797, 446)
        Me.ConsoleTabs.TabIndex = 0
        '
        'tabPlant
        '
        Me.tabPlant.Controls.Add(Me.Button1)
        Me.tabPlant.Controls.Add(Me.Panel5)
        Me.tabPlant.Controls.Add(Me.txtReturnPassword)
        Me.tabPlant.Controls.Add(Me.Label19)
        Me.tabPlant.Location = New System.Drawing.Point(4, 22)
        Me.tabPlant.Name = "tabPlant"
        Me.tabPlant.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPlant.Size = New System.Drawing.Size(789, 420)
        Me.tabPlant.TabIndex = 9
        Me.tabPlant.Text = "Contacts"
        Me.tabPlant.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(441, 144)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(43, 23)
        Me.Button1.TabIndex = 26
        Me.Button1.Text = "Copy"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.CompanyContactEmail)
        Me.Panel5.Controls.Add(Me.CompanyContactName)
        Me.Panel5.Controls.Add(Me.btnCompany)
        Me.Panel5.Controls.Add(Me.PlantCoordPhone)
        Me.Panel5.Controls.Add(Me.PlantCoordEmail)
        Me.Panel5.Controls.Add(Me.PlantCoordName)
        Me.Panel5.Controls.Add(Me.btnPlantCoordinator)
        Me.Panel5.Controls.Add(Me.Label64)
        Me.Panel5.Controls.Add(Me.Label65)
        Me.Panel5.Controls.Add(Me.Label66)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(3, 3)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(783, 103)
        Me.Panel5.TabIndex = 23
        '
        'CompanyContactEmail
        '
        Me.CompanyContactEmail.AutoSize = True
        Me.CompanyContactEmail.Location = New System.Drawing.Point(330, 63)
        Me.CompanyContactEmail.Name = "CompanyContactEmail"
        Me.CompanyContactEmail.Size = New System.Drawing.Size(119, 13)
        Me.CompanyContactEmail.TabIndex = 35
        Me.CompanyContactEmail.Text = "Company Contact Email"
        '
        'CompanyContactName
        '
        Me.CompanyContactName.AutoSize = True
        Me.CompanyContactName.Location = New System.Drawing.Point(139, 63)
        Me.CompanyContactName.Name = "CompanyContactName"
        Me.CompanyContactName.Size = New System.Drawing.Size(122, 13)
        Me.CompanyContactName.TabIndex = 34
        Me.CompanyContactName.Text = "Company Contact Name"
        '
        'btnCompany
        '
        Me.btnCompany.Location = New System.Drawing.Point(19, 58)
        Me.btnCompany.Name = "btnCompany"
        Me.btnCompany.Size = New System.Drawing.Size(112, 23)
        Me.btnCompany.TabIndex = 33
        Me.btnCompany.Text = "Company Contact"
        Me.btnCompany.UseVisualStyleBackColor = True
        '
        'PlantCoordPhone
        '
        Me.PlantCoordPhone.AutoSize = True
        Me.PlantCoordPhone.Location = New System.Drawing.Point(635, 32)
        Me.PlantCoordPhone.Name = "PlantCoordPhone"
        Me.PlantCoordPhone.Size = New System.Drawing.Size(122, 13)
        Me.PlantCoordPhone.TabIndex = 28
        Me.PlantCoordPhone.Text = "Plant Coordinator Phone"
        '
        'PlantCoordEmail
        '
        Me.PlantCoordEmail.AutoSize = True
        Me.PlantCoordEmail.Location = New System.Drawing.Point(330, 32)
        Me.PlantCoordEmail.Name = "PlantCoordEmail"
        Me.PlantCoordEmail.Size = New System.Drawing.Size(116, 13)
        Me.PlantCoordEmail.TabIndex = 27
        Me.PlantCoordEmail.Text = "Plant Coordinator Email"
        '
        'PlantCoordName
        '
        Me.PlantCoordName.AutoSize = True
        Me.PlantCoordName.Location = New System.Drawing.Point(139, 32)
        Me.PlantCoordName.Name = "PlantCoordName"
        Me.PlantCoordName.Size = New System.Drawing.Size(119, 13)
        Me.PlantCoordName.TabIndex = 26
        Me.PlantCoordName.Text = "Plant Coordinator Name"
        '
        'btnPlantCoordinator
        '
        Me.btnPlantCoordinator.Location = New System.Drawing.Point(19, 27)
        Me.btnPlantCoordinator.Name = "btnPlantCoordinator"
        Me.btnPlantCoordinator.Size = New System.Drawing.Size(112, 23)
        Me.btnPlantCoordinator.TabIndex = 23
        Me.btnPlantCoordinator.Text = "Plant Coordinator"
        Me.btnPlantCoordinator.UseVisualStyleBackColor = True
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label64.Location = New System.Drawing.Point(635, 9)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(67, 13)
        Me.Label64.TabIndex = 22
        Me.Label64.Text = "Telephone"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label65.Location = New System.Drawing.Point(330, 9)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(86, 13)
        Me.Label65.TabIndex = 21
        Me.Label65.Text = "Email Address"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label66.Location = New System.Drawing.Point(139, 9)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(39, 13)
        Me.Label66.TabIndex = 20
        Me.Label66.Text = "Name"
        '
        'txtReturnPassword
        '
        Me.txtReturnPassword.Location = New System.Drawing.Point(159, 144)
        Me.txtReturnPassword.Name = "txtReturnPassword"
        Me.txtReturnPassword.ReadOnly = True
        Me.txtReturnPassword.Size = New System.Drawing.Size(272, 20)
        Me.txtReturnPassword.TabIndex = 13
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(22, 147)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(107, 13)
        Me.Label19.TabIndex = 12
        Me.Label19.Text = "Return File Password"
        '
        'tabSupport
        '
        Me.tabSupport.Controls.Add(Me.Label3)
        Me.tabSupport.Controls.Add(Me.Panel1)
        Me.tabSupport.Controls.Add(Me.Label2)
        Me.tabSupport.Controls.Add(Me.btnLaunch)
        Me.tabSupport.Controls.Add(Me.clbSummaryCalcFiles)
        Me.tabSupport.Location = New System.Drawing.Point(4, 22)
        Me.tabSupport.Name = "tabSupport"
        Me.tabSupport.Padding = New System.Windows.Forms.Padding(3)
        Me.tabSupport.Size = New System.Drawing.Size(789, 420)
        Me.tabSupport.TabIndex = 12
        Me.tabSupport.Text = "Support Files"
        Me.tabSupport.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(602, 25)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(141, 16)
        Me.Label3.TabIndex = 38
        Me.Label3.Text = "Other Support Files"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btnPT)
        Me.Panel1.Controls.Add(Me.btnCT)
        Me.Panel1.Location = New System.Drawing.Point(587, 50)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(169, 292)
        Me.Panel1.TabIndex = 37
        '
        'btnPT
        '
        Me.btnPT.Enabled = False
        Me.btnPT.Location = New System.Drawing.Point(17, 17)
        Me.btnPT.Margin = New System.Windows.Forms.Padding(1)
        Me.btnPT.Name = "btnPT"
        Me.btnPT.Size = New System.Drawing.Size(132, 32)
        Me.btnPT.TabIndex = 35
        Me.btnPT.Text = "Prelim Client Table (PT)"
        Me.btnPT.UseVisualStyleBackColor = True
        '
        'btnCT
        '
        Me.btnCT.Enabled = False
        Me.btnCT.Location = New System.Drawing.Point(17, 64)
        Me.btnCT.Margin = New System.Windows.Forms.Padding(1)
        Me.btnCT.Name = "btnCT"
        Me.btnCT.Size = New System.Drawing.Size(132, 32)
        Me.btnCT.TabIndex = 36
        Me.btnCT.Text = "Client Table (CT)"
        Me.btnCT.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(25, 25)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(115, 16)
        Me.Label2.TabIndex = 34
        Me.Label2.Text = "Summary Calcs"
        '
        'btnLaunch
        '
        Me.btnLaunch.Location = New System.Drawing.Point(23, 369)
        Me.btnLaunch.Name = "btnLaunch"
        Me.btnLaunch.Size = New System.Drawing.Size(100, 23)
        Me.btnLaunch.TabIndex = 1
        Me.btnLaunch.Text = "Launch Checked"
        Me.btnLaunch.UseVisualStyleBackColor = True
        '
        'clbSummaryCalcFiles
        '
        Me.clbSummaryCalcFiles.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.clbSummaryCalcFiles.FormattingEnabled = True
        Me.clbSummaryCalcFiles.Location = New System.Drawing.Point(28, 50)
        Me.clbSummaryCalcFiles.Name = "clbSummaryCalcFiles"
        Me.clbSummaryCalcFiles.Size = New System.Drawing.Size(519, 292)
        Me.clbSummaryCalcFiles.TabIndex = 0
        '
        'txtVersion
        '
        Me.txtVersion.BackColor = System.Drawing.SystemColors.Control
        Me.txtVersion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVersion.Location = New System.Drawing.Point(27, 4)
        Me.txtVersion.Name = "txtVersion"
        Me.txtVersion.ReadOnly = True
        Me.txtVersion.Size = New System.Drawing.Size(100, 15)
        Me.txtVersion.TabIndex = 39
        Me.VersionToolTip.SetToolTip(Me.txtVersion, "This is the new version")
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'ConsoleTimer
        '
        Me.ConsoleTimer.AutoSize = True
        Me.ConsoleTimer.ForeColor = System.Drawing.Color.Green
        Me.ConsoleTimer.Location = New System.Drawing.Point(22, 129)
        Me.ConsoleTimer.Name = "ConsoleTimer"
        Me.ConsoleTimer.Size = New System.Drawing.Size(0, 13)
        Me.ConsoleTimer.TabIndex = 40
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(29, 131)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(0, 13)
        Me.Label14.TabIndex = 41
        '
        'btnKillProcesses
        '
        Me.btnKillProcesses.Location = New System.Drawing.Point(766, 4)
        Me.btnKillProcesses.Name = "btnKillProcesses"
        Me.btnKillProcesses.Size = New System.Drawing.Size(27, 23)
        Me.btnKillProcesses.TabIndex = 42
        Me.btnKillProcesses.Text = "X"
        Me.btnKillProcesses.UseVisualStyleBackColor = True
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.DarkGreen
        Me.lblStatus.Location = New System.Drawing.Point(12, 600)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(47, 15)
        Me.lblStatus.TabIndex = 44
        Me.lblStatus.Text = "Ready"
        '
        'PowerMainConsole
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(797, 622)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.btnKillProcesses)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.ConsoleTimer)
        Me.Controls.Add(Me.txtVersion)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.lblInHolding)
        Me.Controls.Add(Me.btnValidate)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.cboConsultant)
        Me.Controls.Add(Me.lblWarning)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label60)
        Me.Controls.Add(Me.cboRefNum)
        Me.Controls.Add(Me.Label59)
        Me.Controls.Add(Me.cboStudy)
        Me.Controls.Add(Me.ConsoleTabs)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "PowerMainConsole"
        Me.Text = "Power Validation Console"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSS.ResumeLayout(False)
        Me.tabSS.PerformLayout()
        Me.tabCheckList.ResumeLayout(False)
        Me.tabCheckList.PerformLayout()
        Me.tabSummary.ResumeLayout(False)
        Me.tabSummary.PerformLayout()
        Me.ConsoleTabs.ResumeLayout(False)
        Me.tabPlant.ResumeLayout(False)
        Me.tabPlant.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.tabSupport.ResumeLayout(False)
        Me.tabSupport.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboStudy As System.Windows.Forms.ComboBox
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents cboRefNum As System.Windows.Forms.ComboBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cboConsultant As System.Windows.Forms.ComboBox
    Friend WithEvents btnValidate As System.Windows.Forms.Button
    Friend WithEvents lblInHolding As System.Windows.Forms.Label
    Friend WithEvents btnHelp As System.Windows.Forms.Button
    Friend WithEvents tabSS As System.Windows.Forms.TabPage
    Friend WithEvents tabCheckList As System.Windows.Forms.TabPage
    Friend WithEvents lblItemCount As System.Windows.Forms.Label
    Friend WithEvents btnAddIssue As System.Windows.Forms.Button
    Friend WithEvents cboCheckListView As System.Windows.Forms.ComboBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents txtIssueName As System.Windows.Forms.TextBox
    Friend WithEvents txtIssueID As System.Windows.Forms.TextBox
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents ValCheckList As System.Windows.Forms.CheckedListBox
    Friend WithEvents tabSummary As System.Windows.Forms.TabPage
    Friend WithEvents txtValidationIssues As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents lblLastCalcDate As System.Windows.Forms.Label
    Friend WithEvents lblLastUpload As System.Windows.Forms.Label
    Friend WithEvents lblLU As System.Windows.Forms.Label
    Friend WithEvents lblLCD As System.Windows.Forms.Label
    Friend WithEvents ConsoleTabs As System.Windows.Forms.TabControl
    Friend WithEvents tvwCorrespondence As System.Windows.Forms.TreeView
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboDir As System.Windows.Forms.ComboBox
    Friend WithEvents tvwDrawings As System.Windows.Forms.TreeView
    Friend WithEvents tabPlant As System.Windows.Forms.TabPage
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents PlantCoordPhone As System.Windows.Forms.Label
    Friend WithEvents PlantCoordEmail As System.Windows.Forms.Label
    Friend WithEvents PlantCoordName As System.Windows.Forms.Label
    Friend WithEvents btnPlantCoordinator As System.Windows.Forms.Button
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents txtReturnPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents tvwClientAttachments As System.Windows.Forms.TreeView
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cboDir2 As System.Windows.Forms.ComboBox
    Friend WithEvents tvwClientAttachments2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwCompCorr2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwCompCorr As System.Windows.Forms.TreeView
    Friend WithEvents tvwCorrespondence2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwDrawings2 As System.Windows.Forms.TreeView
    Friend WithEvents btnUpdateIssue As System.Windows.Forms.Button
    Friend WithEvents btnCancelIssue As System.Windows.Forms.Button
    Friend WithEvents txtPostedOn As System.Windows.Forms.TextBox
    Friend WithEvents txtPostedBy As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtCompletedOn As System.Windows.Forms.TextBox
    Friend WithEvents txtCompletedBy As System.Windows.Forms.TextBox
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents btnSecureSend As System.Windows.Forms.Button
    Friend WithEvents tvIssues As System.Windows.Forms.TreeView
    Friend WithEvents txtVersion As System.Windows.Forms.TextBox
    Friend WithEvents VersionToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents lblFileModify As System.Windows.Forms.Label
    Friend WithEvents lblOU As System.Windows.Forms.Label
    Friend WithEvents btnDirRefresh As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ConsoleTimer As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lblValidationStatus As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents btnKillProcesses As System.Windows.Forms.Button
    Friend WithEvents chkLockPN As System.Windows.Forms.CheckBox
    Friend WithEvents CompanyContactEmail As System.Windows.Forms.Label
    Friend WithEvents CompanyContactName As System.Windows.Forms.Label
    Friend WithEvents btnCompany As System.Windows.Forms.Button
    Friend WithEvents lblPricingHub As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents tabSupport As System.Windows.Forms.TabPage
    Friend WithEvents clbSummaryCalcFiles As System.Windows.Forms.CheckedListBox
    Friend WithEvents btnLaunch As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnPT As System.Windows.Forms.Button
    Friend WithEvents btnCT As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label


End Class
