﻿Public Class ExplorerTree


    Private mIcons As New Hashtable
    Private mRootPath As String = "K:\STUDY"

    Property RootPath() As String
        Get
            Return mRootPath
        End Get
        Set(ByVal value As String)
            mRootPath = value
            SetTree()

        End Set
    End Property


    Public Sub SetTree()
        TreeView1.Nodes.Clear()
        Dim mRootNode As New TreeNode
        mRootNode.Text = mRootPath
        mRootNode.Tag = mRootPath
        mRootNode.ImageKey = CacheShellIcon(mRootPath)
        mRootNode.SelectedImageKey = mRootNode.ImageKey & "-open"
        mRootNode.Nodes.Add("*DUMMY*")
        TreeView1.Nodes.Add(mRootNode)
        TreeView1.ExpandAll()

    End Sub

    Private Sub UserControl1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' when our component is loaded, we initialize the TreeView by adding the root node
        SetTree()
    End Sub

    Private Sub TreeView1_BeforeCollapse(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs) Handles TreeView1.BeforeCollapse
        ' clear the node that is being collapsed
        e.Node.Nodes.Clear()
        ' add a dummy TreeNode to the node being collapsed so it is expandable
        e.Node.Nodes.Add("*DUMMY*")
    End Sub

    Private Sub TreeView1_BeforeExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs) Handles TreeView1.BeforeExpand
        Try
            ' clear the argNode so we can re-populate, or else we end up with duplicate nodes
            e.Node.Nodes.Clear()
            ' get the directory representing this node
            Dim mNodeDirectory As IO.DirectoryInfo
            mNodeDirectory = New IO.DirectoryInfo(e.Node.Tag.ToString)
            ' add each directory from the file system that is a child of the argNode that was passed in
            For Each mDirectory As IO.DirectoryInfo In mNodeDirectory.GetDirectories
                ' declare a TreeNode for this directory
                Dim mDirectoryNode As New TreeNode
                ' store the full path to this directory in the directory TreeNode's Tag property
                mDirectoryNode.Tag = mDirectory.FullName
                ' set the directory TreeNodes's display text
                mDirectoryNode.Text = mDirectory.Name
                ' add a dummy TreeNode to the directory node so that it is expandable
                mDirectoryNode.Nodes.Add("*DUMMY*")
                ' get the icon/open icon for this directory
                mDirectoryNode.ImageKey = CacheShellIcon(mDirectory.FullName)
                mDirectoryNode.SelectedImageKey = mDirectoryNode.ImageKey & "-open"
                ' add this directory treenode to the treenode that is being populated
                e.Node.Nodes.Add(mDirectoryNode)
            Next

            ' add each file from the file system that is a child of the argNode that was passed in
            For Each mFile As IO.FileInfo In mNodeDirectory.GetFiles
                ' declare a TreeNode for this directory
                Dim mFileNode As New TreeNode
                ' store the full path to this directory in the directory TreeNode's Tag property
                mFileNode.Tag = mFile.FullName
                ' set the directory TreeNodes's display text
                mFileNode.Text = mFile.Name
                ' get the icon/open icon for this file
                mFileNode.ImageKey = CacheShellIcon(mFile.FullName)
                mFileNode.SelectedImageKey = mFileNode.ImageKey
                'mFileNode.SelectedImageKey = mFileNode.ImageKey & "-open"
                ' add this directory treenode to the treenode that is being populated
                e.Node.Nodes.Add(mFileNode)
            Next
        Catch

        End Try

    End Sub

    Function CacheShellIcon(ByVal argPath As String) As String
        Dim mKey As String = Nothing
        ' determine the icon key for the file/folder specified in argPath
        If IO.Directory.Exists(argPath) = True Then
            mKey = "folder"
        ElseIf IO.File.Exists(argPath) = True Then
            mKey = IO.Path.GetExtension(argPath)
        End If
        ' check if an icon for this key has already been added to the collection
        If ImageList1.Images.ContainsKey(mKey) = False Then
            ImageList1.Images.Add(mKey, GetShellIconAsImage(argPath))
            If mKey = "folder" Then ImageList1.Images.Add(mKey & "-open", GetShellOpenIconAsImage(argPath))
        End If
        Return mKey
    End Function

    Private Sub TreeView1_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles TreeView1.NodeMouseDoubleClick
        ' only proceed if the node represents a file
        If e.Node.ImageKey = "folder" Then Exit Sub
        If e.Node.Tag = "" Then Exit Sub
        ' try to open the file
        Try
            Process.Start(e.Node.Tag)
        Catch ex As Exception
            MessageBox.Show("Error opening file: " & ex.Message)
        End Try
    End Sub

    Private Sub TreeView1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TreeView1.KeyDown
        Dim ans As DialogResult
        Dim files As String
        Select Case e.KeyCode
            Case Keys.Delete
                ans = MessageBox.Show("Are you sure you want to delete?", "Wait", MessageBoxButtons.YesNo)
                If ans = DialogResult.Yes Then
                    files = TreeView1.SelectedNode.Text
                    IO.File.Delete(mRootPath & "\" & files)
                    SetTree()
                End If

            Case Keys.Space
                Process.Start(mRootPath & "\")
        End Select

    End Sub



End Class
