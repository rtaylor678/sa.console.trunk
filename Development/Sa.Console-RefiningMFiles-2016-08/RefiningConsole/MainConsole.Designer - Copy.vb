﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainConsole
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainConsole))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.lblInterimPhone = New System.Windows.Forms.Label()
        Me.lblInterimEmail = New System.Windows.Forms.Label()
        Me.lblInterimName = New System.Windows.Forms.Label()
        Me.lblAltCoCoPhone = New System.Windows.Forms.Label()
        Me.lblAltCoCoEmail = New System.Windows.Forms.Label()
        Me.lblAltCoCoName = New System.Windows.Forms.Label()
        Me.lblCoCoPhone = New System.Windows.Forms.Label()
        Me.lblCoCoEmail = New System.Windows.Forms.Label()
        Me.lblCoCoName = New System.Windows.Forms.Label()
        Me.btnShowIntCo = New System.Windows.Forms.Button()
        Me.btnShowAltCo = New System.Windows.Forms.Button()
        Me.btnShowCoCo = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblCompanyPassword = New System.Windows.Forms.Label()
        Me.cboSendFormat = New System.Windows.Forms.ComboBox()
        Me.txtCorrNotes = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.lblReturnFilePassword = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblGenPhone = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.lblTechMgrEmail = New System.Windows.Forms.Label()
        Me.lblTechMgrName = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.lblMaintMgrEmail = New System.Windows.Forms.Label()
        Me.lblMaintMgrName = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.lblOpsMgrEmail = New System.Windows.Forms.Label()
        Me.lblOpsMgrName = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.lblRefMgrEmail = New System.Windows.Forms.Label()
        Me.lblRefMgrName = New System.Windows.Forms.Label()
        Me.lblRefAltPhone = New System.Windows.Forms.Label()
        Me.lblRefAltEmail = New System.Windows.Forms.Label()
        Me.lblRefAltName = New System.Windows.Forms.Label()
        Me.lblRefCoPhone = New System.Windows.Forms.Label()
        Me.lblRefCoEmail = New System.Windows.Forms.Label()
        Me.lblRefCoName = New System.Windows.Forms.Label()
        Me.btnShowRefAltCo = New System.Windows.Forms.Button()
        Me.btnShowRefCo = New System.Windows.Forms.Button()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnHYC = New System.Windows.Forms.Button()
        Me.btnLog = New System.Windows.Forms.Button()
        Me.btnUnitReview = New System.Windows.Forms.Button()
        Me.btnSpecFrac = New System.Windows.Forms.Button()
        Me.btnDrawings = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtComments = New System.Windows.Forms.TextBox()
        Me.btnPrintIssues = New System.Windows.Forms.Button()
        Me.btnViewWordFile = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnPA = New System.Windows.Forms.Button()
        Me.btnRAM = New System.Windows.Forms.Button()
        Me.btnVI = New System.Windows.Forms.Button()
        Me.btnPT = New System.Windows.Forms.Button()
        Me.btnSC = New System.Windows.Forms.Button()
        Me.btnCT = New System.Windows.Forms.Button()
        Me.txtValidationIssues = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.lblItemsRemaining = New System.Windows.Forms.Label()
        Me.lblBlueFlags = New System.Windows.Forms.Label()
        Me.lblRedFlags = New System.Windows.Forms.Label()
        Me.lblLastCalcDate = New System.Windows.Forms.Label()
        Me.lblLastUpload = New System.Windows.Forms.Label()
        Me.lblLastFileSave = New System.Windows.Forms.Label()
        Me.lblValidationStatus = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.lblItemCount = New System.Windows.Forms.Label()
        Me.btnAddIssue = New System.Windows.Forms.Button()
        Me.cboCheckListView = New System.Windows.Forms.ComboBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.txtCompletedOn = New System.Windows.Forms.TextBox()
        Me.txtCompletedBy = New System.Windows.Forms.TextBox()
        Me.txtIssueName = New System.Windows.Forms.TextBox()
        Me.txtIssueID = New System.Windows.Forms.TextBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.ValCheckList = New System.Windows.Forms.CheckedListBox()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.btnTransPrice = New System.Windows.Forms.Button()
        Me.btnFLComp = New System.Windows.Forms.Button()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.btnOpenValFax = New System.Windows.Forms.Button()
        Me.btnValFax = New System.Windows.Forms.Button()
        Me.btnBuildVRFile = New System.Windows.Forms.Button()
        Me.btnReceiptAck = New System.Windows.Forms.Button()
        Me.lstReturnFiles = New System.Windows.Forms.ListBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.lstVRFiles = New System.Windows.Forms.ListBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.lstVFFiles = New System.Windows.Forms.ListBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.lstVFNumbers = New System.Windows.Forms.ListBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.dgValidator = New System.Windows.Forms.DataGridView()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.txtConsultingOpps = New System.Windows.Forms.TextBox()
        Me.txtCurrentIssues = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.TabPage8 = New System.Windows.Forms.TabPage()
        Me.ETCorrespondence = New TreeViewFileBrowserPart2.ExplorerTree()
        Me.TabPage9 = New System.Windows.Forms.TabPage()
        Me.ETCompany = New TreeViewFileBrowserPart2.ExplorerTree()
        Me.TabPage10 = New System.Windows.Forms.TabPage()
        Me.ETDrawings = New TreeViewFileBrowserPart2.ExplorerTree()
        Me.cboStudy = New System.Windows.Forms.ComboBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.cboRefNum = New System.Windows.Forms.ComboBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lblWarning = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboConsultant = New System.Windows.Forms.ComboBox()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.btnValidate = New System.Windows.Forms.Button()
        Me.chkJustLooking = New System.Windows.Forms.CheckBox()
        Me.lblInHolding = New System.Windows.Forms.Label()
        Me.btnHelp = New System.Windows.Forms.Button()
        Me.btnDensity = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        CType(Me.dgValidator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage7.SuspendLayout()
        Me.TabPage8.SuspendLayout()
        Me.TabPage9.SuspendLayout()
        Me.TabPage10.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.AllowDrop = True
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage7)
        Me.TabControl1.Controls.Add(Me.TabPage8)
        Me.TabControl1.Controls.Add(Me.TabPage9)
        Me.TabControl1.Controls.Add(Me.TabPage10)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.TabControl1.Enabled = False
        Me.TabControl1.Location = New System.Drawing.Point(0, 149)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(797, 445)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Panel2)
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(789, 419)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Company"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.CheckBox1)
        Me.Panel2.Controls.Add(Me.lblInterimPhone)
        Me.Panel2.Controls.Add(Me.lblInterimEmail)
        Me.Panel2.Controls.Add(Me.lblInterimName)
        Me.Panel2.Controls.Add(Me.lblAltCoCoPhone)
        Me.Panel2.Controls.Add(Me.lblAltCoCoEmail)
        Me.Panel2.Controls.Add(Me.lblAltCoCoName)
        Me.Panel2.Controls.Add(Me.lblCoCoPhone)
        Me.Panel2.Controls.Add(Me.lblCoCoEmail)
        Me.Panel2.Controls.Add(Me.lblCoCoName)
        Me.Panel2.Controls.Add(Me.btnShowIntCo)
        Me.Panel2.Controls.Add(Me.btnShowAltCo)
        Me.Panel2.Controls.Add(Me.btnShowCoCo)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(783, 147)
        Me.Panel2.TabIndex = 21
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(11, 115)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(143, 17)
        Me.CheckBox1.TabIndex = 35
        Me.CheckBox1.Text = "CC the Alternate Contact"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'lblInterimPhone
        '
        Me.lblInterimPhone.AutoSize = True
        Me.lblInterimPhone.Location = New System.Drawing.Point(576, 90)
        Me.lblInterimPhone.Name = "lblInterimPhone"
        Me.lblInterimPhone.Size = New System.Drawing.Size(0, 13)
        Me.lblInterimPhone.TabIndex = 34
        '
        'lblInterimEmail
        '
        Me.lblInterimEmail.AutoSize = True
        Me.lblInterimEmail.Location = New System.Drawing.Point(359, 90)
        Me.lblInterimEmail.Name = "lblInterimEmail"
        Me.lblInterimEmail.Size = New System.Drawing.Size(0, 13)
        Me.lblInterimEmail.TabIndex = 33
        '
        'lblInterimName
        '
        Me.lblInterimName.AutoSize = True
        Me.lblInterimName.Location = New System.Drawing.Point(139, 90)
        Me.lblInterimName.Name = "lblInterimName"
        Me.lblInterimName.Size = New System.Drawing.Size(0, 13)
        Me.lblInterimName.TabIndex = 32
        '
        'lblAltCoCoPhone
        '
        Me.lblAltCoCoPhone.AutoSize = True
        Me.lblAltCoCoPhone.Location = New System.Drawing.Point(576, 61)
        Me.lblAltCoCoPhone.Name = "lblAltCoCoPhone"
        Me.lblAltCoCoPhone.Size = New System.Drawing.Size(83, 13)
        Me.lblAltCoCoPhone.TabIndex = 31
        Me.lblAltCoCoPhone.Text = "Alternate Phone"
        '
        'lblAltCoCoEmail
        '
        Me.lblAltCoCoEmail.AutoSize = True
        Me.lblAltCoCoEmail.Location = New System.Drawing.Point(359, 61)
        Me.lblAltCoCoEmail.Name = "lblAltCoCoEmail"
        Me.lblAltCoCoEmail.Size = New System.Drawing.Size(77, 13)
        Me.lblAltCoCoEmail.TabIndex = 30
        Me.lblAltCoCoEmail.Text = "Alternate Email"
        '
        'lblAltCoCoName
        '
        Me.lblAltCoCoName.AutoSize = True
        Me.lblAltCoCoName.Location = New System.Drawing.Point(139, 61)
        Me.lblAltCoCoName.Name = "lblAltCoCoName"
        Me.lblAltCoCoName.Size = New System.Drawing.Size(80, 13)
        Me.lblAltCoCoName.TabIndex = 29
        Me.lblAltCoCoName.Text = "Alternate Name"
        '
        'lblCoCoPhone
        '
        Me.lblCoCoPhone.AutoSize = True
        Me.lblCoCoPhone.Location = New System.Drawing.Point(576, 32)
        Me.lblCoCoPhone.Name = "lblCoCoPhone"
        Me.lblCoCoPhone.Size = New System.Drawing.Size(142, 13)
        Me.lblCoCoPhone.TabIndex = 28
        Me.lblCoCoPhone.Text = "Company Coordinator Phone"
        '
        'lblCoCoEmail
        '
        Me.lblCoCoEmail.AutoSize = True
        Me.lblCoCoEmail.Location = New System.Drawing.Point(359, 32)
        Me.lblCoCoEmail.Name = "lblCoCoEmail"
        Me.lblCoCoEmail.Size = New System.Drawing.Size(136, 13)
        Me.lblCoCoEmail.TabIndex = 27
        Me.lblCoCoEmail.Text = "Company Coordinator Email"
        '
        'lblCoCoName
        '
        Me.lblCoCoName.AutoSize = True
        Me.lblCoCoName.Location = New System.Drawing.Point(139, 32)
        Me.lblCoCoName.Name = "lblCoCoName"
        Me.lblCoCoName.Size = New System.Drawing.Size(139, 13)
        Me.lblCoCoName.TabIndex = 26
        Me.lblCoCoName.Text = "Company Coordinator Name"
        '
        'btnShowIntCo
        '
        Me.btnShowIntCo.Location = New System.Drawing.Point(6, 85)
        Me.btnShowIntCo.Name = "btnShowIntCo"
        Me.btnShowIntCo.Size = New System.Drawing.Size(127, 23)
        Me.btnShowIntCo.TabIndex = 25
        Me.btnShowIntCo.Text = "Interim Contact"
        Me.btnShowIntCo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShowIntCo.UseVisualStyleBackColor = True
        '
        'btnShowAltCo
        '
        Me.btnShowAltCo.Location = New System.Drawing.Point(6, 56)
        Me.btnShowAltCo.Name = "btnShowAltCo"
        Me.btnShowAltCo.Size = New System.Drawing.Size(127, 23)
        Me.btnShowAltCo.TabIndex = 24
        Me.btnShowAltCo.Text = "Alternate Contact"
        Me.btnShowAltCo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShowAltCo.UseVisualStyleBackColor = True
        '
        'btnShowCoCo
        '
        Me.btnShowCoCo.Location = New System.Drawing.Point(6, 27)
        Me.btnShowCoCo.Name = "btnShowCoCo"
        Me.btnShowCoCo.Size = New System.Drawing.Size(127, 23)
        Me.btnShowCoCo.TabIndex = 23
        Me.btnShowCoCo.Text = "Company Coordinator"
        Me.btnShowCoCo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShowCoCo.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(576, 9)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 13)
        Me.Label6.TabIndex = 22
        Me.Label6.Text = "Telephone"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(359, 9)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(73, 13)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Email Address"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(139, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 13)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = "Name"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblCompanyPassword)
        Me.Panel1.Controls.Add(Me.cboSendFormat)
        Me.Panel1.Controls.Add(Me.txtCorrNotes)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(783, 413)
        Me.Panel1.TabIndex = 20
        '
        'lblCompanyPassword
        '
        Me.lblCompanyPassword.AutoSize = True
        Me.lblCompanyPassword.Location = New System.Drawing.Point(181, 168)
        Me.lblCompanyPassword.Name = "lblCompanyPassword"
        Me.lblCompanyPassword.Size = New System.Drawing.Size(156, 13)
        Me.lblCompanyPassword.TabIndex = 9
        Me.lblCompanyPassword.Text = "Show Company Password Here"
        '
        'cboSendFormat
        '
        Me.cboSendFormat.FormattingEnabled = True
        Me.cboSendFormat.Location = New System.Drawing.Point(176, 198)
        Me.cboSendFormat.Name = "cboSendFormat"
        Me.cboSendFormat.Size = New System.Drawing.Size(271, 21)
        Me.cboSendFormat.TabIndex = 8
        '
        'txtCorrNotes
        '
        Me.txtCorrNotes.Location = New System.Drawing.Point(3, 249)
        Me.txtCorrNotes.Multiline = True
        Me.txtCorrNotes.Name = "txtCorrNotes"
        Me.txtCorrNotes.ReadOnly = True
        Me.txtCorrNotes.Size = New System.Drawing.Size(774, 158)
        Me.txtCorrNotes.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(4, 233)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(257, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Notes about correspondence with company contacts"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(4, 168)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Company Password"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(4, 201)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(162, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Method for Sending Attachments"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Panel4)
        Me.TabPage2.Controls.Add(Me.Panel3)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(789, 419)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Refinery"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.lblReturnFilePassword)
        Me.Panel4.Controls.Add(Me.Label7)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(3, 213)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(783, 203)
        Me.Panel4.TabIndex = 23
        '
        'lblReturnFilePassword
        '
        Me.lblReturnFilePassword.AutoSize = True
        Me.lblReturnFilePassword.Location = New System.Drawing.Point(179, 12)
        Me.lblReturnFilePassword.Name = "lblReturnFilePassword"
        Me.lblReturnFilePassword.Size = New System.Drawing.Size(163, 13)
        Me.lblReturnFilePassword.TabIndex = 8
        Me.lblReturnFilePassword.Text = "Show Return File Password Here"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(11, 12)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(107, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Return File Password"
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.lblGenPhone)
        Me.Panel3.Controls.Add(Me.Label33)
        Me.Panel3.Controls.Add(Me.Label30)
        Me.Panel3.Controls.Add(Me.lblTechMgrEmail)
        Me.Panel3.Controls.Add(Me.lblTechMgrName)
        Me.Panel3.Controls.Add(Me.Label27)
        Me.Panel3.Controls.Add(Me.lblMaintMgrEmail)
        Me.Panel3.Controls.Add(Me.lblMaintMgrName)
        Me.Panel3.Controls.Add(Me.Label24)
        Me.Panel3.Controls.Add(Me.lblOpsMgrEmail)
        Me.Panel3.Controls.Add(Me.lblOpsMgrName)
        Me.Panel3.Controls.Add(Me.Label23)
        Me.Panel3.Controls.Add(Me.lblRefMgrEmail)
        Me.Panel3.Controls.Add(Me.lblRefMgrName)
        Me.Panel3.Controls.Add(Me.lblRefAltPhone)
        Me.Panel3.Controls.Add(Me.lblRefAltEmail)
        Me.Panel3.Controls.Add(Me.lblRefAltName)
        Me.Panel3.Controls.Add(Me.lblRefCoPhone)
        Me.Panel3.Controls.Add(Me.lblRefCoEmail)
        Me.Panel3.Controls.Add(Me.lblRefCoName)
        Me.Panel3.Controls.Add(Me.btnShowRefAltCo)
        Me.Panel3.Controls.Add(Me.btnShowRefCo)
        Me.Panel3.Controls.Add(Me.Label16)
        Me.Panel3.Controls.Add(Me.Label17)
        Me.Panel3.Controls.Add(Me.Label18)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(3, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(783, 210)
        Me.Panel3.TabIndex = 22
        '
        'lblGenPhone
        '
        Me.lblGenPhone.AutoSize = True
        Me.lblGenPhone.Location = New System.Drawing.Point(576, 182)
        Me.lblGenPhone.Name = "lblGenPhone"
        Me.lblGenPhone.Size = New System.Drawing.Size(78, 13)
        Me.lblGenPhone.TabIndex = 45
        Me.lblGenPhone.Text = "General Phone"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(11, 182)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(142, 13)
        Me.Label33.TabIndex = 44
        Me.Label33.Text = "General/Switchboard Phone"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(11, 159)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(99, 13)
        Me.Label30.TabIndex = 43
        Me.Label30.Text = "Technical Manager"
        '
        'lblTechMgrEmail
        '
        Me.lblTechMgrEmail.AutoSize = True
        Me.lblTechMgrEmail.Location = New System.Drawing.Point(361, 159)
        Me.lblTechMgrEmail.Name = "lblTechMgrEmail"
        Me.lblTechMgrEmail.Size = New System.Drawing.Size(78, 13)
        Me.lblTechMgrEmail.TabIndex = 42
        Me.lblTechMgrEmail.Text = "TechMgr Email"
        '
        'lblTechMgrName
        '
        Me.lblTechMgrName.AutoSize = True
        Me.lblTechMgrName.Location = New System.Drawing.Point(139, 159)
        Me.lblTechMgrName.Name = "lblTechMgrName"
        Me.lblTechMgrName.Size = New System.Drawing.Size(81, 13)
        Me.lblTechMgrName.TabIndex = 41
        Me.lblTechMgrName.Text = "TechMgr Name"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(11, 136)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(114, 13)
        Me.Label27.TabIndex = 40
        Me.Label27.Text = "Maintenance Manager"
        '
        'lblMaintMgrEmail
        '
        Me.lblMaintMgrEmail.AutoSize = True
        Me.lblMaintMgrEmail.Location = New System.Drawing.Point(361, 136)
        Me.lblMaintMgrEmail.Name = "lblMaintMgrEmail"
        Me.lblMaintMgrEmail.Size = New System.Drawing.Size(79, 13)
        Me.lblMaintMgrEmail.TabIndex = 39
        Me.lblMaintMgrEmail.Text = "MaintMgr Email"
        '
        'lblMaintMgrName
        '
        Me.lblMaintMgrName.AutoSize = True
        Me.lblMaintMgrName.Location = New System.Drawing.Point(139, 136)
        Me.lblMaintMgrName.Name = "lblMaintMgrName"
        Me.lblMaintMgrName.Size = New System.Drawing.Size(82, 13)
        Me.lblMaintMgrName.TabIndex = 38
        Me.lblMaintMgrName.Text = "MaintMgr Name"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(11, 113)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(103, 13)
        Me.Label24.TabIndex = 37
        Me.Label24.Text = "Operations Manager"
        '
        'lblOpsMgrEmail
        '
        Me.lblOpsMgrEmail.AutoSize = True
        Me.lblOpsMgrEmail.Location = New System.Drawing.Point(361, 113)
        Me.lblOpsMgrEmail.Name = "lblOpsMgrEmail"
        Me.lblOpsMgrEmail.Size = New System.Drawing.Size(72, 13)
        Me.lblOpsMgrEmail.TabIndex = 36
        Me.lblOpsMgrEmail.Text = "OpsMgr Email"
        '
        'lblOpsMgrName
        '
        Me.lblOpsMgrName.AutoSize = True
        Me.lblOpsMgrName.Location = New System.Drawing.Point(139, 113)
        Me.lblOpsMgrName.Name = "lblOpsMgrName"
        Me.lblOpsMgrName.Size = New System.Drawing.Size(75, 13)
        Me.lblOpsMgrName.TabIndex = 35
        Me.lblOpsMgrName.Text = "OpsMgr Name"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(11, 90)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(91, 13)
        Me.Label23.TabIndex = 34
        Me.Label23.Text = "Refinery Manager"
        '
        'lblRefMgrEmail
        '
        Me.lblRefMgrEmail.AutoSize = True
        Me.lblRefMgrEmail.Location = New System.Drawing.Point(361, 90)
        Me.lblRefMgrEmail.Name = "lblRefMgrEmail"
        Me.lblRefMgrEmail.Size = New System.Drawing.Size(70, 13)
        Me.lblRefMgrEmail.TabIndex = 33
        Me.lblRefMgrEmail.Text = "RefMgr Email"
        '
        'lblRefMgrName
        '
        Me.lblRefMgrName.AutoSize = True
        Me.lblRefMgrName.Location = New System.Drawing.Point(139, 90)
        Me.lblRefMgrName.Name = "lblRefMgrName"
        Me.lblRefMgrName.Size = New System.Drawing.Size(73, 13)
        Me.lblRefMgrName.TabIndex = 32
        Me.lblRefMgrName.Text = "RefMgr Name"
        '
        'lblRefAltPhone
        '
        Me.lblRefAltPhone.AutoSize = True
        Me.lblRefAltPhone.Location = New System.Drawing.Point(576, 61)
        Me.lblRefAltPhone.Name = "lblRefAltPhone"
        Me.lblRefAltPhone.Size = New System.Drawing.Size(83, 13)
        Me.lblRefAltPhone.TabIndex = 31
        Me.lblRefAltPhone.Text = "Alternate Phone"
        '
        'lblRefAltEmail
        '
        Me.lblRefAltEmail.AutoSize = True
        Me.lblRefAltEmail.Location = New System.Drawing.Point(361, 61)
        Me.lblRefAltEmail.Name = "lblRefAltEmail"
        Me.lblRefAltEmail.Size = New System.Drawing.Size(77, 13)
        Me.lblRefAltEmail.TabIndex = 30
        Me.lblRefAltEmail.Text = "Alternate Email"
        '
        'lblRefAltName
        '
        Me.lblRefAltName.AutoSize = True
        Me.lblRefAltName.Location = New System.Drawing.Point(139, 61)
        Me.lblRefAltName.Name = "lblRefAltName"
        Me.lblRefAltName.Size = New System.Drawing.Size(80, 13)
        Me.lblRefAltName.TabIndex = 29
        Me.lblRefAltName.Text = "Alternate Name"
        '
        'lblRefCoPhone
        '
        Me.lblRefCoPhone.AutoSize = True
        Me.lblRefCoPhone.Location = New System.Drawing.Point(576, 32)
        Me.lblRefCoPhone.Name = "lblRefCoPhone"
        Me.lblRefCoPhone.Size = New System.Drawing.Size(142, 13)
        Me.lblRefCoPhone.TabIndex = 28
        Me.lblRefCoPhone.Text = "Company Coordinator Phone"
        '
        'lblRefCoEmail
        '
        Me.lblRefCoEmail.AutoSize = True
        Me.lblRefCoEmail.Location = New System.Drawing.Point(361, 32)
        Me.lblRefCoEmail.Name = "lblRefCoEmail"
        Me.lblRefCoEmail.Size = New System.Drawing.Size(136, 13)
        Me.lblRefCoEmail.TabIndex = 27
        Me.lblRefCoEmail.Text = "Company Coordinator Email"
        '
        'lblRefCoName
        '
        Me.lblRefCoName.AutoSize = True
        Me.lblRefCoName.Location = New System.Drawing.Point(139, 32)
        Me.lblRefCoName.Name = "lblRefCoName"
        Me.lblRefCoName.Size = New System.Drawing.Size(139, 13)
        Me.lblRefCoName.TabIndex = 26
        Me.lblRefCoName.Text = "Company Coordinator Name"
        '
        'btnShowRefAltCo
        '
        Me.btnShowRefAltCo.Location = New System.Drawing.Point(6, 56)
        Me.btnShowRefAltCo.Name = "btnShowRefAltCo"
        Me.btnShowRefAltCo.Size = New System.Drawing.Size(127, 23)
        Me.btnShowRefAltCo.TabIndex = 24
        Me.btnShowRefAltCo.Text = "Alternate Contact"
        Me.btnShowRefAltCo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShowRefAltCo.UseVisualStyleBackColor = True
        '
        'btnShowRefCo
        '
        Me.btnShowRefCo.Location = New System.Drawing.Point(6, 27)
        Me.btnShowRefCo.Name = "btnShowRefCo"
        Me.btnShowRefCo.Size = New System.Drawing.Size(127, 23)
        Me.btnShowRefCo.TabIndex = 23
        Me.btnShowRefCo.Text = "Refinery Coordinator"
        Me.btnShowRefCo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShowRefCo.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(576, 9)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(58, 13)
        Me.Label16.TabIndex = 22
        Me.Label16.Text = "Telephone"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(361, 9)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(73, 13)
        Me.Label17.TabIndex = 21
        Me.Label17.Text = "Email Address"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(139, 9)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(35, 13)
        Me.Label18.TabIndex = 20
        Me.Label18.Text = "Name"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Label11)
        Me.TabPage3.Controls.Add(Me.Label10)
        Me.TabPage3.Controls.Add(Me.btnHYC)
        Me.TabPage3.Controls.Add(Me.btnLog)
        Me.TabPage3.Controls.Add(Me.btnUnitReview)
        Me.TabPage3.Controls.Add(Me.btnSpecFrac)
        Me.TabPage3.Controls.Add(Me.btnDrawings)
        Me.TabPage3.Controls.Add(Me.Label9)
        Me.TabPage3.Controls.Add(Me.txtComments)
        Me.TabPage3.Controls.Add(Me.btnPrintIssues)
        Me.TabPage3.Controls.Add(Me.btnViewWordFile)
        Me.TabPage3.Controls.Add(Me.FlowLayoutPanel1)
        Me.TabPage3.Controls.Add(Me.txtValidationIssues)
        Me.TabPage3.Controls.Add(Me.Label46)
        Me.TabPage3.Controls.Add(Me.lblItemsRemaining)
        Me.TabPage3.Controls.Add(Me.lblBlueFlags)
        Me.TabPage3.Controls.Add(Me.lblRedFlags)
        Me.TabPage3.Controls.Add(Me.lblLastCalcDate)
        Me.TabPage3.Controls.Add(Me.lblLastUpload)
        Me.TabPage3.Controls.Add(Me.lblLastFileSave)
        Me.TabPage3.Controls.Add(Me.lblValidationStatus)
        Me.TabPage3.Controls.Add(Me.Label38)
        Me.TabPage3.Controls.Add(Me.Label37)
        Me.TabPage3.Controls.Add(Me.Label36)
        Me.TabPage3.Controls.Add(Me.Label35)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(789, 419)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Summary"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(679, 4)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(81, 13)
        Me.Label11.TabIndex = 24
        Me.Label11.Text = "Support Files"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(679, 230)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(81, 13)
        Me.Label10.TabIndex = 23
        Me.Label10.Text = "Support Files"
        '
        'btnHYC
        '
        Me.btnHYC.Location = New System.Drawing.Point(653, 280)
        Me.btnHYC.Margin = New System.Windows.Forms.Padding(1)
        Me.btnHYC.Name = "btnHYC"
        Me.btnHYC.Size = New System.Drawing.Size(132, 32)
        Me.btnHYC.TabIndex = 21
        Me.btnHYC.Text = "Hydrocracker Details"
        Me.btnHYC.UseVisualStyleBackColor = True
        '
        'btnLog
        '
        Me.btnLog.Enabled = False
        Me.btnLog.Location = New System.Drawing.Point(653, 314)
        Me.btnLog.Margin = New System.Windows.Forms.Padding(1)
        Me.btnLog.Name = "btnLog"
        Me.btnLog.Size = New System.Drawing.Size(132, 32)
        Me.btnLog.TabIndex = 18
        Me.btnLog.Text = "Log File (Log)"
        Me.btnLog.UseVisualStyleBackColor = True
        '
        'btnUnitReview
        '
        Me.btnUnitReview.Location = New System.Drawing.Point(653, 348)
        Me.btnUnitReview.Margin = New System.Windows.Forms.Padding(1)
        Me.btnUnitReview.Name = "btnUnitReview"
        Me.btnUnitReview.Size = New System.Drawing.Size(132, 32)
        Me.btnUnitReview.TabIndex = 19
        Me.btnUnitReview.Text = "Unit Review"
        Me.btnUnitReview.UseVisualStyleBackColor = True
        '
        'btnSpecFrac
        '
        Me.btnSpecFrac.Location = New System.Drawing.Point(653, 382)
        Me.btnSpecFrac.Margin = New System.Windows.Forms.Padding(1)
        Me.btnSpecFrac.Name = "btnSpecFrac"
        Me.btnSpecFrac.Size = New System.Drawing.Size(132, 32)
        Me.btnSpecFrac.TabIndex = 20
        Me.btnSpecFrac.Text = "Spec Frac"
        Me.btnSpecFrac.UseVisualStyleBackColor = True
        '
        'btnDrawings
        '
        Me.btnDrawings.Location = New System.Drawing.Point(653, 246)
        Me.btnDrawings.Margin = New System.Windows.Forms.Padding(1)
        Me.btnDrawings.Name = "btnDrawings"
        Me.btnDrawings.Size = New System.Drawing.Size(132, 32)
        Me.btnDrawings.TabIndex = 22
        Me.btnDrawings.Text = "Drawings"
        Me.btnDrawings.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(324, 129)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(64, 13)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Comments"
        '
        'txtComments
        '
        Me.txtComments.Location = New System.Drawing.Point(324, 145)
        Me.txtComments.Multiline = True
        Me.txtComments.Name = "txtComments"
        Me.txtComments.ReadOnly = True
        Me.txtComments.Size = New System.Drawing.Size(303, 271)
        Me.txtComments.TabIndex = 16
        '
        'btnPrintIssues
        '
        Me.btnPrintIssues.Location = New System.Drawing.Point(395, 83)
        Me.btnPrintIssues.Name = "btnPrintIssues"
        Me.btnPrintIssues.Size = New System.Drawing.Size(114, 23)
        Me.btnPrintIssues.TabIndex = 15
        Me.btnPrintIssues.Text = "Print Issues"
        Me.btnPrintIssues.UseVisualStyleBackColor = True
        '
        'btnViewWordFile
        '
        Me.btnViewWordFile.Location = New System.Drawing.Point(275, 83)
        Me.btnViewWordFile.Name = "btnViewWordFile"
        Me.btnViewWordFile.Size = New System.Drawing.Size(114, 23)
        Me.btnViewWordFile.TabIndex = 14
        Me.btnViewWordFile.Text = "View Word File"
        Me.btnViewWordFile.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoScroll = True
        Me.FlowLayoutPanel1.Controls.Add(Me.btnPA)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnRAM)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnVI)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnPT)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnSC)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCT)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(652, 20)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(134, 204)
        Me.FlowLayoutPanel1.TabIndex = 13
        Me.FlowLayoutPanel1.WrapContents = False
        '
        'btnPA
        '
        Me.btnPA.Enabled = False
        Me.btnPA.Location = New System.Drawing.Point(1, 1)
        Me.btnPA.Margin = New System.Windows.Forms.Padding(1)
        Me.btnPA.Name = "btnPA"
        Me.btnPA.Size = New System.Drawing.Size(132, 32)
        Me.btnPA.TabIndex = 4
        Me.btnPA.Text = "Gap File (PA)"
        Me.btnPA.UseVisualStyleBackColor = True
        '
        'btnRAM
        '
        Me.btnRAM.Enabled = False
        Me.btnRAM.Location = New System.Drawing.Point(1, 35)
        Me.btnRAM.Margin = New System.Windows.Forms.Padding(1)
        Me.btnRAM.Name = "btnRAM"
        Me.btnRAM.Size = New System.Drawing.Size(132, 32)
        Me.btnRAM.TabIndex = 5
        Me.btnRAM.Text = "RAM Gaps (RAM)"
        Me.btnRAM.UseVisualStyleBackColor = True
        '
        'btnVI
        '
        Me.btnVI.Enabled = False
        Me.btnVI.Location = New System.Drawing.Point(1, 69)
        Me.btnVI.Margin = New System.Windows.Forms.Padding(1)
        Me.btnVI.Name = "btnVI"
        Me.btnVI.Size = New System.Drawing.Size(132, 32)
        Me.btnVI.TabIndex = 6
        Me.btnVI.Text = "Validated Input (VI)"
        Me.btnVI.UseVisualStyleBackColor = True
        '
        'btnPT
        '
        Me.btnPT.Enabled = False
        Me.btnPT.Location = New System.Drawing.Point(1, 103)
        Me.btnPT.Margin = New System.Windows.Forms.Padding(1)
        Me.btnPT.Name = "btnPT"
        Me.btnPT.Size = New System.Drawing.Size(132, 32)
        Me.btnPT.TabIndex = 0
        Me.btnPT.Text = "Prelim Client Table (PT)"
        Me.btnPT.UseVisualStyleBackColor = True
        '
        'btnSC
        '
        Me.btnSC.Enabled = False
        Me.btnSC.Location = New System.Drawing.Point(1, 137)
        Me.btnSC.Margin = New System.Windows.Forms.Padding(1)
        Me.btnSC.Name = "btnSC"
        Me.btnSC.Size = New System.Drawing.Size(132, 32)
        Me.btnSC.TabIndex = 2
        Me.btnSC.Text = "Sum Calc (SC)"
        Me.btnSC.UseVisualStyleBackColor = True
        '
        'btnCT
        '
        Me.btnCT.Enabled = False
        Me.btnCT.Location = New System.Drawing.Point(1, 171)
        Me.btnCT.Margin = New System.Windows.Forms.Padding(1)
        Me.btnCT.Name = "btnCT"
        Me.btnCT.Size = New System.Drawing.Size(132, 32)
        Me.btnCT.TabIndex = 1
        Me.btnCT.Text = "Client Table (CT)"
        Me.btnCT.UseVisualStyleBackColor = True
        '
        'txtValidationIssues
        '
        Me.txtValidationIssues.Location = New System.Drawing.Point(15, 145)
        Me.txtValidationIssues.Multiline = True
        Me.txtValidationIssues.Name = "txtValidationIssues"
        Me.txtValidationIssues.ReadOnly = True
        Me.txtValidationIssues.Size = New System.Drawing.Size(303, 271)
        Me.txtValidationIssues.TabIndex = 12
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(12, 129)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(209, 13)
        Me.Label46.TabIndex = 11
        Me.Label46.Text = "Validation Issues/Presenter's Notes"
        '
        'lblItemsRemaining
        '
        Me.lblItemsRemaining.AutoSize = True
        Me.lblItemsRemaining.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItemsRemaining.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblItemsRemaining.Location = New System.Drawing.Point(272, 60)
        Me.lblItemsRemaining.Name = "lblItemsRemaining"
        Me.lblItemsRemaining.Size = New System.Drawing.Size(174, 13)
        Me.lblItemsRemaining.TabIndex = 10
        Me.lblItemsRemaining.Text = "10 Checklist Items Remaining"
        '
        'lblBlueFlags
        '
        Me.lblBlueFlags.AutoSize = True
        Me.lblBlueFlags.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBlueFlags.ForeColor = System.Drawing.Color.Navy
        Me.lblBlueFlags.Location = New System.Drawing.Point(272, 37)
        Me.lblBlueFlags.Name = "lblBlueFlags"
        Me.lblBlueFlags.Size = New System.Drawing.Size(91, 13)
        Me.lblBlueFlags.TabIndex = 9
        Me.lblBlueFlags.Text = "100 Blue Flags"
        '
        'lblRedFlags
        '
        Me.lblRedFlags.AutoSize = True
        Me.lblRedFlags.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRedFlags.ForeColor = System.Drawing.Color.Red
        Me.lblRedFlags.Location = New System.Drawing.Point(272, 14)
        Me.lblRedFlags.Name = "lblRedFlags"
        Me.lblRedFlags.Size = New System.Drawing.Size(82, 13)
        Me.lblRedFlags.TabIndex = 8
        Me.lblRedFlags.Text = "17 Red Flags"
        '
        'lblLastCalcDate
        '
        Me.lblLastCalcDate.AutoSize = True
        Me.lblLastCalcDate.Location = New System.Drawing.Point(105, 83)
        Me.lblLastCalcDate.Name = "lblLastCalcDate"
        Me.lblLastCalcDate.Size = New System.Drawing.Size(111, 13)
        Me.lblLastCalcDate.TabIndex = 7
        Me.lblLastCalcDate.Text = "6/8/2010 3:51:08 PM"
        '
        'lblLastUpload
        '
        Me.lblLastUpload.AutoSize = True
        Me.lblLastUpload.Location = New System.Drawing.Point(105, 60)
        Me.lblLastUpload.Name = "lblLastUpload"
        Me.lblLastUpload.Size = New System.Drawing.Size(111, 13)
        Me.lblLastUpload.TabIndex = 6
        Me.lblLastUpload.Text = "6/8/2010 3:51:08 PM"
        '
        'lblLastFileSave
        '
        Me.lblLastFileSave.AutoSize = True
        Me.lblLastFileSave.Location = New System.Drawing.Point(105, 37)
        Me.lblLastFileSave.Name = "lblLastFileSave"
        Me.lblLastFileSave.Size = New System.Drawing.Size(111, 13)
        Me.lblLastFileSave.TabIndex = 5
        Me.lblLastFileSave.Text = "6/8/2010 3:51:08 PM"
        '
        'lblValidationStatus
        '
        Me.lblValidationStatus.AutoSize = True
        Me.lblValidationStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidationStatus.Location = New System.Drawing.Point(105, 14)
        Me.lblValidationStatus.Name = "lblValidationStatus"
        Me.lblValidationStatus.Size = New System.Drawing.Size(63, 13)
        Me.lblValidationStatus.TabIndex = 4
        Me.lblValidationStatus.Text = "Validating"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(9, 60)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(67, 13)
        Me.Label38.TabIndex = 3
        Me.Label38.Text = "Last Upload:"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(9, 37)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(77, 13)
        Me.Label37.TabIndex = 2
        Me.Label37.Text = "Last File Save:"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(9, 83)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(80, 13)
        Me.Label36.TabIndex = 1
        Me.Label36.Text = "Last Calc Date:"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(9, 14)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(89, 13)
        Me.Label35.TabIndex = 0
        Me.Label35.Text = "Validation Status:"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.lblItemCount)
        Me.TabPage4.Controls.Add(Me.btnAddIssue)
        Me.TabPage4.Controls.Add(Me.cboCheckListView)
        Me.TabPage4.Controls.Add(Me.Label52)
        Me.TabPage4.Controls.Add(Me.txtCompletedOn)
        Me.TabPage4.Controls.Add(Me.txtCompletedBy)
        Me.TabPage4.Controls.Add(Me.txtIssueName)
        Me.TabPage4.Controls.Add(Me.txtIssueID)
        Me.TabPage4.Controls.Add(Me.Label51)
        Me.TabPage4.Controls.Add(Me.Label50)
        Me.TabPage4.Controls.Add(Me.Label49)
        Me.TabPage4.Controls.Add(Me.Label48)
        Me.TabPage4.Controls.Add(Me.Label47)
        Me.TabPage4.Controls.Add(Me.txtDescription)
        Me.TabPage4.Controls.Add(Me.ValCheckList)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(789, 419)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Checklist"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'lblItemCount
        '
        Me.lblItemCount.AutoSize = True
        Me.lblItemCount.Location = New System.Drawing.Point(6, 36)
        Me.lblItemCount.Name = "lblItemCount"
        Me.lblItemCount.Size = New System.Drawing.Size(63, 13)
        Me.lblItemCount.TabIndex = 26
        Me.lblItemCount.Text = "Issue Name"
        '
        'btnAddIssue
        '
        Me.btnAddIssue.Location = New System.Drawing.Point(194, 6)
        Me.btnAddIssue.Name = "btnAddIssue"
        Me.btnAddIssue.Size = New System.Drawing.Size(102, 23)
        Me.btnAddIssue.TabIndex = 25
        Me.btnAddIssue.Text = "Add New Issue"
        Me.btnAddIssue.UseVisualStyleBackColor = True
        '
        'cboCheckListView
        '
        Me.cboCheckListView.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCheckListView.FormattingEnabled = True
        Me.cboCheckListView.Items.AddRange(New Object() {"All", "Incomplete", "Complete"})
        Me.cboCheckListView.Location = New System.Drawing.Point(36, 8)
        Me.cboCheckListView.Name = "cboCheckListView"
        Me.cboCheckListView.Size = New System.Drawing.Size(152, 21)
        Me.cboCheckListView.TabIndex = 24
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(3, 11)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(30, 13)
        Me.Label52.TabIndex = 23
        Me.Label52.Text = "View"
        '
        'txtCompletedOn
        '
        Me.txtCompletedOn.Location = New System.Drawing.Point(639, 54)
        Me.txtCompletedOn.Name = "txtCompletedOn"
        Me.txtCompletedOn.Size = New System.Drawing.Size(142, 20)
        Me.txtCompletedOn.TabIndex = 22
        '
        'txtCompletedBy
        '
        Me.txtCompletedBy.Location = New System.Drawing.Point(548, 54)
        Me.txtCompletedBy.Name = "txtCompletedBy"
        Me.txtCompletedBy.Size = New System.Drawing.Size(69, 20)
        Me.txtCompletedBy.TabIndex = 21
        '
        'txtIssueName
        '
        Me.txtIssueName.Location = New System.Drawing.Point(303, 54)
        Me.txtIssueName.Name = "txtIssueName"
        Me.txtIssueName.Size = New System.Drawing.Size(230, 20)
        Me.txtIssueName.TabIndex = 20
        '
        'txtIssueID
        '
        Me.txtIssueID.Location = New System.Drawing.Point(197, 54)
        Me.txtIssueID.Name = "txtIssueID"
        Me.txtIssueID.Size = New System.Drawing.Size(100, 20)
        Me.txtIssueID.TabIndex = 19
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(636, 37)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(74, 13)
        Me.Label51.TabIndex = 18
        Me.Label51.Text = "Completed On"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(545, 37)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(72, 13)
        Me.Label50.TabIndex = 17
        Me.Label50.Text = "Completed By"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(300, 37)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(63, 13)
        Me.Label49.TabIndex = 16
        Me.Label49.Text = "Issue Name"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(194, 37)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(46, 13)
        Me.Label48.TabIndex = 15
        Me.Label48.Text = "Issue ID"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(194, 76)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(60, 13)
        Me.Label47.TabIndex = 14
        Me.Label47.Text = "Description"
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(194, 95)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ReadOnly = True
        Me.txtDescription.Size = New System.Drawing.Size(587, 318)
        Me.txtDescription.TabIndex = 13
        '
        'ValCheckList
        '
        Me.ValCheckList.FormattingEnabled = True
        Me.ValCheckList.HorizontalScrollbar = True
        Me.ValCheckList.Location = New System.Drawing.Point(0, 52)
        Me.ValCheckList.Name = "ValCheckList"
        Me.ValCheckList.Size = New System.Drawing.Size(188, 364)
        Me.ValCheckList.TabIndex = 0
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.btnTransPrice)
        Me.TabPage5.Controls.Add(Me.btnFLComp)
        Me.TabPage5.Controls.Add(Me.btnRefresh)
        Me.TabPage5.Controls.Add(Me.btnOpenValFax)
        Me.TabPage5.Controls.Add(Me.btnValFax)
        Me.TabPage5.Controls.Add(Me.btnBuildVRFile)
        Me.TabPage5.Controls.Add(Me.btnReceiptAck)
        Me.TabPage5.Controls.Add(Me.lstReturnFiles)
        Me.TabPage5.Controls.Add(Me.Label58)
        Me.TabPage5.Controls.Add(Me.lstVRFiles)
        Me.TabPage5.Controls.Add(Me.Label57)
        Me.TabPage5.Controls.Add(Me.lstVFFiles)
        Me.TabPage5.Controls.Add(Me.Label56)
        Me.TabPage5.Controls.Add(Me.lstVFNumbers)
        Me.TabPage5.Controls.Add(Me.Label55)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(789, 419)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Correspondence"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'btnTransPrice
        '
        Me.btnTransPrice.Location = New System.Drawing.Point(533, 202)
        Me.btnTransPrice.Name = "btnTransPrice"
        Me.btnTransPrice.Size = New System.Drawing.Size(96, 23)
        Me.btnTransPrice.TabIndex = 14
        Me.btnTransPrice.Text = "Trans Price"
        Me.btnTransPrice.UseVisualStyleBackColor = True
        Me.btnTransPrice.Visible = False
        '
        'btnFLComp
        '
        Me.btnFLComp.Location = New System.Drawing.Point(533, 173)
        Me.btnFLComp.Name = "btnFLComp"
        Me.btnFLComp.Size = New System.Drawing.Size(96, 23)
        Me.btnFLComp.TabIndex = 13
        Me.btnFLComp.Text = "FL Comp"
        Me.btnFLComp.UseVisualStyleBackColor = True
        Me.btnFLComp.Visible = False
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(708, 6)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(75, 36)
        Me.btnRefresh.TabIndex = 12
        Me.btnRefresh.Text = "Refresh this Screen"
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'btnOpenValFax
        '
        Me.btnOpenValFax.Location = New System.Drawing.Point(533, 78)
        Me.btnOpenValFax.Name = "btnOpenValFax"
        Me.btnOpenValFax.Size = New System.Drawing.Size(96, 50)
        Me.btnOpenValFax.TabIndex = 11
        Me.btnOpenValFax.Text = "Open My Val Fax"
        Me.btnOpenValFax.UseVisualStyleBackColor = True
        '
        'btnValFax
        '
        Me.btnValFax.Location = New System.Drawing.Point(533, 24)
        Me.btnValFax.Name = "btnValFax"
        Me.btnValFax.Size = New System.Drawing.Size(96, 50)
        Me.btnValFax.TabIndex = 10
        Me.btnValFax.Text = "New Validation Fax"
        Me.btnValFax.UseVisualStyleBackColor = True
        '
        'btnBuildVRFile
        '
        Me.btnBuildVRFile.Location = New System.Drawing.Point(196, 113)
        Me.btnBuildVRFile.Name = "btnBuildVRFile"
        Me.btnBuildVRFile.Size = New System.Drawing.Size(122, 23)
        Me.btnBuildVRFile.TabIndex = 9
        Me.btnBuildVRFile.Text = "Build a VR File"
        Me.btnBuildVRFile.UseVisualStyleBackColor = True
        '
        'btnReceiptAck
        '
        Me.btnReceiptAck.Location = New System.Drawing.Point(62, 113)
        Me.btnReceiptAck.Name = "btnReceiptAck"
        Me.btnReceiptAck.Size = New System.Drawing.Size(127, 23)
        Me.btnReceiptAck.TabIndex = 8
        Me.btnReceiptAck.Text = "Receipt Acknowledged"
        Me.btnReceiptAck.UseVisualStyleBackColor = True
        '
        'lstReturnFiles
        '
        Me.lstReturnFiles.ColumnWidth = 300
        Me.lstReturnFiles.FormattingEnabled = True
        Me.lstReturnFiles.Items.AddRange(New Object() {"File Name 1.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/1/2011", "File Name 2.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/2/2011", "Really really long file name that should never be used.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/3/2011"})
        Me.lstReturnFiles.Location = New System.Drawing.Point(62, 286)
        Me.lstReturnFiles.Name = "lstReturnFiles"
        Me.lstReturnFiles.Size = New System.Drawing.Size(465, 82)
        Me.lstReturnFiles.TabIndex = 7
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(59, 269)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(63, 13)
        Me.Label58.TabIndex = 6
        Me.Label58.Text = "Return Files"
        '
        'lstVRFiles
        '
        Me.lstVRFiles.ColumnWidth = 300
        Me.lstVRFiles.FormattingEnabled = True
        Me.lstVRFiles.Items.AddRange(New Object() {"File Name 1.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/1/2011", "File Name 2.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/2/2011", "Really really long file name that should never be used.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/3/2011"})
        Me.lstVRFiles.Location = New System.Drawing.Point(62, 173)
        Me.lstVRFiles.Name = "lstVRFiles"
        Me.lstVRFiles.Size = New System.Drawing.Size(465, 82)
        Me.lstVRFiles.TabIndex = 5
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(59, 156)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(142, 13)
        Me.Label57.TabIndex = 4
        Me.Label57.Text = "Validation Reply Files (VR*.*)"
        '
        'lstVFFiles
        '
        Me.lstVFFiles.ColumnWidth = 300
        Me.lstVFFiles.FormattingEnabled = True
        Me.lstVFFiles.Items.AddRange(New Object() {"File Name 1.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/1/2011", "File Name 2.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/2/2011", "Really really long file name that should never be used.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/3/2011"})
        Me.lstVFFiles.Location = New System.Drawing.Point(62, 24)
        Me.lstVFFiles.Name = "lstVFFiles"
        Me.lstVFFiles.Size = New System.Drawing.Size(465, 82)
        Me.lstVFFiles.TabIndex = 3
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(59, 7)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(130, 13)
        Me.Label56.TabIndex = 2
        Me.Label56.Text = "Validation Fax Files (VF*.*)"
        '
        'lstVFNumbers
        '
        Me.lstVFNumbers.FormattingEnabled = True
        Me.lstVFNumbers.Location = New System.Drawing.Point(12, 24)
        Me.lstVFNumbers.Name = "lstVFNumbers"
        Me.lstVFNumbers.Size = New System.Drawing.Size(41, 342)
        Me.lstVFNumbers.Sorted = True
        Me.lstVFNumbers.TabIndex = 1
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.Location = New System.Drawing.Point(9, 7)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(30, 13)
        Me.Label55.TabIndex = 0
        Me.Label55.Text = "VF#"
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.Panel5)
        Me.TabPage6.Controls.Add(Me.dgValidator)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(789, 419)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Validator"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(3, 3)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(783, 76)
        Me.Panel5.TabIndex = 2
        '
        'dgValidator
        '
        Me.dgValidator.AllowUserToAddRows = False
        Me.dgValidator.AllowUserToDeleteRows = False
        Me.dgValidator.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgValidator.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgValidator.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgValidator.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.dgValidator.Location = New System.Drawing.Point(3, 76)
        Me.dgValidator.Name = "dgValidator"
        Me.dgValidator.ReadOnly = True
        Me.dgValidator.Size = New System.Drawing.Size(783, 340)
        Me.dgValidator.TabIndex = 0
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.txtConsultingOpps)
        Me.TabPage7.Controls.Add(Me.txtCurrentIssues)
        Me.TabPage7.Controls.Add(Me.Label54)
        Me.TabPage7.Controls.Add(Me.Label53)
        Me.TabPage7.Location = New System.Drawing.Point(4, 22)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage7.Size = New System.Drawing.Size(789, 419)
        Me.TabPage7.TabIndex = 6
        Me.TabPage7.Text = "Issues"
        Me.TabPage7.UseVisualStyleBackColor = True
        '
        'txtConsultingOpps
        '
        Me.txtConsultingOpps.Location = New System.Drawing.Point(12, 229)
        Me.txtConsultingOpps.Multiline = True
        Me.txtConsultingOpps.Name = "txtConsultingOpps"
        Me.txtConsultingOpps.Size = New System.Drawing.Size(769, 187)
        Me.txtConsultingOpps.TabIndex = 15
        '
        'txtCurrentIssues
        '
        Me.txtCurrentIssues.Location = New System.Drawing.Point(12, 23)
        Me.txtCurrentIssues.Multiline = True
        Me.txtCurrentIssues.Name = "txtCurrentIssues"
        Me.txtCurrentIssues.Size = New System.Drawing.Size(769, 187)
        Me.txtCurrentIssues.TabIndex = 14
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.Location = New System.Drawing.Point(9, 213)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(145, 13)
        Me.Label54.TabIndex = 1
        Me.Label54.Text = "Consulting Opportunities"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label53.Location = New System.Drawing.Point(9, 7)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(107, 13)
        Me.Label53.TabIndex = 0
        Me.Label53.Text = "Continuing Issues"
        '
        'TabPage8
        '
        Me.TabPage8.Controls.Add(Me.ETCorrespondence)
        Me.TabPage8.Location = New System.Drawing.Point(4, 22)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage8.Size = New System.Drawing.Size(789, 419)
        Me.TabPage8.TabIndex = 7
        Me.TabPage8.Text = "Correspondence Folder"
        Me.TabPage8.UseVisualStyleBackColor = True
        '
        'ETCorrespondence
        '
        Me.ETCorrespondence.AllowDrop = True
        Me.ETCorrespondence.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ETCorrespondence.Location = New System.Drawing.Point(3, 3)
        Me.ETCorrespondence.Name = "ETCorrespondence"
        Me.ETCorrespondence.RootPath = "K:\STUDY"
        Me.ETCorrespondence.Size = New System.Drawing.Size(783, 413)
        Me.ETCorrespondence.TabIndex = 0
        '
        'TabPage9
        '
        Me.TabPage9.Controls.Add(Me.ETCompany)
        Me.TabPage9.Location = New System.Drawing.Point(4, 22)
        Me.TabPage9.Name = "TabPage9"
        Me.TabPage9.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage9.Size = New System.Drawing.Size(789, 419)
        Me.TabPage9.TabIndex = 8
        Me.TabPage9.Text = "Company Folder"
        Me.TabPage9.UseVisualStyleBackColor = True
        '
        'ETCompany
        '
        Me.ETCompany.AllowDrop = True
        Me.ETCompany.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ETCompany.Location = New System.Drawing.Point(3, 3)
        Me.ETCompany.Name = "ETCompany"
        Me.ETCompany.RootPath = "K:\STUDY"
        Me.ETCompany.Size = New System.Drawing.Size(783, 413)
        Me.ETCompany.TabIndex = 0
        '
        'TabPage10
        '
        Me.TabPage10.Controls.Add(Me.ETDrawings)
        Me.TabPage10.Location = New System.Drawing.Point(4, 22)
        Me.TabPage10.Name = "TabPage10"
        Me.TabPage10.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage10.Size = New System.Drawing.Size(789, 419)
        Me.TabPage10.TabIndex = 9
        Me.TabPage10.Text = "Drawings Folder"
        Me.TabPage10.UseVisualStyleBackColor = True
        '
        'ETDrawings
        '
        Me.ETDrawings.AllowDrop = True
        Me.ETDrawings.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ETDrawings.Location = New System.Drawing.Point(3, 3)
        Me.ETDrawings.Name = "ETDrawings"
        Me.ETDrawings.RootPath = "K:\STUDY"
        Me.ETDrawings.Size = New System.Drawing.Size(783, 413)
        Me.ETDrawings.TabIndex = 1
        '
        'cboStudy
        '
        Me.cboStudy.FormattingEnabled = True
        Me.cboStudy.Location = New System.Drawing.Point(254, 25)
        Me.cboStudy.Name = "cboStudy"
        Me.cboStudy.Size = New System.Drawing.Size(90, 21)
        Me.cboStudy.TabIndex = 1
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(205, 26)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(44, 17)
        Me.Label59.TabIndex = 21
        Me.Label59.Text = "Study"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.Location = New System.Drawing.Point(189, 57)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(59, 17)
        Me.Label60.TabIndex = 23
        Me.Label60.Text = "RefNum"
        '
        'cboRefNum
        '
        Me.cboRefNum.FormattingEnabled = True
        Me.cboRefNum.Location = New System.Drawing.Point(254, 57)
        Me.cboRefNum.Name = "cboRefNum"
        Me.cboRefNum.Size = New System.Drawing.Size(342, 21)
        Me.cboRefNum.TabIndex = 22
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(27, 25)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(135, 96)
        Me.PictureBox1.TabIndex = 24
        Me.PictureBox1.TabStop = False
        '
        'lblWarning
        '
        Me.lblWarning.AutoSize = True
        Me.lblWarning.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWarning.ForeColor = System.Drawing.Color.Red
        Me.lblWarning.Location = New System.Drawing.Point(350, 28)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(108, 13)
        Me.lblWarning.TabIndex = 25
        Me.lblWarning.Text = "Not Current Study"
        Me.lblWarning.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(183, 90)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(66, 17)
        Me.Label8.TabIndex = 27
        Me.Label8.Text = "Assigned"
        '
        'cboConsultant
        '
        Me.cboConsultant.FormattingEnabled = True
        Me.cboConsultant.Location = New System.Drawing.Point(255, 90)
        Me.cboConsultant.Name = "cboConsultant"
        Me.cboConsultant.Size = New System.Drawing.Size(90, 21)
        Me.cboConsultant.TabIndex = 26
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(372, 90)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(75, 23)
        Me.btnUpdate.TabIndex = 28
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = True
        Me.btnUpdate.Visible = False
        '
        'btnValidate
        '
        Me.btnValidate.Location = New System.Drawing.Point(453, 90)
        Me.btnValidate.Name = "btnValidate"
        Me.btnValidate.Size = New System.Drawing.Size(75, 23)
        Me.btnValidate.TabIndex = 29
        Me.btnValidate.Text = "Validate"
        Me.btnValidate.UseVisualStyleBackColor = True
        '
        'chkJustLooking
        '
        Me.chkJustLooking.AutoSize = True
        Me.chkJustLooking.Location = New System.Drawing.Point(27, 2)
        Me.chkJustLooking.Name = "chkJustLooking"
        Me.chkJustLooking.Size = New System.Drawing.Size(86, 17)
        Me.chkJustLooking.TabIndex = 30
        Me.chkJustLooking.Text = "Just Looking"
        Me.chkJustLooking.UseVisualStyleBackColor = True
        '
        'lblInHolding
        '
        Me.lblInHolding.AutoSize = True
        Me.lblInHolding.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInHolding.ForeColor = System.Drawing.Color.Red
        Me.lblInHolding.Location = New System.Drawing.Point(463, 116)
        Me.lblInHolding.Name = "lblInHolding"
        Me.lblInHolding.Size = New System.Drawing.Size(65, 13)
        Me.lblInHolding.TabIndex = 31
        Me.lblInHolding.Text = "In Holding"
        Me.lblInHolding.Visible = False
        '
        'btnHelp
        '
        Me.btnHelp.Location = New System.Drawing.Point(763, 4)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(27, 23)
        Me.btnHelp.TabIndex = 32
        Me.btnHelp.Text = "&?"
        Me.btnHelp.UseVisualStyleBackColor = True
        '
        'btnDensity
        '
        Me.btnDensity.Location = New System.Drawing.Point(714, 33)
        Me.btnDensity.Name = "btnDensity"
        Me.btnDensity.Size = New System.Drawing.Size(75, 23)
        Me.btnDensity.TabIndex = 33
        Me.btnDensity.Text = "Density Calc"
        Me.btnDensity.UseVisualStyleBackColor = True
        '
        'MainConsole
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(797, 594)
        Me.Controls.Add(Me.btnDensity)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.lblInHolding)
        Me.Controls.Add(Me.chkJustLooking)
        Me.Controls.Add(Me.btnValidate)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.cboConsultant)
        Me.Controls.Add(Me.lblWarning)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label60)
        Me.Controls.Add(Me.cboRefNum)
        Me.Controls.Add(Me.Label59)
        Me.Controls.Add(Me.cboStudy)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "MainConsole"
        Me.Text = "Refining Validation Console"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        CType(Me.dgValidator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage7.ResumeLayout(False)
        Me.TabPage7.PerformLayout()
        Me.TabPage8.ResumeLayout(False)
        Me.TabPage9.ResumeLayout(False)
        Me.TabPage10.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents lblInterimPhone As System.Windows.Forms.Label
    Friend WithEvents lblInterimEmail As System.Windows.Forms.Label
    Friend WithEvents lblInterimName As System.Windows.Forms.Label
    Friend WithEvents lblAltCoCoPhone As System.Windows.Forms.Label
    Friend WithEvents lblAltCoCoEmail As System.Windows.Forms.Label
    Friend WithEvents lblAltCoCoName As System.Windows.Forms.Label
    Friend WithEvents lblCoCoPhone As System.Windows.Forms.Label
    Friend WithEvents lblCoCoEmail As System.Windows.Forms.Label
    Friend WithEvents lblCoCoName As System.Windows.Forms.Label
    Friend WithEvents btnShowIntCo As System.Windows.Forms.Button
    Friend WithEvents btnShowAltCo As System.Windows.Forms.Button
    Friend WithEvents btnShowCoCo As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCorrNotes As System.Windows.Forms.TextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblRefMgrEmail As System.Windows.Forms.Label
    Friend WithEvents lblRefMgrName As System.Windows.Forms.Label
    Friend WithEvents lblRefAltPhone As System.Windows.Forms.Label
    Friend WithEvents lblRefAltEmail As System.Windows.Forms.Label
    Friend WithEvents lblRefAltName As System.Windows.Forms.Label
    Friend WithEvents lblRefCoPhone As System.Windows.Forms.Label
    Friend WithEvents lblRefCoEmail As System.Windows.Forms.Label
    Friend WithEvents lblRefCoName As System.Windows.Forms.Label
    Friend WithEvents btnShowRefAltCo As System.Windows.Forms.Button
    Friend WithEvents btnShowRefCo As System.Windows.Forms.Button
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lblReturnFilePassword As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents lblTechMgrEmail As System.Windows.Forms.Label
    Friend WithEvents lblTechMgrName As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents lblMaintMgrEmail As System.Windows.Forms.Label
    Friend WithEvents lblMaintMgrName As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents lblOpsMgrEmail As System.Windows.Forms.Label
    Friend WithEvents lblOpsMgrName As System.Windows.Forms.Label
    Friend WithEvents lblGenPhone As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents lblLastCalcDate As System.Windows.Forms.Label
    Friend WithEvents lblLastUpload As System.Windows.Forms.Label
    Friend WithEvents lblLastFileSave As System.Windows.Forms.Label
    Friend WithEvents lblValidationStatus As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents lblItemsRemaining As System.Windows.Forms.Label
    Friend WithEvents lblBlueFlags As System.Windows.Forms.Label
    Friend WithEvents lblRedFlags As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtValidationIssues As System.Windows.Forms.TextBox
    Friend WithEvents btnPT As System.Windows.Forms.Button
    Friend WithEvents btnCT As System.Windows.Forms.Button
    Friend WithEvents btnSC As System.Windows.Forms.Button
    Friend WithEvents btnPA As System.Windows.Forms.Button
    Friend WithEvents btnRAM As System.Windows.Forms.Button
    Friend WithEvents btnVI As System.Windows.Forms.Button
    Friend WithEvents btnPrintIssues As System.Windows.Forms.Button
    Friend WithEvents btnViewWordFile As System.Windows.Forms.Button
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents ValCheckList As System.Windows.Forms.CheckedListBox
    Friend WithEvents cboCheckListView As System.Windows.Forms.ComboBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents txtCompletedOn As System.Windows.Forms.TextBox
    Friend WithEvents txtCompletedBy As System.Windows.Forms.TextBox
    Friend WithEvents txtIssueName As System.Windows.Forms.TextBox
    Friend WithEvents txtIssueID As System.Windows.Forms.TextBox
    Friend WithEvents btnAddIssue As System.Windows.Forms.Button
    Friend WithEvents txtConsultingOpps As System.Windows.Forms.TextBox
    Friend WithEvents txtCurrentIssues As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents lstVFFiles As System.Windows.Forms.ListBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents lstVFNumbers As System.Windows.Forms.ListBox
    Friend WithEvents lstVRFiles As System.Windows.Forms.ListBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents btnBuildVRFile As System.Windows.Forms.Button
    Friend WithEvents btnReceiptAck As System.Windows.Forms.Button
    Friend WithEvents lstReturnFiles As System.Windows.Forms.ListBox
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents btnOpenValFax As System.Windows.Forms.Button
    Friend WithEvents btnValFax As System.Windows.Forms.Button
    Friend WithEvents dgValidator As System.Windows.Forms.DataGridView
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents cboStudy As System.Windows.Forms.ComboBox
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents cboRefNum As System.Windows.Forms.ComboBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cboConsultant As System.Windows.Forms.ComboBox
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents btnValidate As System.Windows.Forms.Button
    Friend WithEvents chkJustLooking As System.Windows.Forms.CheckBox
    Friend WithEvents TabPage8 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage9 As System.Windows.Forms.TabPage
    Friend WithEvents lblCompanyPassword As System.Windows.Forms.Label
    Friend WithEvents cboSendFormat As System.Windows.Forms.ComboBox
    Friend WithEvents lblInHolding As System.Windows.Forms.Label
    Friend WithEvents txtComments As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblItemCount As System.Windows.Forms.Label
    Friend WithEvents btnTransPrice As System.Windows.Forms.Button
    Friend WithEvents btnFLComp As System.Windows.Forms.Button
    Friend WithEvents ETCorrespondence As TreeViewFileBrowserPart2.ExplorerTree
    Friend WithEvents ETCompany As TreeViewFileBrowserPart2.ExplorerTree
    Friend WithEvents btnHelp As System.Windows.Forms.Button
    Friend WithEvents btnDensity As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnHYC As System.Windows.Forms.Button
    Friend WithEvents btnLog As System.Windows.Forms.Button
    Friend WithEvents btnUnitReview As System.Windows.Forms.Button
    Friend WithEvents btnSpecFrac As System.Windows.Forms.Button
    Friend WithEvents btnDrawings As System.Windows.Forms.Button
    Friend WithEvents TabPage10 As System.Windows.Forms.TabPage
    Friend WithEvents ETDrawings As TreeViewFileBrowserPart2.ExplorerTree
End Class
