﻿Public Class Contacts
    Private _CoCoFirstName As String
    Dim CoCoName As String

    Public Property CoCoFirstName() As String
        Get
            Return _CoCoFirstName
        End Get
        Set(ByVal value As String)
            _CoCoFirstName = value
        End Set
    End Property
    Private _CoCoLastName As String
    Public Property CoCoLastName() As String
        Get
            Return _CoCoLastName
        End Get
        Set(ByVal value As String)
            _CoCoLastName = value
        End Set
    End Property

    Private _CoCoEmail As String
    Public Property CoCoEmail() As String
        Get
            Return _CoCoEmail
        End Get
        Set(ByVal value As String)
            _CoCoEmail = value
        End Set
    End Property

    Private _CoCoPhone As String
    Public Property CoCoPhone() As String
        Get
            Return _CoCoPhone
        End Get
        Set(ByVal value As String)
            _CoCoPhone = value
        End Set
    End Property


    Private _CoCoAltFirstName As String
    Public Property CoCoAltFirstName() As String
        Get
            Return _CoCoAltFirstName
        End Get
        Set(ByVal value As String)
            _CoCoAltFirstName = value
        End Set
    End Property


    Private _CoCoAltLastName As String
    Public Property CoCoAltLastName() As String
        Get
            Return _CoCoAltLastName
        End Get
        Set(ByVal value As String)
            _CoCoAltLastName = value
        End Set
    End Property

    Private _CoCoAltEmail As String
    Public Property CoCoAltEmail() As String
        Get
            Return _CoCoAltEmail
        End Get
        Set(ByVal value As String)
            _CoCoAltEmail = value
        End Set
    End Property

    Private _CoCoAltPhone As String
    Public Property CoCoAltPhone() As String
        Get
            Return _CoCoAltPhone
        End Get
        Set(ByVal value As String)
            _CoCoAltPhone = value
        End Set
    End Property

    Private _CoCoMailAdd1 As String
    Public Property CoCoMailAdd1() As String
        Get
            Return _CoCoMailAdd1
        End Get
        Set(ByVal value As String)
            _CoCoMailAdd1 = value
        End Set
    End Property

    Private _CoCoMailAdd2 As String
    Public Property CoCoMailAdd2() As String
        Get
            Return _CoCoMailAdd2
        End Get
        Set(ByVal value As String)
            _CoCoMailAdd2 = value
        End Set
    End Property

    Private _CoCoMailAdd3 As String
    Public Property CoCoMailAdd3() As String
        Get
            Return _CoCoMailAdd3
        End Get
        Set(ByVal value As String)
            _CoCoMailAdd3 = value
        End Set
    End Property


    Private _CoCoMailAdd4 As String
    Public Property CoCoMailAdd4() As String
        Get
            Return _CoCoMailAdd4
        End Get
        Set(ByVal value As String)
            _CoCoMailAdd4 = value
        End Set
    End Property


    Private _CoCoMailAdd5 As String
    Public Property CoCoMailAdd5() As String
        Get
            Return _CoCoMailAdd5
        End Get
        Set(ByVal value As String)
            _CoCoMailAdd5 = value
        End Set
    End Property

    Private _CoCoMailAdd6 As String
    Public Property CoCoMailAdd6() As String
        Get
            Return _CoCoMailAdd6
        End Get
        Set(ByVal value As String)
            _CoCoMailAdd6 = value
        End Set
    End Property

    Private _CoCoMailAdd7 As String
    Public Property CoCoMailAdd7() As String
        Get
            Return _CoCoMailAdd7
        End Get
        Set(ByVal value As String)
            _CoCoMailAdd7 = value
        End Set
    End Property

    Private _CoCoStreetAdd1 As String
    Public Property CoCoStreetAdd1() As String
        Get
            Return _CoCoStreetAdd1
        End Get
        Set(ByVal value As String)
            _CoCoStreetAdd1 = value
        End Set
    End Property

    Private _CoCoStreetAdd2 As String
    Public Property CoCoStreetAdd2() As String
        Get
            Return _CoCoStreetAdd2
        End Get
        Set(ByVal value As String)
            _CoCoStreetAdd2 = value
        End Set
    End Property

    Private _CoCoStreetAdd3 As String
    Public Property CoCoStreetAdd3() As String
        Get
            Return _CoCoStreetAdd3
        End Get
        Set(ByVal value As String)
            _CoCoStreetAdd3 = value
        End Set
    End Property


    Private _CoCoStreetAdd4 As String
    Public Property CoCoStreetAdd4() As String
        Get
            Return _CoCoStreetAdd4
        End Get
        Set(ByVal value As String)
            _CoCoStreetAdd4 = value
        End Set
    End Property


    Private _CoCoStreetAdd5 As String
    Public Property CoCoStreetAdd5() As String
        Get
            Return _CoCoStreetAdd5
        End Get
        Set(ByVal value As String)
            _CoCoStreetAdd5 = value
        End Set
    End Property

    Private _CoCoStreetAdd6 As String
    Public Property CoCoStreetAdd6() As String
        Get
            Return _CoCoStreetAdd6
        End Get
        Set(ByVal value As String)
            _CoCoStreetAdd6 = value
        End Set
    End Property
    Private _CoCoStreetAdd7 As String
    Public Property CoCoStreetAdd7() As String
        Get
            Return _CoCoStreetAdd7
        End Get
        Set(ByVal value As String)
            _CoCoStreetAdd7 = value
        End Set
    End Property

    Private _CoCoTitle As String
    Public Property CoCoTitle() As String
        Get
            Return _CoCoTitle
        End Get
        Set(ByVal value As String)
            _CoCoTitle = value
        End Set
    End Property


    Private _CoCoFax As String
    Public Property CoCoFax() As String
        Get
            Return _CoCoFax
        End Get
        Set(ByVal value As String)
            _CoCoFax = value
        End Set
    End Property

    Private _CoCoFax2 As String
    Public Property CoCoFax2() As String
        Get
            Return _CoCoFax2
        End Get
        Set(ByVal value As String)
            _CoCoFax2 = value
        End Set
    End Property

    Private _CoCoPassword As String
    Public Property CoCoPassword() As String
        Get
            Return _CoCoPassword
        End Get
        Set(ByVal value As String)
            _CoCoPassword = value
        End Set
    End Property

    Private _CoCoSendMethod As String
    Public Property CoCoSendMethod() As String
        Get
            Return _CoCoSendMethod
        End Get
        Set(ByVal value As String)
            _CoCoSendMethod = value
        End Set
    End Property
    Public Sub Clear()

        CoCoFirstName = ""
        CoCoLastName = ""
        CoCoEmail = ""
        CoCoPhone = ""
        CoCoAltFirstName = ""
        CoCoAltLastName = ""
        CoCoAltEmail = ""
        CoCoAltPhone = ""
        CoCoFax = ""
        CoCoMailAdd1 = ""
        CoCoMailAdd2 = ""
        CoCoMailAdd3 = ""
        CoCoMailAdd4 = ""
        CoCoMailAdd5 = ""
        CoCoMailAdd6 = ""
        CoCoMailAdd7 = ""
        CoCoStreetAdd1 = ""
        CoCoStreetAdd2 = ""
        CoCoStreetAdd3 = ""
        CoCoStreetAdd4 = ""
        CoCoStreetAdd5 = ""
        CoCoStreetAdd6 = ""
        CoCoTitle = ""
        CoCoPassword = ""
        CoCoSendMethod = ""

    End Sub


End Class
Public Class OlefinsCompanyContact
    Private _FirstName As String
    Public Property FirstName() As String
        Get
            Return _FirstName
        End Get
        Set(ByVal value As String)
            _FirstName = value
        End Set
    End Property
    Private _LastName As String
    Public Property LastName() As String
        Get
            Return _LastName
        End Get
        Set(ByVal value As String)
            _LastName = value
        End Set
    End Property

    Private _Email As String
    Public Property Email() As String
        Get
            Return _Email
        End Get
        Set(ByVal value As String)
            _Email = value
        End Set
    End Property

    Private _Phone As String
    Public Property Phone() As String
        Get
            Return _Phone
        End Get
        Set(ByVal value As String)
            _Phone = value
        End Set
    End Property


    Private _AltName As String
    Public Property AltName() As String
        Get
            Return _AltName
        End Get
        Set(ByVal value As String)
            _AltName = value
        End Set
    End Property

    Private _AltEmail As String
    Public Property AltEmail() As String
        Get
            Return _AltEmail
        End Get
        Set(ByVal value As String)
            _AltEmail = value
        End Set
    End Property

    Private _AltPhone As String
    Public Property AltPhone() As String
        Get
            Return _AltPhone
        End Get
        Set(ByVal value As String)
            _AltPhone = value
        End Set
    End Property

    Private _MailAdd1 As String
    Public Property MailAdd1() As String
        Get
            Return _MailAdd1
        End Get
        Set(ByVal value As String)
            _MailAdd1 = value
        End Set
    End Property

    Private _MailAdd2 As String
    Public Property MailAdd2() As String
        Get
            Return _MailAdd2
        End Get
        Set(ByVal value As String)
            _MailAdd2 = value
        End Set
    End Property

    Private _MailAdd3 As String
    Public Property MailAdd3() As String
        Get
            Return _MailAdd3
        End Get
        Set(ByVal value As String)
            _MailAdd3 = value
        End Set
    End Property


    Private _MailAdd4 As String
    Public Property MailAdd4() As String
        Get
            Return _MailAdd4
        End Get
        Set(ByVal value As String)
            _MailAdd4 = value
        End Set
    End Property


    Private _MailAdd5 As String
    Public Property MailAdd5() As String
        Get
            Return _MailAdd5
        End Get
        Set(ByVal value As String)
            _MailAdd5 = value
        End Set
    End Property

    Private _MailAdd6 As String
    Public Property MailAdd6() As String
        Get
            Return _MailAdd6
        End Get
        Set(ByVal value As String)
            _MailAdd6 = value
        End Set
    End Property

    Private _MailAdd7 As String
    Public Property MailAdd7() As String
        Get
            Return _MailAdd7
        End Get
        Set(ByVal value As String)
            _MailAdd7 = value
        End Set
    End Property

    Private _StreetAdd1 As String
    Public Property StreetAdd1() As String
        Get
            Return _StreetAdd1
        End Get
        Set(ByVal value As String)
            _StreetAdd1 = value
        End Set
    End Property

    Private _StreetAdd2 As String
    Public Property StreetAdd2() As String
        Get
            Return _StreetAdd2
        End Get
        Set(ByVal value As String)
            _StreetAdd2 = value
        End Set
    End Property

    Private _StreetAdd3 As String
    Public Property StreetAdd3() As String
        Get
            Return _StreetAdd3
        End Get
        Set(ByVal value As String)
            _StreetAdd3 = value
        End Set
    End Property


    Private _StreetAdd4 As String
    Public Property StreetAdd4() As String
        Get
            Return _StreetAdd4
        End Get
        Set(ByVal value As String)
            _StreetAdd4 = value
        End Set
    End Property


    Private _StreetAdd5 As String
    Public Property StreetAdd5() As String
        Get
            Return _StreetAdd5
        End Get
        Set(ByVal value As String)
            _StreetAdd5 = value
        End Set
    End Property

    Private _StreetAdd6 As String
    Public Property StreetAdd6() As String
        Get
            Return _StreetAdd6
        End Get
        Set(ByVal value As String)
            _StreetAdd6 = value
        End Set
    End Property


    Private _Title As String
    Public Property Title() As String
        Get
            Return _Title
        End Get
        Set(ByVal value As String)
            _Title = value
        End Set
    End Property


    Private _Fax As String
    Public Property Fax() As String
        Get
            Return _Fax
        End Get
        Set(ByVal value As String)
            _Fax = value
        End Set
    End Property

    Private _Fax2 As String
    Public Property Fax2() As String
        Get
            Return _Fax2
        End Get
        Set(ByVal value As String)
            _Fax2 = value
        End Set
    End Property

    Private _Password As String
    Public Property Password() As String
        Get
            Return _Password
        End Get
        Set(ByVal value As String)
            _Password = value
        End Set
    End Property

    Private _SendMethod As String
    Public Property SendMethod() As String
        Get
            Return _SendMethod
        End Get
        Set(ByVal value As String)
            _SendMethod = value
        End Set
    End Property
    Public Sub Clear()

        FirstName = ""
        LastName = ""
        Email = ""
        Phone = ""
        AltName = ""
        AltEmail = ""
        AltPhone = ""
        Fax = ""
        MailAdd1 = ""
        MailAdd2 = ""
        MailAdd3 = ""
        MailAdd4 = ""
        MailAdd5 = ""
        MailAdd6 = ""
        MailAdd7 = ""
        StreetAdd1 = ""
        StreetAdd2 = ""
        StreetAdd3 = ""
        StreetAdd4 = ""
        StreetAdd5 = ""
        StreetAdd6 = ""
        Title = ""
        Password = ""
        SendMethod = ""

    End Sub


End Class
Public Class RefineryContact
    Private _CoName As String
    Public Property CoName() As String
        Get
            Return _CoName
        End Get
        Set(ByVal value As String)
            _CoName = value
        End Set
    End Property

    Private _CoTitle As String
    Public Property CoTitle() As String
        Get
            Return _CoTitle
        End Get
        Set(ByVal value As String)
            _CoTitle = value
        End Set
    End Property

    Private _CoPhone As String
    Public Property CoPhone() As String
        Get
            Return _CoPhone
        End Get
        Set(ByVal value As String)
            _CoPhone = value
        End Set
    End Property

    Private _CoEmail As String
    Public Property CoEmail() As String
        Get
            Return _CoEmail
        End Get
        Set(ByVal value As String)
            _CoEmail = value
        End Set
    End Property

    Private _CoFax As String
    Public Property CoFax() As String
        Get
            Return _CoFax
        End Get
        Set(ByVal value As String)
            _CoFax = value
        End Set
    End Property

    Private _CoName2 As String
    Public Property CoName2() As String
        Get
            Return _CoName2
        End Get
        Set(ByVal value As String)
            _CoName2 = value
        End Set
    End Property

    Private _CoTitle2 As String
    Public Property CoTitle2() As String
        Get
            Return _CoTitle2
        End Get
        Set(ByVal value As String)
            _CoTitle2 = value
        End Set
    End Property

    Private _CoEmail2 As String
    Public Property CoEmail2() As String
        Get
            Return _CoEmail2
        End Get
        Set(ByVal value As String)
            _CoEmail2 = value
        End Set
    End Property

    Private _CoPhone2 As String
    Public Property CoPhone2() As String
        Get
            Return _CoPhone2
        End Get
        Set(ByVal value As String)
            _CoPhone2 = value
        End Set
    End Property

    Private _CoFax2 As String
    Public Property CoFax2() As String
        Get
            Return _CoFax2
        End Get
        Set(ByVal value As String)
            _CoFax2 = value
        End Set
    End Property


    Private _MaintMgrName As String
    Public Property MaintMgrName() As String
        Get
            Return _MaintMgrName
        End Get
        Set(ByVal value As String)
            _MaintMgrName = value
        End Set
    End Property

    Private _TechMgrName As String
    Public Property TechMgrName() As String
        Get
            Return _TechMgrName
        End Get
        Set(ByVal value As String)
            _TechMgrName = value
        End Set
    End Property

    Private _OpsMgrName As String
    Public Property OpsMgrName() As String
        Get
            Return _OpsMgrName
        End Get
        Set(ByVal value As String)
            _OpsMgrName = value
        End Set
    End Property

    Private _RefMgrName As String
    Public Property RefMgrName() As String
        Get
            Return _RefMgrName
        End Get
        Set(ByVal value As String)
            _RefMgrName = value
        End Set
    End Property



    Private _MaintMgrEmail As String
    Public Property MaintMgrEmail() As String
        Get
            Return _MaintMgrEmail
        End Get
        Set(ByVal value As String)
            _MaintMgrEmail = value
        End Set
    End Property

    Private _TechMgrEmail As String
    Public Property TechMgrEmail() As String
        Get
            Return _TechMgrEmail
        End Get
        Set(ByVal value As String)
            _TechMgrEmail = value
        End Set
    End Property

    Private _OpsMgrEmail As String
    Public Property OpsMgrEmail() As String
        Get
            Return _OpsMgrEmail
        End Get
        Set(ByVal value As String)
            _OpsMgrEmail = value
        End Set
    End Property

    Private _RefMgrEmail As String
    Public Property RefMgrEmail() As String
        Get
            Return _RefMgrEmail
        End Get
        Set(ByVal value As String)
            _RefMgrEmail = value
        End Set
    End Property

    Private _RefGeneralPhone As String
    Public Property GeneralPhone() As String
        Get
            Return _RefGeneralPhone
        End Get
        Set(ByVal value As String)
            _RefGeneralPhone = value
        End Set
    End Property

    Public Sub Clear()
        CoName = ""
        CoName2 = ""
        CoTitle = ""
        CoTitle2 = ""
        CoEmail = ""
        CoEmail2 = ""
        CoPhone = ""
        CoPhone2 = ""
        MaintMgrName = ""
        MaintMgrEmail = ""
        OpsMgrName = ""
        OpsMgrEmail = ""
        TechMgrName = ""
        TechMgrEmail = ""
        RefMgrName = ""
        RefMgrEmail = ""
        GeneralPhone = ""
    End Sub

End Class
Public Class PlantContact

    Private _PlantName As String
    Public Property PlantName() As String
        Get
            Return _PlantName
        End Get
        Set(ByVal value As String)
            _PlantName = value
        End Set
    End Property


    Private _PlantPhone As String
    Public Property PlantPhone() As String
        Get
            Return _PlantPhone
        End Get
        Set(ByVal value As String)
            _PlantPhone = value
        End Set
    End Property

    Private _PlantEmail As String
    Public Property PlantEmail() As String
        Get
            Return _PlantEmail
        End Get
        Set(ByVal value As String)
            _PlantEmail = value
        End Set
    End Property
    Private _PlantFax As String
    Public Property PlantFax() As String
        Get
            Return _PlantFax
        End Get
        Set(ByVal value As String)
            _PlantFax = value
        End Set
    End Property
    Private _PlantStreet As String
    Public Property PlantStreet() As String
        Get
            Return _PlantStreet
        End Get
        Set(ByVal value As String)
            _PlantStreet = value
        End Set
    End Property
    Private _PlantCoordTitle As String
    Public Property PlantCoordTitle() As String
        Get
            Return _PlantCoordTitle
        End Get
        Set(ByVal value As String)
            _PlantCoordTitle = value
        End Set
    End Property
    Private _PlantCity As String
    Public Property PlantCity() As String
        Get
            Return _PlantCity
        End Get
        Set(ByVal value As String)
            _PlantCity = value
        End Set
    End Property
    Private _PlantState As String
    Public Property PlantState() As String
        Get
            Return _PlantState
        End Get
        Set(ByVal value As String)
            _PlantState = value
        End Set
    End Property
    Private _PlantCountry As String
    Public Property PlantCountry() As String
        Get
            Return _PlantCountry
        End Get
        Set(ByVal value As String)
            _PlantCountry = value
        End Set
    End Property
    Private _PlantZip As String
    Public Property PlantZip() As String
        Get
            Return _PlantZip
        End Get
        Set(ByVal value As String)
            _PlantZip = value
        End Set
    End Property


    Public Sub Clear()
        PlantZip = ""
        PlantName = ""
        PlantEmail = ""
        PlantPhone = ""
        PlantStreet = ""
        PlantCity = ""
        PlantCoordTitle = ""
        PlantCountry = ""
        PlantState = ""

    End Sub
End Class
Public Class InterimContact

    Private _IntName As String
    Public Property IntName() As String
        Get
            Return _IntName
        End Get
        Set(ByVal value As String)
            _IntName = value
        End Set
    End Property

    Private _IntTitle As String
    Public Property IntTitle() As String
        Get
            Return _IntTitle
        End Get
        Set(ByVal value As String)
            _IntTitle = value
        End Set
    End Property

    Private _IntPhone As String
    Public Property IntPhone() As String
        Get
            Return _IntPhone
        End Get
        Set(ByVal value As String)
            _IntPhone = value
        End Set
    End Property

    Private _IntEmail As String
    Public Property IntEmail() As String
        Get
            Return _IntEmail
        End Get
        Set(ByVal value As String)
            _IntEmail = value
        End Set
    End Property

    Public Sub Clear()
        IntName = ""
        IntEmail = ""
        IntPhone = ""

    End Sub


End Class
