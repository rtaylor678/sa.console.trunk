﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RAMMainConsole
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RAMMainConsole))
        Me.cboStudy = New System.Windows.Forms.ComboBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.cboRefNum = New System.Windows.Forms.ComboBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lblWarning = New System.Windows.Forms.Label()
        Me.btnHelp = New System.Windows.Forms.Button()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.tabSS = New System.Windows.Forms.TabPage()
        Me.tvwDrawings2 = New System.Windows.Forms.TreeView()
        Me.tvwDrawings = New System.Windows.Forms.TreeView()
        Me.tvwSiteData2 = New System.Windows.Forms.TreeView()
        Me.tvwSiteData = New System.Windows.Forms.TreeView()
        Me.btnDirRefresh = New System.Windows.Forms.Button()
        Me.btnSecureSend = New System.Windows.Forms.Button()
        Me.tvwCompCorr2 = New System.Windows.Forms.TreeView()
        Me.tvwCompCorr = New System.Windows.Forms.TreeView()
        Me.tvwCorrespondence2 = New System.Windows.Forms.TreeView()
        Me.tvwClientAttachments2 = New System.Windows.Forms.TreeView()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cboDir2 = New System.Windows.Forms.ComboBox()
        Me.tvwClientAttachments = New System.Windows.Forms.TreeView()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboDir = New System.Windows.Forms.ComboBox()
        Me.tvwCorrespondence = New System.Windows.Forms.TreeView()
        Me.tabCompany = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblUnlockedForUser = New System.Windows.Forms.Label()
        Me.lblDataCollectionTime = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblQualityIssues = New System.Windows.Forms.Label()
        Me.lblOU = New System.Windows.Forms.Label()
        Me.lblNumNeedingApproval = New System.Windows.Forms.Label()
        Me.lblCU = New System.Windows.Forms.Label()
        Me.lblQualityScore = New System.Windows.Forms.Label()
        Me.lblPercentComplete = New System.Windows.Forms.Label()
        Me.lblNoUnits = New System.Windows.Forms.Label()
        Me.lblLU = New System.Windows.Forms.Label()
        Me.lblLFS = New System.Windows.Forms.Label()
        Me.lblLCD = New System.Windows.Forms.Label()
        Me.dgContacts = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ConsoleTabs = New System.Windows.Forms.TabControl()
        Me.tabSummary = New System.Windows.Forms.TabPage()
        Me.chkLockPN = New System.Windows.Forms.CheckBox()
        Me.txtConsultingOpps = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.txtContinuingIssues = New System.Windows.Forms.TextBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnValidationStatus = New System.Windows.Forms.Button()
        Me.btnValidationComments = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtValidationIssues = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.lblCompany = New System.Windows.Forms.Label()
        Me.txtVersion = New System.Windows.Forms.TextBox()
        Me.VersionToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ConsoleTimer = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.btnResort = New System.Windows.Forms.Button()
        Me.btnKill = New System.Windows.Forms.Button()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.btnPresentationSchedule = New System.Windows.Forms.Button()
        Me.btnPresentationPlan = New System.Windows.Forms.Button()
        Me.tvwPolishCorr = New System.Windows.Forms.TreeView()
        Me.tvwPolishCorr2 = New System.Windows.Forms.TreeView()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSS.SuspendLayout()
        Me.tabCompany.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgContacts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConsoleTabs.SuspendLayout()
        Me.tabSummary.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'cboStudy
        '
        Me.cboStudy.CausesValidation = False
        Me.cboStudy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStudy.FormattingEnabled = True
        Me.cboStudy.Location = New System.Drawing.Point(254, 43)
        Me.cboStudy.Name = "cboStudy"
        Me.cboStudy.Size = New System.Drawing.Size(90, 21)
        Me.cboStudy.TabIndex = 1
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(178, 43)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(44, 17)
        Me.Label59.TabIndex = 21
        Me.Label59.Text = "Study"
        '
        'cboRefNum
        '
        Me.cboRefNum.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboRefNum.FormattingEnabled = True
        Me.cboRefNum.Location = New System.Drawing.Point(254, 75)
        Me.cboRefNum.Name = "cboRefNum"
        Me.cboRefNum.Size = New System.Drawing.Size(315, 21)
        Me.cboRefNum.TabIndex = 22
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(27, 25)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(135, 96)
        Me.PictureBox1.TabIndex = 24
        Me.PictureBox1.TabStop = False
        '
        'lblWarning
        '
        Me.lblWarning.AutoSize = True
        Me.lblWarning.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWarning.ForeColor = System.Drawing.Color.Red
        Me.lblWarning.Location = New System.Drawing.Point(245, 27)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(108, 13)
        Me.lblWarning.TabIndex = 25
        Me.lblWarning.Text = "Not Current Study"
        Me.lblWarning.Visible = False
        '
        'btnHelp
        '
        Me.btnHelp.Location = New System.Drawing.Point(737, 4)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(27, 23)
        Me.btnHelp.TabIndex = 32
        Me.btnHelp.Text = "&?"
        Me.btnHelp.UseVisualStyleBackColor = True
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'tabSS
        '
        Me.tabSS.Controls.Add(Me.tvwPolishCorr2)
        Me.tabSS.Controls.Add(Me.tvwPolishCorr)
        Me.tabSS.Controls.Add(Me.tvwDrawings2)
        Me.tabSS.Controls.Add(Me.tvwDrawings)
        Me.tabSS.Controls.Add(Me.tvwSiteData2)
        Me.tabSS.Controls.Add(Me.tvwSiteData)
        Me.tabSS.Controls.Add(Me.btnDirRefresh)
        Me.tabSS.Controls.Add(Me.btnSecureSend)
        Me.tabSS.Controls.Add(Me.tvwCompCorr2)
        Me.tabSS.Controls.Add(Me.tvwCompCorr)
        Me.tabSS.Controls.Add(Me.tvwCorrespondence2)
        Me.tabSS.Controls.Add(Me.tvwClientAttachments2)
        Me.tabSS.Controls.Add(Me.Label13)
        Me.tabSS.Controls.Add(Me.cboDir2)
        Me.tabSS.Controls.Add(Me.tvwClientAttachments)
        Me.tabSS.Controls.Add(Me.Label12)
        Me.tabSS.Controls.Add(Me.cboDir)
        Me.tabSS.Controls.Add(Me.tvwCorrespondence)
        Me.tabSS.Location = New System.Drawing.Point(4, 22)
        Me.tabSS.Name = "tabSS"
        Me.tabSS.Padding = New System.Windows.Forms.Padding(3)
        Me.tabSS.Size = New System.Drawing.Size(789, 411)
        Me.tabSS.TabIndex = 7
        Me.tabSS.Text = "Secure Send"
        Me.tabSS.UseVisualStyleBackColor = True
        '
        'tvwDrawings2
        '
        Me.tvwDrawings2.AllowDrop = True
        Me.tvwDrawings2.CheckBoxes = True
        Me.tvwDrawings2.ImageIndex = 0
        Me.tvwDrawings2.ImageList = Me.ImageList1
        Me.tvwDrawings2.Location = New System.Drawing.Point(403, 36)
        Me.tvwDrawings2.Name = "tvwDrawings2"
        Me.tvwDrawings2.SelectedImageIndex = 0
        Me.tvwDrawings2.Size = New System.Drawing.Size(369, 346)
        Me.tvwDrawings2.TabIndex = 54
        Me.tvwDrawings2.Visible = False
        '
        'tvwDrawings
        '
        Me.tvwDrawings.AllowDrop = True
        Me.tvwDrawings.CheckBoxes = True
        Me.tvwDrawings.ImageIndex = 0
        Me.tvwDrawings.ImageList = Me.ImageList1
        Me.tvwDrawings.Location = New System.Drawing.Point(17, 36)
        Me.tvwDrawings.Name = "tvwDrawings"
        Me.tvwDrawings.SelectedImageIndex = 0
        Me.tvwDrawings.Size = New System.Drawing.Size(369, 346)
        Me.tvwDrawings.TabIndex = 53
        Me.tvwDrawings.Visible = False
        '
        'tvwSiteData2
        '
        Me.tvwSiteData2.AllowDrop = True
        Me.tvwSiteData2.CheckBoxes = True
        Me.tvwSiteData2.ImageIndex = 0
        Me.tvwSiteData2.ImageList = Me.ImageList1
        Me.tvwSiteData2.Location = New System.Drawing.Point(395, 35)
        Me.tvwSiteData2.Name = "tvwSiteData2"
        Me.tvwSiteData2.SelectedImageIndex = 0
        Me.tvwSiteData2.Size = New System.Drawing.Size(377, 346)
        Me.tvwSiteData2.TabIndex = 52
        Me.tvwSiteData2.Visible = False
        '
        'tvwSiteData
        '
        Me.tvwSiteData.AllowDrop = True
        Me.tvwSiteData.CheckBoxes = True
        Me.tvwSiteData.ImageIndex = 0
        Me.tvwSiteData.ImageList = Me.ImageList1
        Me.tvwSiteData.Location = New System.Drawing.Point(17, 35)
        Me.tvwSiteData.Name = "tvwSiteData"
        Me.tvwSiteData.SelectedImageIndex = 0
        Me.tvwSiteData.Size = New System.Drawing.Size(369, 346)
        Me.tvwSiteData.TabIndex = 51
        Me.tvwSiteData.Visible = False
        '
        'btnDirRefresh
        '
        Me.btnDirRefresh.Location = New System.Drawing.Point(688, 386)
        Me.btnDirRefresh.Name = "btnDirRefresh"
        Me.btnDirRefresh.Size = New System.Drawing.Size(84, 23)
        Me.btnDirRefresh.TabIndex = 50
        Me.btnDirRefresh.Text = "Refresh"
        Me.btnDirRefresh.UseVisualStyleBackColor = True
        '
        'btnSecureSend
        '
        Me.btnSecureSend.Location = New System.Drawing.Point(598, 386)
        Me.btnSecureSend.Name = "btnSecureSend"
        Me.btnSecureSend.Size = New System.Drawing.Size(84, 23)
        Me.btnSecureSend.TabIndex = 49
        Me.btnSecureSend.Text = "Secure Send"
        Me.btnSecureSend.UseVisualStyleBackColor = True
        '
        'tvwCompCorr2
        '
        Me.tvwCompCorr2.AllowDrop = True
        Me.tvwCompCorr2.CheckBoxes = True
        Me.tvwCompCorr2.ImageIndex = 0
        Me.tvwCompCorr2.ImageList = Me.ImageList1
        Me.tvwCompCorr2.Location = New System.Drawing.Point(395, 36)
        Me.tvwCompCorr2.Name = "tvwCompCorr2"
        Me.tvwCompCorr2.SelectedImageIndex = 0
        Me.tvwCompCorr2.Size = New System.Drawing.Size(377, 346)
        Me.tvwCompCorr2.TabIndex = 48
        Me.tvwCompCorr2.Visible = False
        '
        'tvwCompCorr
        '
        Me.tvwCompCorr.AllowDrop = True
        Me.tvwCompCorr.CheckBoxes = True
        Me.tvwCompCorr.ImageIndex = 0
        Me.tvwCompCorr.ImageList = Me.ImageList1
        Me.tvwCompCorr.Location = New System.Drawing.Point(17, 35)
        Me.tvwCompCorr.Name = "tvwCompCorr"
        Me.tvwCompCorr.SelectedImageIndex = 0
        Me.tvwCompCorr.Size = New System.Drawing.Size(369, 346)
        Me.tvwCompCorr.TabIndex = 47
        Me.tvwCompCorr.Visible = False
        '
        'tvwCorrespondence2
        '
        Me.tvwCorrespondence2.AllowDrop = True
        Me.tvwCorrespondence2.CheckBoxes = True
        Me.tvwCorrespondence2.ImageIndex = 0
        Me.tvwCorrespondence2.ImageList = Me.ImageList1
        Me.tvwCorrespondence2.Location = New System.Drawing.Point(395, 36)
        Me.tvwCorrespondence2.Name = "tvwCorrespondence2"
        Me.tvwCorrespondence2.SelectedImageIndex = 0
        Me.tvwCorrespondence2.Size = New System.Drawing.Size(377, 345)
        Me.tvwCorrespondence2.TabIndex = 46
        '
        'tvwClientAttachments2
        '
        Me.tvwClientAttachments2.AllowDrop = True
        Me.tvwClientAttachments2.CheckBoxes = True
        Me.tvwClientAttachments2.ImageIndex = 0
        Me.tvwClientAttachments2.ImageList = Me.ImageList1
        Me.tvwClientAttachments2.Location = New System.Drawing.Point(395, 36)
        Me.tvwClientAttachments2.Name = "tvwClientAttachments2"
        Me.tvwClientAttachments2.SelectedImageIndex = 0
        Me.tvwClientAttachments2.Size = New System.Drawing.Size(377, 346)
        Me.tvwClientAttachments2.TabIndex = 43
        Me.tvwClientAttachments2.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(404, 9)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(69, 17)
        Me.Label13.TabIndex = 42
        Me.Label13.Text = "Directory:"
        '
        'cboDir2
        '
        Me.cboDir2.FormattingEnabled = True
        Me.cboDir2.Location = New System.Drawing.Point(479, 8)
        Me.cboDir2.Name = "cboDir2"
        Me.cboDir2.Size = New System.Drawing.Size(164, 21)
        Me.cboDir2.TabIndex = 41
        '
        'tvwClientAttachments
        '
        Me.tvwClientAttachments.AllowDrop = True
        Me.tvwClientAttachments.CheckBoxes = True
        Me.tvwClientAttachments.ImageIndex = 0
        Me.tvwClientAttachments.ImageList = Me.ImageList1
        Me.tvwClientAttachments.Location = New System.Drawing.Point(17, 36)
        Me.tvwClientAttachments.Name = "tvwClientAttachments"
        Me.tvwClientAttachments.SelectedImageIndex = 0
        Me.tvwClientAttachments.Size = New System.Drawing.Size(369, 345)
        Me.tvwClientAttachments.TabIndex = 40
        Me.tvwClientAttachments.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(14, 8)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(69, 17)
        Me.Label12.TabIndex = 39
        Me.Label12.Text = "Directory:"
        '
        'cboDir
        '
        Me.cboDir.FormattingEnabled = True
        Me.cboDir.Location = New System.Drawing.Point(89, 7)
        Me.cboDir.Name = "cboDir"
        Me.cboDir.Size = New System.Drawing.Size(164, 21)
        Me.cboDir.TabIndex = 38
        '
        'tvwCorrespondence
        '
        Me.tvwCorrespondence.AllowDrop = True
        Me.tvwCorrespondence.CheckBoxes = True
        Me.tvwCorrespondence.ImageIndex = 0
        Me.tvwCorrespondence.ImageList = Me.ImageList1
        Me.tvwCorrespondence.Location = New System.Drawing.Point(17, 35)
        Me.tvwCorrespondence.Name = "tvwCorrespondence"
        Me.tvwCorrespondence.SelectedImageIndex = 0
        Me.tvwCorrespondence.Size = New System.Drawing.Size(369, 345)
        Me.tvwCorrespondence.TabIndex = 0
        '
        'tabCompany
        '
        Me.tabCompany.Controls.Add(Me.Panel2)
        Me.tabCompany.Controls.Add(Me.Panel1)
        Me.tabCompany.Location = New System.Drawing.Point(4, 22)
        Me.tabCompany.Name = "tabCompany"
        Me.tabCompany.Padding = New System.Windows.Forms.Padding(3)
        Me.tabCompany.Size = New System.Drawing.Size(789, 411)
        Me.tabCompany.TabIndex = 0
        Me.tabCompany.Text = "Contacts"
        Me.tabCompany.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.lblUnlockedForUser)
        Me.Panel2.Controls.Add(Me.lblDataCollectionTime)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.lblQualityIssues)
        Me.Panel2.Controls.Add(Me.lblOU)
        Me.Panel2.Controls.Add(Me.lblNumNeedingApproval)
        Me.Panel2.Controls.Add(Me.lblCU)
        Me.Panel2.Controls.Add(Me.lblQualityScore)
        Me.Panel2.Controls.Add(Me.lblPercentComplete)
        Me.Panel2.Controls.Add(Me.lblNoUnits)
        Me.Panel2.Controls.Add(Me.lblLU)
        Me.Panel2.Controls.Add(Me.lblLFS)
        Me.Panel2.Controls.Add(Me.lblLCD)
        Me.Panel2.Controls.Add(Me.dgContacts)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(783, 420)
        Me.Panel2.TabIndex = 21
        '
        'lblUnlockedForUser
        '
        Me.lblUnlockedForUser.AutoSize = True
        Me.lblUnlockedForUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblUnlockedForUser.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnlockedForUser.Location = New System.Drawing.Point(592, 97)
        Me.lblUnlockedForUser.Name = "lblUnlockedForUser"
        Me.lblUnlockedForUser.Size = New System.Drawing.Size(17, 18)
        Me.lblUnlockedForUser.TabIndex = 48
        Me.lblUnlockedForUser.Text = "1"
        '
        'lblDataCollectionTime
        '
        Me.lblDataCollectionTime.AutoSize = True
        Me.lblDataCollectionTime.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblDataCollectionTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDataCollectionTime.Location = New System.Drawing.Point(592, 75)
        Me.lblDataCollectionTime.Name = "lblDataCollectionTime"
        Me.lblDataCollectionTime.Size = New System.Drawing.Size(17, 18)
        Me.lblDataCollectionTime.TabIndex = 47
        Me.lblDataCollectionTime.Text = "1"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(397, 97)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 18)
        Me.Label4.TabIndex = 46
        Me.Label4.Text = "Status:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(397, 74)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(171, 18)
        Me.Label1.TabIndex = 45
        Me.Label1.Text = "Data Collection Time:"
        '
        'lblQualityIssues
        '
        Me.lblQualityIssues.AutoSize = True
        Me.lblQualityIssues.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualityIssues.Location = New System.Drawing.Point(174, 97)
        Me.lblQualityIssues.Name = "lblQualityIssues"
        Me.lblQualityIssues.Size = New System.Drawing.Size(17, 18)
        Me.lblQualityIssues.TabIndex = 44
        Me.lblQualityIssues.Text = "1"
        '
        'lblOU
        '
        Me.lblOU.AutoSize = True
        Me.lblOU.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOU.Location = New System.Drawing.Point(25, 97)
        Me.lblOU.Name = "lblOU"
        Me.lblOU.Size = New System.Drawing.Size(119, 18)
        Me.lblOU.TabIndex = 43
        Me.lblOU.Text = "Quality Issues:"
        '
        'lblNumNeedingApproval
        '
        Me.lblNumNeedingApproval.AutoSize = True
        Me.lblNumNeedingApproval.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblNumNeedingApproval.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumNeedingApproval.Location = New System.Drawing.Point(592, 52)
        Me.lblNumNeedingApproval.Name = "lblNumNeedingApproval"
        Me.lblNumNeedingApproval.Size = New System.Drawing.Size(17, 18)
        Me.lblNumNeedingApproval.TabIndex = 42
        Me.lblNumNeedingApproval.Text = "1"
        '
        'lblCU
        '
        Me.lblCU.AutoSize = True
        Me.lblCU.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCU.Location = New System.Drawing.Point(397, 51)
        Me.lblCU.Name = "lblCU"
        Me.lblCU.Size = New System.Drawing.Size(144, 18)
        Me.lblCU.TabIndex = 41
        Me.lblCU.Text = "Needing Approval:"
        '
        'lblQualityScore
        '
        Me.lblQualityScore.AutoSize = True
        Me.lblQualityScore.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualityScore.Location = New System.Drawing.Point(174, 73)
        Me.lblQualityScore.Name = "lblQualityScore"
        Me.lblQualityScore.Size = New System.Drawing.Size(17, 18)
        Me.lblQualityScore.TabIndex = 40
        Me.lblQualityScore.Text = "1"
        '
        'lblPercentComplete
        '
        Me.lblPercentComplete.AutoSize = True
        Me.lblPercentComplete.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercentComplete.ForeColor = System.Drawing.Color.DarkGreen
        Me.lblPercentComplete.Location = New System.Drawing.Point(174, 50)
        Me.lblPercentComplete.Name = "lblPercentComplete"
        Me.lblPercentComplete.Size = New System.Drawing.Size(17, 18)
        Me.lblPercentComplete.TabIndex = 39
        Me.lblPercentComplete.Text = "1"
        '
        'lblNoUnits
        '
        Me.lblNoUnits.AutoSize = True
        Me.lblNoUnits.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoUnits.Location = New System.Drawing.Point(83, 13)
        Me.lblNoUnits.Name = "lblNoUnits"
        Me.lblNoUnits.Size = New System.Drawing.Size(19, 20)
        Me.lblNoUnits.TabIndex = 38
        Me.lblNoUnits.Text = "1"
        '
        'lblLU
        '
        Me.lblLU.AutoSize = True
        Me.lblLU.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLU.Location = New System.Drawing.Point(25, 50)
        Me.lblLU.Name = "lblLU"
        Me.lblLU.Size = New System.Drawing.Size(85, 18)
        Me.lblLU.TabIndex = 37
        Me.lblLU.Text = "Complete:"
        '
        'lblLFS
        '
        Me.lblLFS.AutoSize = True
        Me.lblLFS.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLFS.ForeColor = System.Drawing.Color.Black
        Me.lblLFS.Location = New System.Drawing.Point(19, 12)
        Me.lblLFS.Name = "lblLFS"
        Me.lblLFS.Size = New System.Drawing.Size(56, 20)
        Me.lblLFS.TabIndex = 36
        Me.lblLFS.Text = "Units:"
        '
        'lblLCD
        '
        Me.lblLCD.AutoSize = True
        Me.lblLCD.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLCD.Location = New System.Drawing.Point(25, 73)
        Me.lblLCD.Name = "lblLCD"
        Me.lblLCD.Size = New System.Drawing.Size(115, 18)
        Me.lblLCD.TabIndex = 35
        Me.lblLCD.Text = "Quality Score:"
        '
        'dgContacts
        '
        Me.dgContacts.AllowUserToAddRows = False
        Me.dgContacts.AllowUserToDeleteRows = False
        Me.dgContacts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgContacts.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgContacts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgContacts.Location = New System.Drawing.Point(4, 159)
        Me.dgContacts.Name = "dgContacts"
        Me.dgContacts.ReadOnly = True
        Me.dgContacts.Size = New System.Drawing.Size(773, 244)
        Me.dgContacts.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(783, 405)
        Me.Panel1.TabIndex = 20
        '
        'ConsoleTabs
        '
        Me.ConsoleTabs.AllowDrop = True
        Me.ConsoleTabs.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.ConsoleTabs.Controls.Add(Me.tabCompany)
        Me.ConsoleTabs.Controls.Add(Me.tabSummary)
        Me.ConsoleTabs.Controls.Add(Me.tabSS)
        Me.ConsoleTabs.Enabled = False
        Me.ConsoleTabs.Location = New System.Drawing.Point(0, 149)
        Me.ConsoleTabs.Name = "ConsoleTabs"
        Me.ConsoleTabs.SelectedIndex = 0
        Me.ConsoleTabs.Size = New System.Drawing.Size(797, 437)
        Me.ConsoleTabs.TabIndex = 0
        '
        'tabSummary
        '
        Me.tabSummary.Controls.Add(Me.chkLockPN)
        Me.tabSummary.Controls.Add(Me.txtConsultingOpps)
        Me.tabSummary.Controls.Add(Me.Label54)
        Me.tabSummary.Controls.Add(Me.txtContinuingIssues)
        Me.tabSummary.Controls.Add(Me.Label53)
        Me.tabSummary.Controls.Add(Me.FlowLayoutPanel2)
        Me.tabSummary.Controls.Add(Me.Label11)
        Me.tabSummary.Controls.Add(Me.txtValidationIssues)
        Me.tabSummary.Controls.Add(Me.Label46)
        Me.tabSummary.Location = New System.Drawing.Point(4, 22)
        Me.tabSummary.Name = "tabSummary"
        Me.tabSummary.Padding = New System.Windows.Forms.Padding(3)
        Me.tabSummary.Size = New System.Drawing.Size(789, 411)
        Me.tabSummary.TabIndex = 2
        Me.tabSummary.Text = "Support"
        Me.tabSummary.UseVisualStyleBackColor = True
        '
        'chkLockPN
        '
        Me.chkLockPN.AutoSize = True
        Me.chkLockPN.Checked = True
        Me.chkLockPN.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLockPN.Location = New System.Drawing.Point(250, 13)
        Me.chkLockPN.Name = "chkLockPN"
        Me.chkLockPN.Size = New System.Drawing.Size(85, 17)
        Me.chkLockPN.TabIndex = 35
        Me.chkLockPN.Text = "Lock Entries"
        Me.chkLockPN.UseVisualStyleBackColor = True
        '
        'txtConsultingOpps
        '
        Me.txtConsultingOpps.Location = New System.Drawing.Point(8, 290)
        Me.txtConsultingOpps.Multiline = True
        Me.txtConsultingOpps.Name = "txtConsultingOpps"
        Me.txtConsultingOpps.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtConsultingOpps.Size = New System.Drawing.Size(617, 91)
        Me.txtConsultingOpps.TabIndex = 34
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.Location = New System.Drawing.Point(9, 272)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(145, 13)
        Me.Label54.TabIndex = 33
        Me.Label54.Text = "Consulting Opportunities"
        '
        'txtContinuingIssues
        '
        Me.txtContinuingIssues.Location = New System.Drawing.Point(8, 163)
        Me.txtContinuingIssues.Multiline = True
        Me.txtContinuingIssues.Name = "txtContinuingIssues"
        Me.txtContinuingIssues.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtContinuingIssues.Size = New System.Drawing.Size(617, 91)
        Me.txtContinuingIssues.TabIndex = 32
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label53.Location = New System.Drawing.Point(9, 146)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(107, 13)
        Me.Label53.TabIndex = 31
        Me.Label53.Text = "Continuing Issues"
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.AutoScroll = True
        Me.FlowLayoutPanel2.Controls.Add(Me.btnValidationStatus)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnValidationComments)
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(649, 53)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(134, 341)
        Me.FlowLayoutPanel2.TabIndex = 30
        Me.FlowLayoutPanel2.WrapContents = False
        '
        'btnValidationStatus
        '
        Me.btnValidationStatus.Enabled = False
        Me.btnValidationStatus.Location = New System.Drawing.Point(1, 1)
        Me.btnValidationStatus.Margin = New System.Windows.Forms.Padding(1)
        Me.btnValidationStatus.Name = "btnValidationStatus"
        Me.btnValidationStatus.Size = New System.Drawing.Size(132, 32)
        Me.btnValidationStatus.TabIndex = 35
        Me.btnValidationStatus.Text = "Validation Status"
        Me.btnValidationStatus.UseVisualStyleBackColor = True
        '
        'btnValidationComments
        '
        Me.btnValidationComments.Enabled = False
        Me.btnValidationComments.Location = New System.Drawing.Point(1, 35)
        Me.btnValidationComments.Margin = New System.Windows.Forms.Padding(1)
        Me.btnValidationComments.Name = "btnValidationComments"
        Me.btnValidationComments.Size = New System.Drawing.Size(132, 32)
        Me.btnValidationComments.TabIndex = 36
        Me.btnValidationComments.Text = "Validation Comments"
        Me.btnValidationComments.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(675, 14)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(81, 13)
        Me.Label11.TabIndex = 24
        Me.Label11.Text = "Support Files"
        '
        'txtValidationIssues
        '
        Me.txtValidationIssues.Location = New System.Drawing.Point(8, 43)
        Me.txtValidationIssues.Multiline = True
        Me.txtValidationIssues.Name = "txtValidationIssues"
        Me.txtValidationIssues.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtValidationIssues.Size = New System.Drawing.Size(617, 91)
        Me.txtValidationIssues.TabIndex = 12
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(11, 15)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(209, 13)
        Me.Label46.TabIndex = 11
        Me.Label46.Text = "Validation Issues/Presenter's Notes"
        '
        'lblCompany
        '
        Me.lblCompany.AutoSize = True
        Me.lblCompany.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(178, 75)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(32, 17)
        Me.lblCompany.TabIndex = 37
        Me.lblCompany.Text = "Site"
        '
        'txtVersion
        '
        Me.txtVersion.BackColor = System.Drawing.SystemColors.Control
        Me.txtVersion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVersion.Location = New System.Drawing.Point(27, 4)
        Me.txtVersion.Name = "txtVersion"
        Me.txtVersion.ReadOnly = True
        Me.txtVersion.Size = New System.Drawing.Size(100, 15)
        Me.txtVersion.TabIndex = 39
        Me.VersionToolTip.SetToolTip(Me.txtVersion, "This is the new version")
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'ConsoleTimer
        '
        Me.ConsoleTimer.AutoSize = True
        Me.ConsoleTimer.ForeColor = System.Drawing.Color.Green
        Me.ConsoleTimer.Location = New System.Drawing.Point(22, 129)
        Me.ConsoleTimer.Name = "ConsoleTimer"
        Me.ConsoleTimer.Size = New System.Drawing.Size(0, 13)
        Me.ConsoleTimer.TabIndex = 40
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(29, 131)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(0, 13)
        Me.Label14.TabIndex = 41
        '
        'btnResort
        '
        Me.btnResort.Location = New System.Drawing.Point(575, 75)
        Me.btnResort.Name = "btnResort"
        Me.btnResort.Size = New System.Drawing.Size(86, 23)
        Me.btnResort.TabIndex = 33
        Me.btnResort.Text = "By Site Name"
        Me.btnResort.UseVisualStyleBackColor = True
        '
        'btnKill
        '
        Me.btnKill.Location = New System.Drawing.Point(770, 4)
        Me.btnKill.Name = "btnKill"
        Me.btnKill.Size = New System.Drawing.Size(27, 23)
        Me.btnKill.TabIndex = 42
        Me.btnKill.Text = "X"
        Me.btnKill.UseVisualStyleBackColor = True
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.DarkGreen
        Me.lblStatus.Location = New System.Drawing.Point(22, 591)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(47, 15)
        Me.lblStatus.TabIndex = 43
        Me.lblStatus.Text = "Ready"
        '
        'btnPresentationSchedule
        '
        Me.btnPresentationSchedule.Location = New System.Drawing.Point(524, 119)
        Me.btnPresentationSchedule.Margin = New System.Windows.Forms.Padding(1)
        Me.btnPresentationSchedule.Name = "btnPresentationSchedule"
        Me.btnPresentationSchedule.Size = New System.Drawing.Size(122, 26)
        Me.btnPresentationSchedule.TabIndex = 37
        Me.btnPresentationSchedule.Text = "Presentation Schedule"
        Me.btnPresentationSchedule.UseVisualStyleBackColor = True
        '
        'btnPresentationPlan
        '
        Me.btnPresentationPlan.Location = New System.Drawing.Point(664, 119)
        Me.btnPresentationPlan.Margin = New System.Windows.Forms.Padding(1)
        Me.btnPresentationPlan.Name = "btnPresentationPlan"
        Me.btnPresentationPlan.Size = New System.Drawing.Size(122, 26)
        Me.btnPresentationPlan.TabIndex = 38
        Me.btnPresentationPlan.Text = "Presentation Plan"
        Me.btnPresentationPlan.UseVisualStyleBackColor = True
        '
        'tvwPolishCorr
        '
        Me.tvwPolishCorr.AllowDrop = True
        Me.tvwPolishCorr.CheckBoxes = True
        Me.tvwPolishCorr.ImageIndex = 0
        Me.tvwPolishCorr.ImageList = Me.ImageList1
        Me.tvwPolishCorr.Location = New System.Drawing.Point(17, 36)
        Me.tvwPolishCorr.Name = "tvwPolishCorr"
        Me.tvwPolishCorr.SelectedImageIndex = 0
        Me.tvwPolishCorr.Size = New System.Drawing.Size(369, 346)
        Me.tvwPolishCorr.TabIndex = 55
        Me.tvwPolishCorr.Visible = False
        '
        'tvwPolishCorr2
        '
        Me.tvwPolishCorr2.AllowDrop = True
        Me.tvwPolishCorr2.CheckBoxes = True
        Me.tvwPolishCorr2.ImageIndex = 0
        Me.tvwPolishCorr2.ImageList = Me.ImageList1
        Me.tvwPolishCorr2.Location = New System.Drawing.Point(403, 36)
        Me.tvwPolishCorr2.Name = "tvwPolishCorr2"
        Me.tvwPolishCorr2.SelectedImageIndex = 0
        Me.tvwPolishCorr2.Size = New System.Drawing.Size(369, 346)
        Me.tvwPolishCorr2.TabIndex = 56
        Me.tvwPolishCorr2.Visible = False
        '
        'RAMMainConsole
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(797, 612)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.btnKill)
        Me.Controls.Add(Me.btnPresentationPlan)
        Me.Controls.Add(Me.btnPresentationSchedule)
        Me.Controls.Add(Me.btnResort)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.ConsoleTimer)
        Me.Controls.Add(Me.txtVersion)
        Me.Controls.Add(Me.lblCompany)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.lblWarning)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.cboRefNum)
        Me.Controls.Add(Me.Label59)
        Me.Controls.Add(Me.cboStudy)
        Me.Controls.Add(Me.ConsoleTabs)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "RAMMainConsole"
        Me.Text = "RAM Console"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSS.ResumeLayout(False)
        Me.tabSS.PerformLayout()
        Me.tabCompany.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgContacts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConsoleTabs.ResumeLayout(False)
        Me.tabSummary.ResumeLayout(False)
        Me.tabSummary.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboStudy As System.Windows.Forms.ComboBox
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents cboRefNum As System.Windows.Forms.ComboBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents btnHelp As System.Windows.Forms.Button
    Friend WithEvents tabSS As System.Windows.Forms.TabPage
    Friend WithEvents tabCompany As System.Windows.Forms.TabPage
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ConsoleTabs As System.Windows.Forms.TabControl
    Friend WithEvents tvwCorrespondence As System.Windows.Forms.TreeView
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboDir As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents tvwClientAttachments As System.Windows.Forms.TreeView
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cboDir2 As System.Windows.Forms.ComboBox
    Friend WithEvents tvwClientAttachments2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwCompCorr2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwCompCorr As System.Windows.Forms.TreeView
    Friend WithEvents tvwCorrespondence2 As System.Windows.Forms.TreeView
    Friend WithEvents btnSecureSend As System.Windows.Forms.Button
    Friend WithEvents txtVersion As System.Windows.Forms.TextBox
    Friend WithEvents VersionToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents btnDirRefresh As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ConsoleTimer As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents btnResort As System.Windows.Forms.Button
    Friend WithEvents dgContacts As System.Windows.Forms.DataGridView
    Friend WithEvents tabSummary As System.Windows.Forms.TabPage
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtValidationIssues As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents btnValidationComments As System.Windows.Forms.Button
    Friend WithEvents btnValidationStatus As System.Windows.Forms.Button
    Friend WithEvents btnKill As System.Windows.Forms.Button
    Friend WithEvents lblUnlockedForUser As System.Windows.Forms.Label
    Friend WithEvents lblDataCollectionTime As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblQualityIssues As System.Windows.Forms.Label
    Friend WithEvents lblOU As System.Windows.Forms.Label
    Friend WithEvents lblNumNeedingApproval As System.Windows.Forms.Label
    Friend WithEvents lblCU As System.Windows.Forms.Label
    Friend WithEvents lblQualityScore As System.Windows.Forms.Label
    Friend WithEvents lblPercentComplete As System.Windows.Forms.Label
    Friend WithEvents lblNoUnits As System.Windows.Forms.Label
    Friend WithEvents lblLU As System.Windows.Forms.Label
    Friend WithEvents lblLFS As System.Windows.Forms.Label
    Friend WithEvents lblLCD As System.Windows.Forms.Label
    Friend WithEvents txtConsultingOpps As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents txtContinuingIssues As System.Windows.Forms.TextBox
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents chkLockPN As System.Windows.Forms.CheckBox
    Friend WithEvents tvwSiteData2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwSiteData As System.Windows.Forms.TreeView
    Friend WithEvents tvwDrawings2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwDrawings As System.Windows.Forms.TreeView
    Friend WithEvents btnPresentationSchedule As System.Windows.Forms.Button
    Friend WithEvents btnPresentationPlan As System.Windows.Forms.Button
    Friend WithEvents tvwPolishCorr2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwPolishCorr As System.Windows.Forms.TreeView


End Class
