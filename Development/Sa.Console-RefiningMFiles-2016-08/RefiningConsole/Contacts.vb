﻿Public Class Contacts

    Dim Name As String
    Private _ContactType As String
    Public Property ContactType() As String
        Get
            Return _ContactType
        End Get
        Set(ByVal value As String)
            _ContactType = value
        End Set
    End Property

    Private _FullName As String
    Public Property FullName() As String
        Get
            Return _FullName
        End Get
        Set(ByVal value As String)
            _FullName = value
        End Set
    End Property
    Private _AltFullName As String
    Public Property AltFullName() As String
        Get
            Return _AltFullName
        End Get
        Set(ByVal value As String)
            _AltFullName = value
        End Set
    End Property

    Private _FirstName As String
    Public Property FirstName() As String
        Get
            Return _FirstName
        End Get
        Set(ByVal value As String)
            _FirstName = value
        End Set
    End Property
    Private _LastName As String
    Public Property LastName() As String
        Get
            Return _LastName
        End Get
        Set(ByVal value As String)
            _LastName = value
        End Set
    End Property

    Private _Email As String
    Public Property Email() As String
        Get
            Return _Email
        End Get
        Set(ByVal value As String)
            _Email = value
        End Set
    End Property

    Private _Phone As String
    Public Property Phone() As String
        Get
            Return _Phone
        End Get
        Set(ByVal value As String)
            _Phone = value
        End Set
    End Property


    Private _AltFirstName As String
    Public Property AltFirstName() As String
        Get
            Return _AltFirstName
        End Get
        Set(ByVal value As String)
            _AltFirstName = value
        End Set
    End Property


    Private _AltLastName As String
    Public Property AltLastName() As String
        Get
            Return _AltLastName
        End Get
        Set(ByVal value As String)
            _AltLastName = value
        End Set
    End Property

    Private _AltEmail As String
    Public Property AltEmail() As String
        Get
            Return _AltEmail
        End Get
        Set(ByVal value As String)
            _AltEmail = value
        End Set
    End Property

    Private _AltPhone As String
    Public Property AltPhone() As String
        Get
            Return _AltPhone
        End Get
        Set(ByVal value As String)
            _AltPhone = value
        End Set
    End Property

    Private _MailAdd1 As String
    Public Property MailAdd1() As String
        Get
            Return _MailAdd1
        End Get
        Set(ByVal value As String)
            _MailAdd1 = value
        End Set
    End Property

    Private _MailAdd2 As String
    Public Property MailAdd2() As String
        Get
            Return _MailAdd2
        End Get
        Set(ByVal value As String)
            _MailAdd2 = value
        End Set
    End Property

    Private _MailCity As String
    Public Property MailCity() As String
        Get
            Return _MailCity
        End Get
        Set(ByVal value As String)
            _MailCity = value
        End Set
    End Property


    Private _MailState As String
    Public Property MailState() As String
        Get
            Return _MailState
        End Get
        Set(ByVal value As String)
            _MailState = value
        End Set
    End Property


    Private _MailZip As String
    Public Property MailZip() As String
        Get
            Return _MailZip
        End Get
        Set(ByVal value As String)
            _MailZip = value
        End Set
    End Property

    Private _MailCountry As String
    Public Property MailCountry() As String
        Get
            Return _MailCountry
        End Get
        Set(ByVal value As String)
            _MailCountry = value
        End Set
    End Property

    Private _MailAdd3 As String
    Public Property MailAdd3() As String
        Get
            Return _MailAdd3
        End Get
        Set(ByVal value As String)
            _MailAdd3 = value
        End Set
    End Property

    Private _StreetAdd1 As String
    Public Property StreetAdd1() As String
        Get
            Return _StreetAdd1
        End Get
        Set(ByVal value As String)
            _StreetAdd1 = value
        End Set
    End Property

    Private _StreetAdd2 As String
    Public Property StreetAdd2() As String
        Get
            Return _StreetAdd2
        End Get
        Set(ByVal value As String)
            _StreetAdd2 = value
        End Set
    End Property

    Private _StreetAdd3 As String
    Public Property StreetAdd3() As String
        Get
            Return _StreetAdd3
        End Get
        Set(ByVal value As String)
            _StreetAdd3 = value
        End Set
    End Property


    Private _StreetCity As String
    Public Property StreetCity() As String
        Get
            Return _StreetCity
        End Get
        Set(ByVal value As String)
            _StreetCity = value
        End Set
    End Property


    Private _StreetState As String
    Public Property StreetState() As String
        Get
            Return _StreetState
        End Get
        Set(ByVal value As String)
            _StreetState = value
        End Set
    End Property

    Private _StreetCountry As String
    Public Property StreetCountry() As String
        Get
            Return _StreetCountry
        End Get
        Set(ByVal value As String)
            _StreetCountry = value
        End Set
    End Property
    Private _StreetZip As String
    Public Property StreetZip() As String
        Get
            Return _StreetZip
        End Get
        Set(ByVal value As String)
            _StreetZip = value
        End Set
    End Property

    Private _Title As String
    Public Property Title() As String
        Get
            Return _Title
        End Get
        Set(ByVal value As String)
            _Title = value
        End Set
    End Property


    Private _Fax As String
    Public Property Fax() As String
        Get
            Return _Fax
        End Get
        Set(ByVal value As String)
            _Fax = value
        End Set
    End Property

    Private _Fax2 As String
    Public Property Fax2() As String
        Get
            Return _Fax2
        End Get
        Set(ByVal value As String)
            _Fax2 = value
        End Set
    End Property

    Private _Password As String
    Public Property Password() As String
        Get
            Return _Password
        End Get
        Set(ByVal value As String)
            _Password = value
        End Set
    End Property

    Private _SendMethod As String
    Public Property SendMethod() As String
        Get
            Return _SendMethod
        End Get
        Set(ByVal value As String)
            _SendMethod = value
        End Set
    End Property
    Private _PricingName As String
    Public Property PricingName() As String
        Get
            Return _PricingName
        End Get
        Set(ByVal value As String)
            _PricingName = value
        End Set
    End Property
    Private _PricingEmail As String
    Public Property PricingEmail() As String
        Get
            Return _PricingEmail
        End Get
        Set(ByVal value As String)
            _PricingEmail = value
        End Set
    End Property
    Private _DCName As String
    Public Property DCName() As String
        Get
            Return _DCName
        End Get
        Set(ByVal value As String)
            _DCName = value
        End Set
    End Property
    Private _DCEmail As String
    Public Property DCEmail() As String
        Get
            Return _DCEmail
        End Get
        Set(ByVal value As String)
            _DCEmail = value
        End Set
    End Property
    Private _MaintName As String
    Public Property MaintMgrName() As String
        Get
            Return _MaintName
        End Get
        Set(ByVal value As String)
            _MaintName = value
        End Set
    End Property
    Private _MaintEmail As String
    Public Property MaintEmail() As String
        Get
            Return _MaintEmail
        End Get
        Set(ByVal value As String)
            _MaintEmail = value
        End Set
    End Property
    Private _OpsMgrName As String
    Public Property OpsMgrName() As String
        Get
            Return _OpsMgrName
        End Get
        Set(ByVal value As String)
            _OpsMgrName = value
        End Set
    End Property
    Private _OpsEmail As String
    Public Property OpsEmail() As String
        Get
            Return _OpsEmail
        End Get
        Set(ByVal value As String)
            _OpsEmail = value
        End Set
    End Property
    Private _TechMgrName As String
    Public Property TechMgrName() As String
        Get
            Return _TechMgrName
        End Get
        Set(ByVal value As String)
            _TechMgrName = value
        End Set
    End Property
    Private _TechEmail As String
    Public Property TechEmail() As String
        Get
            Return _TechEmail
        End Get
        Set(ByVal value As String)
            _TechEmail = value
        End Set
    End Property
    Private _RefMgrName As String
    Public Property RefMgrName() As String
        Get
            Return _RefMgrName
        End Get
        Set(ByVal value As String)
            _RefMgrName = value
        End Set
    End Property
    Private _RefEmail As String
    Public Property RefEmail() As String
        Get
            Return _RefEmail
        End Get
        Set(ByVal value As String)
            _RefEmail = value
        End Set
    End Property
    Private _RefAddName As String
    Public Property RefAddName() As String
        Get
            Return _RefAddName
        End Get
        Set(ByVal value As String)
            _RefAddName = value
        End Set
    End Property
    Private _RefAddEmail As String
    Public Property RefAddEmail() As String
        Get
            Return _RefAddEmail
        End Get
        Set(ByVal value As String)
            _RefAddEmail = value
        End Set
    End Property
    Private _RefinerySPD As String
    Public Property RefinerySPD() As String
        Get
            Return _RefinerySPD
        End Get
        Set(ByVal value As String)
            _RefinerySPD = value
        End Set
    End Property
    Private _RefineryCountry As String
    Public Property RefineryCountry() As String
        Get
            Return _RefineryCountry
        End Get
        Set(ByVal value As String)
            _RefineryCountry = value
        End Set
    End Property


    Public Sub Clear()
        FullName = ""
        AltFullName = ""
        FirstName = ""
        LastName = ""
        Email = ""
        Phone = ""
        AltFirstName = ""
        AltLastName = ""
        AltEmail = ""
        AltPhone = ""
        Fax = ""
        MailAdd1 = ""
        MailAdd2 = ""
        MailAdd3 = ""
        MailCity = ""
        MailState = ""
        MailCountry = ""
        MailZip = ""
        StreetAdd1 = ""
        StreetAdd2 = ""
        StreetAdd3 = ""
        StreetCity = ""
        StreetState = ""
        StreetCountry = ""
        Title = ""
        Password = ""
        SendMethod = ""
        PricingName = ""
        PricingEmail = ""
        DCEmail = ""
        DCName = ""
        MaintEmail = ""
        MaintMgrName = ""
        TechEmail = ""
        TechMgrName = ""
        OpsEmail = ""
        OpsMgrName = ""
        RefAddEmail = ""
        RefAddName = ""
        RefinerySPD = ""
        RefineryCountry = ""


    End Sub

    
End Class

