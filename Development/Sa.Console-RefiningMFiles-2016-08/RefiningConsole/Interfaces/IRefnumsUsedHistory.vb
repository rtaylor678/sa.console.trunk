﻿Public Interface IRefnumsUsedHistory
    Function GetLastRefnum(study As String) As String
    Sub UpdateRefnum(study As String, newRefNum As String)
End Interface
