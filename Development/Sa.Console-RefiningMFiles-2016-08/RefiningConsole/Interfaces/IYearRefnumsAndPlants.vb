﻿Public Interface IYearRefnumsAndPlants
    ReadOnly Property GetStudyYear As String
    ReadOnly Property StudyYears As String()
    Function ChangeYear(ByVal newYear As String, ByRef db As SA.Internal.Console.DataObject.DataObject, priorRefnum As String) As YearRefnumPlantsInfo
    Function ChangeRefnum(newRefnum As String) As YearRefnumPlantsInfo
    Function ChangePlant(newPlant As String) As YearRefnumPlantsInfo
    Function ChangedByMouse(sender As ComboBox) As Boolean

End Interface
